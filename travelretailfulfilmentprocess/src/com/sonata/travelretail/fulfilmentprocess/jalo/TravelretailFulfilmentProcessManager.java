/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *  
 */
package com.sonata.travelretail.fulfilmentprocess.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.sonata.travelretail.fulfilmentprocess.constants.TravelretailFulfilmentProcessConstants;

@SuppressWarnings("PMD")
public class TravelretailFulfilmentProcessManager extends GeneratedTravelretailFulfilmentProcessManager
{
	public static final TravelretailFulfilmentProcessManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (TravelretailFulfilmentProcessManager) em.getExtension(TravelretailFulfilmentProcessConstants.EXTENSIONNAME);
	}
	
}
