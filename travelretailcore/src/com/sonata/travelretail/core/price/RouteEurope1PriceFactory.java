/**
 * 
 */
package com.sonata.travelretail.core.price;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.channel.strategies.RetrieveChannelStrategy;
import de.hybris.platform.europe1.constants.Europe1Tools;
import de.hybris.platform.europe1.enums.PriceRowChannel;
import de.hybris.platform.europe1.jalo.Europe1PriceFactory;
import de.hybris.platform.europe1.jalo.PriceRow;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.C2LManager;
import de.hybris.platform.jalo.c2l.Currency;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.jalo.order.AbstractOrderEntry;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.jalo.product.Unit;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.util.DateRange;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.util.localization.Localization;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import javax.annotation.Resource;


/**
 * @author vishal.sharma
 * 
 */
public class RouteEurope1PriceFactory extends Europe1PriceFactory
{

	@Resource
	private SessionService sessionService;

	@Resource
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private ModelService modelService;

	public RouteEurope1PriceFactory()
	{
	}

	private RetrieveChannelStrategy retrieveChannelStrategy1;

	/**
	 * @return the retrieveChannelStrategy1
	 */
	public RetrieveChannelStrategy getRetrieveChannelStrategy1()
	{
		return retrieveChannelStrategy1;
	}

	/**
	 * @param retrieveChannelStrategy1
	 *           the retrieveChannelStrategy1 to set
	 */
	public void setRetrieveChannelStrategy1(final RetrieveChannelStrategy retrieveChannelStrategy1)
	{
		this.retrieveChannelStrategy1 = retrieveChannelStrategy1;
	}

	@Override
	public PriceValue getBasePrice(final AbstractOrderEntry entry) throws JaloPriceFactoryException
	{
		final SessionContext ctx = getSession().getSessionContext();
		final AbstractOrder order = entry.getOrder(ctx);
		Currency currency = null;
		EnumerationValue productGroup = null;
		User user = null;
		EnumerationValue userGroup = null;
		Unit unit = null;
		long quantity = 0L;
		boolean net = false;
		Date date = null;
		final Product product = entry.getProduct();
		final boolean giveAwayMode = entry.isGiveAway(ctx).booleanValue();
		final boolean entryIsRejected = entry.isRejected(ctx).booleanValue();
		PriceRow row;
		if (giveAwayMode && entryIsRejected)
		{
			row = null;
		}
		else
		{
			row = matchPriceRowForPrice(ctx, product, productGroup = getPPG(ctx, product), user = order.getUser(),
					userGroup = getUPG(ctx, user), quantity = entry.getQuantity(ctx).longValue(), unit = entry.getUnit(ctx),
					currency = order.getCurrency(ctx), date = order.getDate(ctx), net = order.isNet().booleanValue(), giveAwayMode);
		}
		if (row != null)
		{
			final Currency rowCurr = row.getCurrency();
			double price;
			if (currency.equals(rowCurr))
			{
				price = row.getPriceAsPrimitive() / row.getUnitFactorAsPrimitive();
			}
			else
			{
				price = rowCurr.convert(currency, row.getPriceAsPrimitive() / row.getUnitFactorAsPrimitive());
			}
			final Unit priceUnit = row.getUnit();
			final Unit entryUnit = entry.getUnit();
			final double convertedPrice = priceUnit.convertExact(entryUnit, price);
			return new PriceValue(currency.getIsoCode(), convertedPrice, row.isNetAsPrimitive());
		}
		if (giveAwayMode)
		{
			return new PriceValue(order.getCurrency(ctx).getIsoCode(), 0.0D, order.isNet().booleanValue());
		}
		else
		{
			final String msg = Localization.getLocalizedString(
					"exception.europe1pricefactory.getbaseprice.jalopricefactoryexception1", new Object[]
					{ product, productGroup, user, userGroup, Long.toString(quantity), unit, currency, date, Boolean.toString(net) });
			throw new JaloPriceFactoryException(msg, 0);
		}
	}

	@Override
	public PriceRow matchPriceRowForPrice(final SessionContext ctx, final Product product, final EnumerationValue productGroup,
			final User user, final EnumerationValue userGroup, final long qtd, final Unit unit, final Currency currency,
			final Date date, final boolean net, final boolean giveAwayMode) throws JaloPriceFactoryException
	{
		if (product == null && productGroup == null)
		{
			throw new JaloPriceFactoryException(
					"cannot match price without product and product group - at least one must be present", 0);
		}
		if (user == null && userGroup == null)
		{
			throw new JaloPriceFactoryException("cannot match price without user and user group - at least one must be present", 0);
		}
		if (currency == null)
		{
			throw new JaloPriceFactoryException("cannot match price without currency", 0);
		}
		if (date == null)
		{
			throw new JaloPriceFactoryException("cannot match price without date", 0);
		}
		if (unit == null)
		{
			throw new JaloPriceFactoryException("cannot match price without unit", 0);
		}
		final Collection rows = queryPriceRows4Price(ctx, product, productGroup, user, userGroup);
		if (!rows.isEmpty())
		{
			final PriceRowChannel channel = retrieveChannelStrategy1.getChannel(ctx);
			final List list = filterPriceRows4Price(rows, qtd, unit, currency, date, giveAwayMode, channel, product);
			if (list.isEmpty())
			{
				return null;
			}
			if (list.size() == 1)
			{
				return (PriceRow) list.get(0);
			}
			else
			{
				Collections.sort(list, new PriceRowMatchComparator(currency, net, unit));
				return (PriceRow) list.get(0);
			}
		}
		else
		{
			return null;
		}
	}

	protected List filterPriceRows4Price(final Collection rows, final long _quantity, final Unit unit, final Currency curr,
			final Date date, final boolean giveAwayMode, final PriceRowChannel channel, final Product product)
	{
		if (rows.isEmpty())
		{
			return Collections.EMPTY_LIST;
		}
		final String sourceInbound = sessionService.getAttribute("sourceInbound");
		final String sourceOutbound = sessionService.getAttribute("sourceOutbound");
		final String destinationInbound = sessionService.getAttribute("destinationInbound");
		final String destinationOutbound = sessionService.getAttribute("destinationOutbound");
		final List<PriceRowModel> tempPriceRowModels = new ArrayList<PriceRowModel>();
		final ProductModel productModel = new ProductModel();
		productModel.setCode(product.getCode());
		final ProductModel model = modelService.get(product.getPK());
		if (null != model.getEurope1Prices())
		{
			for (final PriceRowModel priceRowModel : model.getEurope1Prices())
			{
				if (null != priceRowModel.getSource() && null != priceRowModel.getDestination())
				{
					if ((priceRowModel.getSource().equals(sourceInbound) && priceRowModel.getDestination().equals(destinationInbound))
							|| (priceRowModel.getSource().equals(sourceOutbound) && priceRowModel.getDestination().equals(
									destinationOutbound)))
					{
						tempPriceRowModels.add(priceRowModel);
					}

				}
			}
		}
		final Currency base = curr.isBase().booleanValue() ? null : C2LManager.getInstance().getBaseCurrency();
		final Set convertible = unit.getConvertibleUnits();
		final List ret = new ArrayList(rows);
		final long quantity = _quantity != 0L ? _quantity : 1L;
		boolean hasChannelRowMatching = false;
		for (final ListIterator it = ret.listIterator(); it.hasNext();)
		{
			PriceRowModel priceRowModel = null;
			if (tempPriceRowModels.size() > 0)
			{
				priceRowModel = tempPriceRowModels.get(0);
			}
			final PriceRow priceRow = (PriceRow) it.next();
			if (quantity < priceRow.getMinqtdAsPrimitive())
			{
				it.remove();
			}
			else
			{
				final Currency currency = priceRow.getCurrency();
				if (!curr.equals(currency) && (base == null || !base.equals(currency)))
				{
					it.remove();
				}
				else
				{
					final Unit user = priceRow.getUnit();
					if (!unit.equals(user) && !convertible.contains(user))
					{
						it.remove();
					}
					else
					{
						final DateRange dateRange = priceRow.getDateRange();
						if (dateRange != null && !dateRange.encloses(date))
						{
							it.remove();
						}
						else if (giveAwayMode != priceRow.isGiveAwayPriceAsPrimitive())
						{
							it.remove();
						}
						else if (channel != null && priceRow.getChannel() != null
								&& !priceRow.getChannel().getCode().equalsIgnoreCase(channel.getCode()))
						{
							it.remove();
						}
						else if (priceRowModel != null
								&& priceRow.getPrice().doubleValue() != priceRowModel.getPrice().doubleValue()
								&& ((priceRow.getChannel() != null && priceRowModel.getChannel() != null
										&& priceRow.getChannel().getCode().equalsIgnoreCase(priceRowModel.getChannel().getCode()) || (priceRow
										.getChannel() == null && priceRowModel.getChannel() == null)) && currency.getIsoCode()
										.equalsIgnoreCase(priceRowModel.getCurrency().getIsocode())))
						{
							it.remove();
						}
						else if (channel != null && priceRow.getChannel() != null
								&& priceRow.getChannel().getCode().equalsIgnoreCase(channel.getCode()))
						{
							hasChannelRowMatching = true;
						}
					}
				}
			}
		}

		if (hasChannelRowMatching && ret.size() > 1)
		{
			for (final ListIterator it = ret.listIterator(); it.hasNext();)
			{
				final PriceRow priceRow = (PriceRow) it.next();
				if (priceRow.getChannel() == null)
				{
					it.remove();
				}
			}

		}
		return ret;
	}

	@Override
	public List matchPriceRowsForInfo(final SessionContext ctx, final Product product, final EnumerationValue productGroup,
			final User user, final EnumerationValue userGroup, final Currency currency, final Date date, final boolean net)
			throws JaloPriceFactoryException
	{
		if (product == null && productGroup == null)
		{
			throw new JaloPriceFactoryException(
					"cannot match price info without product and product group - at least one must be present", 0);
		}
		if (user == null && userGroup == null)
		{
			throw new JaloPriceFactoryException(
					"cannot match price info without user and user group - at least one must be present", 0);
		}
		if (currency == null)
		{
			throw new JaloPriceFactoryException("cannot match price info without currency", 0);
		}
		if (date == null)
		{
			throw new JaloPriceFactoryException("cannot match price info without date", 0);
		}
		final Collection rows = queryPriceRows4Price(ctx, product, productGroup, user, userGroup);
		if (!rows.isEmpty())
		{
			final PriceRowChannel channel = retrieveChannelStrategy1.getChannel(ctx);
			final List ret = new ArrayList(rows);
			if (ret.size() > 1)
			{
				Collections.sort(ret, new CustomPriceRowInfoComparator(currency, net));
			}
			return filterPriceRows4Info(ret, currency, date, channel, product);
		}
		else
		{
			return Collections.EMPTY_LIST;
		}
	}

	protected List filterPriceRows4Info(final Collection rows, final Currency curr, final Date date,
			final PriceRowChannel channel, final Product product)
	{
		final String sourceInbound = sessionService.getAttribute("sourceInbound");
		final String sourceOutbound = sessionService.getAttribute("sourceOutbound");
		final String destinationInbound = sessionService.getAttribute("destinationInbound");
		final String destinationOutbound = sessionService.getAttribute("destinationOutbound");
		final List<PriceRowModel> tempPriceRowModels = new ArrayList<PriceRowModel>();
		final ProductModel productModel = new ProductModel();
		productModel.setCode(product.getCode());
		final ProductModel model = modelService.get(product.getPK());
		if (null != model.getEurope1Prices())
		{
			for (final PriceRowModel priceRowModel : model.getEurope1Prices())
			{
				if (null != priceRowModel.getSource() && null != priceRowModel.getDestination())
				{
					if ((priceRowModel.getSource().equals(sourceInbound) && priceRowModel.getDestination().equals(destinationInbound))
							|| (priceRowModel.getSource().equals(sourceOutbound) && priceRowModel.getDestination().equals(
									destinationOutbound)))
					{
						tempPriceRowModels.add(priceRowModel);
					}

				}
			}
		}
		if (rows.isEmpty())
		{
			return Collections.EMPTY_LIST;
		}
		//		final Currency base = curr.isBase().booleanValue() ? null : C2LManager.getInstance().getBaseCurrency();
		final List ret = new ArrayList(rows);
		boolean hasChannelRowMatching = false;
		for (final ListIterator it = ret.listIterator(); it.hasNext();)
		{
			final PriceRow priceRow = (PriceRow) it.next();
			final Currency currency = priceRow.getCurrency();
			if (!curr.equals(currency) /* && (base == null || !base.equals(currency)) */)
			{
				it.remove();
			}
			else
			{
				final DateRange dateRange = priceRow.getDateRange();
				PriceRowModel priceRowModel = null;
				if (tempPriceRowModels.size() > 0)
				{
					priceRowModel = tempPriceRowModels.get(0);
				}
				if (dateRange != null && !dateRange.encloses(date))
				{
					it.remove();
				}
				else if (priceRow.isGiveAwayPriceAsPrimitive())
				{
					it.remove();
				}
				else if (channel != null && priceRow.getChannel() != null
						&& !priceRow.getChannel().getCode().equalsIgnoreCase(channel.getCode()))
				{
					it.remove();
				}


				else if (priceRowModel != null
						&& priceRow.getPrice().doubleValue() != priceRowModel.getPrice().doubleValue()
						&& ((priceRow.getChannel() != null && priceRowModel.getChannel() != null
								&& priceRow.getChannel().getCode().equalsIgnoreCase(priceRowModel.getChannel().getCode()) || (priceRow
								.getChannel() == null && priceRowModel.getChannel() == null)) && currency.getIsoCode().equalsIgnoreCase(
								priceRowModel.getCurrency().getIsocode())))
				{
					it.remove();
				}


				else if (channel != null && priceRow.getChannel() != null
						&& priceRow.getChannel().getCode().equalsIgnoreCase(channel.getCode()))
				{
					hasChannelRowMatching = true;
				}
			}
		}

		if (hasChannelRowMatching && ret.size() > 1)
		{
			for (final ListIterator it = ret.listIterator(); it.hasNext();)
			{
				final PriceRow priceRow = (PriceRow) it.next();
				if (priceRow.getChannel() == null)
				{
					it.remove();
				}
			}

		}
		return ret;
	}

	@Override
	protected List getPriceInformations(final SessionContext ctx, final Product product, final EnumerationValue productGroup,
			final User user, final EnumerationValue userGroup, final Currency curr, final boolean net, final Date date,
			final Collection taxValues) throws JaloPriceFactoryException
	{
		final Collection priceRows = filterPriceRows(matchPriceRowsForInfo(ctx, product, productGroup, user, userGroup, curr, date,
				net));
		final List priceInfos = new ArrayList(priceRows.size());
		Collection theTaxValues = taxValues;
		final List defaultPriceInfos = new ArrayList(priceRows.size());
		final PriceRowChannel channel = retrieveChannelStrategy1.getChannel(ctx);
		for (final Iterator iterator = priceRows.iterator(); iterator.hasNext();)
		{
			final PriceRow row = (PriceRow) iterator.next();
			PriceInformation pInfo = Europe1Tools.createPriceInformation(row, curr);
			if (pInfo.getPriceValue().isNet() != net)
			{
				if (theTaxValues == null)
				{
					theTaxValues = Europe1Tools.getTaxValues(getTaxInformations(product, getPTG(ctx, product), user,
							getUTG(ctx, user), date));
				}
				pInfo = new PriceInformation(pInfo.getQualifiers(), pInfo.getPriceValue().getOtherPrice(theTaxValues));
			}
			if (row.getChannel() == null)
			{
				defaultPriceInfos.add(pInfo);
			}
			if (channel == null && row.getChannel() == null)
			{
				priceInfos.add(pInfo);
			}
			else if (channel != null && row.getChannel() != null && row.getChannel().getCode().equalsIgnoreCase(channel.getCode()))
			{
				priceInfos.add(pInfo);
			}
		}

		if (priceInfos.size() == 0)
		{
			return defaultPriceInfos;
		}
		else
		{
			return priceInfos;
		}
	}


	class CustomPriceRowInfoComparator implements Comparator
	{

		public int compare(final PriceRow row1, final PriceRow row2)
		{
			final long u1Match = row1.getUnit().getPK().getLongValue();
			final long u2Match = row2.getUnit().getPK().getLongValue();
			if (u1Match != u2Match)
			{
				return u1Match >= u2Match ? 1 : -1;
			}
			final long min1 = row1.getMinqtdAsPrimitive();
			final long min2 = row2.getMinqtdAsPrimitive();
			if (min1 != min2)
			{
				return min1 <= min2 ? 1 : -1;
			}
			final int matchValue1 = row1.getMatchValueAsPrimitive();
			final int matchValue2 = row2.getMatchValueAsPrimitive();
			if (matchValue1 != matchValue2)
			{
				return matchValue2 - matchValue1;
			}
			final boolean c1Match = curr.equals(row1.getCurrency());
			final boolean c2Match = curr.equals(row2.getCurrency());
			if (c1Match != c2Match)
			{
				return c1Match ? -1 : 1;
			}
			final boolean n1Match = net == row1.isNetAsPrimitive();
			final boolean n2Match = net == row2.isNetAsPrimitive();
			if (n1Match != n2Match)
			{
				return n1Match ? -1 : 1;
			}
			final boolean row1Range = row1.getStartTime() != null;
			final boolean row2Range = row2.getStartTime() != null;
			if (row1Range != row2Range)
			{
				return row1Range ? -1 : 1;
			}
			else
			{
				return row1.getPK().compareTo(row2.getPK());
			}
		}

		@Override
		public int compare(final Object obj, final Object obj1)
		{
			return compare((PriceRow) obj, (PriceRow) obj1);
		}

		private final Currency curr;
		private final boolean net;

		CustomPriceRowInfoComparator(final Currency curr, final boolean net)
		{
			super();
			this.curr = curr;
			this.net = net;
		}


	}

	class PriceRowMatchComparator implements Comparator
	{

		PriceRowMatchComparator(final Currency curr, final boolean net, final Unit unit)
		{
			super();
			this.curr = curr;
			this.net = net;
			this.unit = unit;
		}

		public int compare(final PriceRow row1, final PriceRow row2)
		{
			final int matchValue1 = row1.getMatchValueAsPrimitive();
			final int matchValue2 = row2.getMatchValueAsPrimitive();
			if (matchValue1 != matchValue2)
			{
				return matchValue2 - matchValue1;
			}
			final boolean c1Match = curr.equals(row1.getCurrency());
			final boolean c2Match = curr.equals(row2.getCurrency());
			if (c1Match != c2Match)
			{
				return c1Match ? -1 : 1;
			}
			final boolean n1Match = net == row1.isNetAsPrimitive();
			final boolean n2Match = net == row2.isNetAsPrimitive();
			if (n1Match != n2Match)
			{
				return n1Match ? -1 : 1;
			}
			final boolean u1Match = unit.equals(row1.getUnit());
			final boolean u2Match = unit.equals(row2.getUnit());
			if (u1Match != u2Match)
			{
				return u1Match ? -1 : 1;
			}
			final long min1 = row1.getMinqtdAsPrimitive();
			final long min2 = row2.getMinqtdAsPrimitive();
			if (min1 != min2)
			{
				return min1 <= min2 ? 1 : -1;
			}
			final boolean row1Range = row1.getStartTime() != null;
			final boolean row2Range = row2.getStartTime() != null;
			if (row1Range != row2Range)
			{
				return row1Range ? -1 : 1;
			}
			else
			{
				//           Europe1PriceFactory.LOG.warn((new StringBuilder("found ambigous price rows ")).append(row1).append(" and ").append(row2).append(" - using PK to distinguish").toString());
				return row1.getPK().compareTo(row2.getPK());
			}
		}

		@Override
		public int compare(final Object obj, final Object obj1)
		{
			return compare((PriceRow) obj, (PriceRow) obj1);
		}

		private final Currency curr;
		private final boolean net;
		private final Unit unit;

	}
}
