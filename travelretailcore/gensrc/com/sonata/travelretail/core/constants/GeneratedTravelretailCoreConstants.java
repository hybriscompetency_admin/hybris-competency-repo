/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Aug 26, 2016 11:25:11 AM                    ---
 * ----------------------------------------------------------------
 */
package com.sonata.travelretail.core.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedTravelretailCoreConstants
{
	public static final String EXTENSIONNAME = "travelretailcore";
	public static class TC
	{
		public static final String APPARELPRODUCT = "ApparelProduct".intern();
		public static final String APPARELSIZEVARIANTPRODUCT = "ApparelSizeVariantProduct".intern();
		public static final String APPARELSTYLEVARIANTPRODUCT = "ApparelStyleVariantProduct".intern();
		public static final String ELECTRONICSCOLORVARIANTPRODUCT = "ElectronicsColorVariantProduct".intern();
		public static final String FLIGHTROUTE = "FlightRoute".intern();
		public static final String PNRBOOKINGDETAILSCOMPONENT = "PnrBookingDetailsComponent".intern();
		public static final String PNRDETAILSCOMPONENT = "PnrDetailsComponent".intern();
		public static final String PRODUCTBOOKING = "ProductBooking".intern();
		public static final String SWATCHCOLORENUM = "SwatchColorEnum".intern();
	}
	public static class Attributes
	{
		public static class Address
		{
			public static final String ISRESORTADDRESS = "isResortAddress".intern();
		}
		public static class PriceRow
		{
			public static final String DESTINATION = "destination".intern();
			public static final String SOURCE = "source".intern();
		}
		public static class Product
		{
			public static final String BV = "bv".intern();
			public static final String FLIGHTROUTES = "FlightRoutes".intern();
			public static final String ISCHILDPRODUCT = "isChildProduct".intern();
			public static final String MAXBOOKINGQUNATITY = "maxBookingQunatity".intern();
			public static final String PV = "pv".intern();
		}
	}
	public static class Enumerations
	{
		public static class SwatchColorEnum
		{
			public static final String BLACK = "BLACK".intern();
			public static final String BLUE = "BLUE".intern();
			public static final String BROWN = "BROWN".intern();
			public static final String GREEN = "GREEN".intern();
			public static final String GREY = "GREY".intern();
			public static final String ORANGE = "ORANGE".intern();
			public static final String PINK = "PINK".intern();
			public static final String PURPLE = "PURPLE".intern();
			public static final String RED = "RED".intern();
			public static final String SILVER = "SILVER".intern();
			public static final String WHITE = "WHITE".intern();
			public static final String YELLOW = "YELLOW".intern();
		}
	}
	public static class Relations
	{
		public static final String PRODUCTS2ROUTESSRELATION = "Products2RoutessRelation".intern();
	}
	
	protected GeneratedTravelretailCoreConstants()
	{
		// private constructor
	}
	
	
}
