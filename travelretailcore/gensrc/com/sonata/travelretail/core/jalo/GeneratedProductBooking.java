/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Aug 26, 2016 11:25:11 AM                    ---
 * ----------------------------------------------------------------
 */
package com.sonata.travelretail.core.jalo;

import com.sonata.travelretail.core.constants.TravelretailCoreConstants;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link com.sonata.travelretail.core.jalo.ProductBooking ProductBooking}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedProductBooking extends GenericItem
{
	/** Qualifier of the <code>ProductBooking.Pnr</code> attribute **/
	public static final String PNR = "Pnr";
	/** Qualifier of the <code>ProductBooking.orderId</code> attribute **/
	public static final String ORDERID = "orderId";
	/** Qualifier of the <code>ProductBooking.source</code> attribute **/
	public static final String SOURCE = "source";
	/** Qualifier of the <code>ProductBooking.destination</code> attribute **/
	public static final String DESTINATION = "destination";
	/** Qualifier of the <code>ProductBooking.ProductCode</code> attribute **/
	public static final String PRODUCTCODE = "ProductCode";
	/** Qualifier of the <code>ProductBooking.Qunatity</code> attribute **/
	public static final String QUNATITY = "Qunatity";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(PNR, AttributeMode.INITIAL);
		tmp.put(ORDERID, AttributeMode.INITIAL);
		tmp.put(SOURCE, AttributeMode.INITIAL);
		tmp.put(DESTINATION, AttributeMode.INITIAL);
		tmp.put(PRODUCTCODE, AttributeMode.INITIAL);
		tmp.put(QUNATITY, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ProductBooking.destination</code> attribute.
	 * @return the destination - Destination of the route.
	 */
	public String getDestination(final SessionContext ctx)
	{
		return (String)getProperty( ctx, DESTINATION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ProductBooking.destination</code> attribute.
	 * @return the destination - Destination of the route.
	 */
	public String getDestination()
	{
		return getDestination( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ProductBooking.destination</code> attribute. 
	 * @param value the destination - Destination of the route.
	 */
	public void setDestination(final SessionContext ctx, final String value)
	{
		setProperty(ctx, DESTINATION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ProductBooking.destination</code> attribute. 
	 * @param value the destination - Destination of the route.
	 */
	public void setDestination(final String value)
	{
		setDestination( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ProductBooking.orderId</code> attribute.
	 * @return the orderId
	 */
	public String getOrderId(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ORDERID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ProductBooking.orderId</code> attribute.
	 * @return the orderId
	 */
	public String getOrderId()
	{
		return getOrderId( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ProductBooking.orderId</code> attribute. 
	 * @param value the orderId
	 */
	public void setOrderId(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ORDERID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ProductBooking.orderId</code> attribute. 
	 * @param value the orderId
	 */
	public void setOrderId(final String value)
	{
		setOrderId( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ProductBooking.Pnr</code> attribute.
	 * @return the Pnr
	 */
	public String getPnr(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PNR);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ProductBooking.Pnr</code> attribute.
	 * @return the Pnr
	 */
	public String getPnr()
	{
		return getPnr( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ProductBooking.Pnr</code> attribute. 
	 * @param value the Pnr
	 */
	public void setPnr(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PNR,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ProductBooking.Pnr</code> attribute. 
	 * @param value the Pnr
	 */
	public void setPnr(final String value)
	{
		setPnr( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ProductBooking.ProductCode</code> attribute.
	 * @return the ProductCode - Products
	 */
	public String getProductCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PRODUCTCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ProductBooking.ProductCode</code> attribute.
	 * @return the ProductCode - Products
	 */
	public String getProductCode()
	{
		return getProductCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ProductBooking.ProductCode</code> attribute. 
	 * @param value the ProductCode - Products
	 */
	public void setProductCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PRODUCTCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ProductBooking.ProductCode</code> attribute. 
	 * @param value the ProductCode - Products
	 */
	public void setProductCode(final String value)
	{
		setProductCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ProductBooking.Qunatity</code> attribute.
	 * @return the Qunatity - Quantity of products booked
	 */
	public Integer getQunatity(final SessionContext ctx)
	{
		return (Integer)getProperty( ctx, QUNATITY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ProductBooking.Qunatity</code> attribute.
	 * @return the Qunatity - Quantity of products booked
	 */
	public Integer getQunatity()
	{
		return getQunatity( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ProductBooking.Qunatity</code> attribute. 
	 * @return the Qunatity - Quantity of products booked
	 */
	public int getQunatityAsPrimitive(final SessionContext ctx)
	{
		Integer value = getQunatity( ctx );
		return value != null ? value.intValue() : 0;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ProductBooking.Qunatity</code> attribute. 
	 * @return the Qunatity - Quantity of products booked
	 */
	public int getQunatityAsPrimitive()
	{
		return getQunatityAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ProductBooking.Qunatity</code> attribute. 
	 * @param value the Qunatity - Quantity of products booked
	 */
	public void setQunatity(final SessionContext ctx, final Integer value)
	{
		setProperty(ctx, QUNATITY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ProductBooking.Qunatity</code> attribute. 
	 * @param value the Qunatity - Quantity of products booked
	 */
	public void setQunatity(final Integer value)
	{
		setQunatity( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ProductBooking.Qunatity</code> attribute. 
	 * @param value the Qunatity - Quantity of products booked
	 */
	public void setQunatity(final SessionContext ctx, final int value)
	{
		setQunatity( ctx,Integer.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ProductBooking.Qunatity</code> attribute. 
	 * @param value the Qunatity - Quantity of products booked
	 */
	public void setQunatity(final int value)
	{
		setQunatity( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ProductBooking.source</code> attribute.
	 * @return the source - Source of the route.
	 */
	public String getSource(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SOURCE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ProductBooking.source</code> attribute.
	 * @return the source - Source of the route.
	 */
	public String getSource()
	{
		return getSource( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ProductBooking.source</code> attribute. 
	 * @param value the source - Source of the route.
	 */
	public void setSource(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SOURCE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ProductBooking.source</code> attribute. 
	 * @param value the source - Source of the route.
	 */
	public void setSource(final String value)
	{
		setSource( getSession().getSessionContext(), value );
	}
	
}
