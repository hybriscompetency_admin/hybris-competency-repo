/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Aug 26, 2016 11:25:11 AM                    ---
 * ----------------------------------------------------------------
 */
package com.sonata.travelretail.core.jalo;

import com.sonata.travelretail.core.constants.TravelretailCoreConstants;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.C2LManager;
import de.hybris.platform.jalo.c2l.Language;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.util.Utilities;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Generated class for type {@link com.sonata.travelretail.core.jalo.FlightRoute FlightRoute}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFlightRoute extends GenericItem
{
	/** Qualifier of the <code>FlightRoute.code</code> attribute **/
	public static final String CODE = "code";
	/** Qualifier of the <code>FlightRoute.name</code> attribute **/
	public static final String NAME = "name";
	/** Qualifier of the <code>FlightRoute.routeCode</code> attribute **/
	public static final String ROUTECODE = "routeCode";
	/** Qualifier of the <code>FlightRoute.source</code> attribute **/
	public static final String SOURCE = "source";
	/** Qualifier of the <code>FlightRoute.destination</code> attribute **/
	public static final String DESTINATION = "destination";
	/** Qualifier of the <code>FlightRoute.Products</code> attribute **/
	public static final String PRODUCTS = "Products";
	/** Relation ordering override parameter constants for Products2RoutessRelation from ((travelretailcore))*/
	protected static String PRODUCTS2ROUTESSRELATION_SRC_ORDERED = "relation.Products2RoutessRelation.source.ordered";
	protected static String PRODUCTS2ROUTESSRELATION_TGT_ORDERED = "relation.Products2RoutessRelation.target.ordered";
	/** Relation disable markmodifed parameter constants for Products2RoutessRelation from ((travelretailcore))*/
	protected static String PRODUCTS2ROUTESSRELATION_MARKMODIFIED = "relation.Products2RoutessRelation.markmodified";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(CODE, AttributeMode.INITIAL);
		tmp.put(NAME, AttributeMode.INITIAL);
		tmp.put(ROUTECODE, AttributeMode.INITIAL);
		tmp.put(SOURCE, AttributeMode.INITIAL);
		tmp.put(DESTINATION, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FlightRoute.code</code> attribute.
	 * @return the code
	 */
	public String getCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FlightRoute.code</code> attribute.
	 * @return the code
	 */
	public String getCode()
	{
		return getCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FlightRoute.code</code> attribute. 
	 * @param value the code
	 */
	public void setCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FlightRoute.code</code> attribute. 
	 * @param value the code
	 */
	public void setCode(final String value)
	{
		setCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FlightRoute.destination</code> attribute.
	 * @return the destination - Destination of the route.
	 */
	public String getDestination(final SessionContext ctx)
	{
		return (String)getProperty( ctx, DESTINATION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FlightRoute.destination</code> attribute.
	 * @return the destination - Destination of the route.
	 */
	public String getDestination()
	{
		return getDestination( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FlightRoute.destination</code> attribute. 
	 * @param value the destination - Destination of the route.
	 */
	public void setDestination(final SessionContext ctx, final String value)
	{
		setProperty(ctx, DESTINATION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FlightRoute.destination</code> attribute. 
	 * @param value the destination - Destination of the route.
	 */
	public void setDestination(final String value)
	{
		setDestination( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FlightRoute.name</code> attribute.
	 * @return the name
	 */
	public String getName(final SessionContext ctx)
	{
		if( ctx == null || ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedFlightRoute.getName requires a session language", 0 );
		}
		return (String)getLocalizedProperty( ctx, NAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FlightRoute.name</code> attribute.
	 * @return the name
	 */
	public String getName()
	{
		return getName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FlightRoute.name</code> attribute. 
	 * @return the localized name
	 */
	public Map<Language,String> getAllName(final SessionContext ctx)
	{
		return (Map<Language,String>)getAllLocalizedProperties(ctx,NAME,C2LManager.getInstance().getAllLanguages());
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FlightRoute.name</code> attribute. 
	 * @return the localized name
	 */
	public Map<Language,String> getAllName()
	{
		return getAllName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FlightRoute.name</code> attribute. 
	 * @param value the name
	 */
	public void setName(final SessionContext ctx, final String value)
	{
		if( ctx == null || ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedFlightRoute.setName requires a session language", 0 );
		}
		setLocalizedProperty(ctx, NAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FlightRoute.name</code> attribute. 
	 * @param value the name
	 */
	public void setName(final String value)
	{
		setName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FlightRoute.name</code> attribute. 
	 * @param value the name
	 */
	public void setAllName(final SessionContext ctx, final Map<Language,String> value)
	{
		setAllLocalizedProperties(ctx,NAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FlightRoute.name</code> attribute. 
	 * @param value the name
	 */
	public void setAllName(final Map<Language,String> value)
	{
		setAllName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FlightRoute.Products</code> attribute.
	 * @return the Products
	 */
	public List<Product> getProducts(final SessionContext ctx)
	{
		final List<Product> items = getLinkedItems( 
			ctx,
			false,
			TravelretailCoreConstants.Relations.PRODUCTS2ROUTESSRELATION,
			"Product",
			null,
			Utilities.getRelationOrderingOverride(PRODUCTS2ROUTESSRELATION_SRC_ORDERED, true),
			Utilities.getRelationOrderingOverride(PRODUCTS2ROUTESSRELATION_TGT_ORDERED, true)
		);
		return items;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FlightRoute.Products</code> attribute.
	 * @return the Products
	 */
	public List<Product> getProducts()
	{
		return getProducts( getSession().getSessionContext() );
	}
	
	public long getProductsCount(final SessionContext ctx)
	{
		return getLinkedItemsCount(
			ctx,
			false,
			TravelretailCoreConstants.Relations.PRODUCTS2ROUTESSRELATION,
			"Product",
			null
		);
	}
	
	public long getProductsCount()
	{
		return getProductsCount( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FlightRoute.Products</code> attribute. 
	 * @param value the Products
	 */
	public void setProducts(final SessionContext ctx, final List<Product> value)
	{
		setLinkedItems( 
			ctx,
			false,
			TravelretailCoreConstants.Relations.PRODUCTS2ROUTESSRELATION,
			null,
			value,
			Utilities.getRelationOrderingOverride(PRODUCTS2ROUTESSRELATION_SRC_ORDERED, true),
			Utilities.getRelationOrderingOverride(PRODUCTS2ROUTESSRELATION_TGT_ORDERED, true),
			Utilities.getMarkModifiedOverride(PRODUCTS2ROUTESSRELATION_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FlightRoute.Products</code> attribute. 
	 * @param value the Products
	 */
	public void setProducts(final List<Product> value)
	{
		setProducts( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to Products. 
	 * @param value the item to add to Products
	 */
	public void addToProducts(final SessionContext ctx, final Product value)
	{
		addLinkedItems( 
			ctx,
			false,
			TravelretailCoreConstants.Relations.PRODUCTS2ROUTESSRELATION,
			null,
			Collections.singletonList(value),
			Utilities.getRelationOrderingOverride(PRODUCTS2ROUTESSRELATION_SRC_ORDERED, true),
			Utilities.getRelationOrderingOverride(PRODUCTS2ROUTESSRELATION_TGT_ORDERED, true),
			Utilities.getMarkModifiedOverride(PRODUCTS2ROUTESSRELATION_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to Products. 
	 * @param value the item to add to Products
	 */
	public void addToProducts(final Product value)
	{
		addToProducts( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from Products. 
	 * @param value the item to remove from Products
	 */
	public void removeFromProducts(final SessionContext ctx, final Product value)
	{
		removeLinkedItems( 
			ctx,
			false,
			TravelretailCoreConstants.Relations.PRODUCTS2ROUTESSRELATION,
			null,
			Collections.singletonList(value),
			Utilities.getRelationOrderingOverride(PRODUCTS2ROUTESSRELATION_SRC_ORDERED, true),
			Utilities.getRelationOrderingOverride(PRODUCTS2ROUTESSRELATION_TGT_ORDERED, true),
			Utilities.getMarkModifiedOverride(PRODUCTS2ROUTESSRELATION_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from Products. 
	 * @param value the item to remove from Products
	 */
	public void removeFromProducts(final Product value)
	{
		removeFromProducts( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FlightRoute.routeCode</code> attribute.
	 * @return the routeCode - code of the route.
	 */
	public String getRouteCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ROUTECODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FlightRoute.routeCode</code> attribute.
	 * @return the routeCode - code of the route.
	 */
	public String getRouteCode()
	{
		return getRouteCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FlightRoute.routeCode</code> attribute. 
	 * @param value the routeCode - code of the route.
	 */
	public void setRouteCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ROUTECODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FlightRoute.routeCode</code> attribute. 
	 * @param value the routeCode - code of the route.
	 */
	public void setRouteCode(final String value)
	{
		setRouteCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FlightRoute.source</code> attribute.
	 * @return the source - Source of the route.
	 */
	public String getSource(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SOURCE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FlightRoute.source</code> attribute.
	 * @return the source - Source of the route.
	 */
	public String getSource()
	{
		return getSource( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FlightRoute.source</code> attribute. 
	 * @param value the source - Source of the route.
	 */
	public void setSource(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SOURCE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FlightRoute.source</code> attribute. 
	 * @param value the source - Source of the route.
	 */
	public void setSource(final String value)
	{
		setSource( getSession().getSessionContext(), value );
	}
	
}
