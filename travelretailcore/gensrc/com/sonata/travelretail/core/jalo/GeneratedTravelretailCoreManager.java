/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Aug 26, 2016 11:25:11 AM                    ---
 * ----------------------------------------------------------------
 */
package com.sonata.travelretail.core.jalo;

import com.sonata.travelretail.core.constants.TravelretailCoreConstants;
import com.sonata.travelretail.core.jalo.ApparelProduct;
import com.sonata.travelretail.core.jalo.ApparelSizeVariantProduct;
import com.sonata.travelretail.core.jalo.ApparelStyleVariantProduct;
import com.sonata.travelretail.core.jalo.ElectronicsColorVariantProduct;
import com.sonata.travelretail.core.jalo.FlightRoute;
import com.sonata.travelretail.core.jalo.ProductBooking;
import de.hybris.platform.acceleratorcms.jalo.components.PnrBookingDetailsComponent;
import de.hybris.platform.acceleratorcms.jalo.components.PnrDetailsComponent;
import de.hybris.platform.europe1.jalo.PDTRow;
import de.hybris.platform.europe1.jalo.PriceRow;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.JaloSystemException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.extension.Extension;
import de.hybris.platform.jalo.link.Link;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.jalo.type.JaloGenericCreationException;
import de.hybris.platform.jalo.user.Address;
import de.hybris.platform.util.Utilities;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Generated class for type <code>TravelretailCoreManager</code>.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedTravelretailCoreManager extends Extension
{
	/** Relation ordering override parameter constants for Products2RoutessRelation from ((travelretailcore))*/
	protected static String PRODUCTS2ROUTESSRELATION_SRC_ORDERED = "relation.Products2RoutessRelation.source.ordered";
	protected static String PRODUCTS2ROUTESSRELATION_TGT_ORDERED = "relation.Products2RoutessRelation.target.ordered";
	/** Relation disable markmodifed parameter constants for Products2RoutessRelation from ((travelretailcore))*/
	protected static String PRODUCTS2ROUTESSRELATION_MARKMODIFIED = "relation.Products2RoutessRelation.markmodified";
	protected static final Map<String, Map<String, AttributeMode>> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, Map<String, AttributeMode>> ttmp = new HashMap();
		Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put("isChildProduct", AttributeMode.INITIAL);
		tmp.put("maxBookingQunatity", AttributeMode.INITIAL);
		tmp.put("pv", AttributeMode.INITIAL);
		tmp.put("bv", AttributeMode.INITIAL);
		ttmp.put("de.hybris.platform.jalo.product.Product", Collections.unmodifiableMap(tmp));
		tmp = new HashMap<String, AttributeMode>();
		tmp.put("source", AttributeMode.INITIAL);
		tmp.put("destination", AttributeMode.INITIAL);
		ttmp.put("de.hybris.platform.europe1.jalo.PriceRow", Collections.unmodifiableMap(tmp));
		tmp = new HashMap<String, AttributeMode>();
		tmp.put("isResortAddress", AttributeMode.INITIAL);
		ttmp.put("de.hybris.platform.jalo.user.Address", Collections.unmodifiableMap(tmp));
		DEFAULT_INITIAL_ATTRIBUTES = ttmp;
	}
	@Override
	public Map<String, AttributeMode> getDefaultAttributeModes(final Class<? extends Item> itemClass)
	{
		Map<String, AttributeMode> ret = new HashMap<>();
		final Map<String, AttributeMode> attr = DEFAULT_INITIAL_ATTRIBUTES.get(itemClass.getName());
		if (attr != null)
		{
			ret.putAll(attr);
		}
		return ret;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.bv</code> attribute.
	 * @return the bv - Attribute is used in calculating bonus value.
	 */
	public Double getBv(final SessionContext ctx, final Product item)
	{
		return (Double)item.getProperty( ctx, TravelretailCoreConstants.Attributes.Product.BV);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.bv</code> attribute.
	 * @return the bv - Attribute is used in calculating bonus value.
	 */
	public Double getBv(final Product item)
	{
		return getBv( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.bv</code> attribute. 
	 * @return the bv - Attribute is used in calculating bonus value.
	 */
	public double getBvAsPrimitive(final SessionContext ctx, final Product item)
	{
		Double value = getBv( ctx,item );
		return value != null ? value.doubleValue() : 0.0d;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.bv</code> attribute. 
	 * @return the bv - Attribute is used in calculating bonus value.
	 */
	public double getBvAsPrimitive(final Product item)
	{
		return getBvAsPrimitive( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.bv</code> attribute. 
	 * @param value the bv - Attribute is used in calculating bonus value.
	 */
	public void setBv(final SessionContext ctx, final Product item, final Double value)
	{
		item.setProperty(ctx, TravelretailCoreConstants.Attributes.Product.BV,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.bv</code> attribute. 
	 * @param value the bv - Attribute is used in calculating bonus value.
	 */
	public void setBv(final Product item, final Double value)
	{
		setBv( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.bv</code> attribute. 
	 * @param value the bv - Attribute is used in calculating bonus value.
	 */
	public void setBv(final SessionContext ctx, final Product item, final double value)
	{
		setBv( ctx, item, Double.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.bv</code> attribute. 
	 * @param value the bv - Attribute is used in calculating bonus value.
	 */
	public void setBv(final Product item, final double value)
	{
		setBv( getSession().getSessionContext(), item, value );
	}
	
	public ApparelProduct createApparelProduct(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( TravelretailCoreConstants.TC.APPARELPRODUCT );
			return (ApparelProduct)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating ApparelProduct : "+e.getMessage(), 0 );
		}
	}
	
	public ApparelProduct createApparelProduct(final Map attributeValues)
	{
		return createApparelProduct( getSession().getSessionContext(), attributeValues );
	}
	
	public ApparelSizeVariantProduct createApparelSizeVariantProduct(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( TravelretailCoreConstants.TC.APPARELSIZEVARIANTPRODUCT );
			return (ApparelSizeVariantProduct)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating ApparelSizeVariantProduct : "+e.getMessage(), 0 );
		}
	}
	
	public ApparelSizeVariantProduct createApparelSizeVariantProduct(final Map attributeValues)
	{
		return createApparelSizeVariantProduct( getSession().getSessionContext(), attributeValues );
	}
	
	public ApparelStyleVariantProduct createApparelStyleVariantProduct(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( TravelretailCoreConstants.TC.APPARELSTYLEVARIANTPRODUCT );
			return (ApparelStyleVariantProduct)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating ApparelStyleVariantProduct : "+e.getMessage(), 0 );
		}
	}
	
	public ApparelStyleVariantProduct createApparelStyleVariantProduct(final Map attributeValues)
	{
		return createApparelStyleVariantProduct( getSession().getSessionContext(), attributeValues );
	}
	
	public ElectronicsColorVariantProduct createElectronicsColorVariantProduct(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( TravelretailCoreConstants.TC.ELECTRONICSCOLORVARIANTPRODUCT );
			return (ElectronicsColorVariantProduct)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating ElectronicsColorVariantProduct : "+e.getMessage(), 0 );
		}
	}
	
	public ElectronicsColorVariantProduct createElectronicsColorVariantProduct(final Map attributeValues)
	{
		return createElectronicsColorVariantProduct( getSession().getSessionContext(), attributeValues );
	}
	
	public FlightRoute createFlightRoute(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( TravelretailCoreConstants.TC.FLIGHTROUTE );
			return (FlightRoute)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FlightRoute : "+e.getMessage(), 0 );
		}
	}
	
	public FlightRoute createFlightRoute(final Map attributeValues)
	{
		return createFlightRoute( getSession().getSessionContext(), attributeValues );
	}
	
	public PnrBookingDetailsComponent createPnrBookingDetailsComponent(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( TravelretailCoreConstants.TC.PNRBOOKINGDETAILSCOMPONENT );
			return (PnrBookingDetailsComponent)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating PnrBookingDetailsComponent : "+e.getMessage(), 0 );
		}
	}
	
	public PnrBookingDetailsComponent createPnrBookingDetailsComponent(final Map attributeValues)
	{
		return createPnrBookingDetailsComponent( getSession().getSessionContext(), attributeValues );
	}
	
	public PnrDetailsComponent createPnrDetailsComponent(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( TravelretailCoreConstants.TC.PNRDETAILSCOMPONENT );
			return (PnrDetailsComponent)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating PnrDetailsComponent : "+e.getMessage(), 0 );
		}
	}
	
	public PnrDetailsComponent createPnrDetailsComponent(final Map attributeValues)
	{
		return createPnrDetailsComponent( getSession().getSessionContext(), attributeValues );
	}
	
	public ProductBooking createProductBooking(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( TravelretailCoreConstants.TC.PRODUCTBOOKING );
			return (ProductBooking)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating ProductBooking : "+e.getMessage(), 0 );
		}
	}
	
	public ProductBooking createProductBooking(final Map attributeValues)
	{
		return createProductBooking( getSession().getSessionContext(), attributeValues );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PriceRow.destination</code> attribute.
	 * @return the destination - Attribute is used display the price for the particular route if exists.
	 */
	public String getDestination(final SessionContext ctx, final PriceRow item)
	{
		return (String)item.getProperty( ctx, TravelretailCoreConstants.Attributes.PriceRow.DESTINATION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PriceRow.destination</code> attribute.
	 * @return the destination - Attribute is used display the price for the particular route if exists.
	 */
	public String getDestination(final PriceRow item)
	{
		return getDestination( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PriceRow.destination</code> attribute. 
	 * @param value the destination - Attribute is used display the price for the particular route if exists.
	 */
	public void setDestination(final SessionContext ctx, final PriceRow item, final String value)
	{
		item.setProperty(ctx, TravelretailCoreConstants.Attributes.PriceRow.DESTINATION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PriceRow.destination</code> attribute. 
	 * @param value the destination - Attribute is used display the price for the particular route if exists.
	 */
	public void setDestination(final PriceRow item, final String value)
	{
		setDestination( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.FlightRoutes</code> attribute.
	 * @return the FlightRoutes - Products to Flight Routes relation
	 */
	public List<FlightRoute> getFlightRoutes(final SessionContext ctx, final Product item)
	{
		final List<FlightRoute> items = item.getLinkedItems( 
			ctx,
			true,
			TravelretailCoreConstants.Relations.PRODUCTS2ROUTESSRELATION,
			"FlightRoute",
			null,
			Utilities.getRelationOrderingOverride(PRODUCTS2ROUTESSRELATION_SRC_ORDERED, true),
			Utilities.getRelationOrderingOverride(PRODUCTS2ROUTESSRELATION_TGT_ORDERED, true)
		);
		return items;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.FlightRoutes</code> attribute.
	 * @return the FlightRoutes - Products to Flight Routes relation
	 */
	public List<FlightRoute> getFlightRoutes(final Product item)
	{
		return getFlightRoutes( getSession().getSessionContext(), item );
	}
	
	public long getFlightRoutesCount(final SessionContext ctx, final Product item)
	{
		return item.getLinkedItemsCount(
			ctx,
			true,
			TravelretailCoreConstants.Relations.PRODUCTS2ROUTESSRELATION,
			"FlightRoute",
			null
		);
	}
	
	public long getFlightRoutesCount(final Product item)
	{
		return getFlightRoutesCount( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.FlightRoutes</code> attribute. 
	 * @param value the FlightRoutes - Products to Flight Routes relation
	 */
	public void setFlightRoutes(final SessionContext ctx, final Product item, final List<FlightRoute> value)
	{
		item.setLinkedItems( 
			ctx,
			true,
			TravelretailCoreConstants.Relations.PRODUCTS2ROUTESSRELATION,
			null,
			value,
			Utilities.getRelationOrderingOverride(PRODUCTS2ROUTESSRELATION_SRC_ORDERED, true),
			Utilities.getRelationOrderingOverride(PRODUCTS2ROUTESSRELATION_TGT_ORDERED, true),
			Utilities.getMarkModifiedOverride(PRODUCTS2ROUTESSRELATION_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.FlightRoutes</code> attribute. 
	 * @param value the FlightRoutes - Products to Flight Routes relation
	 */
	public void setFlightRoutes(final Product item, final List<FlightRoute> value)
	{
		setFlightRoutes( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to FlightRoutes. 
	 * @param value the item to add to FlightRoutes - Products to Flight Routes relation
	 */
	public void addToFlightRoutes(final SessionContext ctx, final Product item, final FlightRoute value)
	{
		item.addLinkedItems( 
			ctx,
			true,
			TravelretailCoreConstants.Relations.PRODUCTS2ROUTESSRELATION,
			null,
			Collections.singletonList(value),
			Utilities.getRelationOrderingOverride(PRODUCTS2ROUTESSRELATION_SRC_ORDERED, true),
			Utilities.getRelationOrderingOverride(PRODUCTS2ROUTESSRELATION_TGT_ORDERED, true),
			Utilities.getMarkModifiedOverride(PRODUCTS2ROUTESSRELATION_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to FlightRoutes. 
	 * @param value the item to add to FlightRoutes - Products to Flight Routes relation
	 */
	public void addToFlightRoutes(final Product item, final FlightRoute value)
	{
		addToFlightRoutes( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from FlightRoutes. 
	 * @param value the item to remove from FlightRoutes - Products to Flight Routes relation
	 */
	public void removeFromFlightRoutes(final SessionContext ctx, final Product item, final FlightRoute value)
	{
		item.removeLinkedItems( 
			ctx,
			true,
			TravelretailCoreConstants.Relations.PRODUCTS2ROUTESSRELATION,
			null,
			Collections.singletonList(value),
			Utilities.getRelationOrderingOverride(PRODUCTS2ROUTESSRELATION_SRC_ORDERED, true),
			Utilities.getRelationOrderingOverride(PRODUCTS2ROUTESSRELATION_TGT_ORDERED, true),
			Utilities.getMarkModifiedOverride(PRODUCTS2ROUTESSRELATION_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from FlightRoutes. 
	 * @param value the item to remove from FlightRoutes - Products to Flight Routes relation
	 */
	public void removeFromFlightRoutes(final Product item, final FlightRoute value)
	{
		removeFromFlightRoutes( getSession().getSessionContext(), item, value );
	}
	
	@Override
	public String getName()
	{
		return TravelretailCoreConstants.EXTENSIONNAME;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.isChildProduct</code> attribute.
	 * @return the isChildProduct
	 */
	public Boolean isIsChildProduct(final SessionContext ctx, final Product item)
	{
		return (Boolean)item.getProperty( ctx, TravelretailCoreConstants.Attributes.Product.ISCHILDPRODUCT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.isChildProduct</code> attribute.
	 * @return the isChildProduct
	 */
	public Boolean isIsChildProduct(final Product item)
	{
		return isIsChildProduct( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.isChildProduct</code> attribute. 
	 * @return the isChildProduct
	 */
	public boolean isIsChildProductAsPrimitive(final SessionContext ctx, final Product item)
	{
		Boolean value = isIsChildProduct( ctx,item );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.isChildProduct</code> attribute. 
	 * @return the isChildProduct
	 */
	public boolean isIsChildProductAsPrimitive(final Product item)
	{
		return isIsChildProductAsPrimitive( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.isChildProduct</code> attribute. 
	 * @param value the isChildProduct
	 */
	public void setIsChildProduct(final SessionContext ctx, final Product item, final Boolean value)
	{
		item.setProperty(ctx, TravelretailCoreConstants.Attributes.Product.ISCHILDPRODUCT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.isChildProduct</code> attribute. 
	 * @param value the isChildProduct
	 */
	public void setIsChildProduct(final Product item, final Boolean value)
	{
		setIsChildProduct( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.isChildProduct</code> attribute. 
	 * @param value the isChildProduct
	 */
	public void setIsChildProduct(final SessionContext ctx, final Product item, final boolean value)
	{
		setIsChildProduct( ctx, item, Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.isChildProduct</code> attribute. 
	 * @param value the isChildProduct
	 */
	public void setIsChildProduct(final Product item, final boolean value)
	{
		setIsChildProduct( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Address.isResortAddress</code> attribute.
	 * @return the isResortAddress - Indicates if the address is resort address or not.
	 */
	public Boolean isIsResortAddress(final SessionContext ctx, final Address item)
	{
		return (Boolean)item.getProperty( ctx, TravelretailCoreConstants.Attributes.Address.ISRESORTADDRESS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Address.isResortAddress</code> attribute.
	 * @return the isResortAddress - Indicates if the address is resort address or not.
	 */
	public Boolean isIsResortAddress(final Address item)
	{
		return isIsResortAddress( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Address.isResortAddress</code> attribute. 
	 * @return the isResortAddress - Indicates if the address is resort address or not.
	 */
	public boolean isIsResortAddressAsPrimitive(final SessionContext ctx, final Address item)
	{
		Boolean value = isIsResortAddress( ctx,item );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Address.isResortAddress</code> attribute. 
	 * @return the isResortAddress - Indicates if the address is resort address or not.
	 */
	public boolean isIsResortAddressAsPrimitive(final Address item)
	{
		return isIsResortAddressAsPrimitive( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Address.isResortAddress</code> attribute. 
	 * @param value the isResortAddress - Indicates if the address is resort address or not.
	 */
	public void setIsResortAddress(final SessionContext ctx, final Address item, final Boolean value)
	{
		item.setProperty(ctx, TravelretailCoreConstants.Attributes.Address.ISRESORTADDRESS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Address.isResortAddress</code> attribute. 
	 * @param value the isResortAddress - Indicates if the address is resort address or not.
	 */
	public void setIsResortAddress(final Address item, final Boolean value)
	{
		setIsResortAddress( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Address.isResortAddress</code> attribute. 
	 * @param value the isResortAddress - Indicates if the address is resort address or not.
	 */
	public void setIsResortAddress(final SessionContext ctx, final Address item, final boolean value)
	{
		setIsResortAddress( ctx, item, Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Address.isResortAddress</code> attribute. 
	 * @param value the isResortAddress - Indicates if the address is resort address or not.
	 */
	public void setIsResortAddress(final Address item, final boolean value)
	{
		setIsResortAddress( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.maxBookingQunatity</code> attribute.
	 * @return the maxBookingQunatity
	 */
	public Integer getMaxBookingQunatity(final SessionContext ctx, final Product item)
	{
		return (Integer)item.getProperty( ctx, TravelretailCoreConstants.Attributes.Product.MAXBOOKINGQUNATITY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.maxBookingQunatity</code> attribute.
	 * @return the maxBookingQunatity
	 */
	public Integer getMaxBookingQunatity(final Product item)
	{
		return getMaxBookingQunatity( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.maxBookingQunatity</code> attribute. 
	 * @return the maxBookingQunatity
	 */
	public int getMaxBookingQunatityAsPrimitive(final SessionContext ctx, final Product item)
	{
		Integer value = getMaxBookingQunatity( ctx,item );
		return value != null ? value.intValue() : 0;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.maxBookingQunatity</code> attribute. 
	 * @return the maxBookingQunatity
	 */
	public int getMaxBookingQunatityAsPrimitive(final Product item)
	{
		return getMaxBookingQunatityAsPrimitive( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.maxBookingQunatity</code> attribute. 
	 * @param value the maxBookingQunatity
	 */
	public void setMaxBookingQunatity(final SessionContext ctx, final Product item, final Integer value)
	{
		item.setProperty(ctx, TravelretailCoreConstants.Attributes.Product.MAXBOOKINGQUNATITY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.maxBookingQunatity</code> attribute. 
	 * @param value the maxBookingQunatity
	 */
	public void setMaxBookingQunatity(final Product item, final Integer value)
	{
		setMaxBookingQunatity( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.maxBookingQunatity</code> attribute. 
	 * @param value the maxBookingQunatity
	 */
	public void setMaxBookingQunatity(final SessionContext ctx, final Product item, final int value)
	{
		setMaxBookingQunatity( ctx, item, Integer.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.maxBookingQunatity</code> attribute. 
	 * @param value the maxBookingQunatity
	 */
	public void setMaxBookingQunatity(final Product item, final int value)
	{
		setMaxBookingQunatity( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.pv</code> attribute.
	 * @return the pv - Attribute is used in calculating bonus value.
	 */
	public Double getPv(final SessionContext ctx, final Product item)
	{
		return (Double)item.getProperty( ctx, TravelretailCoreConstants.Attributes.Product.PV);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.pv</code> attribute.
	 * @return the pv - Attribute is used in calculating bonus value.
	 */
	public Double getPv(final Product item)
	{
		return getPv( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.pv</code> attribute. 
	 * @return the pv - Attribute is used in calculating bonus value.
	 */
	public double getPvAsPrimitive(final SessionContext ctx, final Product item)
	{
		Double value = getPv( ctx,item );
		return value != null ? value.doubleValue() : 0.0d;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.pv</code> attribute. 
	 * @return the pv - Attribute is used in calculating bonus value.
	 */
	public double getPvAsPrimitive(final Product item)
	{
		return getPvAsPrimitive( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.pv</code> attribute. 
	 * @param value the pv - Attribute is used in calculating bonus value.
	 */
	public void setPv(final SessionContext ctx, final Product item, final Double value)
	{
		item.setProperty(ctx, TravelretailCoreConstants.Attributes.Product.PV,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.pv</code> attribute. 
	 * @param value the pv - Attribute is used in calculating bonus value.
	 */
	public void setPv(final Product item, final Double value)
	{
		setPv( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.pv</code> attribute. 
	 * @param value the pv - Attribute is used in calculating bonus value.
	 */
	public void setPv(final SessionContext ctx, final Product item, final double value)
	{
		setPv( ctx, item, Double.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.pv</code> attribute. 
	 * @param value the pv - Attribute is used in calculating bonus value.
	 */
	public void setPv(final Product item, final double value)
	{
		setPv( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PriceRow.source</code> attribute.
	 * @return the source - Attribute is used display the price for the particular route if exists.
	 */
	public String getSource(final SessionContext ctx, final PriceRow item)
	{
		return (String)item.getProperty( ctx, TravelretailCoreConstants.Attributes.PriceRow.SOURCE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PriceRow.source</code> attribute.
	 * @return the source - Attribute is used display the price for the particular route if exists.
	 */
	public String getSource(final PriceRow item)
	{
		return getSource( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PriceRow.source</code> attribute. 
	 * @param value the source - Attribute is used display the price for the particular route if exists.
	 */
	public void setSource(final SessionContext ctx, final PriceRow item, final String value)
	{
		item.setProperty(ctx, TravelretailCoreConstants.Attributes.PriceRow.SOURCE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PriceRow.source</code> attribute. 
	 * @param value the source - Attribute is used display the price for the particular route if exists.
	 */
	public void setSource(final PriceRow item, final String value)
	{
		setSource( getSession().getSessionContext(), item, value );
	}
	
}
