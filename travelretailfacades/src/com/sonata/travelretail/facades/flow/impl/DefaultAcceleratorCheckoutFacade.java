/**
 *
 */
package com.sonata.travelretail.facades.flow.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.InvalidCartException;


/**
 * @author tejaswini.g
 *
 */
public class DefaultAcceleratorCheckoutFacade extends
		de.hybris.platform.acceleratorfacades.order.impl.DefaultAcceleratorCheckoutFacade
{
	@Override
	public OrderData placeOrder() throws InvalidCartException
	{
		final CartModel cartModel = getCart();
		if (cartModel != null)
		{
			final UserModel currentUser = getCurrentUserForCheckout();
			if (cartModel.getUser().equals(currentUser) || getCheckoutCustomerStrategy().isAnonymousCheckout())
			{
				beforePlaceOrder(cartModel);

				final OrderModel orderModel = placeOrder(cartModel);

				final BaseSiteModel currentBaseSite = orderModel.getSite();

				if (("customb2b").equals(currentBaseSite.getUid()))
				{
					afterPlaceOrder(cartModel, orderModel);
				}

				// Convert the order to an order data
				if (orderModel != null)
				{
					return getOrderConverter().convert(orderModel);
				}
			}
		}

		return null;
	}
}
