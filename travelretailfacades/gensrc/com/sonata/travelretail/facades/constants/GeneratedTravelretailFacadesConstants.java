/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Aug 26, 2016 11:25:11 AM                    ---
 * ----------------------------------------------------------------
 */
package com.sonata.travelretail.facades.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedTravelretailFacadesConstants
{
	public static final String EXTENSIONNAME = "travelretailfacades";
	
	protected GeneratedTravelretailFacadesConstants()
	{
		// private constructor
	}
	
	
}
