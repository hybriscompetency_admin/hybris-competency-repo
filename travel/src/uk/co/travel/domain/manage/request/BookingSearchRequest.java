/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.travel.domain.manage.request;

import java.util.Date;

/**
 * @author veena.pn
 */
public class BookingSearchRequest {

    /**
     * The booking Reference Number
     */
    private String bookingReferenceNum;
    /**
     * The Departure Date From
     */
    private Date departureDateFrom;
    /**
     * The Departure Date TO
     */
    private Date departureDateTo;
    /**
     * The passenger surname
     */
    private String paxSurName;
    /**
     * @return the bookingReferenceNum
     */
    public String getBookingReferenceNum() {
        return bookingReferenceNum;
    }
    /**
     * @param bookingReferenceNum the bookingReferenceNum to set
     */
    public void setBookingReferenceNum(String bookingReferenceNum) {
        this.bookingReferenceNum = bookingReferenceNum;
    }
    /**
     * @return the departureDateFrom
     */
    public Date getDepartureDateFrom() {
        if(this.departureDateFrom == null)
        {
            return null;
        }
        return new Date(this.departureDateFrom.getTime());

    }
    /**
     * @param departureDateFrom the departureDateFrom to set
     */
    public void setDepartureDateFrom(Date departureDateFrom) {
        if(departureDateFrom == null)
        {
            this.departureDateFrom = null;
        }
        else
        {
            this.departureDateFrom = new Date(departureDateFrom.getTime());
        }
    }
    /**
     * @return the departureDateTo
     */
    public Date getDepartureDateTo() {
        if(this.departureDateTo == null)
        {
            return null;
        }
        return new Date(this.departureDateTo.getTime());
    }
    /**
     * @param departureDateTo the departureDateTo to set
     */
    public void setDepartureDateTo(Date departureDateTo) {
        if(departureDateTo == null)
        {
            this.departureDateTo = null;
        }
        else
        {
            this.departureDateTo = new Date(departureDateTo.getTime());
        }
    }
    /**
     * @return the paxSurName
     */
    public String getPaxSurName() {
        return paxSurName;
    }
    /**
     * @param paxSurName the paxSurName to set
     */
    public void setPaxSurName(String paxSurName) {
        this.paxSurName = paxSurName;
    }

}
