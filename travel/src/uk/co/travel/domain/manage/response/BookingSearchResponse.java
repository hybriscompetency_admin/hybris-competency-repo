/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.travel.domain.manage.response;

import java.util.List;

/**
 * @author veena.pn
 */
public class BookingSearchResponse {

    /**
     * The list of booking summary
     */
    private List<BookingSummary> bookingSummary;


    private int totalNoOfBookings;
    /**
     * @return the bookingSummary
     */
    public List<BookingSummary> getBookingSummary() {
        return bookingSummary;
    }

    /**
     * @param bookingSummary the bookingSummary to set
     */
    public void setBookingSummary(List<BookingSummary> bookingSummary) {
        this.bookingSummary = bookingSummary;
    }

    /**
     * @return the totalNoOfBookings
     */
    public int getTotalNoOfBookings() {
        return totalNoOfBookings;
    }

    /**
     * @param totalNoOfBookings the totalNoOfBookings to set
     */
    public void setTotalNoOfBookings(int totalNoOfBookings) {
        this.totalNoOfBookings = totalNoOfBookings;
    }



}
