/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.travel.domain.manage.response;

import java.util.Date;
import java.util.List;

/**
 * @author veena.pn
 */
public class BookingSummary {


    /**
     * The leadPaxName
     */
    private String leadPaxName;
    /**
     * The list of paxDetails
     */
    private List<String> paxDetails;
    /**
     * The agent Details
     */
    private String agentDetails;
    /**
     * The departure Date
     */
    private Date departureDate;
    /**
     * The booking Reference
     */
    private String bookingReference;
    /**
     * The booking address
     */
    private BookingAddress bookingAddress;


    /**
     * The accommodation code
     */
    private String accomCode;
    /**
     * @return the leadPaxName
     */
    public String getLeadPaxName() {
        return leadPaxName;
    }
    /**
     * @param leadPaxName the leadPaxName to set
     */
    public void setLeadPaxName(String leadPaxName) {
        this.leadPaxName = leadPaxName;
    }
    /**
     * @return the paxDetails
     */
    public List<String> getPaxDetails() {
        return paxDetails;
    }
    /**
     * @param paxDetails the paxDetails to set
     */
    public void setPaxDetails(List<String> paxDetails) {
        this.paxDetails = paxDetails;
    }
    /**
     * @return the agentDetails
     */
    public String getAgentDetails() {
        return agentDetails;
    }
    /**
     * @param agentDetails the agentDetails to set
     */
    public void setAgentDetails(String agentDetails) {
        this.agentDetails = agentDetails;
    }
    /**
     * @return the departureDate
     */
    public Date getDepartureDate() {
        if (departureDate != null) {
            return (Date) departureDate.clone();
        }
        return null;
    }
    /**
     * @param departureDate the departureDate to set
     */
    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }
    /**
     * @return the bookingReference
     */
    public String getBookingReference() {
        return bookingReference;
    }
    /**
     * @param bookingReference the bookingReference to set
     */
    public void setBookingReference(String bookingReference) {
        this.bookingReference = bookingReference;
    }
    /**
     * @return the bookingAddress
     */
    public BookingAddress getBookingAddress() {
        return bookingAddress;
    }
    /**
     * @param bookingAddress the bookingAddress to set
     */
    public void setBookingAddress(BookingAddress bookingAddress) {
        this.bookingAddress = bookingAddress;
    }
    /**
     * @return the accomCode
     */
    public String getAccomCode() {
        return accomCode;
    }
    /**
     * @param accomCode the accomCode to set
     */
    public void setAccomCode(String accomCode) {
        this.accomCode = accomCode;
    }



}
