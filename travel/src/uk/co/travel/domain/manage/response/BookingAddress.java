/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.travel.domain.manage.response;

import java.util.List;

/**
 *@author veena.pn
 */
public class BookingAddress {

    /**
     * The address
     */
    private List<String> address;
    /**
     * The contact number
     */
    private String contactNumber;
    /**
     * The email address
     */
    private String email;
    /**
     * @return the address
     */
    public List<String> getAddress() {
        return address;
    }
    /**
     * @param address the address to set
     */
    public void setAddress(List<String> address) {
        this.address = address;
    }
    /**
     * @return the contactNumber
     */
    public String getContactNumber() {
        return contactNumber;
    }
    /**
     * @param contactNumber the contactNumber to set
     */
    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }
    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }
    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }



}
