/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement you entered into with hybris.
 */
package uk.co.travel.domain.manage.response;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import uk.co.tui.book.domain.lite.BookingDetails;
import uk.co.tui.book.domain.lite.CommunicationPreference;
import uk.co.tui.book.domain.lite.Deposit;
import uk.co.tui.book.domain.lite.Discount;
import uk.co.tui.book.domain.lite.ExtraFacility;
import uk.co.tui.book.domain.lite.ExtraFacilityCategory;
import uk.co.tui.book.domain.lite.HighLevelBookingType;
import uk.co.tui.book.domain.lite.Inventory;
import uk.co.tui.book.domain.lite.Itinerary;
import uk.co.tui.book.domain.lite.Memo;
import uk.co.tui.book.domain.lite.PackageType;
import uk.co.tui.book.domain.lite.Passenger;
import uk.co.tui.book.domain.lite.Price;
import uk.co.tui.book.domain.lite.PromotionalDiscount;
import uk.co.tui.book.domain.lite.Stay;

/**
 *
 */
public class DisplayBookingResponse
{

   private Discount discount;

   private Inventory inventory;

   private PackageType packageType;

   private List<Passenger> passengers;

   private CommunicationPreference communicationPreference;

   private List<Memo> memos;

   private Itinerary itinerary;

   private String bookingRefNum;

   private List<Deposit> deposits;

   private Price price;

   private BookingDetails bookingDetails;

   private List<ExtraFacility> integratedExtraFacilities;

   private List<ExtraFacility> flightExtraFacilities;

   private List<ExtraFacilityCategory> extraFacilityCategories;

   private Stay stay;

   private List<Price> prices;

   private PromotionalDiscount promotionalDiscount;

   private String bookingStatus;

   private HighLevelBookingType highLevelBookingType;

   private List<String> transferCodes = new ArrayList<String>();

   /**
    * @return the bookingStatus
    */
   public String getBookingStatus()
   {
      return bookingStatus;
   }

   /**
    * @param bookingStatus the bookingStatus to set
    */
   public void setBookingStatus(String bookingStatus)
   {
      this.bookingStatus = bookingStatus;
   }

   /**
    * @return the promotionalDiscount
    */
   public PromotionalDiscount getPromotionalDiscount()
   {
      return promotionalDiscount;
   }

   /**
    * @param promotionalDiscount the promotionalDiscount to set
    */
   public void setPromotionalDiscount(PromotionalDiscount promotionalDiscount)
   {
      this.promotionalDiscount = promotionalDiscount;
   }

   /**
    * @return the prices
    */
   public List<Price> getPrices()
   {
      return prices;
   }

   /**
    * @param prices the prices to set
    */
   public void setPrices(List<Price> prices)
   {
      this.prices = prices;
   }

   /**
    * @return the extraFacilityCategories
    */
   public List<ExtraFacilityCategory> getExtraFacilityCategories()
   {
      if (CollectionUtils.isEmpty(extraFacilityCategories))
      {
         extraFacilityCategories = new ArrayList<ExtraFacilityCategory>();
      }
      return extraFacilityCategories;
   }

   /**
    * @param extraFacilityCategories the extraFacilityCategories to set
    */
   public void setExtraFacilityCategories(List<ExtraFacilityCategory> extraFacilityCategories)
   {
      this.extraFacilityCategories = extraFacilityCategories;
   }

   /**
    * @return the bookingDetails
    */
   public BookingDetails getBookingDetails()
   {
      return bookingDetails;
   }

   /**
    * @param bookingDetails the bookingDetails to set
    */
   public void setBookingDetails(BookingDetails bookingDetails)
   {
      this.bookingDetails = bookingDetails;
   }

   /**
    * @return the price
    */
   public Price getPrice()
   {
      return price;
   }

   /**
    * @param price the price to set
    */
   public void setPrice(Price price)
   {
      this.price = price;
   }

   /**
    * @return the deposits
    */
   public List<Deposit> getDeposits()
   {
      return deposits;
   }

   /**
    * @param deposits the deposits to set
    */
   public void setDeposits(List<Deposit> deposits)
   {
      this.deposits = deposits;
   }

   /**
    * @return the bookingRefNum
    */
   public String getBookingRefNum()
   {
      return bookingRefNum;
   }

   /**
    * @param bookingRefNum the bookingRefNum to set
    */
   public void setBookingRefNum(String bookingRefNum)
   {
      this.bookingRefNum = bookingRefNum;
   }

   /**
    * @return the passengers
    */
   public List<Passenger> getPassengers()
   {
      return passengers;
   }

   /**
    * @param passengers the passengers to set
    */
   public void setPassengers(List<Passenger> passengers)
   {
      this.passengers = passengers;
   }

   /**
    * @return the communicationPreference
    */
   public CommunicationPreference getCommunicationPreference()
   {
      return communicationPreference;
   }

   /**
    * @param communicationPreference the communicationPreference to set
    */
   public void setCommunicationPreference(CommunicationPreference communicationPreference)
   {
      this.communicationPreference = communicationPreference;
   }

   /**
    * @return the memos
    */
   public List<Memo> getMemos()
   {
      if (CollectionUtils.isEmpty(this.memos))
      {
         this.memos = new ArrayList<Memo>();
      }
      return this.memos;
   }

   /**
    * @param memos the memos to set
    */
   public void setMemos(List<Memo> memos)
   {
      this.memos = memos;
   }

   /**
    * @return the itinerary
    */
   public Itinerary getItinerary()
   {
      return itinerary;
   }

   /**
    * @param itinerary the itinerary to set
    */
   public void setItinerary(Itinerary itinerary)
   {
      this.itinerary = itinerary;
   }

   /**
    * @return the discount
    */
   public Discount getDiscount()
   {
      return discount;
   }

   /**
    * @param discount the discount to set
    */
   public void setDiscount(Discount discount)
   {
      this.discount = discount;
   }

   /**
    * @return the inventory
    */
   public Inventory getInventory()
   {
      return inventory;
   }

   /**
    * @param inventory the inventory to set
    */
   public void setInventory(Inventory inventory)
   {
      this.inventory = inventory;
   }

   /**
    * @return the packageType
    */
   public PackageType getPackageType()
   {
      return packageType;
   }

   /**
    * @param packageType the packageType to set
    */
   public void setPackageType(PackageType packageType)
   {
      this.packageType = packageType;
   }

   /**
    * @param stay the stay to set
    */
   public void setStay(Stay stay)
   {
      this.stay = stay;
   }

   /**
    * @return the stay
    */
   public Stay getStay()
   {
      return stay;
   }

   /**
    * @return the highLevelBookingType
    */
   public HighLevelBookingType getHighLevelBookingType()
   {
      return highLevelBookingType;
   }

   /**
    * @param highLevelBookingType the highLevelBookingType to set
    */
   public void setHighLevelBookingType(HighLevelBookingType highLevelBookingType)
   {
      this.highLevelBookingType = highLevelBookingType;
   }

   /**
    * @return the extraFacilityCodes
    */
   public List<String> getTransferCodes()
   {
      return transferCodes;
   }

   /**
    * @param extraFacilityCodes the extraFacilityCodes to set
    */
   public void setTransferCodes(List<String> extraFacilityCodes)
   {
      this.transferCodes = extraFacilityCodes;
   }

   /**
    * @return the integratedExtraFacilities
    */
   public List<ExtraFacility> getIntegratedExtraFacilities()
   {
      if (CollectionUtils.isEmpty(integratedExtraFacilities))
      {
         this.integratedExtraFacilities = new ArrayList<ExtraFacility>();
      }
      return integratedExtraFacilities;
   }

   /**
    * @param integratedExtraFacilities the integratedExtraFacilities to set
    */
   public void setIntegratedExtraFacilities(List<ExtraFacility> integratedExtraFacilities)
   {
      this.integratedExtraFacilities = integratedExtraFacilities;
   }

   /**
    * @return the flightExtraFacilities
    */
   public List<ExtraFacility> getFlightExtraFacilities()
   {
      if (CollectionUtils.isEmpty(flightExtraFacilities))
      {
         this.flightExtraFacilities = new ArrayList<ExtraFacility>();
      }
      return flightExtraFacilities;
   }

   /**
    * @param flightExtraFacilities the flightExtraFacilities to set
    */
   public void setFlightExtraFacilities(List<ExtraFacility> flightExtraFacilities)
   {
      this.flightExtraFacilities = flightExtraFacilities;
   }

}
