/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.async.logging;

import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Async;

/**
 *
 */
public class TUIAsyncLogger implements TUILogger {

    private Logger logger;
    private String className;

    public TUIAsyncLogger(String className) {
        this.className = className;
    }

    public TUIAsyncLogger() {
    }

    public TUILogger initializeLogger(String className) {
        logger = Logger.getLogger(className);
        return this;
    }

    @Async
    @Override
    public void warn(Object message) {
        assertAndInitializeLogger();
        logger.warn(message);
    }

    @Async
    @Override
    public void warn(Object message, Throwable t) {
        assertAndInitializeLogger();
        logger.warn(message, t);
    }

    @Async
    @Override
    public void debug(Object message) {
        assertAndInitializeLogger();
        logger.debug(message);

    }

    @Async
    @Override
    public void debug(Object message, Throwable t) {
        assertAndInitializeLogger();
        logger.debug(message, t);
    }

    @Async
    @Override
    public void info(Object message) {
        assertAndInitializeLogger();
        logger.info(message);
    }

    @Async
    @Override
    public void info(Object message, Throwable t) {
        assertAndInitializeLogger();
        logger.info(message, t);
    }

    @Async
    @Override
    public void error(Object message) {
        assertAndInitializeLogger();
        logger.error(message);
    }

    @Async
    @Override
    public void error(Object message, Throwable t) {
        assertAndInitializeLogger();
        logger.error(message, t);
    }

    @Async
    @Override
    public void trace(Object message) {
        assertAndInitializeLogger();
        logger.trace(message);

    }

    @Async
    @Override
    public void trace(Object message, Throwable t) {
        assertAndInitializeLogger();
        logger.trace(message, t);
    }

    @Async
    @Override
    public void fatal(Object message) {
        assertAndInitializeLogger();
        logger.fatal(message);
    }

    @Async
    @Override
    public void fatal(Object message, Throwable t) {
        assertAndInitializeLogger();
        logger.fatal(message, t);
    }

   /**
     *
     */
    private void assertAndInitializeLogger() {
        if (logger == null) {
            initializeLogger(this.className);
        }
    }

}
