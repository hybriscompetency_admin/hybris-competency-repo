/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.async.logging;


import de.hybris.platform.core.Registry;
import de.hybris.platform.servicelayer.session.SessionService;
import org.apache.commons.lang.StringUtils;



/**
 *
 */
public class TUILogUtils {

    /**
     *
     */
    private static final String CLASS = ".class";
    private TUILogger tuiLogger;
    private SessionService sessionService;
    private boolean asyncLogEnabled;



    public TUILogUtils(final String name) {

        if (StringUtils.isNotBlank(name)) {
            StringBuilder className = new StringBuilder(name);
            className.append(CLASS);
            if (asyncLogEnabled) {
                tuiLogger = (TUILogger) Registry.getApplicationContext()
                        .getBean("tuiLogger");
                sessionService = (SessionService) Registry
                        .getApplicationContext().getBean("sessionService");
                tuiLogger = tuiLogger.initializeLogger(className.toString());

            } else {
                tuiLogger = new TUIAsyncLogger(className.toString());
            }
        }




    }
    public void warn(Object message)
    {
        tuiLogger.warn(formatMessage(message));
    }
    public void warn(Object message, Throwable t)
    {
        tuiLogger.warn(formatMessage(message), t);
    }

    public void debug(Object message)
    {
        if (debugIsEnabled())
        {
            tuiLogger.debug(formatMessage(message));
        }

    }

    public void debug(Object message, Throwable t)
    {
        if (debugIsEnabled())
        {
            tuiLogger.debug(formatMessage(message), t);
        }
    }

    public void info(Object message)
    {
        tuiLogger.info(formatMessage(message));
    }

    public void info(Object message, Throwable t)
    {
        tuiLogger.info(formatMessage(message), t);
    }

    public void error(Object message)
    {
        tuiLogger.error(formatMessage(message));
    }
    public void error(Object message, Throwable t)
    {
        tuiLogger.info(formatMessage(message), t);
    }

    public void trace(Object message)
    {
        tuiLogger.trace(formatMessage(message));
    }
    public void trace(Object message, Throwable t)
    {
        tuiLogger.trace(formatMessage(message), t);
    }

    public void fatal(Object message)
    {
        tuiLogger.fatal(formatMessage(message));
    }
    public void fatal(Object message, Throwable t)
    {
        tuiLogger.fatal(formatMessage(message), t);
    }


    private String formatMessage(final Object message) {
        final StringBuilder messageBuilder = new StringBuilder(" [");
        if (asyncLogEnabled) {
            // jsessionid for the web requests
            String sessionId = sessionService.getAttribute("httpSessionId");
            if (StringUtils.isBlank(sessionId)) {
                // Jalosession id for the non web requests
                sessionId = sessionService.getCurrentSession().getSessionId();
            }
            messageBuilder.append(sessionId);
        }
        messageBuilder.append("] ").append(message);
        return messageBuilder.toString();
    }

    private boolean debugIsEnabled()
    {
        return false;
    }



}
