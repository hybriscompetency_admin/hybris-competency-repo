/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.async.logging;

/**
 *
 */
public interface TUILogger {

    TUILogger initializeLogger(String  className);

    void warn(Object message);
    void warn(Object message, Throwable t);

    void debug(Object message);
    void debug(Object message, Throwable t);

    void info(Object message);
    void info(Object message, Throwable t);

    void error(Object message);
    void error(Object message, Throwable t);

    void trace(Object message);
    void trace(Object message, Throwable t);

    void fatal(Object message);
    void fatal(Object message, Throwable t);

 }
