/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.service.impl;

import uk.co.tui.service.UserService;
import de.hybris.platform.servicelayer.session.SessionService;

import javax.annotation.Resource;

/**
*
*/
public class DefaultUserService implements UserService
{
  @Resource
  private SessionService sessionService;

  @Override
  public String getUserId()
  {
     if (sessionService.hasCurrentSession() && null != sessionService.getAttribute("httpSessionId"))
     {
        return (String) sessionService.getAttribute("httpSessionId");
     }
     return "1";
  }
}