/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement you entered into with hybris.
 */
package uk.co.tui.book.services.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import uk.co.tui.book.domain.lite.BackToBackCruisePackage;
import uk.co.tui.book.domain.lite.BasePackage;
import uk.co.tui.book.domain.lite.CruiseAndStayPackage;
import uk.co.tui.book.domain.lite.Itinerary;
import uk.co.tui.book.domain.lite.PackageHoliday;
import uk.co.tui.book.domain.lite.PackageAdditionals;
import uk.co.tui.book.domain.lite.PackageType;
import uk.co.tui.book.domain.lite.Stay;
import uk.co.tui.book.domain.lite.StayAndCruisePackage;
import uk.co.tui.book.services.PackageComponentService;

/**
 * The Class PackageComponentServiceImpl.
 *
 * @author samantha.gd
 *
 */
@Service(value = "packageComponentService")
public class PackageComponentServiceImpl implements PackageComponentService
{

   /** The Constant INDEX_ZERO. */
   private static final int INDEX_ZERO = 0;

   /** The Constant INDEX_ONE. */
   private static final int INDEX_ONE = 1;

   /** The Constant ONE. */
   private static final int ONE = 1;

   /** The Constant TWO. */
   private static final int TWO = 2;

   /**
    * Gets the accommodation.
    *
    * @param basePackage the base package
    * @return the accommodation
    */
   @Override
   public Stay getHotel(BasePackage basePackage)
   {
      if (isInclusivePackage(basePackage))
      {
         return stay(basePackage);
      }
      if (isCruiseAndStayPackage(basePackage) || isStayAndCruisePackage(basePackage))
      {
         return hotel(basePackage);
      }
      return null;
   }

   /**
    * Gets the all accommodations.
    *
    * @param basePackage the base package
    * @return the all accommodations
    */
   @Override
   public List<Stay> getAllStays(BasePackage basePackage)
   {
      List<Stay> stays = new ArrayList<Stay>();
      if (isPackageHoliday(basePackage))
      {
         stays.add(stay(basePackage));
      }
      getStaysForMulticentre(basePackage, stays);
      return stays;
   }

   /**
    * Gets the stays for multicentre.
    *
    * @param basePackage the base package
    * @param stays the stays
    */
   private void getStaysForMulticentre(BasePackage basePackage, List<Stay> stays)
   {
      if (isCruiseAndStayPackage(basePackage))
      {
         stays.add(cruise(basePackage));
         stays.add(hotel(basePackage));
      }
      if (isStayAndCruisePackage(basePackage))
      {
         stays.add(hotel(basePackage));
         stays.add(cruise(basePackage));
      }
      if (isBackToBackCruisePackage(basePackage))
      {
         stays.addAll(getCruises(basePackage));
      }
   }

   /**
    * Checks if is package holiday.
    *
    * @param basePackage the base package
    * @return true, if is package holiday
    */
   private boolean isPackageHoliday(BasePackage basePackage)
   {
      return isInclusivePackage(basePackage) || isFlyCruisePackage(basePackage);
   }

   /**
    * Gets the all itineraries.
    *
    * @param basePackage the base package
    * @return the all itineraries
    */
   @Override
   public List<Itinerary> getAllItineraries(BasePackage basePackage)
   {
      List<Itinerary> itineraries = new ArrayList<Itinerary>();
      if (isPackageHoliday(basePackage))
      {
         itineraries.add(itinerary(basePackage));
      }
      getItinerariesForMultiCentre(basePackage, itineraries);
      return itineraries;
   }

   /**
    * Gets the itineraries for multi centre.
    *
    * @param basePackage the base package
    * @param itineraries the itineraries
    */
   private void getItinerariesForMultiCentre(BasePackage basePackage, List<Itinerary> itineraries)
   {
      if (isCruiseAndStayPackage(basePackage) || isStayAndCruisePackage(basePackage)
         || isBackToBackCruisePackage(basePackage))
      {
         itineraries.add(flightItinerary(basePackage));
      }
   }

   /**
    * Gets the flight itinerary.
    *
    * @param basePackage the base package
    * @return the flight itinerary
    */
   @Override
   public Itinerary getFlightItinerary(BasePackage basePackage)
   {

      Itinerary itinerary = null;
      if (isInclusivePackage(basePackage) || isFlyCruisePackage(basePackage)
         || isFlightOnlyPackage(basePackage))
      {
         itinerary = itinerary(basePackage);
      }
      return getFlightItineraryForMultiCentre(basePackage, itinerary);
   }

   /**
    * Gets the flight itinerary for cruise and stay.
    *
    * @param basePackage the base package
    * @param itinerary the itinerary
    * @return the flight itinerary for cruise and stay
    */
   private Itinerary getFlightItineraryForMultiCentre(BasePackage basePackage, Itinerary itinerary)
   {
      if (isCruiseAndStayPackage(basePackage) || isStayAndCruisePackage(basePackage)
         || isBackToBackCruisePackage(basePackage))
      {
         return flightItinerary(basePackage);
      }
      return itinerary;
   }

   /**
    * Gets the cruise.
    *
    * @param basePackage the base package
    * @return the cruise for CNS & SNC, for BTB it returns first cruise itinerary.
    */
   @Override
   public Stay getCruise(BasePackage basePackage)
   {
      if (isFlyCruisePackage(basePackage))
      {
         return stay(basePackage);
      }
      return getCruiseForMultiCentre(basePackage);
   }

   /**
    * Gets the Cruise for multicentre.
    *
    * @param basePackage the base package
    */
   private Stay getCruiseForMultiCentre(BasePackage basePackage)
   {
      if (isCruiseAndStayPackage(basePackage) || isStayAndCruisePackage(basePackage))
      {
         return cruise(basePackage);
      }
      return getStayForBackToBackCruise(basePackage);
   }

   /**
    * This method is to get the appropriate cruise based on the centre passed. For BackToBack cruise
    * if centre passed as 1 it returns first cruise itinerary, if centre passed as 2 it returns
    * second cruise itinerary.
    *
    * @param basePackage the base package
    * @param centre the centre
    * @return the cruise
    */
   @Override
   public Stay getCruise(BasePackage basePackage, int centre)
   {
      Stay stay = null;
      if (isBackToBackCruisePackage(basePackage))
      {
         stay = getBackToBackCruise(basePackage, centre);
      }
      return stay;
   }

   /**
    * Gets the cruise.
    *
    * @param basePackage the base package
    * @param centre the centre
    * @return the cruise
    */
   private Stay getBackToBackCruise(BasePackage basePackage, int centre)
   {
      List<Stay> stays = getCruises(basePackage);
      if (CollectionUtils.isNotEmpty(stays))
      {
         if (centre == ONE)
         {
            return stays.get(INDEX_ZERO);
         }
         else if (centre == TWO)
         {
            return stays.get(INDEX_ONE);
         }
      }
      return null;
   }

   /**
    * Gets the cruises.
    *
    * @param basePackage the base package
    * @return the cruises
    */
   @Override
   public List<Stay> getCruises(BasePackage basePackage)
   {
      if (basePackage instanceof BackToBackCruisePackage)
      {
         return ((BackToBackCruisePackage) basePackage).getCruises();
      }
      return Collections.emptyList();
   }

   /**
    * Gets the stay.
    *
    * @param basePackage the base package
    * @return the stay
    */
   @Override
   public Stay getStay(BasePackage basePackage)
   {
      if (isPackageHoliday(basePackage))
      {
         return stay(basePackage);
      }
      if (isCruiseAndStayPackage(basePackage) || isStayAndCruisePackage(basePackage))
      {
         return hotel(basePackage);
      }
      return getStayForBackToBackCruise(basePackage);
   }

   /**
    * Gets the first cruise itinerary for BackToBack cruise.
    *
    * @param basePackage the base package
    * @return the stay
    */
   private Stay getStayForBackToBackCruise(BasePackage basePackage)
   {
      if (isBackToBackCruisePackage(basePackage))
      {
         return getBackToBackCruise(basePackage, ONE);
      }
      return null;
   }

   /**
    * Checks if is inclusive package.
    *
    * @param basePackage the base package
    * @return true, if is inclusive package
    */
   private boolean isInclusivePackage(BasePackage basePackage)
   {
      return basePackage.getPackageType() == PackageType.INCLUSIVE;
   }

   /**
    * Checks if is fly cruise package.
    *
    * @param basePackage the base package
    * @return true, if is fly cruise package
    */
   private boolean isFlyCruisePackage(BasePackage basePackage)
   {
      return basePackage.getPackageType() == PackageType.FNC;
   }

   /**
    * Checks if is flight only package.
    *
    * @param basePackage the base package
    * @return true, if is flight only package
    */
   private boolean isFlightOnlyPackage(BasePackage basePackage)
   {
      return basePackage.getPackageType() == PackageType.FO;
   }

   /**
    * Checks if is cruise and stay package.
    *
    * @param basePackage the base package
    * @return true, if is cruise and stay package
    */
   private boolean isCruiseAndStayPackage(BasePackage basePackage)
   {
      return basePackage.getPackageType() == PackageType.CNS;
   }

   /**
    * Checks if is stay and cruise package.
    *
    * @param basePackage the base package
    * @return true, if is stay and cruise package
    */
   private boolean isStayAndCruisePackage(BasePackage basePackage)
   {
      return basePackage.getPackageType() == PackageType.SNC;
   }

   /**
    * Checks if is back to back cruise package.
    *
    * @param basePackage the base package
    * @return true, if is back to back cruise package
    */
   private boolean isBackToBackCruisePackage(BasePackage basePackage)
   {
      return basePackage.getPackageType() == PackageType.BTB;
   }

   /**
    * Returns stay if the instance is package holiday.
    *
    * @param basePackage the base package holiday.
    *
    * @return Stay if the base package is instance of package holiday otherwise null.
    */
   private Stay stay(BasePackage basePackage)
   {
      if (basePackage instanceof PackageHoliday)
      {
         return ((PackageHoliday) basePackage).getStay();
      }
      return null;
   }

   /**
    * Returns Hotel if the instance is CruiseAndStayPackage or StayAndCruisePackage.
    *
    * @param basePackage the base package holiday.
    *
    * @return Stay if the base package is instance of CruiseAndStayPackage otherwise null.
    */
   private Stay hotel(BasePackage basePackage)
   {
      if (basePackage instanceof CruiseAndStayPackage)
      {
         return ((CruiseAndStayPackage) basePackage).getHotel();
      }
      if (basePackage instanceof StayAndCruisePackage)
      {
         return ((StayAndCruisePackage) basePackage).getHotel();
      }
      return null;
   }

   /**
    * Returns Cruise if the instance is CruiseAndStayPackage or StayAndCruisePackage.
    *
    * @param basePackage the base package holiday.
    *
    * @return Stay if the base package is instance of CruiseAndStayPackage otherwise null.
    */
   private Stay cruise(BasePackage basePackage)
   {
      if (basePackage instanceof CruiseAndStayPackage)
      {
         return ((CruiseAndStayPackage) basePackage).getCruise();
      }
      if (basePackage instanceof StayAndCruisePackage)
      {
         return ((StayAndCruisePackage) basePackage).getCruise();
      }
      return null;
   }

   /**
    * Returns Itinerary if the instance is package holiday.
    *
    * @param basePackage the base package holiday.
    *
    * @return Itinerary if the base package is instance of package holiday otherwise null.
    */
   private Itinerary itinerary(BasePackage basePackage)
   {
      if (basePackage instanceof PackageHoliday)
      {
         return ((PackageHoliday) basePackage).getItinerary();
      }
      return null;
   }

   /**
    * Returns Itinerary if the instance is CruiseAndStayPackage or StayAndCruisePackage.
    *
    * @param basePackage the base package holiday.
    *
    * @return Itinerary if the base package is instance of CruiseAndStayPackage otherwise null.
    */
   private Itinerary flightItinerary(BasePackage basePackage)
   {
      if (basePackage instanceof CruiseAndStayPackage)
      {
         return ((CruiseAndStayPackage) basePackage).getItinerary();
      }
      if (basePackage instanceof StayAndCruisePackage)
      {
         return ((StayAndCruisePackage) basePackage).getItinerary();
      }
      if (basePackage instanceof BackToBackCruisePackage)
      {
         return ((BackToBackCruisePackage) basePackage).getItinerary();
      }
      return null;
   }

   /**
    * Gets the package additionals.
    *
    * @param basePackage the base package
    * @return the package additionals.
    */
   @Override
   public PackageAdditionals getPackageAdditionals(BasePackage basePackage)
   {
      return basePackage.getPackageAdditionals();
   }

   /**
    * Checks if is multi centre.
    *
    * @param basePackage the basePackage
    * @return true, if is multi centre.
    */
   @Override
   public boolean isMultiCentre(BasePackage basePackage)
   {
      return CollectionUtils.size(getAllStays(basePackage)) > ONE;
   }
}
