/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.services.impl;

import uk.co.tui.book.services.CurrencyResolver;

/**
 *
 */
public class DefaultCurrencyResolver implements CurrencyResolver {

    /**
     * Gets the cart currency.
     *
     * @return the cart currency
     */
    @Override
    public String getSiteCurrency() {
        return "GBP";
    }
}
