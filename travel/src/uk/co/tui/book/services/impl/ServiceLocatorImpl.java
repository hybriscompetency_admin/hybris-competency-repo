/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.services.impl;

import java.util.HashMap;
import java.util.Map;

import uk.co.tui.book.services.ServiceLocator;

/**
 * @author shaikMahaboob
 */
public class ServiceLocatorImpl<T> implements ServiceLocator<T>{

    /**
     * Holds the instance with identifier mapping
     */
    private Map<String, T> serviceMap = new HashMap<String, T>();

    /**
     * Constructor
     */
    ServiceLocatorImpl(Map<String, T> locator) {

        this.serviceMap = locator;
    }

    /**
     * Returns the object based on package type
     *
     * @param key
     *            the package type
     * @return object
     */
    public T locateByPackageType(String key) {

        return serviceMap.get(key);
    }

    /**
     * Returns the object based on inventory type
     *
     * @param key
     *            the inventory type
     * @return object
     */
    public T locateByInventory(String key) {

        return serviceMap.get(key);
    }

    /**
     * Returns the object based on attribute type .
     *
     * @param key
     *            the attribute
     * @return object
     */
    public T locate(String key) {

        return serviceMap.get(key);
    }

}
