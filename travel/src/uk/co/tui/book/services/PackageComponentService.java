/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.services;

import java.util.List;

import org.springframework.stereotype.Component;

import uk.co.tui.book.domain.lite.BasePackage;
import uk.co.tui.book.domain.lite.Itinerary;
import uk.co.tui.book.domain.lite.PackageAdditionals;
import uk.co.tui.book.domain.lite.Stay;

/**
 * The Interface PackageComponentService.
 *
 * @author samantha.gd
 */
@Component
public interface PackageComponentService {

    /**
     * Gets the accommodation.
     *
     * @param basePackage
     *            the base package
     * @return the accommodation
     */
    Stay getHotel(BasePackage basePackage);

    /**
     * Gets the cruise.
     *
     * @param basePackage
     *            the base package
     * @return the cruise
     */
    Stay getCruise(BasePackage basePackage);

    /**
     * Gets the cruise.
     *
     * @param basePackage
     *            the base package
     * @param centre
     *            the centre
     * @return the cruise
     */
    Stay getCruise(BasePackage basePackage, int centre);

    /**
     * Gets the cruises.
     *
     * @param basePackage
     *            the base package
     * @return the cruises
     */
    List<Stay> getCruises(BasePackage basePackage);

    /**
     *
     */
    Stay getStay(BasePackage basePackage);

    /**
     * Gets the all accommodations.
     *
     * @param basePackage
     *            the base package
     * @return the all accommodations
     */
    List<Stay> getAllStays(BasePackage basePackage);

    /**
     * Gets the all itineraries.
     *
     * @param basePackage
     *            the base package
     * @return the all itineraries
     */
    List<Itinerary> getAllItineraries(BasePackage basePackage);

    /**
     * Gets the flight itinerary.
     *
     * @param basePackage
     *            the base package
     * @return the flight itinerary
     */
    Itinerary getFlightItinerary(BasePackage basePackage);

    /**
     * Gets the package supplement.
     *
     * @param basePackage
     *            the base package
     * @return the package additional.
     */
    PackageAdditionals getPackageAdditionals(BasePackage basePackage);

    /**
     * Checks if is multi centre.
     *
     * @param basePackage
     *            the basePackage
     * @return true, if is multi centre.
     */
    boolean isMultiCentre(BasePackage basePackage);
}
