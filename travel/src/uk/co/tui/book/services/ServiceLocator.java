/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.services;


/**
 * @author shaikMahaboob
 */
public interface ServiceLocator<T> {

    /**
     * Returns the object based on package type
     *
     * @param key
     *            the package type
     * @return object
     */
     T locateByPackageType(String key);

    /**
     * Returns the object based on inventory type
     *
     * @param key
     *            the inventory type
     * @return object
     */
     T locateByInventory(String key);

    /**
     * Returns the object based on attribute type .
     *
     * @param key
     *            the attribute
     * @return object
     */
     T locate(String key);
}
