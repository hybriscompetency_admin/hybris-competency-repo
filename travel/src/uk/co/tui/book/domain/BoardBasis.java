package uk.co.tui.book.domain;

import java.math.BigDecimal;

/**
 * The Class BoardBasis.
 *
 * @author pushparaja.g
 *
 */
public class BoardBasis {

    /** The code. */
    private String code;

    /** The price. */
    private BigDecimal price;

    /** The is default. */
    private boolean isDefault;

    /** The selected. */
    private boolean selected;

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the code.
     *
     * @param code
     *            the new code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets the price.
     *
     * @return the price
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * Sets the price.
     *
     * @param price
     *            the new price
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * Checks if is default.
     *
     * @return true, if is default
     */
    public boolean isDefault() {
        return isDefault;
    }

    /**
     * Sets the default.
     *
     * @param isDefault
     *            the new default
     */
    public void setDefault(boolean isDefault) {
        this.isDefault = isDefault;
    }

    /**
     * Checks if is selected.
     *
     * @return true, if is selected
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * Sets the selected.
     *
     * @param selected
     *            the new selected
     */
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

}
