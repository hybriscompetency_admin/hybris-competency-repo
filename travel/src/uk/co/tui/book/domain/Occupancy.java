package uk.co.tui.book.domain;

import java.util.Map;

/**
 * The Class Occupancy.
 *
 * @author pushparaja.g
 */
public class Occupancy {

    /** The allocated adults. */
    private int allocatedAdults;

    /** The allocated child. */
    private int allocatedChild;

    /** The allocated infants. */
    private int allocatedInfants;

    /** The allocated pax. */
    private Map<Integer,Integer> allocatedPax;

    /** The priced adults. */
    private int pricedAdults;

    /** The priced childs. */
    private int pricedChilds;

    /** The priced infants. */
    private int pricedInfants;

    /**
     * Gets the allocated adults.
     *
     * @return the allocated adults
     */
    public int getAllocatedAdults() {
        return allocatedAdults;
    }

    /**
     * Sets the allocated adults.
     *
     * @param allocatedAdults
     *            the new allocated adults
     */
    public void setAllocatedAdults(int allocatedAdults) {
        this.allocatedAdults = allocatedAdults;
    }

    /**
     * Gets the allocated child.
     *
     * @return the allocated child
     */
    public int getAllocatedChild() {
        return allocatedChild;
    }

    /**
     * Sets the allocated child.
     *
     * @param allocatedChild
     *            the new allocated child
     */
    public void setAllocatedChild(int allocatedChild) {
        this.allocatedChild = allocatedChild;
    }

    /**
     * Gets the allocated infants.
     *
     * @return the allocated infants
     */
    public int getAllocatedInfants() {
        return allocatedInfants;
    }

    /**
     * Sets the allocated infants.
     *
     * @param allocatedInfants
     *            the new allocated infants
     */
    public void setAllocatedInfants(int allocatedInfants) {
        this.allocatedInfants = allocatedInfants;
    }

    /**
     * Gets the allocated pax.
     *
     * @return the allocated pax
     */
    public Map<Integer, Integer> getAllocatedPax() {
        return allocatedPax;
    }

    /**
     * Sets the allocated pax.
     *
     * @param allocatedPax
     *            the new allocated pax
     */
    public void setAllocatedPax(Map<Integer, Integer> allocatedPax) {
        this.allocatedPax = allocatedPax;
    }

    /**
     * Gets the priced adults.
     *
     * @return the priced adults
     */
    public int getPricedAdults() {
        return pricedAdults;
    }

    /**
     * Sets the priced adults.
     *
     * @param pricedAdults
     *            the new priced adults
     */
    public void setPricedAdults(int pricedAdults) {
        this.pricedAdults = pricedAdults;
    }

    /**
     * Gets the priced childs.
     *
     * @return the priced childs
     */
    public int getPricedChilds() {
        return pricedChilds;
    }

    /**
     * Sets the priced childs.
     *
     * @param pricedChilds
     *            the new priced childs
     */
    public void setPricedChilds(int pricedChilds) {
        this.pricedChilds = pricedChilds;
    }

    /**
     * Gets the priced infants.
     *
     * @return the priced infants
     */
    public int getPricedInfants() {
        return pricedInfants;
    }

    /**
     * Sets the priced infants.
     *
     * @param pricedInfants
     *            the new priced infants
     */
    public void setPricedInfants(int pricedInfants) {
        this.pricedInfants = pricedInfants;
    }

}
