/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import de.hybris.platform.core.enums.Gender;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;


/**
 *
 */
public class Passenger implements Serializable, Cloneable
{
    private static final long serialVersionUID = 4784502291959494352L;

    private Collection<ExtraFacility> extraFacilities;

    private Integer id;

    private PersonType type;

    private Integer age;

    private boolean isLead;

    private List<Address> addresses;

    private String title;

    private String name;

    private String cdmCustomerId;

    private String lastname;

    private String firstname;

    private String middlename;

    private Gender gender;

    private String dateOfBirth;


    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }


    /**
     * @return the cdmCustomerId
     */
    public String getCdmCustomerId() {
        return cdmCustomerId;
    }

    /**
     * @param cdmCustomerId the cdmCustomerId to set
     */
    public void setCdmCustomerId(String cdmCustomerId) {
        this.cdmCustomerId = cdmCustomerId;
    }

    public Collection<ExtraFacility> getExtraFacilities() {
        if(CollectionUtils.isEmpty(extraFacilities)){
            extraFacilities = new ArrayList<ExtraFacility>();
        }
        return extraFacilities;
    }

    public void setExtraFacilities(Collection<ExtraFacility> extraFacilities) {
        this.extraFacilities = extraFacilities;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the type
     */
    public PersonType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(PersonType type) {
        this.type = type;
    }

    /**
     * @return the age
     */
    public Integer getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(Integer age) {
        this.age = age;
    }

    /**
     * @return the isLead
     */
    public boolean isLead() {
        return isLead;
    }

    /**
     * @param isLead the isLead to set
     */
    public void setLead(boolean isLead) {
        this.isLead = isLead;
    }

    /**
     * @return the addresses
     */
    public List<Address> getAddresses() {
        return addresses;
    }

    /**
     * @param addresses the addresses to set
     */
    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }


    /**
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }


    /**
     * @param lastname the lastname to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }


    /**
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }


    /**
     * @param firstname the firstname to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }


    /**
     * @return the middlename
     */
    public String getMiddlename() {
        return middlename;
    }


    /**
     * @param middlename the middlename to set
     */
    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }


    /**
     * @return the gender
     */
    public Gender getGender() {
        return gender;
    }


    /**
     * @param gender the gender to set
     */
    public void setGender(Gender gender) {
        this.gender = gender;
    }


    /**
     * @return the dateOfBirth
     */
    public String getDateOfBirth() {
        return dateOfBirth;
    }


    /**
     * @param dateOfBirth the dateOfBirth to set
     */
    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }


    /**
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }



}
