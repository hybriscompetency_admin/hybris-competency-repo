/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;

/**
 *
 */
public class HighLevelBookingType implements Serializable, Cloneable{

    private static final long serialVersionUID = 6278574336640023147L;
    private boolean atolProtType;
    private boolean  welfareType;
    private boolean pfType;
    private boolean weddingType;
    private boolean groupType;
    private boolean staffType;

    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }
    /**
     * @return the atolProtType
     */
    public boolean isAtolProtType() {
        return atolProtType;
    }
    /**
     * @param atolProtType the atolProtType to set
     */
    public void setAtolProtType(boolean atolProtType) {
        this.atolProtType = atolProtType;
    }
    /**
     * @return the welfareType
     */
    public boolean isWelfareType() {
        return welfareType;
    }
    /**
     * @param welfareType the welfareType to set
     */
    public void setWelfareType(boolean welfareType) {
        this.welfareType = welfareType;
    }
    /**
     * @return the pfType
     */
    public boolean isPfType() {
        return pfType;
    }
    /**
     * @param pfType the pfType to set
     */
    public void setPfType(boolean pfType) {
        this.pfType = pfType;
    }
    /**
     * @return the weddingType
     */
    public boolean isWeddingType() {
        return weddingType;
    }
    /**
     * @param weddingType the weddingType to set
     */
    public void setWeddingType(boolean weddingType) {
        this.weddingType = weddingType;
    }
    /**
     * @return the groupType
     */
    public boolean isGroupType() {
        return groupType;
    }
    /**
     * @param groupType the groupType to set
     */
    public void setGroupType(boolean groupType) {
        this.groupType = groupType;
    }
    /**
     * @return the staffType
     */
    public boolean isStaffType() {
        return staffType;
    }
    /**
     * @param staffType the staffType to set
     */
    public void setStaffType(boolean staffType) {
        this.staffType = staffType;
    }


}
