/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

/**
 *
 */
public class ExtraFacilityCategory implements Serializable, Cloneable{

    private static final long serialVersionUID = 3655067103869764671L;

    private String code;

    private String inventoryCode;

    private String superCategoryCode;

    private String aliasSuperCategoryCode;

    private String description;

    private List<ExtraFacility> extraFacilities;

    private ExtraFacilityGroup extraFacilityGroup;

    private ExtrasRequestType extrasRequestType;

    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the inventoryCode
     */
    public String getInventoryCode() {
        return inventoryCode;
    }

    /**
     * @param inventoryCode the inventoryCode to set
     */
    public void setInventoryCode(String inventoryCode) {
        this.inventoryCode = inventoryCode;
    }

    /**
     * @return the superCategoryCode
     */
    public String getSuperCategoryCode() {
        return superCategoryCode;
    }

    /**
     * @param superCategoryCode the superCategoryCode to set
     */
    public void setSuperCategoryCode(String superCategoryCode) {
        this.superCategoryCode = superCategoryCode;
    }

    /**
     * @return the aliasSuperCategoryCode
     */
    public String getAliasSuperCategoryCode() {
        return aliasSuperCategoryCode;
    }

    /**
     * @param aliasSuperCategoryCode the aliasSuperCategoryCode to set
     */
    public void setAliasSuperCategoryCode(String aliasSuperCategoryCode) {
        this.aliasSuperCategoryCode = aliasSuperCategoryCode;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the extraFacilities
     */
    public List<ExtraFacility> getExtraFacilities() {
        if(CollectionUtils.isEmpty(this.extraFacilities)){
            this.extraFacilities = new ArrayList<ExtraFacility>();
        }
        return this.extraFacilities;
    }

    /**
     * @param extraFacilities the extraFacilities to set
     */
    public void setExtraFacilities(List<ExtraFacility> extraFacilities) {
        this.extraFacilities = extraFacilities;
    }

    /**
     * @return the extraFacilityGroup
     */
    public ExtraFacilityGroup getExtraFacilityGroup() {
        return extraFacilityGroup;
    }

    /**
     * @param extraFacilityGroup the extraFacilityGroup to set
     */
    public void setExtraFacilityGroup(ExtraFacilityGroup extraFacilityGroup) {
        this.extraFacilityGroup = extraFacilityGroup;
    }

    /**
     * @return the extrasRequestType
     */
    public ExtrasRequestType getExtrasRequestType() {
        return extrasRequestType;
    }

    /**
     * @param extrasRequestType the extrasRequestType to set
     */
    public void setExtrasRequestType(ExtrasRequestType extrasRequestType) {
        this.extrasRequestType = extrasRequestType;
    }



}
