/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

/**
 * The Class MultiCenterHolidayPackage.
 *
 * @author samantha.gd
 */
public class MultiCenterHolidayPackage extends BasePackage
        implements
            Serializable,
            Cloneable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -3797608088541586960L;

    /** The itineraries. */
    private List<Itinerary> itineraries;

    /** The stays. */
    private List<Stay> stays;

    /** The supplier product details. */
    private SupplierProductDetails supplierProductDetails;

    private String itinCode;


    /**
    * @return the itinCode
    */
   public String getItinCode()
   {
      return itinCode;
   }

   /**
    * @param itinCode the itinCode to set
    */
   public void setItinCode(String itinCode)
   {
      this.itinCode = itinCode;
   }

   /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }

    /**
     * Gets the itineraries.
     *
     * @return the itineraries
     */
    protected List<Itinerary> getItineraries() {
        if (CollectionUtils.isEmpty(this.itineraries)) {
            this.itineraries = new ArrayList<Itinerary>();
        }
        return this.itineraries;
    }

    /**
     * Gets the stays.
     *
     * @return the stays
     */
    protected List<Stay> getStays() {
        if (CollectionUtils.isEmpty(this.stays)) {
            this.stays = new ArrayList<Stay>();
        }
        return this.stays;
    }

    /**
     * @return the supplierProductDetails
     */
    public SupplierProductDetails getSupplierProductDetails() {
        return supplierProductDetails;
    }

    /**
     * @param supplierProductDetails the supplierProductDetails to set
     */
    public void setSupplierProductDetails(
            SupplierProductDetails supplierProductDetails) {
        this.supplierProductDetails = supplierProductDetails;
    }



}
