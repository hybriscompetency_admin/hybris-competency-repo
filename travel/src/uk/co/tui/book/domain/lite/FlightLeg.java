/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;
import java.util.Date;

/**
 *
 */
public class FlightLeg extends Leg implements Serializable, Cloneable {

    private static final long serialVersionUID = -1855297303808605625L;

    private String routeCode;

    private String atcomId;

    private String flightSeqCode;

    private Date cycDate;

    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }

    /**
     * @return the routeCode
     */
    public String getRouteCode() {
        return routeCode;
    }

    /**
     * @param routeCode
     *            the routeCode to set
     */
    public void setRouteCode(String routeCode) {
        this.routeCode = routeCode;
    }

    /**
     * @return the atcomId
     */
    public String getAtcomId() {
        return atcomId;
    }

    /**
     * @param atcomId
     *            the atcomId to set
     */
    public void setAtcomId(String atcomId) {
        this.atcomId = atcomId;
    }

    /**
     * @return the flightSeqCode
     */
    public String getFlightSeqCode() {
        return flightSeqCode;
    }

    /**
     * @param flightSeqCode
     *            the flightSeqCode to set
     */
    public void setFlightSeqCode(String flightSeqCode) {
        this.flightSeqCode = flightSeqCode;
    }

    /**
     * @return the cycDate
     */
    public Date getCycDate() {
        return new Date(this.cycDate.getTime());
    }

    /**
     * @param cycDate the cycDate to set
     */
    public void setCycDate(Date cycDate) {
         this.cycDate = new Date(cycDate.getTime());
    }

}
