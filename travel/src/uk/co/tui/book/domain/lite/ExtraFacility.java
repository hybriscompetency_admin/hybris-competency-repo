/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement you entered into with hybris.
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

/**
 *
 */
public class ExtraFacility implements Serializable, Cloneable
{

   private static final long serialVersionUID = -5469111047688007946L;

   private String inventoryId;

   private String inventoryCode;

   private String extraFacilityCode;

   private String description;

   private Integer quantity;

   private Integer stock;

   private ExtraFacilityGroup extraFacilityGroup;

   private ExtraFacilityCategory extraFacilityCategory;

   private ExtraFacilityRestrictions extraFacilityRestrictions;

   private ExtraFacilitySchedule extraFacilitySchedule;

   private List<Price> prices;

   private boolean selected;

   private FacilitySelectionType selection;

   private boolean bookable;

   private List<Passenger> passengers;

   private ExtraFacilityType type;

   private Inventory inventory;

   private String corporateCode;

   private String cabinClass;

   private String imageUrl;

   private boolean implied;

   private static final int PRIME = 31;

   /**
    * Clone.
    *
    * @return the object
    * @throws CloneNotSupportedException the clone not supported exception
    */
   @Override
   public Object clone() throws CloneNotSupportedException
   {
      return super.clone();

   }

   /**
    * @return the inventoryId
    */
   public String getInventoryId()
   {
      return inventoryId;
   }

   /**
    * @param inventoryId the inventoryId to set
    */
   public void setInventoryId(String inventoryId)
   {
      this.inventoryId = inventoryId;
   }

   /**
    * @return the inventoryCode
    */
   public String getInventoryCode()
   {
      return inventoryCode;
   }

   /**
    * @param inventoryCode the inventoryCode to set
    */
   public void setInventoryCode(String inventoryCode)
   {
      this.inventoryCode = inventoryCode;
   }

   /**
    * @return the extraFacilityCode
    */
   public String getExtraFacilityCode()
   {
      return extraFacilityCode;
   }

   /**
    * @param extraFacilityCode the extraFacilityCode to set
    */
   public void setExtraFacilityCode(String extraFacilityCode)
   {
      this.extraFacilityCode = extraFacilityCode;
   }

   /**
    * @return the description
    */
   public String getDescription()
   {
      return description;
   }

   /**
    * @param description the description to set
    */
   public void setDescription(String description)
   {
      this.description = description;
   }

   /**
    * @return the quantity
    */
   public Integer getQuantity()
   {
      return quantity;
   }

   /**
    * @param quantity the quantity to set
    */
   public void setQuantity(Integer quantity)
   {
      this.quantity = quantity;
   }

   /**
    * @return the extraFacilityGroup
    */
   public ExtraFacilityGroup getExtraFacilityGroup()
   {
      return extraFacilityGroup;
   }

   /**
    * @param extraFacilityGroup the extraFacilityGroup to set
    */
   public void setExtraFacilityGroup(ExtraFacilityGroup extraFacilityGroup)
   {
      this.extraFacilityGroup = extraFacilityGroup;
   }

   /**
    * @return the extraFacilityCategory
    */
   public ExtraFacilityCategory getExtraFacilityCategory()
   {
      return extraFacilityCategory;
   }

   /**
    * @param extraFacilityCategory the extraFacilityCategory to set
    */
   public void setExtraFacilityCategory(ExtraFacilityCategory extraFacilityCategory)
   {
      this.extraFacilityCategory = extraFacilityCategory;
   }

   /**
    * @return the extraFacilityRestrictions
    */
   public ExtraFacilityRestrictions getExtraFacilityRestrictions()
   {
      return extraFacilityRestrictions;
   }

   /**
    * @param extraFacilityRestrictions the extraFacilityRestrictions to set
    */
   public void setExtraFacilityRestrictions(ExtraFacilityRestrictions extraFacilityRestrictions)
   {
      this.extraFacilityRestrictions = extraFacilityRestrictions;
   }

   /**
    * @return the extraFacilitySchedule
    */
   public ExtraFacilitySchedule getExtraFacilitySchedule()
   {
      if (extraFacilitySchedule == null)
      {
         this.extraFacilitySchedule = new ExtraFacilitySchedule();
      }
      return this.extraFacilitySchedule;
   }

   /**
    * @param extraFacilitySchedule the extraFacilitySchedule to set
    */
   public void setExtraFacilitySchedule(ExtraFacilitySchedule extraFacilitySchedule)
   {
      this.extraFacilitySchedule = extraFacilitySchedule;
   }

   /**
    * @return the prices
    */
   public List<Price> getPrices()
   {
      if (CollectionUtils.isEmpty(prices))
      {
         this.prices = new ArrayList<Price>();
      }
      return this.prices;
   }

   /**
    * @param prices the prices to set
    */
   public void setPrices(List<Price> prices)
   {
      this.prices = prices;
   }

   /**
    * @return the selected
    */
   public boolean isSelected()
   {
      return selected;
   }

   /**
    * @param selected the selected to set
    */
   public void setSelected(boolean selected)
   {
      this.selected = selected;
   }

   /**
    * @return the selection
    */
   public FacilitySelectionType getSelection()
   {
      return selection;
   }

   /**
    * @param selection the selection to set
    */
   public void setSelection(FacilitySelectionType selection)
   {
      this.selection = selection;
   }

   /**
    * @return the bookable
    */
   public boolean isBookable()
   {
      return bookable;
   }

   /**
    * @param bookable the bookable to set
    */
   public void setBookable(boolean bookable)
   {
      this.bookable = bookable;
   }

   /**
    * @return the passengers
    */
   public List<Passenger> getPassengers()
   {
      if (CollectionUtils.isEmpty(this.passengers))
      {
         this.passengers = new ArrayList<Passenger>();
      }
      return this.passengers;
   }

   /**
    * @param passengers the passengers to set
    */
   public void setPassengers(List<Passenger> passengers)
   {
      this.passengers = passengers;
   }

   /**
    * @return the inventory
    */
   public Inventory getInventory()
   {
      return inventory;
   }

   /**
    * @param inventory the inventory to set
    */
   public void setInventory(Inventory inventory)
   {
      this.inventory = inventory;
   }

   public ExtraFacilityType getType()
   {
      return type;
   }

   @Override
   public int hashCode()
   {

      int result = 1;
      result = PRIME * result + ((extraFacilityCode == null) ? 0 : extraFacilityCode.hashCode());
      return result;
   }

   @Override
   public boolean equals(Object object)
   {
      boolean result = false;
      if (object == null || object.getClass() != getClass())
      {
         result = false;
      }
      else
      {
         ExtraFacility extraFacility = (ExtraFacility) object;
         if (this.inventoryCode.equals(extraFacility.getInventoryCode()))
         {
            result = true;
         }
      }
      return result;
   }

   /**
    * @param type the type to set
    */
   public void setType(ExtraFacilityType type)
   {
      this.type = type;
   }

   /**
    * @return the corporateCode
    */
   public String getCorporateCode()
   {
      return corporateCode;
   }

   /**
    * @param corporateCode the corporateCode to set
    */
   public void setCorporateCode(String corporateCode)
   {
      this.corporateCode = corporateCode;
   }

   /**
    * @return the cabinClass
    */
   public String getCabinClass()
   {
      return cabinClass;
   }

   /**
    * @param cabinClass the cabinClass to set
    */
   public void setCabinClass(String cabinClass)
   {
      this.cabinClass = cabinClass;
   }

   /**
    * @return the implied
    */
   public boolean isImplied()
   {
      return implied;
   }

   /**
    * @param implied the implied to set
    */
   public void setImplied(boolean implied)
   {
      this.implied = implied;
   }

/**
 * @return the imageUrl
 */
public String getImageUrl() {
    return imageUrl;
}

/**
 * @param imageUrl the imageUrl to set
 */
public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
}

/**
 * @return the stock
 */
public Integer getStock()
{
   return stock;
}

/**
 * @param stock the stock to set
 */
public void setStock(Integer stock)
{
   this.stock = stock;
}

}
