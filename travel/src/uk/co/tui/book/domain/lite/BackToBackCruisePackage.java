/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.util.List;

/**
 * The Class CruiseAndStayPackage.
 *
 * @author samantha.gd
 *
 */
public class BackToBackCruisePackage extends MultiCenterHolidayPackage {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -8122175725948400003L;

    /**
     * Gets the cruises.
     *
     * @return the cruises
     */
    public List<Stay> getCruises() {
        return this.getStays();
    }

    /**
     * Sets the cruises.
     *
     * @param cruises
     *            the new cruises
     */
    public void setCruises(List<Stay> cruises) {
        this.getStays().addAll(cruises);
    }
    /**
     * Gets the flight itinerary.
     *
     * @return the flight itinerary
     */
    public Itinerary getItinerary() {
        return this.getItineraries().get(0);
    }

    /**
     * Sets the itinerary.
     *
     * @param itinerary
     *            the new itinerary
     */
    public void setItinerary(Itinerary itinerary) {
        this.getItineraries().add(0, itinerary);
    }

}
