/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;

/**
 * The Class Carrier.
 */
public class Carrier implements Serializable, Cloneable {

    /** The Constant serialVersionUID. */
    private static final long  serialVersionUID = 4345883210129395917L;

    /** The number. */
    private String             number;

    /** The code. */
    private String             code;

    /** The series. */
    private String             series;

    /** The carrier information. */
    private CarrierInformation carrierInformation;

    /** The equipement type. */
    private EquipmentType      equipementType;

    /** The equipement description. */
    private String             equipementDescription;

    /**
     * Gets the number.
     *
     * @return the filghtNumber
     */
    public String getNumber() {
        return number;
    }

    /**
     * Sets the number.
     *
     * @param number
     *            the filghtNumber to set
     */
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * Gets the code.
     *
     * @return the Code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the code.
     *
     * @param code the new code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets the series.
     *
     * @return the Series
     */
    public String getSeries() {
        return series;
    }

    /**
     * Sets the series.
     *
     * @param series the new series
     */
    public void setSeries(String series) {
        this.series = series;
    }

    /**
     * Gets the carrier information.
     *
     * @return the carrierInformation
     */
    public CarrierInformation getCarrierInformation() {
        return carrierInformation;
    }

    /**
     * Sets the carrier information.
     *
     * @param carrierInformation
     *            the carrierInformation to set
     */
    public void setCarrierInformation(CarrierInformation carrierInformation) {
        this.carrierInformation = carrierInformation;
    }

    /**
     * Gets the equipement type.
     *
     * @return the equipementType
     */
    public EquipmentType getEquipementType() {
        return equipementType;
    }

    /**
     * Sets the equipement type.
     *
     * @param equipementType
     *            the equipementType to set
     */
    public void setEquipementType(EquipmentType equipementType) {
        this.equipementType = equipementType;
    }

    /**
     * Gets the equipement description.
     *
     * @return the equipementDescription
     */
    public String getEquipementDescription() {
        return equipementDescription;
    }

    /**
     * Sets the equipement description.
     *
     * @param equipementDescription
     *            the equipementDescription to set
     */
    public void setEquipementDescription(String equipementDescription) {
        this.equipementDescription = equipementDescription;
    }

    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }

}
