/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;


/**
 *@author veena.pn
 */
public class BasePackage implements Serializable, Cloneable{

    private static final long serialVersionUID = -7704529598012698276L;

    private PackageType packageType;

    private Price price;

    private List<Price> prices;

    private List<ExtraFacilityCategory> extraFacilityCategories;

    private Integer duration;

    private String id;

    private String productType;

    private boolean saved;

    private Discount discount;

    private List<HighLights> listOfHighlights;

    private List<Deposit> deposits;

    private List<Memo> memos;

    private List<Passenger> passengers;

    private String bookingRefNum;

    private String agtNum;

    private Brand brandType;

    private CrossSellingBrand crossBrandType;

    private Inventory inventory;

    private BookingDetails bookingDetails;

    private PromotionalDiscount promotionalDiscount;

    private Status status = Status.INPROGRESS;

    private List<BookingStatusError> bookingStatusErrors;

    private String siteBrand;

    /**
     * Gets the CommunicationPreferences
     */
    private CommunicationPreference communicationPreference;

    private HighLevelBookingType highLevelBookingType;

    private Deque<String> stackStatus = new LinkedList<String>();

    private PackageAdditionals packageAdditionals;


    private String wishListEntryId;

    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }

    /**
     * @return the status
     */
    public Status getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Status status) {
        this.status = status;
    }

    /**
     * @return the highLevelBookingType
     */
    public HighLevelBookingType getHighLevelBookingType() {
        return highLevelBookingType;
    }

    /**
     * @param highLevelBookingType the highLevelBookingType to set
     */
    public void setHighLevelBookingType(HighLevelBookingType highLevelBookingType) {
        this.highLevelBookingType = highLevelBookingType;
    }



    /**
     * @return the agtNum
     */
    public String getAgtNum() {
        return agtNum;
    }

    /**
     * @param agtNum the agtNum to set
     */
    public void setAgtNum(String agtNum) {
        this.agtNum = agtNum;
    }

    /**
     * @return the packageTypeEnum
     */
    public PackageType getPackageType() {
        return packageType;
    }

    /**
     * @param packageTypeEnum the packageTypeEnum to set
     */
    public void setPackageType(PackageType packageTypeEnum) {
        this.packageType = packageTypeEnum;
    }

    /**
     * @return the price
     */
    public Price getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(Price price) {
        this.price = price;
    }

    /**
     * @return the extraFacilityCategories
     */
    public List<ExtraFacilityCategory> getExtraFacilityCategories() {
        if(CollectionUtils.isEmpty(this.extraFacilityCategories)){
            this.extraFacilityCategories = new ArrayList<ExtraFacilityCategory>();
        }
        return this.extraFacilityCategories;
    }

    /**
     * @param extraFacilityCategories the extraFacilityCategories to set
     */
    public void setExtraFacilityCategories(
            List<ExtraFacilityCategory> extraFacilityCategories) {
        this.extraFacilityCategories = extraFacilityCategories;
    }

    /**
     * @return the duration
     */
    public Integer getDuration() {
        return duration;
    }

    /**
     * @param duration the duration to set
     */
    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the productType
     */
    public String getProductType() {
        return productType;
    }

    /**
     * @param productType the productType to set
     */
    public void setProductType(String productType) {
        this.productType = productType;
    }



    /**
     * @return the brandType
     */
    public Brand getBrandType() {
        return brandType;
    }

    /**
     * @param brandType the brandType to set
     */
    public void setBrandType(Brand brandType) {
        this.brandType = brandType;
    }

    /**
     * @return the saved
     */
    public boolean isSaved() {
        return saved;
    }

    /**
     * @param saved the saved to set
     */
    public void setSaved(boolean saved) {
        this.saved = saved;
    }

    public Discount getDiscount() {
        return discount;
    }

    public void setDiscount(Discount discount) {
        this.discount = discount;
    }

    public List<HighLights> getListOfHighlights() {
        if(CollectionUtils.isEmpty(this.listOfHighlights)){
            this.listOfHighlights = new ArrayList<HighLights>();
        }
        return this.listOfHighlights;

    }

    public void setListOfHighlights(List<HighLights> listOfHighlights) {
        this.listOfHighlights = listOfHighlights;
    }

    public List<Deposit> getDeposits() {
        if(CollectionUtils.isEmpty(this.deposits)){
            this.deposits = new ArrayList<Deposit>();
        }
        return this.deposits;
    }

    public void setDeposits(List<Deposit> deposits) {
        this.deposits = deposits;
    }

    /**
     * @return the memos
     */
    public List<Memo> getMemos() {
        if(CollectionUtils.isEmpty(this.memos)){
            this.memos = new ArrayList<Memo>();
        }
        return this.memos;

    }

    /**
     * @param memos the memos to set
     */
    public void setMemos(List<Memo> memos) {
        this.memos = memos;
    }

    /**
     * @param passengers the passengers to set
     */
    public void setPassengers(List<Passenger> passengers) {
        this.passengers = passengers;
    }

    /**
     * @return the passengers
     */
    public List<Passenger> getPassengers() {
        if(CollectionUtils.isEmpty(this.passengers)){
            this.passengers = new ArrayList<Passenger>();
        }
        return this.passengers;

    }

    /**
     * @return the bookingRefNum
     */
    public String getBookingRefNum() {
        return bookingRefNum;
    }

    /**
     * @param bookingRefNum the bookingRefNum to set
     */
    public void setBookingRefNum(String bookingRefNum) {
        this.bookingRefNum = bookingRefNum;
    }

    /**
     * @return the communicationPreference
     */
    public CommunicationPreference getCommunicationPreference() {
        return communicationPreference;
    }

    /**
     * @param communicationPreference the communicationPreference to set
     */
    public void setCommunicationPreference(CommunicationPreference communicationPreference) {
        this.communicationPreference = communicationPreference;
    }

    /**
     * @return the inventory
     */
    public Inventory getInventory() {
        return inventory;
    }

    /**
     * @param inventory the inventory to set
     */
    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    /**
     * @return the bookingDetails
     */
    public BookingDetails getBookingDetails() {
        return bookingDetails;
    }

    /**
     * @param bookingDetails the bookingDetails to set
     */
    public void setBookingDetails(BookingDetails bookingDetails) {
        this.bookingDetails = bookingDetails;
    }

    /**
     * @return the promotionalDiscount
     */
    public PromotionalDiscount getPromotionalDiscount() {
        return promotionalDiscount;
    }

    /**
     * @param promotionalDiscount the promotionalDiscount to set
     */
    public void setPromotionalDiscount(PromotionalDiscount promotionalDiscount) {
        this.promotionalDiscount = promotionalDiscount;
    }

    /**
     * @return the bookingStatusErrors
     */
    public List<BookingStatusError> getBookingStatusErrors() {
        return bookingStatusErrors;
    }

    /**
     * @param bookingStatusErrors the bookingStatusErrors to set
     */
    public void setBookingStatusErrors(List<BookingStatusError> bookingStatusErrors) {
        this.bookingStatusErrors = bookingStatusErrors;
    }

    /**
     * @return the siteBrand
     */
    public String getSiteBrand() {
        return siteBrand;
    }

    /**
     * @param siteBrand
     *            the siteBrand to set
     */
    public void setSiteBrand(String siteBrand) {
        this.siteBrand = siteBrand;
    }


    /**
     * @return the wishListEntryId
     */
    public String getWishListEntryId() {
        return wishListEntryId;
    }

    /**
     * @param wishListEntryId the wishListEntryId to set
     */
    public void setWishListEntryId(String wishListEntryId) {
        this.wishListEntryId = wishListEntryId;
    }

    /**
        * @return the crossBrandType
        */
       public CrossSellingBrand getCrossBrandType() {
           return crossBrandType;
       }

       /**
        * @param crossBrandType the crossBrandType to set
        */
       public void setCrossBrandType(CrossSellingBrand crossBrandType) {
           this.crossBrandType = crossBrandType;
       }

    /**
     * @return the prices
     */
    public List<Price> getPrices() {
        if(CollectionUtils.isEmpty(this.prices)){
            this.prices = new ArrayList<Price>();
        }
        return prices;
    }

    /**
     * @param prices the prices to set
     */
    public void setPrices(List<Price> prices) {
        this.prices = prices;
    }

    /**
     * @return the stackStatus
     */
    public Deque<String> getStackStatus() {
        return stackStatus;
    }

    /**
     * @return the packageAdditionals
     */
    public PackageAdditionals getPackageAdditionals() {
        return packageAdditionals;
    }

    /**
     * @param packageAdditionals the packageAdditionals to set
     */
    public void setPackageAdditionals(PackageAdditionals packageAdditionals) {
        this.packageAdditionals = packageAdditionals;
    }

}
