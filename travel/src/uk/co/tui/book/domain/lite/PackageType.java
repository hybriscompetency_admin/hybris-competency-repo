/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import org.apache.commons.lang.StringUtils;


/**
 *
 */
public enum PackageType {

    INCLUSIVE("INCLUSIVE", "INCLUSIVE","Inclusive"),

    COMPONENT("COMPONENT", "COMPONENT","Component"),

    AO("AO", "AO","Accommodation Only"),

    FO("FO", "FO","Flights Only"),

    CO("CO", "CO","CruiseOnly"),

    FNC("FLY_CRUISE", "FNC","FlyCruise"),

    CNS("CRUISE_STAY", "CNS","CruiseStay"),

    SNC("STAY_CRUISE", "SNC","StayCruise"),

    BTB("BACK_BACK", "BTB","BackToBack");

    /** The code of this enum. */
    private final String code;
    /** The id of this enum. */
    private final String id;
    /** The is for enum description. */
    private final String description;



    /**
     * Creates a new enum value for this enum type.
     *
     * @param code
     *            the enum value code
     * @param id
     *            the enum value id
     */
    private PackageType(String code, String id,String description) {
        this.code = code.intern();
        this.id = id.intern();
        this.description = description.intern();
    }

    /**
     * Gets the code of this enum value.
     *
     * @return code of value
     */
    public String getCode() {
        return this.code;
    }

    /**
     * Gets the code of this enum value.
     *
     * @return code of value
     */
    public String getId() {
        return this.id;
    }

    /**
     * Gets the description for the enum value.
     * @return the description
     */
    public String getDescription() {
        return this.description;
    }


    /**
     * Returns a PackageType instance representing the specified enum value.
     *
     * @param code
     *            an enum value
     * @return PackageType.
     */
    public static PackageType valueOfPackageType(final String code) {
        for (PackageType packageType : PackageType.values()) {
            if (StringUtils.equalsIgnoreCase(code, packageType.getCode())) {
                return packageType;
            }
        }
        return null;
    }

    /**
     * Returns a PackageType instance representing the specified enum value.
     *
     * @param code
     *            an enum value
     * @return PackageType.
     */
    public static PackageType mapType(final String code) {
        for (PackageType packageType : PackageType.values()) {
            if (StringUtils.contains(code, packageType.getCode())) {
                return packageType;
            }
        }
        return null;
    }

}
