/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

/**
 * The Class Cruise.
 */
public class Cruise extends Stay implements Cloneable, Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The journey code. */
    private String journeyCode = StringUtils.EMPTY;

    /** The journey name. */
    private String journeyName = StringUtils.EMPTY;

    /**
     * Clone.
     * 
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }

    /**
     * Gets the journey code.
     * 
     * @return the journey code
     */
    public String getJourneyCode() {
        return journeyCode;
    }

    /**
     * Sets the journey code.
     * 
     * @param journeyCode
     *            the new journey code
     */
    public void setJourneyCode(String journeyCode) {
        this.journeyCode = journeyCode;
    }

    /**
     * Gets the journey name.
     * 
     * @return the journeyName
     */
    public String getJourneyName() {
        return journeyName;
    }

    /**
     * Sets the journey name.
     * 
     * @param journeyName
     *            the journeyName to set
     */
    public void setJourneyName(String journeyName) {
        this.journeyName = journeyName;
    }

}
