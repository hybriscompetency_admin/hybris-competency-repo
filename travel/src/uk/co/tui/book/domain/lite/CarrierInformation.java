/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;

/**
 *
 */
public class CarrierInformation implements Serializable, Cloneable{

    private static final long serialVersionUID = 7283725622644941587L;

    private String marketingAirlineCode;

    private String operatingAirlineCode;

    private String marketingAirlineName;

    private String operatingAirlineName;

    private String tourOperationFlightId;

    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }

    /**
     * @return the marketingAirlineCode
     */
    public String getMarketingAirlineCode() {
        return marketingAirlineCode;
    }

    /**
     * @param marketingAirlineCode the marketingAirlineCode to set
     */
    public void setMarketingAirlineCode(String marketingAirlineCode) {
        this.marketingAirlineCode = marketingAirlineCode;
    }

    /**
     * @return the operatingAirlineCode
     */
    public String getOperatingAirlineCode() {
        return operatingAirlineCode;
    }

    /**
     * @param operatingAirlineCode the operatingAirlineCode to set
     */
    public void setOperatingAirlineCode(String operatingAirlineCode) {
        this.operatingAirlineCode = operatingAirlineCode;
    }

    /**
     * @return the marketingAirlineName
     */
    public String getMarketingAirlineName() {
        return marketingAirlineName;
    }

    /**
     * @param marketingAirlineName the marketingAirlineName to set
     */
    public void setMarketingAirlineName(String marketingAirlineName) {
        this.marketingAirlineName = marketingAirlineName;
    }

    /**
     * @return the operatingAirlineName
     */
    public String getOperatingAirlineName() {
        return operatingAirlineName;
    }

    /**
     * @param operatingAirlineName the operatingAirlineName to set
     */
    public void setOperatingAirlineName(String operatingAirlineName) {
        this.operatingAirlineName = operatingAirlineName;
    }


    /**
     * @return the tourOperationFlightId
     */
    public String getTourOperationFlightId() {
        return tourOperationFlightId;
    }

    /**
     * @param tourOperationFlightId the tourOperationFlightId to set
     */
    public void setTourOperationFlightId(String tourOperationFlightId) {
        this.tourOperationFlightId = tourOperationFlightId;
    }

}
