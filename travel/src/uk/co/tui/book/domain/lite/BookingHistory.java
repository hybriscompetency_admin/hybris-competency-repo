/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

/**
 *@author veena.pn
 */
public class BookingHistory implements Serializable, Cloneable{

    private static final long serialVersionUID = -6189246850801472946L;

    private Agent agent;

    private SalesChannel salesChannel;

    private BookingType bookingType;

    private String bookingReference;

    private String sessionId;

    private List<PaymentReference> paymentReferences;


    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }
    /**
     * @return the agent
     */
    public Agent getAgent() {
        return agent;
    }
    /**
     * @param agent the agent to set
     */
    public void setAgent(Agent agent) {
        this.agent = agent;
    }



    /**
     * @return the bookingType
     */
    public BookingType getBookingType() {
        return bookingType;
    }
    /**
     * @param bookingType the bookingType to set
     */
    public void setBookingType(BookingType bookingType) {
        this.bookingType = bookingType;
    }

    /**
     * @return the salesChannel
     */
    public SalesChannel getSalesChannel() {
        return salesChannel;
    }
    /**
     * @param salesChannel the salesChannel to set
     */
    public void setSalesChannel(SalesChannel salesChannel) {
        this.salesChannel = salesChannel;
    }

    /**
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }
    /**
     * @param sessionId the sessionId to set
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * @return the bookingReference
     */
    public String getBookingReference() {
        return bookingReference;
    }
    /**
     * @param bookingReference the bookingReference to set
     */
    public void setBookingReference(String bookingReference) {
        this.bookingReference = bookingReference;
    }

    /**
     * @return the paymentReferences
     */
    public List<PaymentReference> getPaymentReferences() {
        if(CollectionUtils.isEmpty(this.paymentReferences)){
            this.paymentReferences = new ArrayList<PaymentReference>();
        }
        return this.paymentReferences;
    }

    /**
     * @param paymentReferences the paymentReferences to set
     */
    public void setPaymentReferences(List<PaymentReference> paymentReferences) {
        this.paymentReferences = paymentReferences;
    }

}
