/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;
import java.util.Date;

/**
 *
 */
public class FlightSchedule extends Schedule implements Serializable, Cloneable{

    private static final long serialVersionUID = -3215777587129136866L;
    private Date checkInTime;

    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }

    /**
     * @return the checkInTime
     */
    public Date getCheckInTime() {
        if (checkInTime != null) {
            return (Date) checkInTime.clone();
        }
        return null;
    }

    /**
     * @param checkInTime the checkInTime to set
     */
    public void setCheckInTime(Date checkInTime) {
        if (checkInTime != null) {
            this.checkInTime = (Date) checkInTime.clone();
        }
    }

}
