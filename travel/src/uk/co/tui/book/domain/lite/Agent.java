/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;

/**
 *@author veena.pn
 */
public class Agent implements Serializable, Cloneable{

    private static final long serialVersionUID = -8466289012032542956L;

    private AgentType agentType;

    private String agentID;

    private String agentCode;

    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }

    /**
     * @return the agentType
     */
    public AgentType getAgentType() {
        return agentType;
    }
    /**
     * @param agentType the agentType to set
     */
    public void setAgentType(AgentType agentType) {
        this.agentType = agentType;
    }
    /**
     * @return the agentID
     */
    public String getAgentID() {
        return agentID;
    }
    /**
     * @param agentID the agentID to set
     */
    public void setAgentID(String agentID) {
        this.agentID = agentID;
    }
    /**
     * @return the agentCode
     */
    public String getAgentCode() {
        return agentCode;
    }
    /**
     * @param agentCode the agentCode to set
     */
    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }


}
