/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;

/**
 *
 */
public class InsuranceExtraFacility extends ExtraFacility
        implements
            Serializable,
            Cloneable {

    private static final long serialVersionUID = -2413219652511933244L;

    private InsuranceExcessWaiver insuranceExcessWaiver;

    private PaxCountBasedRestrictions familyInsuranceRestrictions;

    private InsuranceType insuranceType;

   /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }

    /**
     * @return the insuranceExcessWaiver
     */
    public InsuranceExcessWaiver getInsuranceExcessWaiver() {
        return insuranceExcessWaiver;
    }

    /**
     * @param insuranceExcessWaiver
     *            the insuranceExcessWaiver to set
     */
    public void setInsuranceExcessWaiver(
            InsuranceExcessWaiver insuranceExcessWaiver) {
        this.insuranceExcessWaiver = insuranceExcessWaiver;
    }

    /**
     * @return the familyInsuranceRestrictions
     */
    public PaxCountBasedRestrictions getFamilyInsuranceRestrictions() {
        return familyInsuranceRestrictions;
    }

    /**
     * @param familyInsuranceRestrictions
     *            the familyInsuranceRestrictions to set
     */
    public void setFamilyInsuranceRestrictions(
            PaxCountBasedRestrictions familyInsuranceRestrictions) {
        this.familyInsuranceRestrictions = familyInsuranceRestrictions;
    }

    public InsuranceType getInsuranceType()
    {
       return insuranceType;
    }

    public void setInsuranceType(final InsuranceType insuranceType)
    {
       this.insuranceType = insuranceType;
    }
}
