/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;

/**
 *@author veena.pn
 */
public class LowDeposit extends Deposit  implements Serializable, Cloneable {


    private static final long serialVersionUID = -5114692136159587603L;

    private Price familyPrice;

    private Price personPrice;

    private Price calculatedTotalAmount;

    private Price calculatedPerPersonAmount;

    private Boolean perPersonDeposit;

    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }



    /**
     * @return the calculatedPerPersonAmount
     */
    public Price getCalculatedPerPersonAmount() {
        return calculatedPerPersonAmount;
    }

    /**
     * @param calculatedPerPersonAmount the calculatedPerPersonAmount to set
     */
    public void setCalculatedPerPersonAmount(Price calculatedPerPersonAmount) {
        this.calculatedPerPersonAmount = calculatedPerPersonAmount;
    }

    /**
     * @return the perPersonDeposit
     */
    public Boolean getPerPersonDeposit() {
        return perPersonDeposit;
    }

    /**
     * @param perPersonDeposit the perPersonDeposit to set
     */
    public void setPerPersonDeposit(Boolean perPersonDeposit) {
        this.perPersonDeposit = perPersonDeposit;
    }

    /**
     * @return the familyPrice
     */
    public Price getFamilyPrice() {
        return familyPrice;
    }

    /**
     * @param familyPrice the familyPrice to set
     */
    public void setFamilyPrice(Price familyPrice) {
        this.familyPrice = familyPrice;
    }

    /**
     * @return the personPrice
     */
    public Price getPersonPrice() {
        return personPrice;
    }

    /**
     * @param personPrice the personPrice to set
     */
    public void setPersonPrice(Price personPrice) {
        this.personPrice = personPrice;
    }

    /**
     * @return the calculatedTotalAmount
     */
    public Price getCalculatedTotalAmount() {
        return calculatedTotalAmount;
    }

    /**
     * @param calculatedTotalAmount the calculatedTotalAmount to set
     */
    public void setCalculatedTotalAmount(Price calculatedTotalAmount) {
        this.calculatedTotalAmount = calculatedTotalAmount;
    }



}
