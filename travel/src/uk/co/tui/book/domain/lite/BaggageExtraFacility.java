/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;

/**
 *
 */
public class BaggageExtraFacility extends ExtraFacility implements Serializable, Cloneable{

    private static final long serialVersionUID = -214770049158217956L;

    private int baggagePieces;

    private int baggageWeight;

    private Price price;

    private BaggageExtraFacilityRestrictions baggageExtraFacilityRestrictions;

    /** The default baggage weight. */
    private int defaultBaggageWeight;

    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }

    public int getBaggagePieces() {
        return baggagePieces;
    }

    public void setBaggagePieces(int baggagePieces) {
        this.baggagePieces = baggagePieces;
    }

    public int getBaggageWeight() {
        return baggageWeight;
    }

    public void setBaggageWeight(int baggageWeight) {
        this.baggageWeight = baggageWeight;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public BaggageExtraFacilityRestrictions getBaggageExtraFacilityRestrictions() {
        return baggageExtraFacilityRestrictions;
    }

    public void setBaggageExtraFacilityRestrictions(BaggageExtraFacilityRestrictions baggageExtraFacilityRestrictions) {
        this.baggageExtraFacilityRestrictions = baggageExtraFacilityRestrictions;
    }

    /**
     * @return the defaultBaggageWeight
     */
    public int getDefaultBaggageWeight() {
        return defaultBaggageWeight;
    }

    /**
     * @param defaultBaggageWeight
     *            the defaultBaggageWeight to set
     */
    public void setDefaultBaggageWeight(int defaultBaggageWeight) {
        this.defaultBaggageWeight = defaultBaggageWeight;
    }

}
