/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;

/**
 *
 */
public class ExtraFacilityRestrictions implements Serializable, Cloneable{

    private static final long serialVersionUID = -1206509068294559276L;

    private Integer maxAge;

    private Integer minAge;

    private Integer maxPermittedDays;

    private Integer minPaxAllowed;

    private Integer maxPaxAllowed;

    private Integer allowedNoOfCarOrTaxies;

    private Integer maxSelectableQuantity;

    private Integer minChildAge;

    private Integer maxChildAge;

    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }

    public Integer getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(Integer maxAge) {
        this.maxAge = maxAge;
    }

    public Integer getMinAge() {
        return minAge;
    }

    public void setMinAge(Integer minAge) {
        this.minAge = minAge;
    }

    public Integer getMaxPermittedDays() {
        return maxPermittedDays;
    }

    public void setMaxPermittedDays(Integer maxPermittedDays) {
        this.maxPermittedDays = maxPermittedDays;
    }

    public Integer getMinPaxAllowed() {
        return minPaxAllowed;
    }

    public void setMinPaxAllowed(Integer minPaxAllowed) {
        this.minPaxAllowed = minPaxAllowed;
    }

    public Integer getMaxPaxAllowed() {
        return maxPaxAllowed;
    }

    public void setMaxPaxAllowed(Integer maxPaxAllowed) {
        this.maxPaxAllowed = maxPaxAllowed;
    }

    public Integer getAllowedNoOfCarOrTaxies() {
        return allowedNoOfCarOrTaxies;
    }

    public void setAllowedNoOfCarOrTaxies(Integer allowedNoOfCarOrTaxies) {
        this.allowedNoOfCarOrTaxies = allowedNoOfCarOrTaxies;
    }

    public Integer getMaxSelectableQuantity() {
        return maxSelectableQuantity;
    }

    public void setMaxSelectableQuantity(Integer maxSelectableQuantity) {
        this.maxSelectableQuantity = maxSelectableQuantity;
    }

    /**
     * @return the minChildAge
     */
    public Integer getMinChildAge() {
        return minChildAge;
    }

    /**
     * @param minChildAge the minChildAge to set
     */
    public void setMinChildAge(Integer minChildAge) {
        this.minChildAge = minChildAge;
    }

    /**
     * @return the maxChildAge
     */
    public Integer getmaxChildAge() {
        return maxChildAge;
    }

    /**
     * @param maxChildAge the maxChildAge to set
     */
    public void setmaxChildAge(Integer maxChildAge) {
        this.maxChildAge = maxChildAge;
    }

}
