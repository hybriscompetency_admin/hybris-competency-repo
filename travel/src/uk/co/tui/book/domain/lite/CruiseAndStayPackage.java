/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

/**
 * The Class CruiseAndStayPackage.
 *
 * @author samantha.gd
 *
 */
public class CruiseAndStayPackage extends MultiCenterHolidayPackage {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -8122175725948400003L;

    /**
     * Gets the cruise.
     *
     * @return the cruise
     */
    public Stay getCruise() {
        return this.getStays().get(0);
    }

    /**
     * Sets the cruise.
     *
     * @param cruise
     *            the new cruise
     */
    public void setCruise(Stay cruise) {
        this.getStays().add(0, cruise);
    }

    /**
     * Gets the hotel.
     *
     * @return the hotel
     */
    public Stay getHotel() {
        return this.getStays().get(1);
    }

    /**
     * Sets the hotel.
     *
     * @param hotel
     *            the new hotel
     */
    public void setHotel(Stay hotel) {
        this.getStays().add(1, hotel);
    }

    /**
     * Gets the flight itinerary.
     *
     * @return the flight itinerary
     */
    public Itinerary getItinerary() {
        return this.getItineraries().get(0);
    }

    /**
     * Sets the itinerary.
     *
     * @param itinerary
     *            the new itinerary
     */
    public void setItinerary(Itinerary itinerary) {
        this.getItineraries().add(0, itinerary);
    }

}
