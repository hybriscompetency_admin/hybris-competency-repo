package uk.co.tui.book.domain.lite;

import java.io.Serializable;

/**
 * The Class FlightsOnlyPackage.
 * @author munisekhar.k
 */
public class FlightOnlyPackage extends PackageHoliday implements Serializable, Cloneable {

    private static final long serialVersionUID = 7812241545677967333L;

    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }
}
