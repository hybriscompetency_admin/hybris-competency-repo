/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

/**
 * @author veena.pn
 */
public class Stay implements Serializable, Cloneable {

    private static final long serialVersionUID = 1121580473702302601L;

    private List<Memo> errataMemo;

    private String code;

    private String name;

    private Date startDate;

    private Date endDate;

    private Price price;

    private Rating officialRating;

    private List<Room> rooms;

    private Inventory inventory;

    private StayType type;

    private int sequenceNo;

    private Address address;

    private int duration;

    private SupplierProductDetails supplierProductDetails;

    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }

    /**
     * @return the accommodationCode
     */
    public String getCode() {
        return code;
    }

    /**
     * @param accommodationCode
     *            the accommodationCode to set
     */
    public void setCode(String accommodationCode) {
        this.code = accommodationCode;
    }

    /**
     * @return the accommStartDate
     */
    public Date getStartDate() {
        if (startDate != null) {
            return (Date) startDate.clone();
        }
        return null;
    }

    /**
     * @param accommStartDate
     *            the accommStartDate to set
     */
    public void setStartDate(Date accommStartDate) {
        if (accommStartDate != null) {
            this.startDate = (Date) accommStartDate.clone();
        }
    }

    /**
     * @return the accommEndDate
     */
    public Date getEndDate() {
        if (endDate != null) {
            return (Date) endDate.clone();
        }
        return null;
    }

    /**
     * @param accommEndDate
     *            the accommEndDate to set
     */
    public void setEndDate(Date accommEndDate) {
        if (accommEndDate != null) {
            this.endDate = (Date) accommEndDate.clone();
        }
    }

    /**
     * @return the price
     */
    public Price getPrice() {
        return price;
    }

    /**
     * @param price
     *            the price to set
     */
    public void setPrice(Price price) {
        this.price = price;
    }

    /**
     * @return the officialRating
     */
    public Rating getOfficialRating() {
        return officialRating;
    }

    /**
     * @param officialRating
     *            the officialRating to set
     */
    public void setOfficialRating(Rating officialRating) {
        this.officialRating = officialRating;
    }

    /**
     * @return the rooms
     */
    public List<Room> getRooms() {
        if (CollectionUtils.isEmpty(rooms)) {
            this.rooms = new ArrayList<Room>();
        }
        return this.rooms;
    }

    /**
     * @param rooms
     *            the rooms to set
     */
    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    /**
     * @return the inventory
     */
    public Inventory getInventory() {
        return inventory;
    }

    /**
     * @param inventory
     *            the inventory to set
     */
    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    /**
     * @return the accommodationType
     */
    public StayType getType() {
        return type;
    }

    /**
     * Sets the type.
     *
     * @param type
     *            the new type
     */
    public void setType(StayType type) {
        this.type = type;
    }

    /**
     * @return the sequenceNo
     */
    public int getSequenceNo() {
        return sequenceNo;
    }

    /**
     * @param sequenceNo
     *            the sequenceNo to set
     */
    public void setSequenceNo(int sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    /**
     * @return the errataMemo
     */
    public List<Memo> getErrataMemo() {
        if (CollectionUtils.isEmpty(errataMemo)) {
            this.errataMemo = new ArrayList<Memo>();
        }
        return this.errataMemo;
    }

    /**
     * @param errataMemo
     *            the errataMemo to set
     */
    public void setErrataMemo(List<Memo> errataMemo) {
        this.errataMemo = errataMemo;
    }

    /**
     * @return the address
     */
    public Address getAddress() {
        return address;
    }

    /**
     * @param address
     *            the address to set
     */
    public void setAddress(Address address) {
        this.address = address;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the duration
     */
    public int getDuration() {
        return duration;
    }

    /**
     * @param duration
     *            the duration to set
     */
    public void setDuration(int duration) {
        this.duration = duration;
    }

    /**
     * @return the supplierProductDetails
     */
    public SupplierProductDetails getSupplierProductDetails() {
        return supplierProductDetails;
    }

    /**
     * @param supplierProductDetails
     *            the supplierProductDetails to set
     */
    public void setSupplierProductDetails(
            SupplierProductDetails supplierProductDetails) {
        this.supplierProductDetails = supplierProductDetails;
    }

}
