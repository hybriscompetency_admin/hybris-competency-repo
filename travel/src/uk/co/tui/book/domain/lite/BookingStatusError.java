/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;

/**
 *
 */
public class BookingStatusError implements Serializable, Cloneable{


    private static final long serialVersionUID = -3827162998989377049L;
    private String errorSource;
    private String errorCode;
    private String errorDisc;


    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }

    /**
     * @return the errorSource
     */
    public String getErrorSource() {
        return errorSource;
    }
    /**
     * @param errorSource the errorSource to set
     */
    public void setErrorSource(String errorSource) {
        this.errorSource = errorSource;
    }
    /**
     * @return the errorCode
     */
    public String getErrorCode() {
        return errorCode;
    }
    /**
     * @param errorCode the errorCode to set
     */
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
    /**
     * @return the errorDisc
     */
    public String getErrorDisc() {
        return errorDisc;
    }
    /**
     * @param errorDisc the errorDisc to set
     */
    public void setErrorDisc(String errorDisc) {
        this.errorDisc = errorDisc;
    }

}
