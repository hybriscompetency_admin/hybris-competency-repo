/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;
import java.util.Date;

/**
 *
 */
public class CardDetails implements Serializable, Cloneable{

    private static final long serialVersionUID = 2675656347552875972L;

    private CardTypes cardType;

    private Date validFrom;

    private Date validTo;

    private String cardNumber;

    private String securityCode;

    private String cardHolderName;

    private String billingPostcode;

    private String issueNumber;

    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }

    /**
     * @return the cardType
     */
    public CardTypes getCardType() {
        return cardType;
    }

    /**
     * @param cardType the cardType to set
     */
    public void setCardType(CardTypes cardType) {
        this.cardType = cardType;
    }

    /**
     * @return the validFrom
     */
    public Date getValidFrom() {
        if(validFrom != null){
            return new Date(validFrom.getTime());
        }
        return null;
    }

    /**
     * @param validFrom the validFrom to set
     */
    public void setValidFrom(Date validFrom) {
        this.validFrom = new Date(validFrom.getTime());
    }

    /**
     * @return the validTo
     */
    public Date getValidTo() {
        if(validTo != null){
            return new Date(validTo.getTime());
        }
        return null;
    }

    /**
     * @param validTo the validTo to set
     */
    public void setValidTo(Date validTo) {
        this.validTo = new Date(validTo.getTime());
    }

    /**
     * @return the cardNumber
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * @param cardNumber the cardNumber to set
     */
    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    /**
     * @return the securityCode
     */
    public String getSecurityCode() {
        return securityCode;
    }

    /**
     * @param securityCode the securityCode to set
     */
    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }

    /**
     * @return the cardHolderName
     */
    public String getCardHolderName() {
        return cardHolderName;
    }

    /**
     * @param cardHolderName the cardHolderName to set
     */
    public void setCardHolderName(String cardHolderName) {
        this.cardHolderName = cardHolderName;
    }

    /**
     * @return the billingPostcode
     */
    public String getBillingPostcode() {
        return billingPostcode;
    }

    /**
     * @param billingPostcode the billingPostcode to set
     */
    public void setBillingPostcode(String billingPostcode) {
        this.billingPostcode = billingPostcode;
    }

    /**
     * @return the issueNumber
     */
    public String getIssueNumber() {
        return issueNumber;
    }

    /**
     * @param issueNumber the issueNumber to set
     */
    public void setIssueNumber(String issueNumber) {
        this.issueNumber = issueNumber;
    }
}
