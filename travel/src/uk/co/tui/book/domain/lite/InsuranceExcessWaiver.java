/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;

/**
 *
 */
public class InsuranceExcessWaiver implements Serializable, Cloneable {

    private static final long serialVersionUID = -5973459417833729538L;

    private String inventoryCode;

    private String extraFacilityCode;

    private String description;

    private Price price;

    private boolean selected;

    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }

    /**
     * @return the code
     */
    public String getInventoryCode() {
        return inventoryCode;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setInventoryCode(String code) {
        this.inventoryCode = code;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the price
     */
    public Price getPrice() {
        return price;
    }

    /**
     * @param price
     *            the price to set
     */
    public void setPrice(Price price) {
        this.price = price;
    }

    /**
     * @return the selected
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * @param selected
     *            the selected to set
     */
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    /**
     * @return the extraFacilityCode
     */
    public String getExtraFacilityCode() {
        return extraFacilityCode;
    }

    /**
     * @param extraFacilityCode
     *            the extraFacilityCode to set
     */
    public void setExtraFacilityCode(String extraFacilityCode) {
        this.extraFacilityCode = extraFacilityCode;
    }

}
