/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;


/**
 *@author veena.pn
 */
public class PackageHoliday extends BasePackage implements Serializable, Cloneable {

    private static final long serialVersionUID = 4602693983203929461L;


    private Itinerary itinerary;


    private Stay stay;

    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }

    /**
     * @return the itinerary
     */
    public Itinerary getItinerary() {
        return itinerary;
    }
    /**
     * @param itinerary the itinerary to set
     */
    public void setItinerary(Itinerary itinerary) {
        this.itinerary = itinerary;
    }
    /**
     * @return the availableAccommodation
     */
    public Stay getStay() {
        return stay;
    }
    /**
     * @param stay the availableAccommodation to set
     */
    public void setStay(Stay stay) {
        this.stay = stay;
    }

}
