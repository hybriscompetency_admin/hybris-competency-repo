/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;

/**
 *
 */
public class CarHireExtraFacility extends ExtraFacility implements Serializable, Cloneable{

    private static final long serialVersionUID = 938093064001850798L;

    private Depot pickupDepot;

    private Depot dropUpOff;

    /**
     * @return the pickupDepot
     */
    public Depot getPickupDepot() {
        return pickupDepot;
    }

    /**
     * @param pickupDepot the pickupDepot to set
     */
    public void setPickupDepot(Depot pickupDepot) {
        this.pickupDepot = pickupDepot;
    }

    /**
     * @return the dropUpOff
     */
    public Depot getDropUpOff() {
        return dropUpOff;
    }

    /**
     * @param dropUpOff the dropUpOff to set
     */
    public void setDropUpOff(Depot dropUpOff) {
        this.dropUpOff = dropUpOff;
    }

    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }
}
