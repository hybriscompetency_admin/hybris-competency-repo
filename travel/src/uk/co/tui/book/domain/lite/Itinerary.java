/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


import org.apache.commons.collections.CollectionUtils;

/**
 *@author veena.pn
 */
public class Itinerary implements Serializable, Cloneable {



    private static final long serialVersionUID = 722551926037845613L;

    private String sequenceNo;

    private int minAvail;

    private List<Price> priceList;

    private List<Leg> inBound;

    private List<Leg> outBound;

    private String duration;

    private Brand brand;

    private String promo;

    private BigDecimal id;
    
    private int count;

    /**
     * @return the id
     */
    public BigDecimal getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(BigDecimal id) {
        this.id = id;
    }

    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }

    /**
     * @return the inBound
     */
    public List<Leg> getInBound() {
        if(CollectionUtils.isEmpty(this.inBound)){
            this.inBound= new ArrayList<Leg>();
        }
        return this.inBound;
    }

    /**
     * @param inBound the inBound to set
     */
    public void setInBound(List<Leg> inBound) {
        this.inBound = inBound;
    }

    /**
     * @return the outBound
     */
    public List<Leg> getOutBound() {
        if(CollectionUtils.isEmpty(this.outBound)){
            this.outBound= new ArrayList<Leg>();
        }
        return this.outBound;
    }

    /**
     * @param outBound the outBound to set
     */
    public void setOutBound(List<Leg> outBound) {
        this.outBound = outBound;
    }


    /**
     * @return the priceList
     */
    public List<Price> getPriceList() {
        if (CollectionUtils.isEmpty(this.priceList)) {
            this.priceList = new ArrayList<Price>();
        }
        return this.priceList;
    }

    /**
     * @param priceList the priceList to set
     */
    public void setPriceList(List<Price> priceList) {
        this.priceList = priceList;
    }

    /**
     * @return the minAvail
     */
    public int getMinAvail() {
        return minAvail;
    }

    /**
     * @param minAvail the minAvail to set
     */
    public void setMinAvail(int minAvail) {
        this.minAvail = minAvail;
    }

    /**
     * @return the duration
     */
    public String getDuration() {
        return duration;
    }

    /**
     * @param duration the duration to set
     */
    public void setDuration(String duration) {
        this.duration = duration;
    }

    /**
     * @return the promo
     */
    public String getPromo() {
        return promo;
    }

    /**
     * @param promo the promo to set
     */
    public void setPromo(String promo) {
        this.promo = promo;
    }

    /**
     * @return the brand
     */
    public Brand getBrand() {
        return brand;
    }

    /**
     * @param brand the brand to set
     */
    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    /**
     * @return the sequenceNo
     */
    public String getSequenceNo() {
        return sequenceNo;
    }

    /**
     * @param sequenceNo the sequenceNo to set
     */
    public void setSequenceNo(String sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(int count) {
        this.count = count;
    }    
    



}
