/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

/**
 * The Class FlyNCruisePackage.
 *
 * @author veena.pn
 */
public class FlyCruiseHolidayPackage extends PackageHoliday {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -8567791127055126346L;

    /**
     * Gets the cruise.
     *
     * @return the cruise
     */
    public Stay getCruise() {
        return this.getStay();

    }

}
