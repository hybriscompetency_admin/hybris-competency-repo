/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

/**
 *@author veena.pn
 */
public enum HighLights {

    EXTRA_SPACE_SEATS,
    PREMIUM_SEATS,
    COACH_TRANSFER,
    DAYTIME_FLIGHTS_ONLY,
    DREAM_LINER,
    LAPLAND_DAYTRIP,
    FREE_CAR_HIRE,
    WORLD_CARE_FUND,
    MULTI_CENTRE
}
