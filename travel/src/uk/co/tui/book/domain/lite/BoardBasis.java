/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;

/**
 *
 */
public class BoardBasis implements Serializable, Cloneable{

    private static final long serialVersionUID = -8055390061728706993L;

    private String code;

    private String description;

    private Money upgradePrice;

    private Money price;

    private boolean defaultBoard;

    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }

    /**
     * @return the boardBasisCode
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the boardBasisCode to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the price
     */
    public Money getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(Money price) {
        this.price = price;
    }

    /**
     * @return the upgradePrice
     */
    public Money getUpgradePrice() {
        return upgradePrice;
    }

    /**
     * @param upgradePrice the upgradePrice to set
     */
    public void setUpgradePrice(Money upgradePrice) {
        this.upgradePrice = upgradePrice;
    }

    /**
     * @return the defaultBoard
     */
    public boolean isDefaultBoard() {
        return defaultBoard;
    }

    /**
     * @param defaultBoard the defaultBoard to set
     */
    public void setDefaultBoard(boolean defaultBoard) {
        this.defaultBoard = defaultBoard;
    }

}
