/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

/**
 *
 */
public class PackageAdditionals {

    /** The multi centre prices. */
    private List<Price>    prices;

    /** The multi centre discounts. */
    private List<Discount> discounts;

    public Price getPrice() {
        if (this.getPrices().size() == 1) {
            return this.getPrices().get(0);
        } else {
            BigDecimal totalPrice = BigDecimal.ZERO;
            for (Price prc : this.getPrices()) {
                totalPrice = totalPrice.add(prc.getAmount().getAmount());
            }
            Money money = new Money();
            Price price = new Price();
            money.setAmount(totalPrice);
            price.setAmount(money);
            return price;
        }
    }

    /**
     * @return the prices
     */
    public List<Price> getPrices() {
        return prices;
    }

    /**
     * @param prices
     *            the prices to set
     */
    public void setPrices(List<Price> prices) {
        this.prices = prices;
    }

    /**
     * @return the discounts
     */
    public List<Discount> getDiscounts() {
        if (CollectionUtils.isEmpty(discounts)) {
            discounts = new ArrayList<Discount>();
        }
        return discounts;
    }

    /**
     * @param discounts
     *            the discounts to set
     */
    public void setDiscounts(List<Discount> discounts) {
        this.discounts = discounts;
    }

}
