/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;

/**
 * @author Anand Shankar
 */
public class PaxCountBasedRestrictions extends ExtraFacilityRestrictions
        implements
            Serializable,
            Cloneable {

    private static final long serialVersionUID = 1L;

    /** Minimum number of child required to apply family insurance. */
    private int minChild;

    /** Maximum number of child to apply family insurance. */
    private int maxChild;

    /** Minimum number of adult required to apply family insurance. */
    private int minAdult;

    /** Maximum number of adults to apply family insurance. */
    private int maxAdult;

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    /**
     * @return the minChild
     */
    public int getMinChild() {
        return minChild;
    }

    /**
     * @param minChild
     *            the minChild to set
     */
    public void setMinChild(int minChild) {
        this.minChild = minChild;
    }

    /**
     * @return the maxChild
     */
    public int getMaxChild() {
        return maxChild;
    }

    /**
     * @param maxChild
     *            the maxChild to set
     */
    public void setMaxChild(int maxChild) {
        this.maxChild = maxChild;
    }

    /**
     * @return the minAdult
     */
    public int getMinAdult() {
        return minAdult;
    }

    /**
     * @param minAdult
     *            the minAdult to set
     */
    public void setMinAdult(int minAdult) {
        this.minAdult = minAdult;
    }

    /**
     * @return the maxAdult
     */
    public int getMaxAdult() {
        return maxAdult;
    }

    /**
     * @param maxAdult
     *            the maxAdult to set
     */
    public void setMaxAdult(int maxAdult) {
        this.maxAdult = maxAdult;
    }

}
