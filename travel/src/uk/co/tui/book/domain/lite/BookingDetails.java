/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement you entered into with hybris.
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

/**
 * @author veena.pn
 */
public class BookingDetails implements Serializable, Cloneable
{

   private static final long serialVersionUID = 907524290648613632L;

   private List<BookingHistory> bookingHistory;

   private Money amountPaid;

   private Money amountDue;

   private Date dueDate;

   private String bookingReferenceNumber;

   private String sessionId;

   private List<PaymentReference> paymentReferences;


   /**
    * @return the bookingReferenceNumber
    */
   public String getBookingReferenceNumber()
   {
      return bookingReferenceNumber;
   }

   /**
    * @param bookingReferenceNumber the bookingReferenceNumber to set
    */
   public void setBookingReferenceNumber(String bookingReferenceNumber)
   {
      this.bookingReferenceNumber = bookingReferenceNumber;
   }

   /**
    * @return the sessionId
    */
   public String getSessionId()
   {
      return sessionId;
   }

   /**
    * @param sessionId the sessionId to set
    */
   public void setSessionId(String sessionId)
   {
      this.sessionId = sessionId;
   }

   /**
    * @return the paymentReferences
    */
   public List<PaymentReference> getPaymentReferences()
   {
      return paymentReferences;
   }

   /**
    * @param paymentReferences the paymentReferences to set
    */
   public void setPaymentReferences(List<PaymentReference> paymentReferences)
   {
      this.paymentReferences = paymentReferences;
   }
   // needs to be moved

   /**
    * Clone.
    *
    * @return the object
    * @throws CloneNotSupportedException the clone not supported exception
    */
   @Override
   public Object clone() throws CloneNotSupportedException
   {
      return super.clone();

   }

   /**
    * @return the bookingHistory
    */
   public List<BookingHistory> getBookingHistory()
   {
      if (CollectionUtils.isEmpty(this.bookingHistory))
      {
         this.bookingHistory = new ArrayList<BookingHistory>();
      }
      return this.bookingHistory;
   }

   /**
    * @param bookingHistory the bookingHistory to set
    */
   public void setBookingHistory(List<BookingHistory> bookingHistory)
   {
      this.bookingHistory = bookingHistory;
   }

   /**
    * @return the amountPaid
    */
   public Money getAmountPaid()
   {
      return amountPaid;
   }

   /**
    * @param amountPaid the amountPaid to set
    */
   public void setAmountPaid(Money amountPaid)
   {
      this.amountPaid = amountPaid;
   }

   /**
    * @return the amountDue
    */
   public Money getAmountDue()
   {
      return amountDue;
   }

   /**
    * @param amountDue the amountDue to set
    */
   public void setAmountDue(Money amountDue)
   {
      this.amountDue = amountDue;
   }

   /**
    * @return the dueDate
    */
   public Date getDueDate()
   {
      return new Date(dueDate.getTime());
   }

   /**
    * @param dueDate the dueDate to set
    */
   public void setDueDate(Date dueDate)
   {
      this.dueDate = new Date(dueDate.getTime());
   }

}
