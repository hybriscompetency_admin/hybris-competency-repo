/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;

/**
 *
 */
public class SupplierProductDetails implements Serializable, Cloneable {

    private static final long serialVersionUID = 7047233749393687891L;

    private String supplierNumber;

    private String productCode;

    private String sellingCode;

    private String primeSubProductCode;

    private String subProductcode;

    private String brochureCode;

    private String promoCode;

    /**
     *
     */
    public SupplierProductDetails() {
        
    }

    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }

    /**
     * @return the supplierNumber
     */
    public String getSupplierNumber() {
        return supplierNumber;
    }

    /**
     * @param supplierNumber
     *            the supplierNumber to set
     */
    public void setSupplierNumber(String supplierNumber) {
        this.supplierNumber = supplierNumber;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getSellingCode() {
        return sellingCode;
    }

    public void setSellingCode(String sellingCode) {
        this.sellingCode = sellingCode;
    }

    public String getPrimeSubProductCode() {
        return primeSubProductCode;
    }

    public void setPrimeSubProductCode(String primeSubProductCode) {
        this.primeSubProductCode = primeSubProductCode;
    }

    public String getSubProductcode() {
        return subProductcode;
    }

    public void setSubProductcode(String subProductcode) {
        this.subProductcode = subProductcode;
    }

    public String getBrochureCode() {
        return brochureCode;
    }

    public void setBrochureCode(String brochureCode) {
        this.brochureCode = brochureCode;
    }

    /**
     * @return the promoCode
     */
    public String getPromoCode() {
        return promoCode;
    }

    /**
     * @param promoCode the promoCode to set
     */
    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

}
