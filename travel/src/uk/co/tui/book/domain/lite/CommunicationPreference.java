/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;

/**
 *
 */
public class CommunicationPreference implements Serializable, Cloneable{

    private static final long serialVersionUID = 4704839677049952090L;
    /**
     * Gets by Email
     */
    private boolean byEmail;
    /**
     * Gets by phone
     */
    private boolean byPhone;
    /**
 *
 */
    private boolean byPost;
    /**
   *
   */
    private boolean receiveCommunication;
    /**
   *
   */
    private boolean thirdPartySharing;

    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }

    /**
     * @return the byEmail
     */
    public boolean isByEmail() {
        return byEmail;
    }

    /**
     * @param byEmail
     *            the byEmail to set
     */
    public void setByEmail(boolean byEmail) {
        this.byEmail = byEmail;
    }

    /**
     * @return the byPhone
     */
    public boolean isByPhone() {
        return byPhone;
    }

    /**
     * @param byPhone
     *            the byPhone to set
     */
    public void setByPhone(boolean byPhone) {
        this.byPhone = byPhone;
    }

    /**
     * @return the byPost
     */
    public boolean isByPost() {
        return byPost;
    }

    /**
     * @param byPost
     *            the byPost to set
     */
    public void setByPost(boolean byPost) {
        this.byPost = byPost;
    }

    /**
     * @return the receiveCommunication
     */
    public boolean isReceiveCommunication() {
        return receiveCommunication;
    }

    /**
     * @param receiveCommunication
     *            the receiveCommunication to set
     */
    public void setReceiveCommunication(boolean receiveCommunication) {
        this.receiveCommunication = receiveCommunication;
    }

    /**
     * @return the thirdPartySharing
     */
    public boolean isThirdPartySharing() {
        return thirdPartySharing;
    }

    /**
     * @param thirdPartySharing
     *            the thirdPartySharing to set
     */
    public void setThirdPartySharing(boolean thirdPartySharing) {
        this.thirdPartySharing = thirdPartySharing;
    }

}
