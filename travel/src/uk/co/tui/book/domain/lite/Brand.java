/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

/**
 *
 */
public enum Brand {
    TH("TH", "T"),

    FC ("FC", "F"),

    FJ ("FJ", "E"),

    CR ("CR", "T"),

    FO ("FO", "T");

    /** The code of this enum.*/
    private final String code;
    /** The id of this enum.*/
    private final String id;

    /**
     * Creates a new enum value for this enum type.
     *
     * @param code the enum value code
     * @param id the enum value id
     */
    private Brand(String code, String id){
        this.code = code.intern();
        this.id = id.intern();
    }

    /**
     * Gets the code of this enum value.
     *
     * @return code of value
     */
    public String getCode()
    {
        return this.code;
    }

    /**
     * Gets the code of this enum value.
     *
     * @return code of value
     */
    public String getId()
    {
        return this.id;
    }

}
