/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

/**
 *
 */
public class LegExtraFacility implements Serializable, Cloneable {

    private static final long serialVersionUID = -4948362019131739834L;

    private String legId;

    private String flightNumber;

    private List<ExtraFacilityCategory> extraFacilityCategories;

    private LegType legType;

    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }

    public String getLegId() {
        return legId;
    }

    public void setLegId(String legId) {
        this.legId = legId;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public List<ExtraFacilityCategory> getExtraFacilityCategories() {
        if (CollectionUtils.isEmpty(extraFacilityCategories)) {
            extraFacilityCategories = new ArrayList<ExtraFacilityCategory>();
        }
        return extraFacilityCategories;
    }

    public void setExtraFacilityCategories(List<ExtraFacilityCategory> extraFacilityCategories) {
        this.extraFacilityCategories = extraFacilityCategories;
    }

    public LegType getLegType() {
        return legType;
    }

    public void setLegType(LegType legType) {
        this.legType = legType;
    }



}
