/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

/**
 *
 */
public enum CardTypes {

    AMERICAN_EXPRESS,
    VISA,
    MASTERCARD,
    MASTERCARD_DEBIT,
    SWITCH,
    VISA_DEBIT,
    VISA_DELTA,
    VISA_ELECTRON,
    SOLO,
    EURO_MASTERCARD,
    EURO_MASTERCARD_DEBIT,
    EURO_VISA,
    LASER,
    GIFT_CARD,
    MAESTRO
}
