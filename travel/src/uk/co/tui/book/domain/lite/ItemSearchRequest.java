/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;

/**
 *
 */
public class ItemSearchRequest implements Serializable, Cloneable {

    private static final long serialVersionUID = -7230751063347433752L;

    /** The package model. */
    private BasePackage packageModel;

    /** The request type. */
    private String requestType;

    /** The promo code. */
    private String promoCode;

    /** The loc code. */
    private String locCode;

    /** The loc tp. */
    private String locTp;

    /** The user name. */
    private String userName;

    /** The Agt no. */
    private String agtNo;

    /** The extra facility. */
    private ExtraFacility extraFacility;

    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }

    /**
     * @return the packageModel
     */
    public BasePackage getPackageModel() {
        return packageModel;
    }

    /**
     * @param packageModel the packageModel to set
     */
    public void setPackageModel(BasePackage packageModel) {
        this.packageModel = packageModel;
    }

    /**
     * @return the requestType
     */
    public String getRequestType() {
        return requestType;
    }

    /**
     * @param requestType the requestType to set
     */
    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    /**
     * @return the promoCode
     */
    public String getPromoCode() {
        return promoCode;
    }

    /**
     * @param promoCode the promoCode to set
     */
    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    /**
     * @return the locCode
     */
    public String getLocCode() {
        return locCode;
    }

    /**
     * @param locCode the locCode to set
     */
    public void setLocCode(String locCode) {
        this.locCode = locCode;
    }

    /**
     * @return the locTp
     */
    public String getLocTp() {
        return locTp;
    }

    /**
     * @param locTp the locTp to set
     */
    public void setLocTp(String locTp) {
        this.locTp = locTp;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the agtNo
     */
    public String getAgtNo() {
        return agtNo;
    }

    /**
     * @param agtNo the agtNo to set
     */
    public void setAgtNo(String agtNo) {
        this.agtNo = agtNo;
    }

    /**
     * @return the extraFacility
     */
    public ExtraFacility getExtraFacility() {
        return extraFacility;
    }

    /**
     * @param extraFacility the extraFacility to set
     */
    public void setExtraFacility(ExtraFacility extraFacility) {
        this.extraFacility = extraFacility;
    }

}
