/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;

/**
 *
 */
public class BaggageExtraFacilityRestrictions extends ExtraFacilityRestrictions implements Serializable, Cloneable{

    private static final long serialVersionUID = -365061889107840485L;

    private int maxWeightPerPiece;

    private int maxWeightPerPax;

    private int maxChildAge;

    private int maxPieceperPax;

    private int infantWeight;

    private int baseWeight;

    private int basePieces;

    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }

    public int getMaxWeightPerPiece() {
        return maxWeightPerPiece;
    }

    public void setMaxWeightPerPiece(int maxWeightPerPiece) {
        this.maxWeightPerPiece = maxWeightPerPiece;
    }

    public int getMaxWeightPerPax() {
        return maxWeightPerPax;
    }

    public void setMaxWeightPerPax(int maxWeightPerPax) {
        this.maxWeightPerPax = maxWeightPerPax;
    }

    public int getMaxChildAge() {
        return maxChildAge;
    }

    public void setMaxChildAge(int maxChildAge) {
        this.maxChildAge = maxChildAge;
    }

    public int getMaxPieceperPax() {
        return maxPieceperPax;
    }

    public void setMaxPieceperPax(int maxPieceperPax) {
        this.maxPieceperPax = maxPieceperPax;
    }

    public int getInfantWeight() {
        return infantWeight;
    }

    public void setInfantWeight(int infantWeight) {
        this.infantWeight = infantWeight;
    }

    public int getBaseWeight() {
        return baseWeight;
    }

    public void setBaseWeight(int baseWeight) {
        this.baseWeight = baseWeight;
    }

    public int getBasePieces() {
        return basePieces;
    }

    public void setBasePieces(int basePieces) {
        this.basePieces = basePieces;
    }

}
