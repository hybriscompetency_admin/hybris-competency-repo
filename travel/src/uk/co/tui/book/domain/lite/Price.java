/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;
import java.util.Map;

/**
 *
 */
public class Price implements Serializable, Cloneable{

    private static final long serialVersionUID = -2928528932504333475L;

    private String code;

    private String description;

    private Money rate;

    private Money amount;

    private Integer quantity = 0;

    private PriceType priceType;

    private Map<String, String> keyValuePair;

    private PriceProfile priceProfile;

    private String priceCodeType;

    private boolean visible;

    private String priceStatus;

    private Money vat;

    private Money commission;

    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the rate
     */
    public Money getRate() {
        return rate;
    }

    /**
     * @param rate the rate to set
     */
    public void setRate(Money rate) {
        this.rate = rate;
    }

    /**
     * @return the amount
     */
    public Money getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(Money amount) {
        this.amount = amount;
    }

    /**
     * @return the quantity
     */
    public Integer getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the priceType
     */
    public PriceType getPriceType() {
        return priceType;
    }

    /**
     * @param priceType the priceType to set
     */
    public void setPriceType(PriceType priceType) {
        this.priceType = priceType;
    }

    /**
     * @return the keyValuePair
     */
    public Map<String, String> getKeyValuePair() {
        return keyValuePair;
    }

    /**
     * @param keyValuePair the keyValuePair to set
     */
    public void setKeyValuePair(Map<String, String> keyValuePair) {
        this.keyValuePair = keyValuePair;
    }

    /**
     * @return the priceProfile
     */
    public PriceProfile getPriceProfile() {
        return priceProfile;
    }

    /**
     * @param priceProfile the priceProfile to set
     */
    public void setPriceProfile(PriceProfile priceProfile) {
        this.priceProfile = priceProfile;
    }

    /**
     * @return the priceCodeType
     */
    public String getPriceCodeType() {
        return priceCodeType;
    }

    /**
     * @param priceCodeType the priceCodeType to set
     */
    public void setPriceCodeType(String priceCodeType) {
        this.priceCodeType = priceCodeType;
    }

    /**
     * @return the visible
     */
    public boolean isVisible() {
        return visible;
    }

    /**
     * @param visible the visible to set
     */
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    /**
     * @return the priceStatus
     */
    public String getPriceStatus() {
        return priceStatus;
    }

    /**
     * @param priceStatus the priceStatus to set
     */
    public void setPriceStatus(String priceStatus) {
        this.priceStatus = priceStatus;
    }

    /**
     * @return the vat
     */
    public Money getVat() {
        return vat;
    }

    /**
     * @param vat the vat to set
     */
    public void setVat(Money vat) {
        this.vat = vat;
    }

    /**
     * @return the commission
     */
    public Money getCommission() {
        return commission;
    }

    /**
     * @param commission the commission to set
     */
    public void setCommission(Money commission) {
        this.commission = commission;
    }



}
