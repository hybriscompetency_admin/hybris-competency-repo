/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement you entered into with hybris.
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

/**
 *
 */
public class Memo implements Serializable, Cloneable
{

   private static final long serialVersionUID = 8345778309345651876L;

   public static final int PRIME = 31;

   private String name;

   private String description;

   private String code;

   private MemoType memoType;

   private Date memoDate;

   private MemoSource memoSource;

   /**
    * Clone.
    *
    * @return the object
    * @throws CloneNotSupportedException the clone not supported exception
    */
   @Override
   public Object clone() throws CloneNotSupportedException
   {
      return super.clone();

   }

   /**
    * @return the memoDate
    */
   public Date getMemoDate()
   {
      return new Date(memoDate.getTime());
   }

   /**
    * @param memoDate the memoDate to set
    */
   public void setMemoDate(Date memoDate)
   {
      this.memoDate = new Date(memoDate.getTime());
   }

   /**
    * @return the name
    */
   public String getName()
   {
      return name;
   }

   /**
    * @param name the name to set
    */
   public void setName(String name)
   {
      this.name = name;
   }

   /**
    * @return the description
    */
   public String getDescription()
   {
      return description;
   }

   /**
    * @param description the description to set
    */
   public void setDescription(String description)
   {
      this.description = description;
   }

   /**
    * @return the code
    */
   public String getCode()
   {
      return code;
   }

   /**
    * @param code the code to set
    */
   public void setCode(String code)
   {
      this.code = code;
   }

   /**
    * @return the memoType
    */
   public MemoType getMemoType()
   {
      return memoType;
   }

   /**
    * @param memoType the memoType to set
    */
   public void setMemoType(MemoType memoType)
   {
      this.memoType = memoType;
   }

   /**
    * @return the MemoSource
    */
   public MemoSource getMemoSource()
   {
      return memoSource;
   }

   /**
    * @param memoSource the memoComponent to set
    */
   public void setMemoSource(MemoSource memoSource)
   {
      this.memoSource = memoSource;
   }

   /**
    * Equals.
    *
    * Checks is both the memo names are same
    *
    * @param memo the memo
    * @return true, if successful
    */
   @Override
   public boolean equals(Object memo)
   {

      if (memo == null)
      {
         return false;
      }
      if (memo instanceof Memo)
      {
         // Checks if both the memo names and descriptions are same
         return StringUtils.equalsIgnoreCase(this.getName(), ((Memo) memo).getName())
            && StringUtils.equalsIgnoreCase(this.getDescription(), ((Memo) memo).getDescription());
      }
      return false;
   }

   /**
    * Hash code.
    *
    * @return the current object's hash code
    */
   @Override
   public int hashCode()
   {
      int result = 1;
      result = PRIME * result + ((code == null) ? 0 : code.hashCode());
      return result;
   }

}
