
package uk.co.tui.book.domain.lite;

import java.io.Serializable;
import java.util.Date;

import uk.co.tui.book.domain.lite.DepositType;



public class Deposit implements Serializable, Cloneable{

    private static final long serialVersionUID = -7523524452114304298L;

    private Date balanceDueDate;

    private Price outstandingBalance;

    private Price baseDepositAmount;

    private Date depositDueDate;

    private Boolean applicable;

    private Price depositAmount;

    private DepositType depositType;

    private Boolean available;

    private Boolean selected;

    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }

    public Date getBalanceDueDate() {
        if (balanceDueDate != null) {
            return (Date) balanceDueDate.clone();
        }
        return null;
    }

    public void setBalanceDueDate(Date balanceDueDate) {
        if (balanceDueDate != null) {
            this.balanceDueDate = (Date) balanceDueDate.clone();
        }
    }

    public Price getOutstandingBalance() {
        return outstandingBalance;
    }

    public void setOutstandingBalance(Price outstandingBalance) {
        this.outstandingBalance = outstandingBalance;
    }

    public Price getBaseDepositAmount() {
        return baseDepositAmount;
    }

    public void setBaseDepositAmount(Price baseDepositAmount) {
        this.baseDepositAmount = baseDepositAmount;
    }

    public Date getDepositDueDate() {
        if (depositDueDate != null) {
            return (Date)depositDueDate.clone();
        }
        return null;
    }

    public void setDepositDueDate(Date depositDueDate) {
        if (depositDueDate != null) {
            this.depositDueDate = (Date) depositDueDate.clone();
        }
    }

    public Boolean getApplicable() {
        return applicable;
    }

    public void setApplicable(Boolean applicable) {
        this.applicable = applicable;
    }

    public Price getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(Price depositAmount) {
        this.depositAmount = depositAmount;
    }

    public DepositType getDepositType() {
        return depositType;
    }

    public void setDepositType(DepositType depositType) {
        this.depositType = depositType;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }



}
