/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

public class Room implements Serializable, Cloneable {

    private static final long     serialVersionUID = -1030185885521822312L;

    private String                code;

    private List<Price>           prices;

    private Boolean               freeKids;

    private Boolean               defaultRoom      = Boolean.FALSE;

    private Integer               quantity;

    private Price                 roomUpgradePrice;

    private BoardBasis            boardBasis;

    private List<Discount>        discounts;

    private Collection<Passenger> passgengers;

    private RoomOccupancy         roomOccupancy;

    private String                roomTitle;

    public static final int       PRIME            = 31;

    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }

    public String getCode() {
        return code;
    }

    public void setCode(String roomCode) {
        this.code = roomCode;
    }

    public Price getPrice() {
        if (this.getPrices().size() == 1) {
            return this.getPrices().get(0);
        } else {
            BigDecimal totalRoomPrice = BigDecimal.ZERO;
            for (Price prc : this.getPrices()) {
                totalRoomPrice = totalRoomPrice.add(prc.getAmount().getAmount());
            }
            Money money = new Money();
            Price price = new Price();
            money.setAmount(totalRoomPrice);
            price.setAmount(money);
            return price;
        }
    }

    public Boolean getFreeKids() {
        return freeKids;
    }

    public void setFreeKids(Boolean freeKids) {
        this.freeKids = freeKids;
    }

    public Boolean getDefaultRoom() {
        return defaultRoom;
    }

    public void setDefaultRoom(Boolean defaultRoom) {
        this.defaultRoom = defaultRoom;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Price getRoomUpgradePrice() {
        return roomUpgradePrice;
    }

    public void setRoomUpgradePrice(Price roomUpgradePrice) {
        this.roomUpgradePrice = roomUpgradePrice;
    }

    public BoardBasis getBoardBasis() {
        return boardBasis;
    }

    public void setBoardBasis(BoardBasis boardBasis) {
        this.boardBasis = boardBasis;
    }

    public List<Discount> getDiscounts() {
        if (CollectionUtils.isEmpty(discounts)) {
            discounts = new ArrayList<Discount>();
        }
        return discounts;
    }

    public void setDiscounts(List<Discount> discounts) {
        this.discounts = discounts;
    }

    public Collection<Passenger> getPassgengers() {
        if (CollectionUtils.isEmpty(passgengers)) {
            passgengers = new ArrayList<Passenger>();
        }
        return passgengers;
    }

    public void setPassgengers(Collection<Passenger> passgengers) {
        this.passgengers = passgengers;
    }

    /**
     * @return the roomOccupancy
     */
    public RoomOccupancy getRoomOccupancy() {
        return roomOccupancy;
    }

    /**
     * @param roomOccupancy
     *            the roomOccupancy to set
     */
    public void setRoomOccupancy(RoomOccupancy roomOccupancy) {
        this.roomOccupancy = roomOccupancy;
    }

    /**
     * @return the roomTitle
     */
    public String getRoomTitle() {
        return roomTitle;
    }

    /**
     * @param roomTitle
     *            the roomTitle to set
     */
    public void setRoomTitle(String roomTitle) {
        this.roomTitle = roomTitle;
    }

    /**
     * @return the prices
     */
    public List<Price> getPrices() {
        if (CollectionUtils.isEmpty(this.prices)) {
            this.prices = new ArrayList<Price>();
        }
        return prices;
    }

    /**
     * @param prices
     *            the prices to set
     */
    public void setPrices(List<Price> prices) {
        this.prices = prices;
    }

    /**
     * Equals.
     *
     * Checks is both the room codes are same
     *
     * @param room
     *            the room
     * @return true, if successful
     */
    @Override
    public boolean equals(Object room) {

        if (room == null) {
            return false;
        }
        if (room instanceof Room) {
            // Checks if both the room codes are same & their corresponding selling codes
            return StringUtils.equalsIgnoreCase(this.getCode(), ((Room) room).getCode());
        }
        return false;
    }

    /**
     * Hash code.
     *
     * @return the current object's hash code
     */
    @Override
    public int hashCode() {
        int result = 1;
        result = PRIME * result + ((code == null) ? 0 : code.hashCode());
        return result;
    }

}
