/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;



/**
 *
 */
public class Leg implements Serializable, Cloneable {

    private static final long serialVersionUID = -7658916073518506150L;

    private Port arrivalAirport;

    private Port departureAirport;

    private Schedule schedule;

    private Price price;

    private String inventoryId;

    private String journeyDuration;

    private boolean externalInventory;

    private List<Discount> discounts;

    private List<Price> priceList;

    private PaymentDetails paymentDetails;

    private Carrier carrier;

        /**
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }

    /**
     * @return the discounts
     */
    public List<Discount> getDiscounts() {
        if (CollectionUtils.isEmpty(discounts)) {
            discounts = new ArrayList<Discount>();
        }
        return discounts;
    }

    /**
     * @param discounts the discounts to set
     */
    public void setDiscounts(List<Discount> discounts) {
        this.discounts = discounts;
    }

    /**
     * @return the arrivalAirport
     */
    public Port getArrivalAirport() {
        return arrivalAirport;
    }

    /**
     * @param arrivalAirport the arrivalAirport to set
     */
    public void setArrivalAirport(Port arrivalAirport) {
        this.arrivalAirport = arrivalAirport;
    }

    /**
     * @return the departureAirport
     */
    public Port getDepartureAirport() {
        return departureAirport;
    }

    /**
     * @param departureAirport the departureAirport to set
     */
    public void setDepartureAirport(Port departureAirport) {
        this.departureAirport = departureAirport;
    }

    /**
     * @return the flightSchedule
     */
    public Schedule getSchedule() {
        return schedule;
    }

    /**
     * @param schedule the flightSchedule to set
     */
    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    /**
     * @return the carrier
     */
    public Carrier getCarrier() {
        return carrier;
    }

    /**
     * @param carrier the carrier to set
     */
    public void setCarrier(Carrier carrier) {
        this.carrier = carrier;
    }

    /**
     * @return the price
     */
    public Price getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(Price price) {
        this.price = price;
    }

    /**
     * @return the inventoryId
     */
    public String getInventoryId() {
        return inventoryId;
    }

    /**
     * @param inventoryId the inventoryId to set
     */
    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }

    /**
     * @return the journeyDuration
     */
    public String getJourneyDuration() {
        return journeyDuration;
    }

    /**
     * @param journeyDuration the journeyDuration to set
     */
    public void setJourneyDuration(String journeyDuration) {
        this.journeyDuration = journeyDuration;
    }

    /**
     * @return the externalInventory
     */
    public boolean isExternalInventory() {
        return externalInventory;
    }

    /**
     * @param externalInventory the externalInventory to set
     */
    public void setExternalInventory(boolean externalInventory) {
        this.externalInventory = externalInventory;
    }

    /**
     * @return the priceList
     */
    public List<Price> getPriceList() {
        return priceList;
    }

    /**
     * @param priceList the priceList to set
     */
    public void setPriceList(List<Price> priceList) {
        this.priceList = priceList;
    }

    /**
     * @return the paymentDetails
     */
    public PaymentDetails getPaymentDetails() {
        return paymentDetails;
    }

    /**
     * @param paymentDetails
     *            the externalPaymentDetails to set
     */
    public void setPaymentDetails(PaymentDetails paymentDetails) {
        this.paymentDetails = paymentDetails;
    }
}
