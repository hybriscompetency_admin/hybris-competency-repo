/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;

/**
 * The Class PromotionalDiscount.
 */
public class PromotionalDiscount implements Serializable, Cloneable {

    private static final long serialVersionUID = 3042006953398550376L;

    /** The promotional code. */
    private String code;

    /** The price. */
    private Price  price;

    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the code.
     *
     * @param code
     *            the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets the price.
     *
     * @return the price
     */
    public Price getPrice() {
        return price;
    }

    /**
     * Sets the price.
     *
     * @param price
     *            the price to set
     */
    public void setPrice(Price price) {
        this.price = price;
    }

}
