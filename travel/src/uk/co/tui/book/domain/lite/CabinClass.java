/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement you entered into with hybris.
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;

/**
 * @author veena.pn
 */
public class CabinClass implements Serializable, Cloneable
{

   private static final long serialVersionUID = 683407920021823188L;

   /**
    * The cabin Code
    */
   private String cabinCode;

   /**
    * The cabin Name
    */
   private String cabinName;

   /**
    * @return the cabinCode
    */
   public String getCabinCode()
   {
      return cabinCode;
   }

   /**
    * @param cabinCode the cabinCode to set
    */
   public void setCabinCode(String cabinCode)
   {
      this.cabinCode = cabinCode;
   }

   /**
    * @return the cabinName
    */
   public String getCabinName()
   {
      return cabinName;
   }

   /**
    * @param cabinName the cabinName to set
    */
   public void setCabinName(String cabinName)
   {
      this.cabinName = cabinName;
   }

   /**
    * Clone.
    *
    * @return the object
    * @throws CloneNotSupportedException the clone not supported exception
    */
   @Override
   public Object clone() throws CloneNotSupportedException
   {
      return super.clone();

   }

}
