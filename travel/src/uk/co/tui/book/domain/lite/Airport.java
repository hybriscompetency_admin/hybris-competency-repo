/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

/**
 * The Class Airport.
 */
public class Airport extends Port implements Serializable, Cloneable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -973054772132153430L;

    /** The children. */
    private List<Airport>        children;

    /** The parents. */
    private List<Airport>        parents;

    /**
     * Gets the parents.
     *
     * @return the parents
     */
    public List<Airport> getParents() {
        if (CollectionUtils.isEmpty(parents)) {
            this.parents = new ArrayList<Airport>();
        }
        return this.parents;
    }

    /**
     * Sets the parents.
     *
     * @param parents
     *            the parents to set
     */
    public void setParents(List<Airport> parents) {
        this.parents = parents;
    }

    /**
     * Gets the children.
     *
     * @return the children
     */
    public List<Airport> getChildren() {
        if (CollectionUtils.isEmpty(this.children)) {
            this.children = new ArrayList<Airport>();
        }
        return this.children;
    }

    /**
     * Sets the children.
     *
     * @param children
     *            the children to set
     */
    public void setChildren(List<Airport> children) {
        this.children = children;
    }

}
