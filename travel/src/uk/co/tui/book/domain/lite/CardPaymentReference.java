/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;

/**
 *@author veena.pn
 */
public class CardPaymentReference extends PaymentReference implements Serializable, Cloneable
{

    private static final long serialVersionUID = 3894870228814601333L;

    private Money cardCharges;

    private String paymentReferenceNumber;

    private String merchantReferenceNumber;

    private String authCode;

    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }
    /**
     * @return the cardCharges
     */
    public Money getCardCharges() {
        return cardCharges;
    }
    /**
     * @param cardCharges the cardCharges to set
     */
    public void setCardCharges(Money cardCharges) {
        this.cardCharges = cardCharges;
    }
    /**
     * @return the paymentReferenceNumber
     */
    public String getPaymentReferenceNumber() {
        return paymentReferenceNumber;
    }
    /**
     * @param paymentReferenceNumber the paymentReferenceNumber to set
     */
    public void setPaymentReferenceNumber(String paymentReferenceNumber) {
        this.paymentReferenceNumber = paymentReferenceNumber;
    }
    /**
     * @return the merchantReferenceNumber
     */
    public String getMerchantReferenceNumber() {
        return merchantReferenceNumber;
    }
    /**
     * @param merchantReferenceNumber the merchantReferenceNumber to set
     */
    public void setMerchantReferenceNumber(String merchantReferenceNumber) {
        this.merchantReferenceNumber = merchantReferenceNumber;
    }
    /**
     * @return the authCode
     */
    public String getAuthCode() {
        return authCode;
    }
    /**
     * @param authCode the authCode to set
     */
    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }


}
