/*
 * [y] hybris Platform
 * 
 * Copyright (c) 2000-2014 hybris AG All rights reserved.
 * 
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement you entered into with hybris.
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;

/**
 *
 */
public class Inventory implements Serializable, Cloneable
{
   private static final long serialVersionUID = 7333834335122041038L;

   /**
    * The Supplier ProductDetails
    */
   private SupplierProductDetails supplierProductDetails;

   /** The inventory type. */
   private InventoryType inventoryType;

   /**
    * Clone.
    *
    * @return the object
    * @throws CloneNotSupportedException the clone not supported exception
    */
   @Override
   public Object clone() throws CloneNotSupportedException
   {
      return super.clone();

   }

   /**
    * @return the inventoryType
    */
   public InventoryType getInventoryType()
   {
      return inventoryType;
   }

   /**
    * @param inventoryType the inventoryType to set
    */
   public void setInventoryType(InventoryType inventoryType)
   {
      this.inventoryType = inventoryType;
   }

   /**
    * @return the supplierProductDetails
    */
   public SupplierProductDetails getSupplierProductDetails()
   {
      return supplierProductDetails;
   }

   /**
    * @param supplierProductDetails the supplierProductDetails to set
    */
   public void setSupplierProductDetails(SupplierProductDetails supplierProductDetails)
   {
      this.supplierProductDetails = supplierProductDetails;
   }

}
