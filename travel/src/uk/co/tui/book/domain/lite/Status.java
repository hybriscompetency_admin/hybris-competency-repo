/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

/**
 *@author samantha.gd
 */

/**
 * The Enum Status.
 */
public enum Status {

    /** The inprogress. */
    INPROGRESS,

    /** Pre Process. */
    PREPROCESS,

    /** Pre Processed. */
    PREPROCESSED,

    /** The booked. */
    BOOKED,

    /** Fulfill.*/
    FULFILL,

    /** Fulfilled.*/
    FULFILLED,

    /** Post Process. */
    POSTPROCESS,

    /** Process. */
    PROCESS,

    /** Processed. */
    PROCESSED,

    /** The paymentfailed. */
    PAYMENTFAILED,

    /** The bookingfailed. */
    BOOKINGFAILED,

    PAYMENTSUCCESS
}
