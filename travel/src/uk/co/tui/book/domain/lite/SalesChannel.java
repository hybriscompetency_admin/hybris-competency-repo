/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;

/**
 *@author veena.pn
 */
public class SalesChannel implements Serializable, Cloneable {

    private static final long serialVersionUID = 3678313066737218300L;

    private SalesChannelType salesChannelType;

    /**
     * Clone.
     *
     * @return the object
     * @throws CloneNotSupportedException
     *             the clone not supported exception
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();

    }

    /**
     * @return the salesChannelType
     */
    public SalesChannelType getSalesChannelType() {
        return salesChannelType;
    }

    /**
     * @param salesChannelType the salesChannelType to set
     */
    public void setSalesChannelType(SalesChannelType salesChannelType) {
        this.salesChannelType = salesChannelType;
    }



}
