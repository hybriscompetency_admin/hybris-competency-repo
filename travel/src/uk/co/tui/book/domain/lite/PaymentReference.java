/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement you entered into with hybris.
 */
package uk.co.tui.book.domain.lite;

import java.io.Serializable;
import java.util.Date;

/**
 * @author veena.pn
 */
public class PaymentReference implements Serializable, Cloneable
{

   private static final long serialVersionUID = -2006867002441465493L;

   private Date paymentDate;

   private String paymentType;

   private Money amountPaid;

   private Money amountDue;

   private Money cardCharges;

   private String merchantReferenceNumber;

   private String paymentReferenceNumber;

   private String authorizationCode;

   private CardDetails cardDetails;

   /**
    * Clone.
    *
    * @return the object
    * @throws CloneNotSupportedException the clone not supported exception
    */
   @Override
   public Object clone() throws CloneNotSupportedException
   {
      return super.clone();

   }

   /**
    * @return the paymentType
    */
   public String getPaymentType()
   {
      return paymentType;
   }

   /**
    * @param paymentType the paymentType to set
    */
   public void setPaymentType(String paymentType)
   {
      this.paymentType = paymentType;
   }

   /**
    * @return the paymentDate
    */
   public Date getPaymentDate()
   {
      return new Date(paymentDate.getTime());
   }

   /**
    * @param paymentDate the paymentDate to set
    */
   public void setPaymentDate(Date paymentDate)
   {
      this.paymentDate = new Date(paymentDate.getTime());
   }

   /**
    * @return the amountPaid
    */
   public Money getAmountPaid()
   {
      return amountPaid;
   }

   /**
    * @param amountPaid the amountPaid to set
    */
   public void setAmountPaid(Money amountPaid)
   {
      this.amountPaid = amountPaid;
   }

   /**
    * @return the amountDue
    */
   public Money getAmountDue()
   {
      return amountDue;
   }

   /**
    * @param amountDue the amountDue to set
    */
   public void setAmountDue(Money amountDue)
   {
      this.amountDue = amountDue;
   }

   /**
    * @return the cardCharges
    */
   public Money getCardCharges()
   {
      return cardCharges;
   }

   /**
    * @param cardCharges the cardCharges to set
    */
   public void setCardCharges(Money cardCharges)
   {
      this.cardCharges = cardCharges;
   }

   /**
    * @return the merchantReferenceNumber
    */
   public String getMerchantReferenceNumber()
   {
      return merchantReferenceNumber;
   }

   /**
    * @param merchantReferenceNumber the merchantReferenceNumber to set
    */
   public void setMerchantReferenceNumber(String merchantReferenceNumber)
   {
      this.merchantReferenceNumber = merchantReferenceNumber;
   }

   /**
    * @return the paymentReferenceNumber
    */
   public String getPaymentReferenceNumber()
   {
      return paymentReferenceNumber;
   }

   /**
    * @param paymentReferenceNumber the paymentReferenceNumber to set
    */
   public void setPaymentReferenceNumber(String paymentReferenceNumber)
   {
      this.paymentReferenceNumber = paymentReferenceNumber;
   }

   /**
    * @return the authorizationCode
    */
   public String getAuthorizationCode()
   {
      return authorizationCode;
   }

   /**
    * @param authorizationCode the authorizationCode to set
    */
   public void setAuthorizationCode(String authorizationCode)
   {
      this.authorizationCode = authorizationCode;
   }

   /**
    * @return the cardDetails
    */
   public CardDetails getCardDetails()
   {
      return cardDetails;
   }

   /**
    * @param cardDetails the cardDetails to set
    */
   public void setCardDetails(CardDetails cardDetails)
   {
      this.cardDetails = cardDetails;
   }

}
