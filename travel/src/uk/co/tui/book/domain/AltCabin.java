/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2014 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package uk.co.tui.book.domain;


/**
 * The Class Cabin.
 *
 * @author anithamani.s
 */
public class AltCabin extends AltRoom {


    /** The deck number. */
    private String deckNumber;

    /**
     * Gets the deck number.
     *
     * @return the deckNumber
     */
    public String getDeckNumber() {
        return deckNumber;
    }

    /**
     * Sets the deck number.
     *
     * @param deckNumber the deckNumber to set
     */
    public void setDeckNumber(String deckNumber) {
        this.deckNumber = deckNumber;
    }

}
