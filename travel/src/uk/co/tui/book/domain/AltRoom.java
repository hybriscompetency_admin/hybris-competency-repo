/*
 *
 */
package uk.co.tui.book.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

/**
 * The Class Room.
 *
 * @author pushparaja.g
 */
public class AltRoom {

    /** The room index. */
    private String            roomIndex;

    /** The deposit. */
    private BigDecimal        deposit;

    /** The date. */
    private Date              date;

    /** The board basis. */
    private List<BoardBasis>  boardBasis;

    /** The occupancy. */
    private Occupancy         occupancy;

    /** The room details. */
    private RoomDetails       roomDetails;

    /** The qty available. */
    private int               qtyAvailable;

    /** The qty allocated. */
    private int               qtyAllocated;

    /** The total price. */
    private BigDecimal        totalPrice;

    /** The per person price. */
    private BigDecimal        perPersonPrice;

    /** The discount. */
    private BigDecimal        discount;

    /** The discount per person. */
    private BigDecimal        discountPerPerson;

    /** The is default. */
    private boolean           isDefault;

    /** The selected. */
    private boolean           selected;

    /** The upgrade price. */
    private BigDecimal        upgradePrice;

    /** The world care available. */
    private boolean           worldCareAvailable;

    /** The transfers available. */
    private boolean           transfersAvailable;

    /** The default extras price. */
    private BigDecimal        defaultExtrasPrice;

    /** The title of the room. */
    private String            title;

    /** The room features. */
    private List<RoomFeature> roomFeatures;

    /** The maximum occupancy of the room. */
    private int               maxOcc;

    /** The minimum occupancy of the room. */
    private int               minOcc;

    private BigDecimal        flightSupplement;

    public static final int   PRIME = 31;

    /**
     * Gets the deposit.
     *
     * @return the deposit
     */
    public BigDecimal getDeposit() {
        return deposit;
    }

    /**
     * Sets the deposit.
     *
     * @param deposit
     *            the new deposit
     */
    public void setDeposit(BigDecimal deposit) {
        this.deposit = deposit;
    }

    /**
     * Gets the date.
     *
     * @return the date
     */
    public Date getDate() {
        return (Date) date.clone();
    }

    /**
     * Sets the date.
     *
     * @param date
     *            the new date
     */
    public void setDate(Date date) {
        this.date = (Date) date.clone();
    }

    /**
     * Gets the board basis.
     *
     * @return the board basis
     */
    public List<BoardBasis> getBoardBasis() {
        return boardBasis;
    }

    /**
     * Sets the board basis.
     *
     * @param boardBasis
     *            the new board basis
     */
    public void setBoardBasis(List<BoardBasis> boardBasis) {
        this.boardBasis = boardBasis;
    }

    /**
     * Gets the occupancy.
     *
     * @return the occupancy
     */
    public Occupancy getOccupancy() {
        return occupancy;
    }

    /**
     * Sets the occupancy.
     *
     * @param occupancy
     *            the new occupancy
     */
    public void setOccupancy(Occupancy occupancy) {
        this.occupancy = occupancy;
    }

    /**
     * Gets the room details.
     *
     * @return the room details
     */
    public RoomDetails getRoomDetails() {
        return roomDetails;
    }

    /**
     * Sets the room details.
     *
     * @param roomDetails
     *            the new room details
     */
    public void setRoomDetails(RoomDetails roomDetails) {
        this.roomDetails = roomDetails;
    }

    /**
     * Gets the qty available.
     *
     * @return the qty available
     */
    public int getQtyAvailable() {
        return qtyAvailable;
    }

    /**
     * Sets the qty available.
     *
     * @param qtyAvailable
     *            the new qty available
     */
    public void setQtyAvailable(int qtyAvailable) {
        this.qtyAvailable = qtyAvailable;
    }

    /**
     * Gets the qty allocated.
     *
     * @return the qty allocated
     */
    public int getQtyAllocated() {
        return qtyAllocated;
    }

    /**
     * Sets the qty allocated.
     *
     * @param qtyAllocated
     *            the new qty allocated
     */
    public void setQtyAllocated(int qtyAllocated) {
        this.qtyAllocated = qtyAllocated;
    }

    /**
     * Gets the discount.
     *
     * @return the discount
     */
    public BigDecimal getDiscount() {
        return discount;
    }

    /**
     * Sets the discount.
     *
     * @param discount
     *            the new discount
     */
    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    /**
     * Gets the discount per person.
     *
     * @return the discount per person
     */
    public BigDecimal getDiscountPerPerson() {
        return discountPerPerson;
    }

    /**
     * Sets the discount per person.
     *
     * @param discountPerPerson
     *            the new discount per person
     */
    public void setDiscountPerPerson(BigDecimal discountPerPerson) {
        this.discountPerPerson = discountPerPerson;
    }

    /**
     * Checks if is default.
     *
     * @return true, if is default
     */
    public boolean isDefault() {
        return isDefault;
    }

    /**
     * Sets the default.
     *
     * @param isDefault
     *            the new default
     */
    public void setDefault(boolean isDefault) {
        this.isDefault = isDefault;
    }

    /**
     * Checks if is selected.
     *
     * @return true, if is selected
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * Sets the selected.
     *
     * @param selected
     *            the new selected
     */
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    /**
     * Gets the total price.
     *
     * @return the total price
     */
    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    /**
     * Sets the total price.
     *
     * @param totalPrice
     *            the new total price
     */
    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    /**
     * Gets the per person price.
     *
     * @return the per person price
     */
    public BigDecimal getPerPersonPrice() {
        return perPersonPrice;
    }

    /**
     * Sets the per person price.
     *
     * @param perPersonPrice
     *            the new per person price
     */
    public void setPerPersonPrice(BigDecimal perPersonPrice) {
        this.perPersonPrice = perPersonPrice;
    }

    /**
     * Sets the room index.
     *
     * @param roomIndex
     *            the roomIndex to set
     */
    public void setRoomIndex(String roomIndex) {
        this.roomIndex = roomIndex;
    }

    /**
     * Gets the room index.
     *
     * @return the roomIndex
     */
    public String getRoomIndex() {
        return roomIndex;
    }

    /**
     * Gets the upgrade price.
     *
     * @return the upgradePrice
     */
    public BigDecimal getUpgradePrice() {
        return upgradePrice;
    }

    /**
     * Sets the upgrade price.
     *
     * @param upgradePrice
     *            the upgradePrice to set
     */
    public void setUpgradePrice(BigDecimal upgradePrice) {
        this.upgradePrice = upgradePrice;
    }

    /**
     * Checks if is world care available.
     *
     * @return true, if is world care available
     */
    public boolean isWorldCareAvailable() {
        return worldCareAvailable;
    }

    /**
     * Sets the world care available.
     *
     * @param worldCareAvailable
     *            the new world care available
     */
    public void setWorldCareAvailable(boolean worldCareAvailable) {
        this.worldCareAvailable = worldCareAvailable;
    }

    /**
     * Checks if is transfers available.
     *
     * @return true, if is transfers available
     */
    public boolean isTransfersAvailable() {
        return transfersAvailable;
    }

    /**
     * Sets the transfers available.
     *
     * @param transfersAvailable
     *            the new transfers available
     */
    public void setTransfersAvailable(boolean transfersAvailable) {
        this.transfersAvailable = transfersAvailable;
    }

    /**
     * Gets the default extras price.
     *
     * @return the defaultExtrasPrice
     */
    public BigDecimal getDefaultExtrasPrice() {
        return defaultExtrasPrice;
    }

    /**
     * Sets the default extras price.
     *
     * @param defaultExtrasPrice
     *            the defaultExtrasPrice to set
     */
    public void setDefaultExtrasPrice(BigDecimal defaultExtrasPrice) {
        this.defaultExtrasPrice = defaultExtrasPrice;
    }

    /**
     * Gets the title.
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the title.
     *
     * @param title
     *            the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Gets the room features.
     *
     * @return the room features
     */
    public List<RoomFeature> getRoomFeatures() {
        return roomFeatures;
    }

    /**
     * Sets the room features.
     *
     * @param roomFeatures
     *            the new room features
     */
    public void setRoomFeatures(List<RoomFeature> roomFeatures) {
        this.roomFeatures = roomFeatures;
    }

    /**
     * @return the maxOcc
     */
    public int getMaxOcc() {
        return maxOcc;
    }

    /**
     * @param maxOcc
     *            the maxOcc to set
     */
    public void setMaxOcc(int maxOcc) {
        this.maxOcc = maxOcc;
    }

    /**
     * @return the minOcc
     */
    public int getMinOcc() {
        return minOcc;
    }

    /**
     * @param minOcc
     *            the minOcc to set
     */
    public void setMinOcc(int minOcc) {
        this.minOcc = minOcc;
    }

    /**
     * Equals.
     *
     * Checks is both the room codes are same
     *
     * @param room
     *            the room
     * @return true, if successful
     */
    @Override
    public boolean equals(Object room) {

        if (room == null) {
            return false;
        }
        if (room instanceof AltRoom) {
            // Checks if both the room codes are same & their corresponding selling codes
            return StringUtils.equalsIgnoreCase(this.getRoomDetails().getRoomCode(), ((AltRoom) room).getRoomDetails().getRoomCode())
                    && StringUtils.equalsIgnoreCase(this.getRoomDetails().getSellingCode(), ((AltRoom) room).getRoomDetails().getSellingCode());
        }
        return false;
    }

    /**
     * Hash code.
     *
     * @return the current object's hash code
     */
    @Override
    public int hashCode() {

        int roomCode = 0;
        int sellingCode = 0;
        if (roomDetails != null) {
            roomCode = roomDetails.getRoomCode() == null ? 0 : roomDetails.getRoomCode().hashCode();
            sellingCode = roomDetails.getSellingCode() == null ? 0 : roomDetails.getSellingCode().hashCode();
        }
        return PRIME * roomCode * sellingCode;
    }

    /**
     * @return the flightSupplement
     */
    public BigDecimal getFlightSupplement() {
        return flightSupplement;
    }

    /**
     * @param flightSupplement
     *            the flightSupplement to set
     */
    public void setFlightSupplement(BigDecimal flightSupplement) {
        this.flightSupplement = flightSupplement;
    }

}
