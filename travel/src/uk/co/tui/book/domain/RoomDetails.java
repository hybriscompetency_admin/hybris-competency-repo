/*
 *
 */
package uk.co.tui.book.domain;

/**
 * The Class RoomDetails.
 *
 * @author pushparaja.g
 */
public class RoomDetails {

    /** The brand. */
    private String brand;

    /** The room code. */
    private String roomCode;

    /** The selling code. */
    private String sellingCode;

    /** The package id. */
    private String packageId;

    /** The kids free. */
    private boolean freeChildPlace;

    /** The product code. */
    private String productCode;

    /** The sub product code. */
    private String subProductCode;

    /** The price sub product code. */
    private String primeSubProductCode;

    /** The accom code. */
    private String accomCode;

    /** The accom code. */
    private String promoCode;

    /**
     * Gets the brand.
     *
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * Sets the brand.
     *
     * @param brand
     *            the new brand
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     * Gets the room code.
     *
     * @return the room code
     */
    public String getRoomCode() {
        return roomCode;
    }

    /**
     * Sets the room code.
     *
     * @param roomCode
     *            the new room code
     */
    public void setRoomCode(String roomCode) {
        this.roomCode = roomCode;
    }

    /**
     * Gets the selling code.
     *
     * @return the selling code
     */
    public String getSellingCode() {
        return sellingCode;
    }

    /**
     * Sets the selling code.
     *
     * @param sellingCode
     *            the new selling code
     */
    public void setSellingCode(String sellingCode) {
        this.sellingCode = sellingCode;
    }

    /**
     * Gets the package id.
     *
     * @return the package id
     */
    public String getPackageId() {
        return packageId;
    }

    /**
     * Sets the package id.
     *
     * @param packageId
     *            the new package id
     */
    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    /**
     * Checks if is free child place.
     *
     * @return true, if checks if is free child place
     */
    public boolean isFreeChildPlace() {
        return freeChildPlace;
    }

    /**
     * Sets the free child place.
     *
     * @param freeChildPlace the free child place
     */
    public void setFreeChildPlace(final boolean freeChildPlace) {
        this.freeChildPlace = freeChildPlace;
    }

    /**
     * Gets the product code.
     *
     * @return the product code
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * Sets the product code.
     *
     * @param productCode
     *            the new product code
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    /**
     * Gets the sub product code.
     *
     * @return the sub product code
     */
    public String getSubProductCode() {
        return subProductCode;
    }

    /**
     * Sets the sub product code.
     *
     * @param subProductCode
     *            the new sub product code
     */
    public void setSubProductCode(String subProductCode) {
        this.subProductCode = subProductCode;
    }

    /**
     * Gets the prime sub product code.
     *
     * @return the prime sub product code
     */
    public String getPrimeSubProductCode() {
        return primeSubProductCode;
    }


    /**
     * Sets the prime sub product code.
     *
     * @param primeSubProductCode the new prime sub product code
     */
    public void setPrimeSubProductCode(String primeSubProductCode) {
        this.primeSubProductCode = primeSubProductCode;
    }

    /**
     * Gets the accom code.
     *
     * @return the accom code
     */
    public String getAccomCode() {
        return accomCode;
    }

    /**
     * Sets the accom code.
     *
     * @param accomCode
     *            the new accom code
     */
    public void setAccomCode(String accomCode) {
        this.accomCode = accomCode;
    }

    /**
     * @return the promoCode
     */
    public String getPromoCode() {
        return promoCode;
    }

    /**
     * @param promoCode the promoCode to set
     */
    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }
}
