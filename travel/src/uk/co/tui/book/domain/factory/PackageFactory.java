/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2013 hybris AG All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement you entered into with hybris.
 */
/**
 * @author samantha.gd
 */
package uk.co.tui.book.domain.factory;

import uk.co.tui.book.domain.lite.BackToBackCruisePackage;
import uk.co.tui.book.domain.lite.BasePackage;
import uk.co.tui.book.domain.lite.CruiseAndStayPackage;
import uk.co.tui.book.domain.lite.FlightOnlyPackage;
import uk.co.tui.book.domain.lite.FlyCruiseHolidayPackage;
import uk.co.tui.book.domain.lite.InclusivePackage;
import uk.co.tui.book.domain.lite.PackageType;
import uk.co.tui.book.domain.lite.StayAndCruisePackage;

/**
 * A factory for creating Package objects.
 */
public final class PackageFactory implements Cloneable
{

   /** The package factory. */
   private static PackageFactory PACKAGE_FACTORY;

   /**
    * Instantiates a new package factory.
    */
   private PackageFactory()
   {
      // Intentionally creating a private constructor
   }

   /**
    * Gets the single instance of PackageFactory.
    *
    * @return single instance of PackageFactory
    */
   public static synchronized PackageFactory getInstance()
   {
      if (PACKAGE_FACTORY == null)
      {
         synchronized (PackageFactory.class)
         {
            PACKAGE_FACTORY = new PackageFactory();
         }
      }
      return PACKAGE_FACTORY;
   }

   /**
    * Clone.
    *
    * @return the object
    * @throws CloneNotSupportedException the clone not supported exception
    */
   @Override
   public Object clone() throws CloneNotSupportedException
   {
      throw new CloneNotSupportedException();
   }

   /**
    * The Factory that creates different types of Package based on Package Type Enumeration.
    *
    * @param packageType the package type
    * @return the base package
    */
   public static synchronized BasePackage createPackage(PackageType packageType)
   {

      switch (packageType)
      {
         case INCLUSIVE:
            return new InclusivePackage();
         case FNC:
            return new FlyCruiseHolidayPackage();
         case CNS:
            return new CruiseAndStayPackage();
         case SNC:
            return new StayAndCruisePackage();
         case BTB:
            return new BackToBackCruisePackage();
         case FO:
            return new FlightOnlyPackage();
         default:
            return new InclusivePackage();
      }
   }

}
