/**
 *
 */
package uk.co.tui.book.passenger.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

import uk.co.tui.book.domain.lite.Passenger;
import uk.co.tui.book.domain.lite.PersonType;

/**
 * The Class PassengerUtils.
 *
 * @author extps3
 */
public final class PassengerUtils {

    /**
     * private constructor for utils class.
     */
    private PassengerUtils() {

    }

    /**
     * This method returns count of person type for given collection of
     * Passengers.
     *
     * @param passengers
     *            the passengers
     * @param types
     *            the types
     * @return int
     */
    public static int getPersonTypeCountFromPassengers(
        final Collection<Passenger> passengers,
        final Set<PersonType> types) {

    return CollectionUtils.countMatches(passengers, new Predicate() {
       @Override
       public boolean evaluate(Object passenger) {
       return types.contains(((Passenger) passenger).getType());
       }
   });
    }

    /**
     * This method returns count of person type for given collection of
     * Passengers.
     *
     * @param passengers
     *            the passengers
     * @param types
     *            the types
     * @return int
     */
    public static int getPersonTypeCount(
        final Collection<Passenger> passengers,
        final Set<PersonType> types) {

    return CollectionUtils.countMatches(passengers, new Predicate() {
       @Override
       public boolean evaluate(Object passenger) {
       return types.contains(((Passenger) passenger).getType());
       }
   });
    }

    /**
     * Gets the person type passengers from passengers.
     *
     * @param passengers
     *            the passengers
     * @param types
     *            the types
     * @return the person type passengers from passengers
     */
    @SuppressWarnings("unchecked")
    public static Collection<Passenger> getApplicablePassengers(
        final Collection<Passenger> passengers,
        final Set<PersonType> types) {

    return CollectionUtils.select(passengers, new Predicate() {
        @Override
        public boolean evaluate(final Object passenger) {
        return types.contains(((Passenger) passenger).getType());
        }
    });

    }

    /**
     * Gets the child ages.
     *
     * @param passengers
     *            the passengers
     * @return the child ages
     */
    public static List<Integer> getChildAges(
        final Collection<Passenger> passengers) {
    final List<Integer> childAges = new ArrayList<Integer>();
    for (Passenger passenger : getNonAdults(passengers)) {
        childAges.add(passenger.getAge());
    }
    return childAges;
    }

    /**
     * Gets the non adults such as child and infant pax.
     *
     * @param passengers
     *            the passengers
     * @return the non adults
     */
    @SuppressWarnings("unchecked")
    private static Collection<Passenger> getNonAdults(
        final Collection<Passenger> passengers) {
    final Set<PersonType> types = EnumSet.of(PersonType.CHILD,
        PersonType.INFANT);
    Collection<Passenger> nonAdultPaxs = null;
    nonAdultPaxs = (List<Passenger>) CollectionUtils.select(
        passengers, new Predicate() {
            @Override
            public boolean evaluate(final Object input) {
            return types.contains(Passenger.class.cast(input)
                .getType());
            }
        });
    return nonAdultPaxs;
    }

    /**
     * Gets the child ages without infant.
     *
     * @param passengers
     *            the passengers
     * @return the child ages
     */
    public static List<Integer> getChildAgesWithoutInfant(
        final Collection<Passenger> passengers) {
    final List<Integer> childAges = new ArrayList<Integer>();
    for (Passenger passenger : getChildren(passengers)) {
        childAges.add(passenger.getAge());
    }
    return childAges;
    }

    /**
     * Gets the children such as child and infant pax.
     *
     * @param passengers
     *            the passengers
     * @return the non adults
     */
    @SuppressWarnings("unchecked")
    private static Collection<Passenger> getChildren(
        final Collection<Passenger> passengers) {
    final Set<PersonType> types = EnumSet.of(PersonType.CHILD);
    Collection<Passenger> nonAdultPaxs = null;
    nonAdultPaxs = (List<Passenger>) CollectionUtils.select(
        passengers, new Predicate() {
            @Override
            public boolean evaluate(final Object input) {
            return types.contains(Passenger.class.cast(input)
                .getType());
            }
        });
    return nonAdultPaxs;
    }

    /**
     * Gets the chargeable pax count.
     *
     * @param passengers
     *            the passengers
     * @return the chargeable pax count
     */
    public static int getChargeablePaxCount(
        final Collection<Passenger> passengers) {
    return getPersonTypeCountFromPassengers(passengers, EnumSet.of(
        PersonType.ADULT, PersonType.TEEN,PersonType.CHILD, PersonType.SENIOR,
        PersonType.SUPERSENIOR));
    }

    /**
     * Gets the chargeable pax count.
     *
     * @param passengers
     *            the passengers
     * @return the chargeable pax count
     */
    public static int getChargeablePassengerCount(
        final Collection<Passenger> passengers) {
    return getPersonTypeCount(passengers, EnumSet.of(
        PersonType.ADULT,PersonType.TEEN, PersonType.CHILD, PersonType.SENIOR,
        PersonType.SUPERSENIOR));
    }

    /**
     * Gets the adult passengers required for extra.
     *
     * @param passengers
     *            the passengers
     * @param extraFacilityApplicableAge
     *            the extra facility applicable age
     * @return the adult passengers required for extra
     */
    public static Collection<Passenger> getAdultPassengersRequiredForExtra(
        final Collection<Passenger> passengers,
        final Integer extraFacilityApplicableAge) {
    Collection<Passenger> adults = getApplicablePassengers(passengers,
        EnumSet.of(PersonType.ADULT,PersonType.TEEN, PersonType.SENIOR,
            PersonType.SUPERSENIOR));
    for (final Passenger eachPassenger : passengers) {
        if (childToBeConsideredAsAdult(extraFacilityApplicableAge,
            eachPassenger)) {
        adults.add(eachPassenger);
        }
    }
    return adults;
    }

    /**
     * Gets the child passengers required for extra.
     *
     * @param passengers
     *            the passengers
     * @param extraFacilityApplicableAge
     *            the extra facility applicable age
     * @return the child passengers required for extra
     */
    public static Collection<Passenger> getChildPassengersRequiredForExtra(
        final Collection<Passenger> passengers,
        final Integer extraFacilityApplicableAge) {
    Collection<Passenger> children = getApplicablePassengers(
        passengers, EnumSet.of(PersonType.CHILD));
    Collection<Passenger> childrenTobeRemoved = new ArrayList<Passenger>();
    for (final Passenger eachPassenger : passengers) {
        if (childToBeConsideredAsAdult(extraFacilityApplicableAge,
            eachPassenger)) {
        childrenTobeRemoved.add(eachPassenger);
        }
    }
    children.removeAll(childrenTobeRemoved);
    return children;
    }

    /**
     * Gets the adult passenger count required for extra.
     *
     * @param passengers
     *            the passengers
     * @param extraFacilityApplicableAge
     *            the extra facility applicable age
     * @return the adult passenger count required for extra
     */
    public static int getAdultPassengerCountRequiredForExtra(
        final Collection<Passenger> passengers,
        final Integer extraFacilityApplicableAge) {
    int adultQuantity = getPersonTypeCountFromPassengers(passengers,
        EnumSet.of(PersonType.ADULT, PersonType.TEEN,PersonType.SENIOR,
            PersonType.SUPERSENIOR));
    for (final Passenger eachPassenger : passengers) {
        if (childToBeConsideredAsAdult(extraFacilityApplicableAge,
            eachPassenger)) {
        adultQuantity++;
        }
    }
    return adultQuantity;
    }

    /**
     * Gets the child passenger count required for extra.
     *
     * @param passengers
     *            the passengers
     * @param extraFacilityApplicableAge
     *            the extra facility applicable age
     * @return the child passenger count required for extra
     */
    public static int getChildPassengerCountRequiredForExtra(
        final Collection<Passenger> passengers,
        final Integer extraFacilityApplicableAge) {
    int childQuantity = getPersonTypeCountFromPassengers(passengers,
        EnumSet.of(PersonType.CHILD));
    for (final Passenger eachPassenger : passengers) {
        if (childToBeConsideredAsAdult(extraFacilityApplicableAge,
            eachPassenger)) {
        childQuantity--;
        }
    }
    return childQuantity;
    }

    /**
     * Child to be considered as adult.
     *
     * @param extraFacilityApplicableAge
     *            the extra facility applicable age
     * @param eachPassenger
     *            the each passenger
     * @return true, if successful
     */
    public static boolean childToBeConsideredAsAdult(
        final Integer extraFacilityApplicableAge,
        final Passenger eachPassenger) {
    int actualChildAge = 0;
    int permissibleChildAge = 0;
    if (eachPassenger.getType() == PersonType.CHILD) {
        actualChildAge = eachPassenger.getAge().intValue();
        permissibleChildAge = extraFacilityApplicableAge.intValue();
        return actualChildAge != 0 && actualChildAge > permissibleChildAge;
    }
    return false;
    }

    /**
     * Checks if is child is there in passengers.
     *
     * @param passengers
     *            the passengers
     * @return true, if is child in passengers
     */
    public static boolean isChildInPassengers(List<Passenger> passengers) {
        for (Passenger pax : passengers) {
            if (pax.getType() == PersonType.CHILD || pax.getType() == PersonType.INFANT) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if is child is there in passengers.
     *
     * @param passengers
     *            the passengers
     * @return true, if is child in passengers
     */
    public static boolean isChildInPassengersLite(List<Passenger> passengers) {
        for (Passenger pax : passengers) {
            if (pax.getType() == uk.co.tui.book.domain.lite.PersonType.CHILD || pax.getType() == uk.co.tui.book.domain.lite.PersonType.INFANT) {
                return true;
            }
        }
        return false;
    }
    /**
     * Gets the passengers List with given age rang.
     *
     * @param minPaxAge
     *            the min pax age
     * @param maxPaxAge
     *            the max pax age
     * @param passengers
     *            the passengers
     * @return the passengers with age rang
     */
    public static List<Passenger> getPassengersWithAgeRang(
        final Integer minPaxAge, final Integer maxPaxAge,
        final Collection<Passenger> passengers) {
    List<Passenger> applicablePaxList = new ArrayList<Passenger>();
    for (final Passenger eachPassenger : passengers) {
        if (passengerSatisfiesAgeRange(eachPassenger.getAge(), minPaxAge,
            maxPaxAge)) {
        applicablePaxList.add(eachPassenger);
        }
    }
    return applicablePaxList;
    }

    /**
     * Gets the passenger count with given age rang.
     *
     * @param minPaxAge
     *            the min pax age
     * @param maxPaxAge
     *            the max pax age
     * @param passengers
     *            the passengers
     * @return the passenger count with age rang
     */
    public static int getPassengerCountWithAgeRang(final Integer minPaxAge,
        final Integer maxPaxAge, final Collection<Passenger> passengers) {
    int applicablePaxCount = 0;
    for (final Passenger eachPassenger : passengers) {
        if (passengerSatisfiesAgeRange(eachPassenger.getAge(), minPaxAge,
            maxPaxAge)) {
        applicablePaxCount++;
        }
    }
    return applicablePaxCount;
    }

    /**
     * Gets the passenger count with given age rang.
     *
     * @param minPaxAge
     *            the min pax age
     * @param maxPaxAge
     *            the max pax age
     * @param passengers
     *            the passengers
     * @return the passenger count with age rang
     */
    public static int getPassengerCountWithAgeRangLite(final Integer minPaxAge,
        final Integer maxPaxAge, final Collection<Passenger> passengers) {
    int applicablePaxCount = 0;
    for (final Passenger eachPassenger : passengers) {
        if (passengerSatisfiesAgeRange(eachPassenger.getAge(), minPaxAge,
            maxPaxAge)) {
        applicablePaxCount++;
        }
    }
    return applicablePaxCount;
    }
    /**
     * Passenger satisfies age range.
     *
     * @param paxAge
     *            the pax age
     * @param minPaxAge
     *            the min pax age
     * @param maxPaxAge
     *            the max pax age
     * @return true, if successful
     */
    private static boolean passengerSatisfiesAgeRange(final Integer paxAge,
        final Integer minPaxAge, final Integer maxPaxAge) {
    return paxAge >= minPaxAge && paxAge <= maxPaxAge;
    }

    /**
     * Gets the passengers List with given age rang.
     *
     * @param minPaxAge
     *            the min pax age
     * @param maxPaxAge
     *            the max pax age
     * @param passengers
     *            the passengers
     * @return the passengers with age rang
     */
    public static List<String> getPassengersIdWithAgeRang(
        final Integer minPaxAge, final Integer maxPaxAge,
        final Collection<Passenger> passengers) {
    List<String> applicablePaxList = new ArrayList<String>();
    for (final Passenger eachPassenger : passengers) {
        if (passengerSatisfiesAgeRange(eachPassenger.getAge(), minPaxAge,
            maxPaxAge)) {
        applicablePaxList.add(eachPassenger.getId().toString());
        }
    }
    return applicablePaxList;
    }


    /**
     * Gets the passengers ids.
     *
     * @param passengers
     *            the passengers
     * @param types
     *            the types
     * @return the passengers ids
     */
    public static List<String> getPassengersIds(
        final Collection<Passenger> passengers,
        final Set<PersonType> types) {
    List<String> applicablePaxList = new ArrayList<String>();
    for (final Passenger eachPassenger : passengers) {
        if (types.contains(eachPassenger.getType())) {
        applicablePaxList.add(eachPassenger.getId().toString());
        }
    }
    return applicablePaxList;
    }

}


