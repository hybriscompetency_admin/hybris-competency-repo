/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.hybris.customb2b.storefront.controllers;


// FIXME - move to acceleratorstorefrontcommons ThirdPartyConstants
public interface B2BThirdPartyConstants
{
	interface TinyMCE
	{
		String META_ROBOTS = "metaRobots";
		String INDEX_FOLLOW = "index,follow";
		String INDEX_NOFOLLOW = "index,nofollow";
		String NOINDEX_FOLLOW = "noindex,follow";
		String NOINDEX_NOFOLLOW = "noindex,nofollow";
	}
}
