package com.hybris.customb2b.storefront.controllers.pages;


import de.hybris.platform.acceleratorfacades.order.AcceleratorCheckoutFacade;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCheckoutController.SelectOption;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2bacceleratorfacades.api.cart.CartFacade;
import de.hybris.platform.b2bacceleratorfacades.order.B2BOrderFacade;
import de.hybris.platform.b2bacceleratorservices.company.impl.DefaultB2BCommerceUserService;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.model.site.CMSSiteModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.order.data.CardTypeData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.TitleData;
import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.impex.jalo.Importer;
import de.hybris.platform.impex.jalo.imp.ImpExImportReader;
import de.hybris.platform.order.CartService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.impex.ImportConfig.ValidationMode;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.hybris.customb2b.storefront.controllers.ControllerConstants;
import com.hybris.customb2b.storefront.forms.AddressForm;
import com.hybris.customb2b.storefront.forms.PaymentDetailsForm;




@Controller
@RequestMapping(value = "/register/payment")
public class PaymentViewController extends AbstractPageController
{
	private static final String AMWAY_REGISTER_PAGE_LABEL = "amwayregister";
	private static final String REDIRECT_MY_ACCOUNT = REDIRECT_PREFIX + "/my-account";
	protected static final Map<String, String> cybersourceSopCardTypes = new HashMap<String, String>();

	@Resource(name = "checkoutFacade")
	private CheckoutFacade checkoutFacade;


	@ModelAttribute("billingCountries")
	public Collection<CountryData> getBillingCountries()
	{
		return checkoutFacade.getBillingCountries();
	}

	@ModelAttribute("cardTypes")
	public Collection<CardTypeData> getCardTypes()
	{
		return checkoutFacade.getSupportedCardTypes();
	}

	@ModelAttribute("titles")
	public Collection<TitleData> getTitles()
	{
		return getUserFacade().getTitles();
	}

	@Resource
	private SessionService sessionService;


	@Resource(name = "cmsSiteService")
	private CMSSiteService cmsSiteService;

	@Resource
	DefaultB2BCommerceUserService b2bCommerceUserService;

	@Resource
	private UserService userService;

	@Resource
	private CartService cartService;

	@SuppressWarnings("deprecation")
	@Resource(name = "b2bCartFacade")
	private CartFacade cartFacade;

	@Resource(name = "productService")
	private ProductService productService;

	@Resource(name = "acceleratorCheckoutFacade")
	private AcceleratorCheckoutFacade accCheckoutFacade;

	@Resource(name = "checkoutCustomerStrategy")
	private CheckoutCustomerStrategy checkoutCustomerStrategy;

	@Resource(name = "b2bOrderFacade")
	private B2BOrderFacade b2bOrderFacade;

	@Resource(name = "b2bProductFacade")
	private ProductFacade productFacade;

	@Resource(name = "modelService")
	private ModelService modelService;

	private static final String CHECKOUT_ORDER_CONFIRMATION_CMS_PAGE = "orderConfirmationPage";

	@Resource(name = "cmsPageService")
	private CMSPageService cmsPageService;

	@ModelAttribute("months")
	public List<SelectOption> getMonths()
	{
		final List<SelectOption> months = new ArrayList<SelectOption>();

		months.add(new SelectOption("1", "01"));
		months.add(new SelectOption("2", "02"));
		months.add(new SelectOption("3", "03"));
		months.add(new SelectOption("4", "04"));
		months.add(new SelectOption("5", "05"));
		months.add(new SelectOption("6", "06"));
		months.add(new SelectOption("7", "07"));
		months.add(new SelectOption("8", "08"));
		months.add(new SelectOption("9", "09"));
		months.add(new SelectOption("10", "10"));
		months.add(new SelectOption("11", "11"));
		months.add(new SelectOption("12", "12"));

		return months;
	}

	@ModelAttribute("startYears")
	public List<SelectOption> getStartYears()
	{
		final List<SelectOption> startYears = new ArrayList<SelectOption>();
		final Calendar calender = new GregorianCalendar();

		for (int i = calender.get(Calendar.YEAR); i > (calender.get(Calendar.YEAR) - 6); i--)
		{
			startYears.add(new SelectOption(String.valueOf(i), String.valueOf(i)));
		}

		return startYears;
	}

	@ModelAttribute("expiryYears")
	public List<SelectOption> getExpiryYears()
	{
		final List<SelectOption> expiryYears = new ArrayList<SelectOption>();
		final Calendar calender = new GregorianCalendar();

		for (int i = calender.get(Calendar.YEAR); i < (calender.get(Calendar.YEAR) + 11); i++)
		{
			expiryYears.add(new SelectOption(String.valueOf(i), String.valueOf(i)));
		}

		return expiryYears;
	}

	@RequestMapping(value = "/paymentview", method = RequestMethod.GET)
	public String enterStep(final Model model, final HttpServletRequest request) throws CMSItemNotFoundException
	{

		final String fName = request.getParameter("fname");
		final String lName = request.getParameter("lname");
		final String emailId = request.getParameter("mail");
		final String Title = request.getParameter("title");
		sessionService.setAttribute("fname", fName);
		sessionService.setAttribute("lname", lName);
		sessionService.setAttribute("mail", emailId);
		sessionService.setAttribute("title", Title);

		setupAddPaymentPage(model);
		/*
		 * final String fn = request.getParameter("fname"); final String ln = request.getParameter("lname");
		 */

		// If not using HOP or SOP we need to build up the payment details form
		final PaymentDetailsForm paymentDetailsForm = new PaymentDetailsForm();
		final AddressForm addressForm = new AddressForm();
		paymentDetailsForm.setBillingAddress(addressForm);
		model.addAttribute(paymentDetailsForm);

		return ControllerConstants.Views.Pages.Account.AddPaymentMethodPage;
	}



	protected void setupAddPaymentPage(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("metaRobots", "noindex,nofollow");
		/*
		 * * prepareDataForPage(model); model.addAttribute(WebConstants.BREADCRUMBS_KEY,
		 * getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.paymentMethod.breadcrumb")); final
		 * ContentPageModel contentPage = getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL);
		 * storeCmsPageInModel(model, contentPage); setUpMetaDataForContentPage(model, contentPage);
		 *
		 * }
		 *
		 *
		 * protected void prepareDataForPage(final Model model) throws CMSItemNotFoundException {
		 * model.addAttribute("isOmsEnabled", Boolean.valueOf(getSiteConfigService().getBoolean("oms.enabled", false)));
		 * model.addAttribute("supportedCountries", getCartFacade().getDeliveryCountries());
		 *
		 * }
		 */
		storeContentPageTitleInModel(model, "Amway Register Page");
		storeCmsPageInModel(model, getContentPageForLabelOrId(AMWAY_REGISTER_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(AMWAY_REGISTER_PAGE_LABEL));

	}










	/*
	 * @RequestMapping(value = "/paymentview", method = RequestMethod.POST) public void add(final Model model, final
	 * AddressForm addressForm) throws CMSItemNotFoundException { setupAddPaymentPage(model);
	 *
	 * final AddressData addressData = new AddressData(); if (addressForm != null) {
	 *
	 * addressData.setFirstName(addressForm.getFirstName()); addressData.setLastName(addressForm.getLastName());
	 * addressData.setTitle(addressForm.getTitleCode()); addressData.setEmail(addressForm.getEmailId());
	 *
	 *
	 * }
	 */


	/**
	 * Adds the kit to cart.
	 *
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the cMS item not found exception
	 */
	@RequestMapping(value = "/addKitToCart", method = RequestMethod.GET)
	public String addKitToCart(final Model model, final HttpServletRequest request) throws CMSItemNotFoundException
	{
		final String firstname = sessionService.getAttribute("fname");
		if (firstname == null || firstname.equals(""))
		{
			return REDIRECT_MY_ACCOUNT;
		}

		final ProductModel productModel = productService.getProductForCode("bo1");
		if (null != productModel)
		{
			cartFacade.addOrderEntry(getOrderEntryData(1, "bo1", null));
		}
		final ProductModel productModel1 = productService.getProductForCode("ha1");
		if (null != productModel1)
		{
			cartFacade.addOrderEntry(getOrderEntryData(1, "ha1", null));
		}
		String userId = "";
		try
		{
			userId = createUser();
		}
		catch (final ImpExException e1)
		{
			e1.printStackTrace();
		}
		//System.out.println("UserId is ",userId);
		cartService.changeCurrentCartUser(userService.getUserForUID(userId));

		/*
		 * boolean isPaymentUthorized = false; try { // isPaymentUthorized =
		 * accCheckoutFacade.authorizePayment(placeOrderForm.getSecurityCode()); isPaymentUthorized =
		 * accCheckoutFacade.authorizePayment("123"); } catch (final AdapterException ae) { // handle a case where a wrong
		 * paymentProvider configurations on the store see getCommerceCheckoutService().getPaymentProvider()
		 * //LOG.error(ae.getMessage(), ae); }
		 */
		/*
		 * if (!isPaymentUthorized) { GlobalMessages.addErrorMessage(model, "checkout.error.authorization.failed"); return
		 * enterStep(model, redirectModel); }
		 */

		OrderData orderData = null;
		try
		{
			orderData = accCheckoutFacade.placeOrder();
		}
		catch (final Exception e)
		{
			System.out.println("Failed to place Order" + e);
		}

		return redirectToOrderConfirmationPage(orderData, model, userId);
	}

	protected static final String REDIRECT_URL_ORDER_CONFIRMATION = REDIRECT_PREFIX + "/checkout/orderConfirmation/";

	protected String redirectToOrderConfirmationPage(final OrderData orderData, final Model model, final String userId)
	{
		//return REDIRECT_URL_ORDER_CONFIRMATION + orderData.getCode();

		final OrderData orderDetails = b2bOrderFacade.getOrderDetailsForCode(orderData.getCode());

		if (orderDetails.getEntries() != null && !orderDetails.getEntries().isEmpty())
		{
			for (final OrderEntryData entry : orderDetails.getEntries())
			{
				final String productCode = entry.getProduct().getCode();
				final ProductData product = productFacade.getProductForCodeAndOptions(productCode,
						Arrays.asList(ProductOption.BASIC, ProductOption.PRICE));
				entry.setProduct(product);
			}
		}


		model.addAttribute("orderCode", orderData.getCode());
		model.addAttribute("orderData", orderDetails);
		model.addAttribute("allItems", orderDetails.getEntries());
		model.addAttribute("deliveryAddress", orderDetails.getDeliveryAddress());
		model.addAttribute("deliveryMode", orderDetails.getDeliveryMode());
		model.addAttribute("paymentInfo", orderDetails.getPaymentInfo());
		model.addAttribute("email", userId);
		model.addAttribute("pageType", PageType.ORDERCONFIRMATION.name());
		model.addAttribute("metaRobots", "no-index,no-follow");
		AbstractPageModel cmsPage = null;
		try
		{
			cmsPage = getContentPageForLabelOrId(CHECKOUT_ORDER_CONFIRMATION_CMS_PAGE);
		}
		catch (final CMSItemNotFoundException e)
		{
			e.printStackTrace();
		}
		storeCmsPageInModel(model, cmsPage);

		return ControllerConstants.Views.Pages.Checkout.CheckoutConfirmationPage;
	}

	@Override
	protected ContentPageModel getContentPageForLabelOrId(final String labelOrId) throws CMSItemNotFoundException
	{
		String key = labelOrId;
		if (StringUtils.isEmpty(labelOrId))
		{
			// Fallback to site home page
			final ContentPageModel homePage = cmsPageService.getHomepage();
			if (homePage != null)
			{
				key = cmsPageService.getLabelOrId(homePage);
			}
			else
			{
				// Fallback to site start page label
				final CMSSiteModel site = cmsSiteService.getCurrentSite();
				if (site != null)
				{
					key = cmsSiteService.getStartPageLabelOrId(site);
				}
			}
		}

		// Actually resolve the label or id - running cms restrictions
		return cmsPageService.getPageForLabelOrId(key);
	}

	/**
	 * Gets the order entry data.
	 *
	 * @param quantity
	 *           the quantity
	 * @param productCode
	 *           the product code
	 * @param entryNumber
	 *           the entry number
	 * @return the order entry data
	 */
	protected OrderEntryData getOrderEntryData(final long quantity, final String productCode, final Integer entryNumber)
	{

		final OrderEntryData orderEntry = new OrderEntryData();
		orderEntry.setQuantity(quantity);
		orderEntry.setProduct(new ProductData());
		orderEntry.getProduct().setCode(productCode);
		orderEntry.setEntryNumber(entryNumber);

		return orderEntry;
	}


	protected String createUser() throws CMSItemNotFoundException, ImpExException
	{

		final String firstname = sessionService.getAttribute("fname").toString();
		final String lastname = sessionService.getAttribute("lname").toString();
		final String title = sessionService.getAttribute("title").toString();
		final String email = sessionService.getAttribute("mail").toString();

		String groups = "";
		String reportingOrg = "";

		final Random random = new Random();

		final int randomInteger = random.nextInt(100000);

		final B2BCustomerModel customer = b2bCommerceUserService.getCustomerForUid(sessionService.getAttribute("referrerId")
				.toString());
		final Set<PrincipalGroupModel> groupsSet = new HashSet<PrincipalGroupModel>(customer.getGroups());
		final List<PrincipalGroupModel> groupList = new ArrayList<PrincipalGroupModel>(groupsSet);
		//groups = groupList.get(0).getName();

		final B2BUnitModel reporting = customer.getDefaultB2BUnit();
		reportingOrg = reporting.getReportingOrganization().getName();

		for (final PrincipalGroupModel temp : groupList)
		{
			if (temp.getName() != null && !temp.getName().equals("") && !groups.equals(temp.getName())
					&& !temp.getName().contains("Premium Permissions"))
			{
				groups = groups + temp.getName() + ",";
			}

		}

		String description = "Unit " + randomInteger;
		final String b2b2Uid = "ABO" + randomInteger;
		String name = "ABO" + randomInteger;
		final String locName = "ABO" + randomInteger;

		try
		{
			// Create B2B Unit
			String impexCode = "INSERT_UPDATE B2BUnit;description;uid[unique=true];name;locName[lang=en];groups(uid);reportingOrganization(uid);Addresses(&addId);&B2BUnitID;accountManager(uid);creditLimit(code);approvalProcessCode;\n"
					+ ";"
					+ description
					+ ";"
					+ b2b2Uid
					+ ";"
					+ name
					+ ";"
					+ locName
					+ ";"
					+ groups
					+ ";"
					+ reportingOrg
					+ ";ABOAddrID" + randomInteger + ";ABOID" + randomInteger + ";";

			//Add Address Ref
			impexCode = impexCode
					+ "\n"
					+ "INSERT_UPDATE Address;streetname[unique=true];postalcode[unique=true];town;country(isocode);billingAddress;contactAddress;shippingAddress;unloadingAddress;firstname;lastname;email;title(code);&addId;owner(&B2BUnitID)[unique=true];\n"
					+ ";1000 Bagby Street;Texas;Houston;US;TRUE;TRUE;TRUE;TRUE;" + firstname + ";" + lastname + ";" + email
					+ ";mr;ABOAddrID" + randomInteger + ";ABOID" + randomInteger + ";";

			ImpExImportReader b2BReader = new ImpExImportReader(impexCode); //"++"

			Importer b2bImporter = new Importer(b2BReader);

			b2bImporter.getReader().setValidationMode(ValidationMode.STRICT.getCode());
			b2bImporter.importAll();

			// Create B2BCostCenter
			impexCode = "INSERT_UPDATE B2BCostCenter;code[unique=true];name[lang=en];Unit(uid);budgets(code);currency(isocode)[default='USD']\n"
					+ ";" + name + ";" + name + ";" + name + ";Monthly 4K USD;";

			b2BReader = new ImpExImportReader(impexCode); //"++"

			b2bImporter = new Importer(b2BReader);

			b2bImporter.getReader().setValidationMode(ValidationMode.STRICT.getCode());
			b2bImporter.importAll();

			// Create budget
			impexCode = "INSERT_UPDATE B2BBudget;code[unique=true];Unit(uid);budget;currency(isocode)[allownull=true];dateRange[dateformat=dd.MM.yyyy hh:mm:ss,allownull=true];name[lang=en];;\n"
					+ ";Monthly 4K USD;" + name + ";4000;USD;01.01.2010 00:00:00,12.31.2032 12:59:59;Monthly 4K USD;";

			b2BReader = new ImpExImportReader(impexCode); //"++"

			b2bImporter = new Importer(b2BReader);

			b2bImporter.getReader().setValidationMode(ValidationMode.STRICT.getCode());
			b2bImporter.importAll();

			//Create Customer
			description = "Customer " + randomInteger;
			name = firstname + " " + lastname;

			impexCode = "INSERT_UPDATE B2BCustomer;description;uid[unique=true];originalUid;email;name;title(code);groups(uid);permissionGroups(uid);sessionCurrency(isocode)[default='USD'];defaultB2BUnit(uid)\n"
					+ ";"
					+ description
					+ ";"
					+ email
					+ ";"
					+ email
					+ ";"
					+ email
					+ ";"
					+ name
					+ ";"
					+ "mr"
					+ ";"
					+ b2b2Uid
					+ ", b2badmingroup, b2bcustomergroup, premiumPermissions;;;" + b2b2Uid + ";;;";

			final ImpExImportReader customerReader = new ImpExImportReader(impexCode);

			final Importer customerImporter = new Importer(customerReader);

			customerImporter.getReader().setValidationMode(ValidationMode.STRICT.getCode());
			customerImporter.importAll();

			/*
			 * impexCode = "UPDATE B2BCustomer;originalUid[unique=true];uid[unique=true];permissions(code,Unit(uid));\n" +
			 * ";" + ";" + email + ";" + email + ";Rustic 3K USD MONTH:Rustic;";
			 * 
			 * customerReader = new ImpExImportReader(impexCode);
			 * 
			 * customerImporter = new Importer(customerReader);
			 * 
			 * customerImporter.getReader().setValidationMode(ValidationMode.STRICT.getCode());
			 * customerImporter.importAll();
			 */

			final UserModel userData = userService.getUserForUID(email);
			userService.setPassword(userData.getUid(), "1234");

			sessionService.removeAttribute("fname");
			sessionService.removeAttribute("lname");
			sessionService.removeAttribute("title");
			sessionService.removeAttribute("mail");

		}
		catch (final Exception e)
		{
			e.printStackTrace();
		}

		return email;
	}
}
