/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.hybris.customb2b.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.catalog.CatalogService;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;




/**
 * Login Controller. Handles login and register for the account flow.
 */
@Controller
@Scope("tenant")
public class RegisterPageController extends AbstractPageController
{

	private static final String AMWAY_REGISTER_PAGE_LABEL = "amwayregister";

	@Resource(name = "simpleBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "catalogService")
	private CatalogService catalogService;

	@Resource(name = "catalogVersionService")
	private CatalogVersionService catalogVersionService;

	@Resource
	private SessionService sessionService;

	@RequestMapping(value = "/amwayregister", method = RequestMethod.GET)
	public String showDutyFreePage(@RequestParam(value = "referrerId") final String referrerId,
			@RequestParam(value = "mailId") final String mailId, final Model model) throws CMSItemNotFoundException
	{
		sessionService.setAttribute("referrerId", referrerId);
		model.addAttribute("mailId", mailId);
		storeContentPageTitleInModel(model, "Amway Register Page");
		storeCmsPageInModel(model, getContentPageForLabelOrId(AMWAY_REGISTER_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(AMWAY_REGISTER_PAGE_LABEL));
		model.addAttribute("");
		return getViewForPage(model);
	}


}
