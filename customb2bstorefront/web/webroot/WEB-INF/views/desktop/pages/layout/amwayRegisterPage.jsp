<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<template:page pageTitle="${pageTitle}">
	<div id="globalMessages">
		<common:globalMessages />
	</div>

	<cms:pageSlot position="Section1" var="feature">
		<cms:component component="${feature}" element="div"
			class="span-24 section1 cms_disp-img_slot" />
	</cms:pageSlot>

	<div class="span-24 section2">
		<cms:pageSlot position="Section2A" var="feature" element="div"
			class="span-12 zone_a cms_disp-img_slot">
			<cms:component component="${feature}" />
		</cms:pageSlot>

		<cms:pageSlot position="Section2B" var="feature" element="div"
			class="span-6 zone_b thumbnail_detail">
			<cms:component component="${feature}" />
		</cms:pageSlot>

		<cms:pageSlot position="Section2C" var="feature" element="div"
			class="span-6 zone_c last thumbnail_detail">
			<cms:component component="${feature}" />
		</cms:pageSlot>
	</div>

	<cms:pageSlot position="Section3" var="feature" element="div"
		class="span-24 section3 cms_disp-img_slot">
		<cms:component component="${feature}" />
	</cms:pageSlot>

	<cms:pageSlot position="Section4" var="feature" element="div"
		class="span-24">
		<cms:component component="${feature}" element="div"
			class="span-6 section4 small_detail ${(elementPos%4 == 3) ? 'last' : ''}" />
	</cms:pageSlot>

	<cms:pageSlot position="Section5" var="feature" element="div"
		class="span-24 section5 cms_disp-img_slot">
		<cms:component component="${feature}" />
	</cms:pageSlot>


	<!-- <form action="/powertools/en/USD/register/payment/paymentview" method="get" id="form1">
		First name: <input type="text" name="fname"><br> Last
		name: <input type="text" name="lname"><br>
		<button type="submit" form="form1" value="Submit">Submit</button>
	</form> -->

<form action="/customb2bstorefront/customb2b/en/USD/register/payment/paymentview" method="get" id="form1">
	<div class="formBlock">
		<div class="headline">Applicant</div>
		
		<div class="control-group">
			<label class="control-label ">Title:<span class="skip"></span></label>
			<select name="title" required>
				<option value="select">Please select a title</option>
				<option value="mr">Mr.</option>
				<option value="miss">Miss</option>
				<option value="dr">Dr.</option>
	
			</select>
		</div>
		
		<div class="control-group">
			<label class="control-label ">Application Status:<spanclass="mandatory">*</span><span class="skip"></span></label>
			<div class="controls">
				<input type="text" class="formInput" name="as" />
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label ">First name:<span
				class="mandatory">*</span><span class="skip"></span></label>
			<div class="controls">
				<input type="text" class="formInput" name="fname" required />
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label ">Last name:<span
				class="mandatory">*</span><span class="skip"></span></label>
			<div class="controls">
				<input type="text" class="formInput" name="lname" />
			</div>
		</div>
		
		<div class="control-group">
			<label class="control-label ">Date of Birth:<span class="mandatory">*</span><span class="skip"></span></label>
			<div class="controls">
				<input type="text" class="formInput" name="dob" />
			</div>
		</div>
	
		<div class="control-group">
			<label class="control-label ">Name Desired On Id Card:<span class="mandatory">*</span><span class="skip"></span></label>
			<div class="controls">
				<input type="text" class="formInput" name="Noi" />
			</div>
		</div>
	</div>

	<div class="formBlock">
		<div class="headline">Mailing Information</div>
		<div class="control-group">
			<label class="control-label ">Address:<span class="mandatory">*</span><span class="skip"></span></label>
			<div class="controls">
				<input type="text" class="formInput" name="add" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label ">Address Line 2:<span class="mandatory">*</span><span class="skip"></span></label>
			<div class="controls">
				<input type="text" class="formInput" name="add2" />
			</div>
		</div>
	
		<div class="control-group">
			<label class="control-label ">Land Mark:<span class="mandatory">*</span><span class="skip"></span></label>
			<div class="controls">
				<input type="text" class="formInput" name="lm" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label ">City:<span class="mandatory">*</span><span class="skip"></span></label>
			<div class="controls">
				<input type="text" class="formInput" name="city" />
			</div>
		</div>
	
		<div class="control-group">
			<label class="control-label ">Post Office:<span class="mandatory">*</span><span class="skip"></span></label>
			<div class="controls">
				<input type="text" class="formInput" name="po" />
			</div>
		</div>
	
		<div class="control-group">
			<label class="control-label ">District:<span class="mandatory">*</span><span class="skip"></span></label>
			<div class="controls">
				<input type="text" class="formInput" name="district" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label ">Pin Code:<span class="mandatory">*</span><span class="skip"></span></label>
			<div class="controls">
				<input type="text" class="formInput" name="pincode" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label ">State:<span class="mandatory">*</span><span class="skip"></span></label>
			<div class="controls">
				<input type="text" class="formInput" name="state" />
			</div>
		</div>
	</div>
	
	<div class="formBlock">
		<div class="headline">Contact information</div>
		<div class="control-group">
			<label class="control-label ">Email:<span class="mandatory">*</span><span class="skip"></span></label>
			<div class="controls">
				<input type="text" class="formInput" value="${mailId}" name="mail" readonly/>
			</div>
		</div>
	
		<div class="control-group">
			<label class="control-label ">Re-Entry Email:<span class="mandatory">*</span><span class="skip"></span></label>
			<div class="controls">
				<input type="text" class="formInput" name="remail" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label ">Phone Number(Office):<span class="mandatory">*</span><span class="skip"></span></label>
			<div class="controls">
				<input type="text" class="formInput" name="on" />
			</div>
		</div>
		<div class="control-group">
			<label class="control-label ">Phone Number(Mobile):<span class="mandatory">*</span><span class="skip"></span></label>
			<div class="controls">
				<input type="text" class="formInput" name="mn" />
			</div>
		</div>
	</div>
	<button type="submit">Register</button>

</form>
</template:page>