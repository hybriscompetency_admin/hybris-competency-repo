package de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.checkout.steps;

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */


import de.hybris.platform.acceleratorfacades.payment.PaymentFacade;
import de.hybris.platform.acceleratorservices.customer.CustomerLocationService;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ContentPageBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutGroup;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ControllerConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCheckoutController;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.AddressValidator;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.PaymentDetailsValidator;
import de.hybris.platform.acceleratorstorefrontcommons.forms.verification.AddressVerificationResultHandler;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.TitleData;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;


public abstract class AbstractCheckoutStepController extends AbstractCheckoutController implements CheckoutStepController
{
	protected static final String MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL = "multiStepCheckoutSummary";
	protected static final String REDIRECT_URL_ADD_DELIVERY_ADDRESS = REDIRECT_PREFIX + "/checkout/multi/delivery-address/add";
	protected static final String REDIRECT_URL_CHOOSE_DELIVERY_METHOD = REDIRECT_PREFIX + "/checkout/multi/delivery-method/choose";
	protected static final String REDIRECT_URL_ADD_PAYMENT_METHOD = REDIRECT_PREFIX + "/checkout/multi/payment-method/add";
	protected static final String REDIRECT_URL_SUMMARY = REDIRECT_PREFIX + "/checkout/multi/summary/view";
	protected static final String REDIRECT_URL_CART = REDIRECT_PREFIX + "/cart";
	protected static final String REDIRECT_URL_ERROR = REDIRECT_PREFIX + "/checkout/multi/hop/error";

	@Resource(name = "paymentDetailsValidator")
	private PaymentDetailsValidator paymentDetailsValidator;

	@Resource(name = "accProductFacade")
	private ProductFacade productFacade;

	@Resource(name = "multiStepCheckoutBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

	@Resource(name = "paymentFacade")
	private PaymentFacade paymentFacade;

	@Resource(name = "addressValidator")
	private AddressValidator addressValidator;

	@Resource(name = "customerLocationService")
	private CustomerLocationService customerLocationService;

	@Resource(name = "cartFacade")
	private CartFacade cartFacade;

	@Resource(name = "addressVerificationResultHandler")
	private AddressVerificationResultHandler addressVerificationResultHandler;

	@Resource(name = "contentPageBreadcrumbBuilder")
	private ContentPageBreadcrumbBuilder contentPageBreadcrumbBuilder;

	@Resource(name = "checkoutFlowGroupMap")
	private Map<String, CheckoutGroup> checkoutFlowGroupMap;

	@Resource(name = "cartService")
	@Autowired
	private CartService cartService;

	@Resource(name = "commerceCartService")
	@Autowired
	private CommerceCartService commerceCartService;

	@Resource(name = "baseSiteService")
	@Autowired
	private BaseSiteService baseSiteService;

	@Resource(name = "userService")
	@Autowired
	private UserService userService;

	@ModelAttribute("titles")
	public Collection<TitleData> getTitles()
	{
		return getUserFacade().getTitles();
	}

	@ModelAttribute("countries")
	public Collection<CountryData> getCountries()
	{
		return getCheckoutFacade().getDeliveryCountries();
	}

	@ModelAttribute("countryDataMap")
	public Map<String, CountryData> getCountryDataMap()
	{
		final Map<String, CountryData> countryDataMap = new HashMap<String, CountryData>();
		for (final CountryData countryData : getCountries())
		{
			countryDataMap.put(countryData.getIsocode(), countryData);
		}
		return countryDataMap;
	}

	@ModelAttribute("checkoutSteps")
	public List<CheckoutSteps> addCheckoutStepsToModel()
	{
		final CheckoutGroup checkoutGroup = getCheckoutFlowGroupMap().get(getCheckoutFacade().getCheckoutFlowGroupForCheckout());
		final Map<String, CheckoutStep> progressBarMap = checkoutGroup.getCheckoutProgressBar();
		final List<CheckoutSteps> checkoutSteps = new ArrayList<CheckoutSteps>(progressBarMap.size());

		for (final Map.Entry<String, CheckoutStep> entry : progressBarMap.entrySet())
		{
			final CheckoutStep checkoutStep = entry.getValue();
			checkoutSteps.add(new CheckoutSteps(checkoutStep.getProgressBarId(), StringUtils.remove(checkoutStep.currentStep(),
					"redirect:"), Integer.valueOf(entry.getKey())));
		}
		return checkoutSteps;
	}

	protected void prepareDataForPage(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("isOmsEnabled", Boolean.valueOf(getSiteConfigService().getBoolean("oms.enabled", false)));
		model.addAttribute("supportedCountries", getCartFacade().getDeliveryCountries());
		model.addAttribute("expressCheckoutAllowed", Boolean.valueOf(getCheckoutFacade().isExpressCheckoutAllowedForCart()));
		model.addAttribute("taxEstimationEnabled", Boolean.valueOf(getCheckoutFacade().isTaxEstimationEnabledForCart()));
	}

	protected CheckoutStep getCheckoutStep(final String currentController)
	{
		final CheckoutGroup checkoutGroup = getCheckoutFlowGroupMap().get(getCheckoutFacade().getCheckoutFlowGroupForCheckout());
		return checkoutGroup.getCheckoutStepMap().get(currentController);
	}

	protected void setCheckoutStepLinksForModel(final Model model, final CheckoutStep checkoutStep)
	{
		model.addAttribute("previousStepUrl", StringUtils.remove(checkoutStep.previousStep(), "redirect:"));
		model.addAttribute("nextStepUrl", StringUtils.remove(checkoutStep.nextStep(), "redirect:"));
		model.addAttribute("currentStepUrl", StringUtils.remove(checkoutStep.currentStep(), "redirect:"));
		model.addAttribute("progressBarId", checkoutStep.getProgressBarId());
	}

	protected Map<String, String> getRequestParameterMap(final HttpServletRequest request)
	{
		final Map<String, String> map = new HashMap<String, String>();

		final Enumeration myEnum = request.getParameterNames();
		while (myEnum.hasMoreElements())
		{
			final String paramName = (String) myEnum.nextElement();
			final String paramValue = request.getParameter(paramName);
			map.put(paramName, paramValue);
		}

		return map;
	}

	public static class CheckoutSteps
	{
		private final String progressBarId;
		private final String url;
		private final Integer stepNumber;

		public CheckoutSteps(final String progressBarId, final String url, final Integer stepNumber)
		{
			this.progressBarId = progressBarId;
			this.url = url;
			this.stepNumber = stepNumber;
		}

		public String getProgressBarId()
		{
			return progressBarId;
		}

		public String getUrl()
		{
			return url;
		}

		public Integer getStepNumber()
		{
			return stepNumber;
		}
	}

	@Override
	protected CartFacade getCartFacade()
	{
		return cartFacade;
	}

	protected ProductFacade getProductFacade()
	{
		return productFacade;
	}

	protected PaymentDetailsValidator getPaymentDetailsValidator()
	{
		return paymentDetailsValidator;
	}

	protected ResourceBreadcrumbBuilder getResourceBreadcrumbBuilder()
	{
		return resourceBreadcrumbBuilder;
	}

	protected PaymentFacade getPaymentFacade()
	{
		return paymentFacade;
	}

	protected AddressValidator getAddressValidator()
	{
		return addressValidator;
	}

	protected CustomerLocationService getCustomerLocationService()
	{
		return customerLocationService;
	}

	protected AddressVerificationResultHandler getAddressVerificationResultHandler()
	{
		return addressVerificationResultHandler;
	}

	public ContentPageBreadcrumbBuilder getContentPageBreadcrumbBuilder()
	{
		return contentPageBreadcrumbBuilder;
	}

	public Map<String, CheckoutGroup> getCheckoutFlowGroupMap()
	{
		return checkoutFlowGroupMap;
	}

	protected void setUserCheckoutCartToSession(final String checkoutType) throws Exception
	{
		//final String checkoutType = getSessionService().getAttribute("checkoutType");

		if (StringUtils.isNotEmpty(checkoutType))
		{
			//final List<CartData> cartList = getCartFacade().getCartsForCurrentUser();
			CartModel sessionCartModel = getCartService().getSessionCart();

			if (!checkoutType.equals(sessionCartModel.getName()))
			{
				//				final List<CartModel> cartModelList = getCommerceCartService().getCartsForSiteAndUser(
				//						baseSiteService.getCurrentBaseSite(), getUserService().getCurrentUser());

				final List<CartModel> cartModelList = getSessionService().getAttribute("Users_Cart_List");

				if (org.apache.commons.collections.CollectionUtils.isNotEmpty(cartModelList)
						&& StringUtils.isNotEmpty(sessionCartModel.getName()))
				{
					CartModel cartModel = getMatchingCart(cartModelList, checkoutType);
					if (cartModelList.size() == 1)
					{
						cartModel = cartModelList.get(0);
					}
					if (cartModel != null)
					{
						cartService.setSessionCart(cartModel);
					}
					else
					{
						throw new Exception("Could not get Associated cart");
					}
				}
				sessionCartModel = cartService.getSessionCart();
				System.out.println("new cart" + sessionCartModel.getCode());
			}
		}
	}

	protected boolean isCombinedCheckout()
	{
		final String checkoutType = getSessionService().getAttribute("checkoutType");
		final boolean isCombinedCheckout = checkoutType.equals(ControllerConstants.Views.Fragments.UserCart.CombinedCart);
		final List<CartData> cartDataList = getCartFacade().getCartsForCurrentUser();
		if (cartDataList.size() == 1)
		{
			cartDataList.add(getCartFacade().getSessionCart());
		}
		if (isCombinedCheckout && CollectionUtils.isNotEmpty(cartDataList) && cartDataList.size() >= 2)
		{
			return true;
		}
		return false;
	}


	public CartModel getMatchingCart(final List<CartModel> cartModelList, final String cartName)
	{
		for (final CartModel cartModel : cartModelList)
		{
			if (cartModel.getName().equals(cartName))
			{
				return cartModel;
			}
		}

		return null;

	}

	public CartData getMatchingCartData(final List<CartData> cartDataList, final String cartName)
	{
		for (final CartData cartData : cartDataList)
		{
			if (cartData.getName().equals(cartName))
			{
				return cartData;
			}
		}

		return null;

	}



	public List<CartModel> getCartsForUser()
	{
		final List<CartModel> cartModelList = commerceCartService.getCartsForSiteAndUser(baseSiteService.getCurrentBaseSite(),
				userService.getCurrentUser());

		final List<CartModel> modifiableList = new ArrayList<CartModel>(cartModelList);

		Collections.sort(modifiableList, new Comparator()
		{

			public int compare(final Object arg0, final Object arg1)
			{
				if (!(arg0 instanceof CartModel))
				{
					return -1;
				}
				if (!(arg1 instanceof CartModel))
				{
					return -1;
				}

				final CartModel cart0 = (CartModel) arg0;
				final CartModel cart1 = (CartModel) arg1;


				// COMPARE NOW WHAT YOU WANT
				// Thanks to Steve Kuo for your comment!
				return Integer.parseInt(cart0.getCode()) - Integer.parseInt(cart1.getCode());
			}
		});

		final List<CartModel> newCartModelList = new ArrayList<CartModel>();

		for (int i = 0; i < 2 && cartModelList.size() > i; i++)
		{
			newCartModelList.add(cartModelList.get(i));
		}
		return newCartModelList;
	}



	public List<CartData> getUserCheckedOutCart()
	{

		final List<CartData> cartDataList = new ArrayList<CartData>();
		String checkoutType = "";
		if (isCombinedCheckout())
		{
			for (int i = 0; i < 2; i++)
			{
				switch (i)
				{
					case 0:
						checkoutType = ControllerConstants.Views.Fragments.UserCart.OutBoundCart;
						break;
					case 1:
						checkoutType = ControllerConstants.Views.Fragments.UserCart.InBoundCart;
						break;
				}

				try
				{
					setUserCheckoutCartToSession(checkoutType);
				}
				catch (final Exception e)
				{
					// YTODO Auto-generated catch block
					e.printStackTrace();
				}
				cartDataList.add(getCartFacade().getSessionCart());
			}
		}
		else
		{

			checkoutType = getSessionService().getAttribute("checkoutType");
			try
			{
				setUserCheckoutCartToSession(checkoutType);
			}
			catch (final Exception e)
			{
				// YTODO Auto-generated catch block
				e.printStackTrace();
			}
			cartDataList.add(getCartFacade().getSessionCart());
		}
		return cartDataList;
	}





	/**
	 * @return the cartService
	 */
	public CartService getCartService()
	{
		return cartService;
	}

	/**
	 * @param cartService
	 *           the cartService to set
	 */
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}

	/**
	 * @return the commerceCartService
	 */
	public CommerceCartService getCommerceCartService()
	{
		return commerceCartService;
	}

	/**
	 * @param commerceCartService
	 *           the commerceCartService to set
	 */
	public void setCommerceCartService(final CommerceCartService commerceCartService)
	{
		this.commerceCartService = commerceCartService;
	}

	/**
	 * @return the baseSiteService
	 */
	public BaseSiteService getBaseSiteService()
	{
		return baseSiteService;
	}

	/**
	 * @param baseSiteService
	 *           the baseSiteService to set
	 */
	public void setBaseSiteService(final BaseSiteService baseSiteService)
	{
		this.baseSiteService = baseSiteService;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}
}
