/**
 *
 */
package uk.co.tui.manage.populator;

import de.hybris.platform.commerceservices.converter.Populator;

import java.util.List;

import uk.co.travel.domain.manage.response.DisplayBookingResponse;
import uk.co.tui.book.domain.lite.ExtraFacility;
import uk.co.tui.book.domain.lite.ExtraFacilityCategory;
import uk.co.tui.book.domain.lite.ExtraFacilityGroup;
import uk.co.tui.book.domain.lite.PackageHoliday;


/**
 * @author venkatasuresh.t
 *
 */
public class InclusivePackageHolidayPopulator implements Populator<DisplayBookingResponse, PackageHoliday>
{


    public void populate(final DisplayBookingResponse source, final PackageHoliday target)
    {
        target.setDiscount(source.getDiscount());
        target.setInventory(source.getInventory());
        target.setPackageType(source.getPackageType());
        target.setPassengers(source.getPassengers());
        target.setCommunicationPreference(source.getCommunicationPreference());
        target.setMemos(source.getMemos());
        target.setItinerary(source.getItinerary());
        target.setBookingRefNum(source.getBookingRefNum());
        target.setDeposits(source.getDeposits());
        target.setPrice(source.getPrice());
        target.setPrices(source.getPrices());
        target.setHighLevelBookingType(source.getHighLevelBookingType());
        target.setBookingDetails(source.getBookingDetails());
        target.setExtraFacilityCategories(source.getExtraFacilityCategories());
        target.setStay(source.getStay());
        target.setPromotionalDiscount(source.getPromotionalDiscount());
      target.getExtraFacilityCategories().add(
         populateFlightCategories(source.getFlightExtraFacilities()));
      target.getExtraFacilityCategories().add(
         populatePackageCategories(source.getIntegratedExtraFacilities()));
    }

   /**
    * @param integratedExtraFacilities
    * @return extraCat
    */
   private ExtraFacilityCategory populatePackageCategories(
      List<ExtraFacility> integratedExtraFacilities)
   {
      ExtraFacilityCategory extraCat = new ExtraFacilityCategory();
      extraCat.setExtraFacilityGroup(ExtraFacilityGroup.PACKAGE);
      extraCat.setExtraFacilities(integratedExtraFacilities);
      return extraCat;
   }

   /**
    * @param flightExtraFacilities
    * @return extraCat
    */
   private ExtraFacilityCategory populateFlightCategories(
      List<ExtraFacility> flightExtraFacilities)
   {
      ExtraFacilityCategory extraCat = new ExtraFacilityCategory();
      extraCat.setExtraFacilityGroup(ExtraFacilityGroup.FLIGHT);
      extraCat.setExtraFacilities(flightExtraFacilities);
      return extraCat;
   }

}
