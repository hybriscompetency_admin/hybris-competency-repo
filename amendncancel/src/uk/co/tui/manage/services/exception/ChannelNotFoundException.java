/**
 *
 */
package uk.co.tui.manage.services.exception;

/**
 * @author veena.pn
 *
 */
public class ChannelNotFoundException extends TUIBusinessException
{

   /**
    * serialVersionUID
    */
   private static final long serialVersionUID = 1L;

   /**
    * @param code
    */
   public ChannelNotFoundException(final String code)
   {
      super(code, null, null);

   }

   /**
    * @return the serialversionuid
    */
   public static long getSerialversionuid()
   {
      return serialVersionUID;
   }

}
