/**
 *
 */
package uk.co.tui.manage.services.exception;



/**
 * @author premkumar.nd
 *
 */
public class TUIBusinessException extends Exception
{

    private final String message;

    /** The custom message. */
    private final String code;

    /** The nested cause. */
    private final Throwable nestedCause;

    /**
     * This constructor is used to set the
     *
     * error message from resource bundle based on the code
     *
     * @param message
    * @param thr YTODO
    * @param code YTODO
     */
    public TUIBusinessException(final String message, Throwable thr, String code)
    {
          super(thr);
          this.message = message;
          this.nestedCause=thr;
          this.code=code;
    }


    /**
     * This method is used to get the message
     *
     * @return the message
     */
    @Override
    public String getMessage()
    {
        return this.message;
    }

    /**
     * @return the code
     */
    public String getCode()
    {
        return code;
    }

    /**
     * @return the nestedCause
     */
    public Throwable getNestedCause()
    {
        return nestedCause;
    }

}
