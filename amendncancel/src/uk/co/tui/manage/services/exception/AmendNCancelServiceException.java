/**
 *
 */
package uk.co.tui.manage.services.exception;



/**
 * @author sumit.ks
 *
 */
public class AmendNCancelServiceException extends Exception
{

    /** The error code. */
    private final String errorCode;

    /** The custom message. */
    private final String customMessage;

    /** The nested cause. */
    private final Throwable nestedCause;

    /**
     * Constructor used Instantiate a new AmmendNCancelService exception with error code in case of bookflow related
     * failures.
     *
     * @param errorCode
     *           the error code
    * @param customMessage YTODO
    * @param nestedCause YTODO
     */
    public AmendNCancelServiceException(final String errorCode, String customMessage, Throwable nestedCause)
    {
        super(nestedCause);
        this.errorCode = errorCode;
        this.customMessage=customMessage;
        this.nestedCause=nestedCause;
    }

    /**
     * @return the errorCode
     */
    public String getErrorCode()
    {
        return errorCode;
    }

    /**
     * @return the customMessage
     */
    public String getCustomMessage()
    {
        return customMessage;
    }


    /**
     * @return the nestedCause
     */
    public Throwable getNestedCause()
    {
        return nestedCause;
    }


}
