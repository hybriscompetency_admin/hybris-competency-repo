/**
 *
 */
package uk.co.tui.manage.services.exception;


/**
 * @author premkumar.nd
 *
 */
public class BookingNotFoundException extends TUIBusinessException
{

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;



    /**
     * @param errorCode
     */
    public BookingNotFoundException(final String code)
    {
         super(code, null, null);


    }


    /**
     * @return the serialversionuid
     */
    public static long getSerialversionuid()
    {
        return serialVersionUID;
    }


}
