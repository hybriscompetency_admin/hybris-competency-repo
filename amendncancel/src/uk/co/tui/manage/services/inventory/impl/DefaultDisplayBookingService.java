/**
 *
 */
package uk.co.tui.manage.services.inventory.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Days;

import uk.co.travel.domain.manage.request.BookingSearchRequest;
import uk.co.travel.domain.manage.response.DisplayBookingResponse;
import uk.co.tui.book.domain.lite.BasePackage;
import uk.co.tui.book.domain.lite.Discount;
import uk.co.tui.book.domain.lite.Itinerary;
import uk.co.tui.book.domain.lite.Leg;
import uk.co.tui.book.domain.lite.PackageHoliday;
import uk.co.tui.book.domain.lite.Passenger;
import uk.co.tui.book.domain.lite.Price;
import uk.co.tui.book.domain.lite.PromotionalDiscount;
import uk.co.tui.book.domain.lite.Room;
import uk.co.tui.book.domain.lite.Schedule;
import uk.co.tui.book.domain.lite.Stay;
import uk.co.tui.book.services.PackageComponentService;
import uk.co.tui.inventoryservices.anite.service.AniteInventoryService;
import uk.co.tui.manage.populator.InclusivePackageHolidayPopulator;
import uk.co.tui.manage.services.exception.AmendNCancelServiceException;
import uk.co.tui.manage.services.exception.BookingNotFoundException;
import uk.co.tui.manage.services.exception.ChannelNotFoundException;
import uk.co.tui.manage.services.exception.TUIBusinessException;
import uk.co.tui.manage.services.inventory.DisplayBookingService;
import co.uk.tui.phoenix.book.aniteservices.exeption.AniteException;


/**
 * The Class DefaultDisplayBookingService.
 *
 * @author veena.pn
 */
public class DefaultDisplayBookingService implements DisplayBookingService
{

	/** The logger. */
	private static final Logger LOGGER = Logger.getLogger(DefaultDisplayBookingService.class);

	/** The display booking inventory service. */
	@Resource
	private AniteInventoryService<BookingSearchRequest, DisplayBookingResponse> displayBookingInventoryService;

	@Resource
	private InclusivePackageHolidayPopulator inclusivePackageHolidayPopulator;

	/** The Constant NO_BOOKING_FOUND_EXCEPTIONCODE. */
	private static final String NO_BOOKING_FOUND_EXCEPTIONCODE = "TIL-BIZ-ANI-E0755";

	/** The Constant BASIC_CARHIRE_RESPONSE_CODE. */
	private static final String BASIC_CARHIRE_RESPONSE_CODE = "CARH";

	/** The Constant BASIC_CARHIRE_TRACS_CODE. */
	private static final String BASIC_CARHIRE_TRACS_CODE = "ACR";

	/** The Constant TRANSPORT_GROUP. */
	private static final String TRANSPORT_GROUP = "Transport";

	/** The Constant CARHIRE_GROUP. */
	private static final String CARHIRE_GROUP = "Car Hire";

	private static final String CANCELLED_BOOKING = "Manage_My_Booking_Cancelled_Booking";

	private static final String BOOKING_NOT_FOUND = "Manage_My_Booking_Unable_To_Find";

	private static final String ANITE_EX = "Manage_My_Booking_Anite_Ex";

	private static final String DEFAULT_EX = "Manage_My_Booking_Default_Ex";

	private static final String CHANNEL_NOT_FOUND = "Manage_My_Booking_Channel_Not_Found";

	private static final int NUMBER_ELEVEN = 11;

	/** The package component service. */
	@Resource
	private PackageComponentService packageComponentService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see uk.co.tui.manage.services.inventory.DisplayBookingService#displayBooking (uk.co.travel.domain.manage.request.
	 * BookingSearchRequest)
	 */
	@Override
	public PackageHoliday displayBooking(final BookingSearchRequest bookingSearchRequest) throws AmendNCancelServiceException,
			TUIBusinessException
	{
		final PackageHoliday packageHoliday = new PackageHoliday();

		LOGGER.info("Entering Default DisplayBookingService ");
		final DisplayBookingResponse response = processRequest(bookingSearchRequest);

		if (null != response.getPrices())
		{
			response.setPromotionalDiscount(getPromotionalDiscount(response.getPrices()));
		}
		inclusivePackageHolidayPopulator.populate(response, packageHoliday);

		populateAdditionalDetails(packageHoliday);

		// packageCartService.calculate(packageHoliday);
		return packageHoliday;
	}

	/**
	 * @param bookingSearchRequest
	 * @return response
	 * @throws BookingNotFoundException
	 * @throws ChannelNotFoundException
	 * @throws AmendNCancelServiceException
	 * @throws TUIBusinessException
	 */
	private DisplayBookingResponse processRequest(final BookingSearchRequest bookingSearchRequest)
			throws AmendNCancelServiceException, TUIBusinessException
	{
		DisplayBookingResponse response = null;
		try
		{
			response = displayBookingInventoryService.sendRequest(bookingSearchRequest);
			validateBookingDetails(bookingSearchRequest, response);
			validateCancellationForBookings(response);
		}
		catch (final AniteException e)
		{
			throw new TUIBusinessException("Anite Exception", e, null);
		}
		return response;
	}

	/**
	 * @param bookingSearchRequest
	 * @param response
	 * @throws BookingNotFoundException
	 * @throws ChannelNotFoundException
	 */
	private void validateBookingDetails(final BookingSearchRequest bookingSearchRequest, final DisplayBookingResponse response)
			throws BookingNotFoundException, ChannelNotFoundException
	{
		if (!validateBookingCriteria(bookingSearchRequest, response))
		{
			throw new BookingNotFoundException("Booking Not Found!!");
		}

		if ((!(response.getBookingDetails().getBookingHistory().get(0).getAgent().getAgentID().equalsIgnoreCase("H6265")))
				&& (!(response.getBookingDetails().getBookingHistory().get(0).getAgent().getAgentID().equalsIgnoreCase("01075"))))

		{
			throw new ChannelNotFoundException("Channel Not Found!!");

		}
	}

	/**
	 * Populate additional details.
	 *
	 * @param packageHoliday
	 *           the package holiday
	 */
	private void populateAdditionalDetails(final BasePackage packageHoliday)
	{
		populateAdditionalFlightDetails(packageHoliday);
		populateAdditionalAccomDetails(packageHoliday);
		moveFlightPriceToFirstRoom(packageHoliday);
		packageHoliday.setDuration(calculateDuration(packageComponentService.getStay(packageHoliday)));
	}

	/**
	 * @param packageHoliday
	 */
	private void populateAdditionalAccomDetails(final BasePackage packageHoliday)
	{
		final List<Room> roomList = packageComponentService.getStay(packageHoliday).getRooms();
		populateDiscountForAllRooms(roomList);
		populateRoomUpgradePriceWithTotalRoomPrice(roomList);
	}

	/**
	 * populates roomUpgrade price with total price excluding discount
	 *
	 * @param roomList
	 */
	private void populateRoomUpgradePriceWithTotalRoomPrice(final List<Room> roomList)
	{
		for (final Room room : roomList)
		{
			final Price price = room.getPrice();
			final BigDecimal discount = calculateDiscount(room.getDiscounts());
			final BigDecimal amount = price.getAmount().getAmount().add(discount.negate());
			price.getAmount().setAmount(amount);
			room.setRoomUpgradePrice(price);
		}
	}

	/**
	 * gets the total discount
	 *
	 * @param discounts
	 */
	private BigDecimal calculateDiscount(final List<Discount> discounts)
	{
		BigDecimal sumOfDiscount = new BigDecimal(BigInteger.ZERO);
		for (final Discount discount : discounts)
		{
			sumOfDiscount = sumOfDiscount.add(discount.getPrice().getAmount().getAmount());
		}
		return sumOfDiscount;

	}

	/**
	 * Move flight price to first room(Acc to Bookflow).
	 *
	 * @param packageHoliday
	 *           the package holiday
	 */
	private void moveFlightPriceToFirstRoom(final BasePackage packageHoliday)
	{
		final Itinerary flightItinerary = packageComponentService.getFlightItinerary(packageHoliday);
		final List<Leg> inboundLegs = flightItinerary.getInBound();
		final List<Leg> outboundLegs = flightItinerary.getOutBound();
		final Stay stay = packageComponentService.getStay(packageHoliday);
		for (final Leg eachLeg : outboundLegs)
		{
			stay.getRooms().get(0).getPrices().addAll(eachLeg.getPriceList());
			eachLeg.setPriceList(new ArrayList());
		}
		for (final Leg eachLeg : inboundLegs)
		{
			stay.getRooms().get(0).getPrices().addAll(eachLeg.getPriceList());
			eachLeg.setPriceList(new ArrayList());
		}
	}

	/**
	 * @param packageHoliday
	 */
	private void populateAdditionalFlightDetails(final BasePackage packageHoliday)
	{
		populateDiscountForFlight(packageComponentService.getFlightItinerary(packageHoliday));
		populateArrivalDepartureTime(packageComponentService.getFlightItinerary(packageHoliday));
	}

	/**
	 * @param flightItinerary
	 */
	private void populateArrivalDepartureTime(final Itinerary flightItinerary)
	{
		final List<Leg> legs = new ArrayList<Leg>();
		legs.addAll(flightItinerary.getInBound());
		legs.addAll(flightItinerary.getOutBound());
		populateArrivalDepartureTimeForLegs(legs);
	}

	/**
	 * @param legs
	 */
	private void populateArrivalDepartureTimeForLegs(final List<Leg> legs)
	{
		final SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");
		for (final Leg leg : legs)
		{
			final Schedule schedule = leg.getSchedule();
			schedule.setArrivalTime(timeFormatter.format(schedule.getArrivalDate()));
			schedule.setDepartureTime(timeFormatter.format(schedule.getDepartureDate()));
		}
	}

	/**
	 * Populate discount for flight.
	 *
	 * @param itinerary
	 *           the itinerary
	 */
	private void populateDiscountForFlight(final Itinerary itinerary)
	{
		populateDiscountForLeg(itinerary.getInBound());
		populateDiscountForLeg(itinerary.getOutBound());
	}

	/**
	 * Populate discount for leg.
	 *
	 * @param legs
	 *           the legs
	 */
	private void populateDiscountForLeg(final List<Leg> legs)
	{
		for (final Leg leg : legs)
		{
			leg.setDiscounts(getDiscountsFromPrices(leg.getPriceList()));
		}
	}

	/**
	 * Populate discount for all rooms.
	 *
	 * @param rooms
	 *           the rooms
	 */
	private void populateDiscountForAllRooms(final List<Room> rooms)
	{
		for (final Room room : rooms)
		{
			room.setDiscounts(getDiscountsFromPrices(room.getPrices()));
		}
	}

	/**
	 * Gets the discounts from prices and also removes discount from list of prices
	 *
	 * @param prices
	 *           the prices
	 * @return the discounts from prices
	 */
	private List<Discount> getDiscountsFromPrices(final List<Price> prices)
	{
		// YIESUP,TODISC,RTDISC,ONDISC
		final List<String> discountCodes = Arrays.asList("YIESUP", "TODISC", "RTDISC", "ONDISC");
		final List<Discount> discounts = new ArrayList<Discount>();

		final Iterator<Price> iterator = prices.iterator();
		while (iterator.hasNext())
		{
			final Price price = iterator.next();
			if (discountCodes.contains(price.getPriceCodeType()))
			{
				final Discount discount = new Discount();
				final Price discountPrice = new Price();
				discountPrice.setCode("SAV");
				discountPrice.setAmount(price.getAmount());
				discountPrice.setRate(price.getRate());
				discount.setPrice(discountPrice);
				discounts.add(discount);
			}
		}
		return discounts;
	}

	/**
	 * Calculate duration.
	 *
	 * @param stay
	 *           the stay
	 * @return Integer
	 */
	private Integer calculateDuration(final Stay stay)
	{
		return Days.daysBetween(new DateTime(stay.getEndDate()), new DateTime(stay.getStartDate())).getDays();
	}

	/**
	 * Validate booking criteria.
	 *
	 * @param bookingSearchRequest
	 *           the booking search request
	 * @param response
	 *           the package holiday
	 * @return boolean
	 */
	private boolean validateBookingCriteria(final BookingSearchRequest bookingSearchRequest, final DisplayBookingResponse response)
	{
		return StringUtils
				.equalsIgnoreCase(bookingSearchRequest.getPaxSurName(), getLeadPassengerSurName(response.getPassengers()))
				&& validateDate(bookingSearchRequest.getDepartureDateFrom(), response.getItinerary().getOutBound().get(0)
						.getSchedule().getDepartureDate());
	}

	/**
	 * Compares dates.
	 *
	 * @param criteriaDepartureDate
	 *           the criteria departure date
	 * @param responseDepartureDate
	 *           the response departure date
	 * @return boolean
	 */
	private boolean validateDate(final Date criteriaDepartureDate, final Date responseDepartureDate)
	{
		final Calendar cal1 = Calendar.getInstance();
		cal1.setTime(criteriaDepartureDate);
		final Calendar cal2 = Calendar.getInstance();
		cal2.setTime(responseDepartureDate);

		return cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) && cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
				&& cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
	}

	/**
	 * Gets the lead passenger Sur name.
	 *
	 * @param passengers
	 *           the passengers
	 * @return string
	 */
	private String getLeadPassengerSurName(final List<Passenger> passengers)
	{
		for (final Passenger passenger : passengers)
		{
			if (passenger.isLead())
			{
				return passenger.getLastname();
			}
		}

		return null;
	}

	private void validateCancellationForBookings(final DisplayBookingResponse response) throws AmendNCancelServiceException,
			TUIBusinessException
	{
		if ("CANCELED".equalsIgnoreCase(response.getBookingStatus()))
		{
			throw new TUIBusinessException("Cancelled Booking", null, null);
		}
		if ("QUOTE".equalsIgnoreCase(response.getBookingStatus()))
		{
			throw new TUIBusinessException("Cancelled Booking", null, null);
		}
		if ("OPTION".equalsIgnoreCase(response.getBookingStatus()))
		{
			throw new TUIBusinessException("Cancelled Booking", null, null);
		}

	}

	private PromotionalDiscount getPromotionalDiscount(final List<Price> priceList)
	{
		// PTDISC
		PromotionalDiscount promotionalDiscount = null;
		final List<String> discountCodes = Arrays.asList("PTDISC");
		for (final Price price : priceList)
		{
			if (discountCodes.contains(price.getPriceCodeType()))
			{
				promotionalDiscount = new PromotionalDiscount();

				promotionalDiscount.setPrice(price);
				promotionalDiscount.setCode(price.getCode());
			}
		}
		return promotionalDiscount;
	}

}
