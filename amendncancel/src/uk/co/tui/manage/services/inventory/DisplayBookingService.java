package uk.co.tui.manage.services.inventory;

import uk.co.travel.domain.manage.request.BookingSearchRequest;
import uk.co.tui.book.domain.lite.PackageHoliday;
import uk.co.tui.manage.services.exception.AmendNCancelServiceException;
import uk.co.tui.manage.services.exception.TUIBusinessException;


/**
 * The Interface DisplayBookingService.
 *
 * @author veena.pn
 */
public interface DisplayBookingService
{

    /**
     * Display booking.
     *
     * @param bookingSearchRequest
     *           the booking search request
     * @return the package holiday
     *
     * @throws TUIBusinessException
     *            the tUI business exception
     * @throws AmendNCancelServiceException
     *            the amend n cancel service exception
     */
    PackageHoliday displayBooking(BookingSearchRequest bookingSearchRequest) throws TUIBusinessException,
            AmendNCancelServiceException;

}
