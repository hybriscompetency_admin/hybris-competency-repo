/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.sonata.travelretail.service.label;

import de.hybris.platform.btgcockpit.service.label.AbstractBTGItemCollectionLabelProvider;

import java.util.Collection;

import com.sonata.travelretail.core.model.FlightRouteModel;
import com.sonata.travelretail.core.model.operands.BTGReferenceProductRoutesInCartOperandModel;


/**
 *
 */
public class BTGReferenceProductRoutesInCartOperandLabelProvider extends AbstractBTGItemCollectionLabelProvider
{

	public BTGReferenceProductRoutesInCartOperandLabelProvider()
	{
		System.out.println("*******************  "
				+ "**************************** Bean BTGReferenceProductRoutesInCartOperandLabelProvider loaded");
	}

	@Override
	protected String getMessagePrefix()
	{
		return "Flight Routes";
	}

	protected Collection getItemObjectCollection(final BTGReferenceProductRoutesInCartOperandModel item)
	{
		return item.getProductFlightRoutes();
	}

	protected String getItemObjectName(final FlightRouteModel itemObject)
	{
		return itemObject.getSource() + " - " + itemObject.getDestination();
	}

	@Override
	protected Collection getItemObjectCollection(final Object obj)
	{

		return getItemObjectCollection((BTGReferenceProductRoutesInCartOperandModel) obj);
	}

	@Override
	protected String getItemObjectName(final Object obj)
	{
		return getItemObjectName((FlightRouteModel) obj);
	}


}
