package com.sonata.travelretail.btg.operands.valueproviders;

import de.hybris.platform.btg.condition.operand.valueproviders.AbstractNoRestrictionsOperandValueProvider;
import de.hybris.platform.btg.condition.operand.valueproviders.CollectionOperandValueProvider;
import de.hybris.platform.btg.enums.BTGConditionEvaluationScope;
import de.hybris.platform.core.model.user.UserModel;

import org.apache.commons.collections.CollectionUtils;

import com.sonata.travelretail.btg.condition.operand.types.ProductRouteSet;
import com.sonata.travelretail.core.model.FlightRouteModel;
import com.sonata.travelretail.core.model.operands.BTGReferenceProductRoutesInCartOperandModel;


/**
 * The Class ReferenceProductRoutesOperandValueProvider.
 */
public class ReferenceProductRoutesOperandValueProvider extends
		AbstractNoRestrictionsOperandValueProvider<BTGReferenceProductRoutesInCartOperandModel> implements
		CollectionOperandValueProvider<BTGReferenceProductRoutesInCartOperandModel>
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.btg.condition.operand.valueproviders.AbstractNoRestrictionsOperandValueProvider#doGetValue(
	 * de.hybris.platform.btg.model.BTGOperandModel, de.hybris.platform.core.model.user.UserModel,
	 * de.hybris.platform.btg.enums.BTGConditionEvaluationScope)
	 */
	@Override
	public Object doGetValue(final BTGReferenceProductRoutesInCartOperandModel operand, final UserModel user,
			final BTGConditionEvaluationScope evaluationScope)
	{
		//passs the Routes into ProductRouteSet Hash Set
		if (null != operand.getProductFlightRoutes() && CollectionUtils.isNotEmpty(operand.getProductFlightRoutes()))
		{
			return new ProductRouteSet(operand.getProductFlightRoutes());
		}
		return CollectionUtils.EMPTY_COLLECTION;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.btg.condition.operand.valueproviders.AbstractNoRestrictionsOperandValueProvider#getValueType
	 * (de.hybris.platform.btg.model.BTGOperandModel)
	 */
	@Override
	public Class getValueType(final BTGReferenceProductRoutesInCartOperandModel operand)
	{
		return ProductRouteSet.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.btg.condition.operand.valueproviders.CollectionOperandValueProvider#getAtomicValueType(de.hybris
	 * .platform.btg.model.BTGOperandModel)
	 */
	@Override
	public Class getAtomicValueType(final BTGReferenceProductRoutesInCartOperandModel operand)
	{
		return FlightRouteModel.class;
	}



}