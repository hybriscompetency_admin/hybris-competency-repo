package com.sonata.travelretail.btg.operands.valueproviders;

import de.hybris.platform.btg.condition.operand.valueproviders.AbstractCartOperandValueProvider;
import de.hybris.platform.btg.condition.operand.valueproviders.CollectionOperandValueProvider;
import de.hybris.platform.btg.enums.BTGConditionEvaluationScope;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.sonata.travelretail.btg.condition.operand.types.ProductRouteSet;
import com.sonata.travelretail.core.model.FlightRouteModel;
import com.sonata.travelretail.core.model.operands.BTGProductRoutesInCartOperandModel;



/**
 * The Class ProductRoutesOperandValueProvider.
 */
public class ProductRoutesOperandValueProvider extends AbstractCartOperandValueProvider implements
		CollectionOperandValueProvider<BTGProductRoutesInCartOperandModel>
{




	/**
	 * Instantiates a new product routes operand value provider.
	 */
	public ProductRoutesOperandValueProvider()
	{

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.btg.condition.operand.OperandValueProvider#getValue(de.hybris.platform.btg.model.BTGOperandModel
	 * , de.hybris.platform.core.model.user.UserModel, de.hybris.platform.btg.enums.BTGConditionEvaluationScope)
	 */
	@Override
	public Object getValue(final BTGProductRoutesInCartOperandModel operand, final UserModel user,
			final BTGConditionEvaluationScope evaluationScope)
	{
		return new ProductRouteSet(getCartProducts());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.platform.btg.condition.operand.OperandValueProvider#getValueType(de.hybris.platform.btg.model.
	 * BTGOperandModel)
	 */
	@Override
	public Class getValueType(final BTGProductRoutesInCartOperandModel operand)
	{
		return ProductRouteSet.class;
	}

	/**
	 * Gets the cart products.
	 *
	 * @return the cart products
	 */
	protected Collection<FlightRouteModel> getCartProducts()
	{
		final CartModel cart = cartService.getSessionCart();
		if (cart == null)
		{
			return null;
		}
		final Set<FlightRouteModel> result = new HashSet();
		for (final AbstractOrderEntryModel entry : cart.getEntries())
		{
			result.addAll(entry.getProduct().getFlightRoutes());
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.btg.condition.operand.valueproviders.CollectionOperandValueProvider#getAtomicValueType(de.hybris
	 * .platform.btg.model.BTGOperandModel)
	 */
	@Override
	public Class getAtomicValueType(final BTGProductRoutesInCartOperandModel paramT)
	{
		return FlightRouteModel.class;
	}

}
