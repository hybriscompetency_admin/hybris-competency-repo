/**
 *
 */
package com.sonata.travelretail.btg.condition.operand.types;

import java.util.Collection;
import java.util.HashSet;

import com.sonata.travelretail.core.model.FlightRouteModel;


/**
 * The Class ProductRouteSet.
 *
 * @author anjaneya.rb
 */
public class ProductRouteSet extends HashSet<FlightRouteModel>
{

	/**
	 * Instantiates a new product route set.
	 */
	public ProductRouteSet()
	{
	}

	/**
	 * Instantiates a new product route set.
	 *
	 * @param productModels
	 *           the product models
	 */
	public ProductRouteSet(final Collection<? extends FlightRouteModel> flightRouteModels)
	{
		super(flightRouteModels);
	}

	/**
	 * Instantiates a new product route set.
	 *
	 * @param initialCapacity
	 *           the initial capacity
	 * @param loadFactor
	 *           the load factor
	 */
	public ProductRouteSet(final int initialCapacity, final float loadFactor)
	{
		super(initialCapacity, loadFactor);
	}

	/**
	 * Instantiates a new product route set.
	 *
	 * @param initialCapacity
	 *           the initial capacity
	 */
	public ProductRouteSet(final int initialCapacity)
	{
		super(initialCapacity);
	}
}
