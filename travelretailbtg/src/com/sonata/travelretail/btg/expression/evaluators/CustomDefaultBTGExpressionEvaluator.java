/**
 *
 */
package com.sonata.travelretail.btg.expression.evaluators;

import de.hybris.platform.btg.condition.ConditionEvaluationException;
import de.hybris.platform.btg.condition.ExpressionEvaluator;
import de.hybris.platform.btg.condition.ExpressionEvaluatorRegistry;
import de.hybris.platform.btg.condition.impl.DefaultBTGExpressionEvaluator;
import de.hybris.platform.btg.condition.operand.OperandValueProvider;
import de.hybris.platform.btg.condition.operand.factory.OperandValueProviderNotFoundException;
import de.hybris.platform.btg.condition.operand.factory.OperandValueProviderRegistry;
import de.hybris.platform.btg.dao.BTGDao;
import de.hybris.platform.btg.enums.BTGConditionEvaluationScope;
import de.hybris.platform.btg.enums.BTGResultScope;
import de.hybris.platform.btg.invalidation.BTGInvalidationDataContainer;
import de.hybris.platform.btg.model.BTGConditionModel;
import de.hybris.platform.btg.model.BTGConditionResultModel;
import de.hybris.platform.btg.model.BTGExpressionModel;
import de.hybris.platform.btg.model.BTGOperandModel;
import de.hybris.platform.btg.model.BTGOperatorModel;
import de.hybris.platform.btg.model.BTGSegmentModel;
import de.hybris.platform.btg.services.impl.BTGEvaluationContext;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.Config;

import org.springframework.beans.factory.annotation.Required;


/**
 * The Class CustomDefaultBTGExpressionEvaluator.
 * 
 * @author anjaneya.rb
 */
public class CustomDefaultBTGExpressionEvaluator extends DefaultBTGExpressionEvaluator
{

	/**
	 * Instantiates a new custom default btg expression evaluator.
	 */
	public CustomDefaultBTGExpressionEvaluator()
	{

	}

	/** The expression evaluator registry. */
	private ExpressionEvaluatorRegistry expressionEvaluatorRegistry;

	/** The operand value provider registry. */
	private OperandValueProviderRegistry operandValueProviderRegistry;

	/** The model service. */
	private ModelService modelService;

	/** The btg dao. */
	private BTGDao btgDao;

	/** The invalidation data container. */
	private BTGInvalidationDataContainer<BTGConditionResultModel> invalidationDataContainer;

	/** The store first negative result. */
	private boolean storeFirstNegativeResult = Config.getBoolean("btg.storefirstnegativeresult", true);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.btg.condition.impl.DefaultBTGExpressionEvaluator#earlyEvaluate(de.hybris.platform.core.model
	 * .user.UserModel, de.hybris.platform.btg.model.BTGConditionModel,
	 * de.hybris.platform.btg.services.impl.BTGEvaluationContext, de.hybris.platform.btg.model.BTGConditionResultModel)
	 */
	@Override
	protected Boolean earlyEvaluate(final UserModel user, final BTGConditionModel condition, final BTGEvaluationContext context,
			final BTGConditionResultModel lastConditionResult)
	{
		if (lastConditionResult != null)
		{
			if ((lastConditionResult.isFulfilled()) && (condition.getRule() != null) && (condition.getRule().getSegment() != null)
					&& (BTGResultScope.PERMANENT.equals(context.getResultScope(condition.getRule().getSegment(), user))))
			{
				if (this.invalidationDataContainer.isInvalidated(lastConditionResult))
				{
					this.invalidationDataContainer.setInvalidated(lastConditionResult, false);
				}
				return Boolean.TRUE;
			}
			if ((!this.invalidationDataContainer.isInvalidated(lastConditionResult)) && (lastConditionResult.isFulfilled()))
			{
				return Boolean.TRUE;
			}
			if (lastConditionResult.isForced())
			{
				return Boolean.valueOf(lastConditionResult.isFulfilled());
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.platform.btg.condition.impl.DefaultBTGExpressionEvaluator#evaluate(de.hybris.platform.btg.model.
	 * BTGExpressionModel, de.hybris.platform.core.model.user.UserModel,
	 * de.hybris.platform.btg.services.impl.BTGEvaluationContext)
	 */
	@Override
	public boolean evaluate(final BTGExpressionModel condition, final UserModel user, final BTGEvaluationContext context)
			throws ConditionEvaluationException
	{
		final BTGSegmentModel segment = condition.getRule().getSegment();

		final BTGConditionResultModel lastConditionResult = this.btgDao.getLastResult(user, condition,
				context.getResultScope(segment, user), context.getJaloSessionId());

		final Boolean earlyEval = earlyEvaluate(user, condition, context, lastConditionResult);
		if (earlyEval != null)
		{
			return earlyEval.booleanValue();
		}
		final BTGOperandModel leftOperand = condition.getLeftOperand();
		final BTGOperandModel rightOperand = condition.getRightOperand();
		final BTGOperatorModel operator = condition.getOperator();
		Class type;
		Object leftOperandValue;
		Object rightOperandValue;
		try
		{
			final BTGConditionEvaluationScope scope = condition.getEvaluationScope();
			final OperandValueProvider leftOperandValueProvider = this.operandValueProviderRegistry
					.getOperandValueProvider(leftOperand.getClass());
			leftOperandValue = leftOperandValueProvider.getValue(leftOperand, user, scope);
			final OperandValueProvider rightOperandValueProvider = this.operandValueProviderRegistry
					.getOperandValueProvider(rightOperand.getClass());
			rightOperandValue = rightOperandValueProvider.getValue(rightOperand, user, scope);
			type = leftOperandValueProvider.getValueType(leftOperand);
		}
		catch (final OperandValueProviderNotFoundException e)
		{
			throw new ConditionEvaluationException("Error evaluating condition: " + e.getMessage(), e);
		}

		final ExpressionEvaluator expEval = this.expressionEvaluatorRegistry.getExpressionEvaluator(type);

		final boolean result = expEval.evaluate(leftOperandValue, operator.getCode(), rightOperandValue);

		setConditionResultAfterEvaluation(user, condition, context.getResultScope(segment, user), result,
				context.getJaloSessionId(), false, lastConditionResult);

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.btg.condition.impl.DefaultBTGExpressionEvaluator#setModelService(de.hybris.platform.servicelayer
	 * .model.ModelService)
	 */
	@Override
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.btg.condition.impl.DefaultBTGExpressionEvaluator#setBtgDao(de.hybris.platform.btg.dao.BTGDao)
	 */
	@Override
	@Required
	public void setBtgDao(final BTGDao btgDao)
	{
		this.btgDao = btgDao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.btg.condition.impl.DefaultBTGExpressionEvaluator#setBtgInvalidationDataContainer(de.hybris.
	 * platform.btg.invalidation.BTGInvalidationDataContainer)
	 */
	@Override
	@Required
	public void setBtgInvalidationDataContainer(
			final BTGInvalidationDataContainer<BTGConditionResultModel> invalidationDataContainer)
	{
		this.invalidationDataContainer = invalidationDataContainer;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.btg.condition.impl.DefaultBTGExpressionEvaluator#setConditionResult(de.hybris.platform.core
	 * .model.user.UserModel, de.hybris.platform.btg.model.BTGExpressionModel,
	 * de.hybris.platform.btg.enums.BTGResultScope, boolean, java.lang.String, boolean)
	 */
	@Override
	public void setConditionResult(final UserModel user, final BTGExpressionModel condition, final BTGResultScope resultScope,
			final boolean newResult, final String jaloSessionId, final boolean forced)
	{
		BTGResultScope scope = resultScope;
		if (scope == null)
		{
			scope = condition.getRule().getSegment().getDefaultResultScope();
		}
		final BTGConditionResultModel conditionResult = this.btgDao.getLastResult(user, condition, resultScope, jaloSessionId);
		if ((!this.storeFirstNegativeResult) && (conditionResult == null) && (!newResult))
		{
			return;
		}
		final boolean conditionResultChanged = this.btgDao.isConditionResultChanged(user, condition, resultScope, newResult,
				jaloSessionId);
		if (conditionResultChanged)
		{
			this.btgDao.createConditionResult(user, condition, scope, newResult, jaloSessionId, forced);
		}
		else
		{
			updateExistingConditionResult(conditionResult, forced);
		}
	}

	/**
	 * Sets the condition result after evaluation.
	 * 
	 * @param user
	 *           the user
	 * @param condition
	 *           the condition
	 * @param resultScope
	 *           the result scope
	 * @param result
	 *           the result
	 * @param jaloSessionId
	 *           the jalo session id
	 * @param forced
	 *           the forced
	 * @param lastConditionResult
	 *           the last condition result
	 */
	private void setConditionResultAfterEvaluation(final UserModel user, final BTGExpressionModel condition,
			final BTGResultScope resultScope, final boolean result, final String jaloSessionId, final boolean forced,
			final BTGConditionResultModel lastConditionResult)
	{
		BTGResultScope scope = resultScope;
		if (scope == null)
		{
			scope = condition.getRule().getSegment().getDefaultResultScope();
		}
		if ((!this.storeFirstNegativeResult) && (lastConditionResult == null) && (!result))
		{
			return;
		}
		final boolean conditionResultChanged = (lastConditionResult == null) || (lastConditionResult.isFulfilled() != result);
		if (conditionResultChanged)
		{
			this.btgDao.createConditionResult(user, condition, scope, result, jaloSessionId, forced);
		}
		else
		{
			updateExistingConditionResult(lastConditionResult, forced);
		}
	}

	/**
	 * Update existing condition result.
	 * 
	 * @param conditionResult
	 *           the condition result
	 * @param forced
	 *           the forced
	 */
	private void updateExistingConditionResult(final BTGConditionResultModel conditionResult, final boolean forced)
	{
		boolean mustSave = false;
		if (conditionResult.isForced() != forced)
		{
			conditionResult.setForced(forced);
			mustSave = true;
		}
		if (this.invalidationDataContainer.isInvalidated(conditionResult))
		{
			this.invalidationDataContainer.setInvalidated(conditionResult, false);
		}
		if (mustSave)
		{
			this.modelService.save(conditionResult);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.btg.condition.impl.DefaultBTGExpressionEvaluator#setExpressionEvaluatorRegistry(de.hybris.platform
	 * .btg.condition.ExpressionEvaluatorRegistry)
	 */
	@Override
	public void setExpressionEvaluatorRegistry(final ExpressionEvaluatorRegistry expressionEvaluatorRegistry)
	{
		this.expressionEvaluatorRegistry = expressionEvaluatorRegistry;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.btg.condition.impl.DefaultBTGExpressionEvaluator#checkConditionForUser(de.hybris.platform.core
	 * .model.user.UserModel, de.hybris.platform.btg.model.BTGConditionModel,
	 * de.hybris.platform.btg.services.impl.BTGEvaluationContext)
	 */
	@Override
	public BTGConditionResultModel checkConditionForUser(final UserModel user, final BTGConditionModel condition,
			final BTGEvaluationContext context)
	{
		final BTGSegmentModel segment = condition.getRule().getSegment();
		final BTGResultScope resultScope = context.getResultScope(segment, user);
		return this.btgDao.getLastResult(user, condition, resultScope, context.getJaloSessionId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.btg.condition.impl.DefaultBTGExpressionEvaluator#setOperandValueProviderRegistry(de.hybris.
	 * platform.btg.condition.operand.factory.OperandValueProviderRegistry)
	 */
	@Override
	public void setOperandValueProviderRegistry(final OperandValueProviderRegistry operandValueProviderRegistry)
	{
		this.operandValueProviderRegistry = operandValueProviderRegistry;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.btg.condition.impl.DefaultBTGExpressionEvaluator#invalidateCondition(de.hybris.platform.core
	 * .model.user.UserModel, de.hybris.platform.btg.model.BTGConditionModel,
	 * de.hybris.platform.btg.services.impl.BTGEvaluationContext)
	 */
	@Override
	public boolean invalidateCondition(final UserModel user, final BTGConditionModel condition, final BTGEvaluationContext context)
	{
		boolean ret = false;

		final BTGSegmentModel segment = condition.getRule().getSegment();
		final BTGConditionResultModel conditionResult = this.btgDao.getLastResult(user, condition,
				context.getResultScope(segment, user), context.getJaloSessionId());
		if (conditionResult != null)
		{
			this.invalidationDataContainer.setInvalidated(conditionResult, true);
			ret = true;
		}
		else
		{
			ret = true;
		}
		return ret;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.platform.btg.condition.impl.DefaultBTGExpressionEvaluator#setStoreFirstNegativeResult(boolean)
	 */
	@Override
	public void setStoreFirstNegativeResult(final boolean storeFirstNegativeResult)
	{
		this.storeFirstNegativeResult = storeFirstNegativeResult;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.platform.btg.condition.impl.DefaultBTGExpressionEvaluator#isStoreFirstNegativeResult()
	 */
	@Override
	public boolean isStoreFirstNegativeResult()
	{
		return this.storeFirstNegativeResult;
	}

}
