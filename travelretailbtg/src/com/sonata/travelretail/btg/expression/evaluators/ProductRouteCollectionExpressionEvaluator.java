package com.sonata.travelretail.btg.expression.evaluators;

import de.hybris.platform.btg.condition.impl.AbstractCollectionDistinctExpressionEvaluator;

import java.util.Map;
import java.util.Set;

import com.sonata.travelretail.core.model.FlightRouteModel;


/**
 * The Class ProductRouteCollectionExpressionEvaluator.
 */
public class ProductRouteCollectionExpressionEvaluator extends AbstractCollectionDistinctExpressionEvaluator<FlightRouteModel>
{

	/** The Constant NUMBER_OF_MATCHED_DISTINCT_ITEMS_QUERY. */
	private static final String NUMBER_OF_MATCHED_DISTINCT_ITEMS_QUERY = "SELECT DISTINCT {p.pk} FROM {Flightroute AS p}, {Flightroute AS p2} WHERE {p.pk} IN (?right) AND {p2.pk} IN (?left) AND {p.routecode} = {p2.routecode}";

	/** The Constant NUMBER_OF_DISTINCT_ITEMS_QUERY. */
	private static final String NUMBER_OF_DISTINCT_ITEMS_QUERY = "SELECT DISTINCT {p.routecode}as code  FROM {Flightroute AS p}WHERE {p.pk} IN (?left)";

	/**
	 * Instantiates a new product route collection expression evaluator.
	 */
	public ProductRouteCollectionExpressionEvaluator()
	{
		System.out.println("((((((((((((( ProductRouteCollectionExpressionEvaluator )))))))))))))))))))))))");
	}

	/**
	 * Instantiates a new product route collection expression evaluator.
	 *
	 * @param operatorMap
	 *           the operator map
	 */
	public ProductRouteCollectionExpressionEvaluator(final Map<String, Set<Class>> operatorMap)
	{
		super(operatorMap);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.platform.btg.condition.impl.AbstractCollectionDistinctExpressionEvaluator#
	 * getNumberOfMatchedDistinctItemsQuery()
	 */
	@Override
	protected String getNumberOfMatchedDistinctItemsQuery()
	{
		return "SELECT DISTINCT {p.pk} FROM {Flightroute AS p}, {Flightroute AS p2} WHERE {p.pk} IN (?right) AND {p2.pk} IN (?left) AND {p.routecode} = {p2.routecode}";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.btg.condition.impl.AbstractCollectionDistinctExpressionEvaluator#getNumberOfDistinctItemsQuery
	 * ()
	 */
	@Override
	protected String getNumberOfDistinctItemsQuery()
	{
		return "SELECT DISTINCT {p.routecode}as code  FROM {Flightroute AS p}WHERE {p.pk} IN (?left)";
	}
}
