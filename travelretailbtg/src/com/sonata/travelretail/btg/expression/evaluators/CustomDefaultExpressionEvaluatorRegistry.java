/**
 *
 */
package com.sonata.travelretail.btg.expression.evaluators;

import de.hybris.platform.btg.condition.ExpressionEvaluator;
import de.hybris.platform.btg.condition.impl.CompositeExpressionEvaluator;
import de.hybris.platform.btg.condition.impl.DefaultExpressionEvaluatorRegistry;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;


/**
 * The Class CustomDefaultExpressionEvaluatorRegistry.
 * 
 * @author anjaneya.rb
 */
public class CustomDefaultExpressionEvaluatorRegistry extends DefaultExpressionEvaluatorRegistry
{

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(DefaultExpressionEvaluatorRegistry.class);



	/** The expression evaluators. */
	private Map<Class, ExpressionEvaluator> expressionEvaluators;



	/**
	 * Instantiates a new custom default expression evaluator registry.
	 */
	public CustomDefaultExpressionEvaluatorRegistry()
	{
		expressionEvaluators = new HashMap();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.btg.condition.impl.DefaultExpressionEvaluatorRegistry#getExpressionEvaluator(java.lang.Class)
	 */
	@Override
	public ExpressionEvaluator getExpressionEvaluator(final Class type)
	{
		return expressionEvaluators.get(type);
	}



	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.btg.condition.impl.DefaultExpressionEvaluatorRegistry#addExpressionEvaluator(de.hybris.platform
	 * .btg.condition.ExpressionEvaluator)
	 */
	@Override
	public void addExpressionEvaluator(final ExpressionEvaluator evaluator)
	{
		final Class type = evaluator.getLeftType();


		if (expressionEvaluators.containsKey(type))
		{

			final ExpressionEvaluator existingExpEval = expressionEvaluators.get(type);



			if ((existingExpEval instanceof CompositeExpressionEvaluator))
			{

				((CompositeExpressionEvaluator) existingExpEval).addExpressionEvaluator(evaluator);



			}
			else if (!existingExpEval.getClass().equals(evaluator.getClass()))
			{




				final CompositeExpressionEvaluator compositeExpEval = new CompositeExpressionEvaluator(type);
				compositeExpEval.addExpressionEvaluator(existingExpEval);
				compositeExpEval.addExpressionEvaluator(evaluator);
				expressionEvaluators.put(type, compositeExpEval);
			}
			else
			{
				LOG.warn("Ignoring " + evaluator.getClass().getSimpleName() + ": was already added to this registry");

			}


		}
		else
		{

			expressionEvaluators.put(type, evaluator);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.btg.condition.impl.DefaultExpressionEvaluatorRegistry#setExpressionEvaluators(java.util.Map)
	 */
	@Override
	public void setExpressionEvaluators(final Map<Class, ExpressionEvaluator> expressionEvaluators)
	{
		this.expressionEvaluators = expressionEvaluators;
	}
}
