/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Aug 26, 2016 11:25:11 AM                    ---
 * ----------------------------------------------------------------
 */
package com.sonata.travelretail.core.jalo.operands;

import com.sonata.travelretail.constants.TravelretailbtgConstants;
import com.sonata.travelretail.core.jalo.FlightRoute;
import de.hybris.platform.btg.jalo.BTGAbstractReferenceOperand;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.util.Utilities;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Generated class for type {@link com.sonata.travelretail.core.jalo.operands.BTGReferenceProductRoutesInCartOperand BTGReferenceProductRoutesInCartOperand}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedBTGReferenceProductRoutesInCartOperand extends BTGAbstractReferenceOperand
{
	/** Qualifier of the <code>BTGReferenceProductRoutesInCartOperand.productFlightRoutes</code> attribute **/
	public static final String PRODUCTFLIGHTROUTES = "productFlightRoutes";
	/** Relation ordering override parameter constants for BTGReferenceOperandToProductsRoutes from ((travelretailbtg))*/
	protected static String BTGREFERENCEOPERANDTOPRODUCTSROUTES_SRC_ORDERED = "relation.BTGReferenceOperandToProductsRoutes.source.ordered";
	protected static String BTGREFERENCEOPERANDTOPRODUCTSROUTES_TGT_ORDERED = "relation.BTGReferenceOperandToProductsRoutes.target.ordered";
	/** Relation disable markmodifed parameter constants for BTGReferenceOperandToProductsRoutes from ((travelretailbtg))*/
	protected static String BTGREFERENCEOPERANDTOPRODUCTSROUTES_MARKMODIFIED = "relation.BTGReferenceOperandToProductsRoutes.markmodified";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>(BTGAbstractReferenceOperand.DEFAULT_INITIAL_ATTRIBUTES);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>BTGReferenceProductRoutesInCartOperand.productFlightRoutes</code> attribute.
	 * @return the productFlightRoutes
	 */
	public Collection<FlightRoute> getProductFlightRoutes(final SessionContext ctx)
	{
		final List<FlightRoute> items = getLinkedItems( 
			ctx,
			true,
			TravelretailbtgConstants.Relations.BTGREFERENCEOPERANDTOPRODUCTSROUTES,
			"FlightRoute",
			null,
			false,
			false
		);
		return items;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>BTGReferenceProductRoutesInCartOperand.productFlightRoutes</code> attribute.
	 * @return the productFlightRoutes
	 */
	public Collection<FlightRoute> getProductFlightRoutes()
	{
		return getProductFlightRoutes( getSession().getSessionContext() );
	}
	
	public long getProductFlightRoutesCount(final SessionContext ctx)
	{
		return getLinkedItemsCount(
			ctx,
			true,
			TravelretailbtgConstants.Relations.BTGREFERENCEOPERANDTOPRODUCTSROUTES,
			"FlightRoute",
			null
		);
	}
	
	public long getProductFlightRoutesCount()
	{
		return getProductFlightRoutesCount( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>BTGReferenceProductRoutesInCartOperand.productFlightRoutes</code> attribute. 
	 * @param value the productFlightRoutes
	 */
	public void setProductFlightRoutes(final SessionContext ctx, final Collection<FlightRoute> value)
	{
		setLinkedItems( 
			ctx,
			true,
			TravelretailbtgConstants.Relations.BTGREFERENCEOPERANDTOPRODUCTSROUTES,
			null,
			value,
			false,
			false,
			Utilities.getMarkModifiedOverride(BTGREFERENCEOPERANDTOPRODUCTSROUTES_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>BTGReferenceProductRoutesInCartOperand.productFlightRoutes</code> attribute. 
	 * @param value the productFlightRoutes
	 */
	public void setProductFlightRoutes(final Collection<FlightRoute> value)
	{
		setProductFlightRoutes( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to productFlightRoutes. 
	 * @param value the item to add to productFlightRoutes
	 */
	public void addToProductFlightRoutes(final SessionContext ctx, final FlightRoute value)
	{
		addLinkedItems( 
			ctx,
			true,
			TravelretailbtgConstants.Relations.BTGREFERENCEOPERANDTOPRODUCTSROUTES,
			null,
			Collections.singletonList(value),
			false,
			false,
			Utilities.getMarkModifiedOverride(BTGREFERENCEOPERANDTOPRODUCTSROUTES_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to productFlightRoutes. 
	 * @param value the item to add to productFlightRoutes
	 */
	public void addToProductFlightRoutes(final FlightRoute value)
	{
		addToProductFlightRoutes( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from productFlightRoutes. 
	 * @param value the item to remove from productFlightRoutes
	 */
	public void removeFromProductFlightRoutes(final SessionContext ctx, final FlightRoute value)
	{
		removeLinkedItems( 
			ctx,
			true,
			TravelretailbtgConstants.Relations.BTGREFERENCEOPERANDTOPRODUCTSROUTES,
			null,
			Collections.singletonList(value),
			false,
			false,
			Utilities.getMarkModifiedOverride(BTGREFERENCEOPERANDTOPRODUCTSROUTES_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from productFlightRoutes. 
	 * @param value the item to remove from productFlightRoutes
	 */
	public void removeFromProductFlightRoutes(final FlightRoute value)
	{
		removeFromProductFlightRoutes( getSession().getSessionContext(), value );
	}
	
}
