/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Aug 26, 2016 11:25:11 AM                    ---
 * ----------------------------------------------------------------
 */
package com.sonata.travelretail.jalo;

import com.sonata.travelretail.constants.TravelretailbtgConstants;
import com.sonata.travelretail.core.jalo.operands.BTGProductRoutesInCartOperand;
import com.sonata.travelretail.core.jalo.operands.BTGReferenceProductRoutesInCartOperand;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.JaloSystemException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.extension.Extension;
import de.hybris.platform.jalo.link.Link;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.jalo.type.JaloGenericCreationException;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type <code>TravelretailbtgManager</code>.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedTravelretailbtgManager extends Extension
{
	protected static final Map<String, Map<String, AttributeMode>> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, Map<String, AttributeMode>> ttmp = new HashMap();
		DEFAULT_INITIAL_ATTRIBUTES = ttmp;
	}
	@Override
	public Map<String, AttributeMode> getDefaultAttributeModes(final Class<? extends Item> itemClass)
	{
		Map<String, AttributeMode> ret = new HashMap<>();
		final Map<String, AttributeMode> attr = DEFAULT_INITIAL_ATTRIBUTES.get(itemClass.getName());
		if (attr != null)
		{
			ret.putAll(attr);
		}
		return ret;
	}
	
	public BTGProductRoutesInCartOperand createBTGProductRoutesInCartOperand(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( TravelretailbtgConstants.TC.BTGPRODUCTROUTESINCARTOPERAND );
			return (BTGProductRoutesInCartOperand)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating BTGProductRoutesInCartOperand : "+e.getMessage(), 0 );
		}
	}
	
	public BTGProductRoutesInCartOperand createBTGProductRoutesInCartOperand(final Map attributeValues)
	{
		return createBTGProductRoutesInCartOperand( getSession().getSessionContext(), attributeValues );
	}
	
	public BTGReferenceProductRoutesInCartOperand createBTGReferenceProductRoutesInCartOperand(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( TravelretailbtgConstants.TC.BTGREFERENCEPRODUCTROUTESINCARTOPERAND );
			return (BTGReferenceProductRoutesInCartOperand)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating BTGReferenceProductRoutesInCartOperand : "+e.getMessage(), 0 );
		}
	}
	
	public BTGReferenceProductRoutesInCartOperand createBTGReferenceProductRoutesInCartOperand(final Map attributeValues)
	{
		return createBTGReferenceProductRoutesInCartOperand( getSession().getSessionContext(), attributeValues );
	}
	
	@Override
	public String getName()
	{
		return TravelretailbtgConstants.EXTENSIONNAME;
	}
	
}
