/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Aug 26, 2016 11:25:11 AM                    ---
 * ----------------------------------------------------------------
 */
package com.sonata.travelretail.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedTravelretailbtgConstants
{
	public static final String EXTENSIONNAME = "travelretailbtg";
	public static class TC
	{
		public static final String BTGPRODUCTROUTESINCARTOPERAND = "BTGProductRoutesInCartOperand".intern();
		public static final String BTGREFERENCEPRODUCTROUTESINCARTOPERAND = "BTGReferenceProductRoutesInCartOperand".intern();
	}
	public static class Attributes
	{
		// no constants defined.
	}
	public static class Relations
	{
		public static final String BTGREFERENCEOPERANDTOPRODUCTSROUTES = "BTGReferenceOperandToProductsRoutes".intern();
	}
	
	protected GeneratedTravelretailbtgConstants()
	{
		// private constructor
	}
	
	
}
