/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.sonata.travelretail.initialdata.setup;

import de.hybris.platform.commerceservices.dataimport.impl.CoreDataImportService;
import de.hybris.platform.commerceservices.dataimport.impl.SampleDataImportService;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.commerceservices.setup.data.ImportData;
import de.hybris.platform.commerceservices.setup.events.CoreDataImportedEvent;
import de.hybris.platform.commerceservices.setup.events.SampleDataImportedEvent;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.sonata.travelretail.initialdata.constants.TravelretailInitialDataConstants;


/**
 * This class provides hooks into the system's initialization and update processes.
 *
 * @see "https://wiki.hybris.com/display/release4/Hooks+for+Initialization+and+Update+Process"
 */
@SystemSetup(extension = TravelretailInitialDataConstants.EXTENSIONNAME)
public class InitialDataSystemSetup extends AbstractSystemSetup
{
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(InitialDataSystemSetup.class);

	private static final String IMPORT_CORE_DATA = "importCoreData";
	private static final String IMPORT_SAMPLE_DATA = "importSampleData";
	private static final String ACTIVATE_SOLR_CRON_JOBS = "activateSolrCronJobs";
	public static final String TRAVELRETAIL = "travelretail";
	public static final String CUSTOMB2B = "customb2b";

	private CoreDataImportService coreDataImportService;
	private SampleDataImportService sampleDataImportService;

	/**
	 * Generates the Dropdown and Multi-select boxes for the project data import
	 */
	@Override
	@SystemSetupParameterMethod
	public List<SystemSetupParameter> getInitializationOptions()
	{
		final List<SystemSetupParameter> params = new ArrayList<SystemSetupParameter>();

		params.add(createBooleanSystemSetupParameter(IMPORT_CORE_DATA, "Import Core Data", true));
		params.add(createBooleanSystemSetupParameter(IMPORT_SAMPLE_DATA, "Import Sample Data", true));
		params.add(createBooleanSystemSetupParameter(ACTIVATE_SOLR_CRON_JOBS, "Activate Solr Cron Jobs", true));
		// Add more Parameters here as you require

		return params;
	}

	/**
	 * Implement this method to create initial objects. This method will be called by system creator during
	 * initialization and system update. Be sure that this method can be called repeatedly.
	 *
	 * @param context
	 *           the context provides the selected parameters and values
	 */
	@SystemSetup(type = Type.ESSENTIAL, process = Process.ALL)
	public void createEssentialData(final SystemSetupContext context)
	{
		// Add Essential Data here as you require
	}

	/**
	 * Implement this method to create data that is used in your project. This method will be called during the system
	 * initialization. <br>
	 * Add import data for each site you have configured
	 *
	 * <pre>
	 * final List&lt;ImportData&gt; importData = new ArrayList&lt;ImportData&gt;();
	 * 
	 * final ImportData sampleImportData = new ImportData();
	 * sampleImportData.setProductCatalogName(SAMPLE_PRODUCT_CATALOG_NAME);
	 * sampleImportData.setContentCatalogNames(Arrays.asList(SAMPLE_CONTENT_CATALOG_NAME));
	 * sampleImportData.setStoreNames(Arrays.asList(SAMPLE_STORE_NAME));
	 * importData.add(sampleImportData);
	 * 
	 * getCoreDataImportService().execute(this, context, importData);
	 * getEventService().publishEvent(new CoreDataImportedEvent(context, importData));
	 * 
	 * getSampleDataImportService().execute(this, context, importData);
	 * getEventService().publishEvent(new SampleDataImportedEvent(context, importData));
	 * </pre>
	 *
	 * @param context
	 *           the context provides the selected parameters and values
	 */
	@SystemSetup(type = Type.PROJECT, process = Process.ALL)
	public void createProjectData(final SystemSetupContext context)
	{
		final List<ImportData> importData = new ArrayList<ImportData>();

		final ImportData hybrisImportData = new ImportData();
		hybrisImportData.setProductCatalogName("travelretail");
		hybrisImportData.setContentCatalogNames(Arrays.asList("travelretail"));
		hybrisImportData.setStoreNames(Arrays.asList("travelretail"));
		importData.add(hybrisImportData);
		getCoreDataImportService().execute(this, context, importData);
		getEventService().publishEvent(new CoreDataImportedEvent(context, importData));

		getSampleDataImportService().execute(this, context, importData);
		getEventService().publishEvent(new SampleDataImportedEvent(context, importData));
		hybrisImportData.setProductCatalogName(CUSTOMB2B);
		hybrisImportData.setContentCatalogNames(Arrays.asList(CUSTOMB2B));
		hybrisImportData.setStoreNames(Arrays.asList(CUSTOMB2B));
		importData.add(hybrisImportData);
		getCoreDataImportService().execute(this, context, importData);
		getEventService().publishEvent(new CoreDataImportedEvent(context, importData));

		getSampleDataImportService().execute(this, context, importData);
		getEventService().publishEvent(new SampleDataImportedEvent(context, importData));
		System.out.println("Begin importing other data for travelretail...");
		System.out.println("hiiiiiiiiiiiiiiiiiiiiiii...");

		importImpexFile(context, "/travelretailinitialdata/import/sampledata/other/others/cart_page_image_remove.impex");
		importImpexFile(context, "/travelretailinitialdata/import/sampledata/other/others/Home_Page_Changes.impex");
		importImpexFile(context, "/travelretailinitialdata/import/sampledata/other/others/Impex_For_login_Page_Changes.impex");
		importImpexFile(context,
				"/travelretailinitialdata/import/sampledata/other/others/Impex_To_Remove_Currency_and_Languages.impex");
		importImpexFile(context, "/travelretailinitialdata/import/sampledata/other/others/ImpexForChangingSiteLogo.impex");
		importImpexFile(context, "/travelretailinitialdata/import/sampledata/other/others/Max_Bookable_Quantity_Staged.impex");
		importImpexFile(context, "/travelretailinitialdata/import/sampledata/other/others/MediaForLoginPageProductBanner.impex");
		importImpexFile(context, "/travelretailinitialdata/import/sampledata/other/others/PNR_Booking_Details.impex");
		importImpexFile(context, "/travelretailinitialdata/import/sampledata/other/others/Product_change.impex");
		importImpexFile(context, "/travelretailinitialdata/import/sampledata/other/others/Product_change_1.impex");
		importImpexFile(context, "/travelretailinitialdata/import/sampledata/other/others/Product_Stock_new.impex");
		importImpexFile(context, "/travelretailinitialdata/import/sampledata/other/others/ProductCarouselProductNameChanges.impex");
		importImpexFile(context, "/travelretailinitialdata/import/sampledata/other/others/Products_de.impex");
		importImpexFile(context, "/travelretailinitialdata/import/sampledata/other/others/ProductsReference.impex");
		importImpexFile(context, "/travelretailinitialdata/import/sampledata/other/others/Remove_CMS SITE.impex");
		importImpexFile(context, "/travelretailinitialdata/import/sampledata/other/others/Login_Page_Changes.impex");
		importImpexFile(context, "/travelretailinitialdata/import/sampledata/other/others/product_restriction.impex");
		importImpexFile(context, "/travelretailinitialdata/import/sampledata/other/others/login_page_restriction.impex");
		importImpexFile(context, "/travelretailinitialdata/import/sampledata/commerceorg/user-groups.impex");
		importImpexFile(context, "/travelretailinitialdata/import/sampledata/commerceorg/login_page_restriction.impex");
		importImpexFile(context, "/travelretailinitialdata/import/sampledata/commerceorg/regpage.impex");
		importImpexFile(context, "/travelretailinitialdata/import/sampledata/commerceorg/user-groups.impex");
		importImpexFile(context, "/travelretailinitialdata/import/sampledata/commerceorg/user-groups_en.impex");
		getSampleDataImportService().synchronizeProductCatalog(this, context, TRAVELRETAIL, true);
		getSampleDataImportService().synchronizeContentCatalog(this, context, TRAVELRETAIL, true);
		getSampleDataImportService().synchronizeProductCatalog(this, context, CUSTOMB2B, true);
		getSampleDataImportService().synchronizeContentCatalog(this, context, CUSTOMB2B, true);
		/*
		 * Add import data for each site you have configured
		 */
	}

	public CoreDataImportService getCoreDataImportService()
	{
		return coreDataImportService;
	}

	@Required
	public void setCoreDataImportService(final CoreDataImportService coreDataImportService)
	{
		this.coreDataImportService = coreDataImportService;
	}

	public SampleDataImportService getSampleDataImportService()
	{
		return sampleDataImportService;
	}

	@Required
	public void setSampleDataImportService(final SampleDataImportService sampleDataImportService)
	{
		this.sampleDataImportService = sampleDataImportService;
	}
}
