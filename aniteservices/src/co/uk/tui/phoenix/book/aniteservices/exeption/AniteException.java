/**
 *
 */
package co.uk.tui.phoenix.book.aniteservices.exeption;

import org.apache.commons.lang.StringUtils;



/**
 * @author naresh.gls
 *
 *         Class used to handle the Anite Exception
 */
public class AniteException extends Exception
{

    /** The error code. */
    private final String errorCode;

    /** The custom message. */
    private final String customMessage;

    /** The nested cause. */
    private final Throwable nestedCause;

    /**
     * Constructor for creating Anite Exception
     *
     * @param errorCode
     *           the error code
     * @param errorDescription
     *           the error description.
     */
    public AniteException(final String errorCode, final String errorDescription)
    {
        super();
        this.errorCode = errorCode;
        this.customMessage = errorDescription;
        this.nestedCause = null;
    }

    /**
     * If call to anite fails it will innstantiate a anite exception with throwable cause.
     *
     * @param cause
     *           the Throwable cause
     */
    public AniteException(final Throwable cause)
    {
        super(cause);
        this.errorCode = StringUtils.EMPTY;
        this.customMessage = StringUtils.EMPTY;
        this.nestedCause = cause;
    }

    /**
     * If call to anite fails it will innstantiate a anite exception with error code and throwable cause.
     *
     * @param errorCode
     *           the string error code
     *
     * @param errorDescription
     *           the error description
     *
     * @param cause
     *           the Throwable cause
     */
    public AniteException(final String errorCode, final String errorDescription, final Throwable cause)
    {
        super(cause);
        this.errorCode = errorCode;
        this.nestedCause = cause;
        this.customMessage = errorDescription;
    }

    /**
     * @return the errorCode
     */
    public String getErrorCode()
    {
        return errorCode;
    }


    /**
     * @return the customMessage
     */
    public String getCustomMessage()
    {
        return customMessage;
    }


    /**
     * @return the nestedCause
     */
    public Throwable getNestedCause()
    {
        return nestedCause;
    }


}
