/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package co.uk.tui.phoenix.book.aniteservices.constants;

/**
 * Global class for all Aniteservices constants. You can add global constants for your extension into this class.
 */
public final class AniteservicesConstants extends GeneratedAniteservicesConstants
{
	public static final String EXTENSIONNAME = "aniteservices";
	public static final String REQUESTID_PREFIX = "requestid.prefix";
	public static final String REQUESTID_LENGTH = "requestid.length";
	public static final String TH_FC = "th_fc";
	public static final String VRP_LOCALE = "vrp.locale";
	public static final String VRP_CLT_SYS_CONTENT = "vrp.contextid";
	public static final String VRP_AGENT_NO = ".vrp.agentid";
	public static final String VRP_AGT_PWD = ".vrp.agentpassword";
	public static final String VRP_TERM_CODE = "vrp.termcode";
	public static final String VRP_USER_NAME = ".vrp.username";
	public static final String VRP_CHANNEL = "vrp.channel";
	public static final String VRP_USER_ROLE = "vrp.userrole";
	public static final String VRP_EXP_NO = "vrp.expno";
	public static final String AGENT_NO = "_agtNo";

	public static final String COMRES_INVENTORY = "atcomres";
	public static final String TRACS_INVENTORY = "tracs";
	public static final String VRP_B2CUSER_NAME = "atcomres.vrp.b2cusername";

	/** The Constant DD_MM_YYYY. */
	public static final String DD_MM_YYYY = "dd-MM-yyyy";

	/** The Constant YYYY_MM_DD. */
	public static final String YYYY_MM_DD = "yyyy-MM-dd";

	public static final int ZERO_VALUE = 0;

	public static final int TWO_VALUE = 2;

	public static final int THREE_VALUE = 3;

	public static final int FOUR_VALUE = 4;

	public static final int SIX_VALUE = 6;

	public static final int SEVEN_VALUE = 7;

	public static final String RNP = ".rnp";

	public static final String TH_AIRWAYS = "Thomson Airways";

	private AniteservicesConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
