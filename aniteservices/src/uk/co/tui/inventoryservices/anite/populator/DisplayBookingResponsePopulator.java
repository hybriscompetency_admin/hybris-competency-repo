/**
 *
 */
package uk.co.tui.inventoryservices.anite.populator;

import de.hybris.platform.commerceservices.converter.Populator;
import de.hybris.platform.core.enums.Gender;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Currency;
import java.util.Date;
import java.util.EnumSet;
import java.util.GregorianCalendar;
import java.util.IllegalFormatException;
import java.util.List;

import javax.annotation.Resource;
import javax.xml.bind.JAXBElement;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;
import org.apache.xerces.dom.ElementNSImpl;

import uk.co.travel.domain.manage.response.DisplayBookingResponse;
import uk.co.tui.book.domain.lite.Address;
import uk.co.tui.book.domain.lite.Agent;
import uk.co.tui.book.domain.lite.AgentType;
import uk.co.tui.book.domain.lite.BaggageExtraFacility;
import uk.co.tui.book.domain.lite.BaggageExtraFacilityRestrictions;
import uk.co.tui.book.domain.lite.BoardBasis;
import uk.co.tui.book.domain.lite.BookingDetails;
import uk.co.tui.book.domain.lite.BookingHistory;
import uk.co.tui.book.domain.lite.BookingType;
import uk.co.tui.book.domain.lite.CabinClass;
import uk.co.tui.book.domain.lite.CarHireExtraFacility;
import uk.co.tui.book.domain.lite.CardDetails;
import uk.co.tui.book.domain.lite.CardTypes;
import uk.co.tui.book.domain.lite.Carrier;
import uk.co.tui.book.domain.lite.CarrierInformation;
import uk.co.tui.book.domain.lite.CommunicationPreference;
import uk.co.tui.book.domain.lite.Deposit;
import uk.co.tui.book.domain.lite.DepositType;
import uk.co.tui.book.domain.lite.Depot;
import uk.co.tui.book.domain.lite.Discount;
import uk.co.tui.book.domain.lite.DiscountType;
import uk.co.tui.book.domain.lite.ExtraFacility;
import uk.co.tui.book.domain.lite.ExtraFacilityGroup;
import uk.co.tui.book.domain.lite.FacilitySelectionType;
import uk.co.tui.book.domain.lite.Flight;
import uk.co.tui.book.domain.lite.FlightItinerary;
import uk.co.tui.book.domain.lite.FlightLeg;
import uk.co.tui.book.domain.lite.FlightSchedule;
import uk.co.tui.book.domain.lite.HighLevelBookingType;
import uk.co.tui.book.domain.lite.Inventory;
import uk.co.tui.book.domain.lite.InventoryType;
import uk.co.tui.book.domain.lite.Itinerary;
import uk.co.tui.book.domain.lite.Leg;
import uk.co.tui.book.domain.lite.Memo;
import uk.co.tui.book.domain.lite.Money;
import uk.co.tui.book.domain.lite.PackageType;
import uk.co.tui.book.domain.lite.Passenger;
import uk.co.tui.book.domain.lite.PaymentReference;
import uk.co.tui.book.domain.lite.PersonType;
import uk.co.tui.book.domain.lite.Port;
import uk.co.tui.book.domain.lite.Price;
import uk.co.tui.book.domain.lite.PriceType;
import uk.co.tui.book.domain.lite.Room;
import uk.co.tui.book.domain.lite.RoomOccupancy;
import uk.co.tui.book.domain.lite.SalesChannel;
import uk.co.tui.book.domain.lite.SalesChannelType;
import uk.co.tui.book.domain.lite.Schedule;
import uk.co.tui.book.domain.lite.Stay;
import uk.co.tui.book.domain.lite.SupplierProductDetails;
import uk.co.tui.book.passenger.utils.PassengerUtils;
import uk.co.tui.book.services.CurrencyResolver;
import uk.co.tui.inventoryservices.anite.constants.InventoryServiceConstants;
import atcomres.common.AtcomresBookingBaseResponse.BkgEnt.Item;
import atcomres.common.AtcomresBookingBaseResponse.BkgEnt.Package.Accom;
import atcomres.common.AtcomresBookingBaseResponse.BkgEnt.Package.Accom.TransferInfo;
import atcomres.common.AtcomresBookingBaseResponse.BkgEnt.Package.RouteList.Routing;
import atcomres.common.AtcomresBookingBaseResponse.CusDet;
import atcomres.common.AtcomresBookingBaseResponse.Pax;
import atcomres.common.AtcomresBookingBaseResponse.PayData.Pay;
import atcomres.common.Car;
import atcomres.common.CarRental;
import atcomres.common.CarRentalDet;
import atcomres.common.FltExtra;
import atcomres.common.FltExtraCat;
import atcomres.common.FltExtraCatList;
import atcomres.common.FreeCars;
import atcomres.common.RouteTp;
import atcomres.common.RouteTp.FltDtTm;
import atcomres.common.RouteTp.Sec;
import atcomres.displayresponse.TILDisplayResponse;

/**
 * @author sunil.bd
 *
 */
public class DisplayBookingResponsePopulator extends AtComresCommonPopulator implements
   Populator<atcomres.displayresponse.TILDisplayResponse, DisplayBookingResponse>
{
   private static final Logger LOGGER = Logger.getLogger(DisplayBookingResponsePopulator.class);

   private static final int FIVE = 5;

   private static final int THIRTY = 30;

   private static final int FOUR = 4;

   private static final int ZERO = 0;

   private static final String SEAT = "SEAT";

   private static final String GSEAT = "GSEAT";

   public static final List<String> ACCOM_PRICES = Arrays.asList(new String[] { "AA", "AI", "AC" });

   public static final List<String> BOARDBASIS = Arrays.asList(new String[] { "HB", "FB", "AI",
      "SC", "BB", "RO", "AB" });

   /* AmendnCancel Start */

   /** The Airport Service . */
   // @Resource
   // private AirportService airportService;

   private final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

   @Resource
   private CurrencyResolver currencyResolver;

   private static final String DATE_PARSE_EXCEPTION = "Date parse exception";

   private static final String DEFAULT_SEAT = "DEFAULT_SEAT";

   private static final String MAST = "MSTR";

   private static final String DR = "DR";

   private static final String DOCTOR = "Doctor";

   private static final String DOCT = "DOCT";

   private static final String INFANT = "INFA";

   private static final String REV = "REV";

   @Override
   public void populate(final atcomres.displayresponse.TILDisplayResponse source,
      final DisplayBookingResponse target) throws ConversionException
   {
      final String status = source.getDisplayResponse().getBkgSts();
      if ("CANCELED".equalsIgnoreCase(status))
      {
         target.setBookingStatus(source.getDisplayResponse().getBkgSts());

      }

      else
      {
         target.setBookingStatus(source.getDisplayResponse().getBkgSts());
         final Discount discount = new Discount();
         discount.setDiscountType(DiscountType.WEB);
         final Price defaultprice = new Price();
         final Money money = new Money();
         money.setAmount(BigDecimal.ZERO);
         money.setCurrency(Currency.getInstance(currencyResolver.getSiteCurrency()));
         defaultprice.setAmount(money);

         final Money rate = new Money();
         rate.setAmount(BigDecimal.ZERO);
         rate.setCurrency(Currency.getInstance(currencyResolver.getSiteCurrency()));
         defaultprice.setRate(rate);

         defaultprice.setQuantity(Integer.valueOf(0));
         discount.setPrice(defaultprice);
         target.setDiscount(discount);
         discount.setCurrency(Currency.getInstance(currencyResolver.getSiteCurrency()).toString());
         LOGGER.info("Populating Display Booking Reponse");

         final Inventory inventory = new Inventory();
         inventory.setInventoryType(InventoryType.ATCOM);
         inventory.setSupplierProductDetails(new SupplierProductDetails());
         target.setInventory(inventory);
         target.setPackageType(PackageType.INCLUSIVE);

         populateHighLevelBooking(source, target);
         populateFlightExtra(source, target);
         populatePackageExtras(source, target);
         populateCar(source, target);

         // Implied Transfers are checked for 1st Accom
         target.setTransferCodes(getTransferCodesFromAccom((Accom) source.getDisplayResponse()
            .getBkgEnt().getPackage().get(0).getAccomOrMultiCentreOrRoundtrip().get(0)));

         final CommunicationPreference communicationPreference = new CommunicationPreference();
         populateDefaultValues(communicationPreference);
         target.setCommunicationPreference(communicationPreference);
         target.setBookingRefNum(source.getDisplayResponse().getBkgNum().getBkgId());
         totalPackagePrice(source, target);
         populatePaymentData(source, target);

         if (source.getDisplayResponse().getBkgEnt() != null)
         {
            target.getMemos().addAll(
               populateErrataMemoForBkng(source.getDisplayResponse().getBkgEnt().getMemo()));
         }

         if (!source.getDisplayResponse().getBkgEnt().getRouteList().isEmpty())
         {

            final List<atcomres.common.AtcomresBookingBaseResponse.BkgEnt.RouteList> routeList =
               source.getDisplayResponse().getBkgEnt().getRouteList();
            final List<atcomres.common.AtcomresBookingBaseResponse.BkgEnt.RouteList.Routing> routingList =
               routeList.get(0).getRouting();

            Itinerary itinerary = new FlightItinerary();
            for (final atcomres.common.AtcomresBookingBaseResponse.BkgEnt.RouteList.Routing routing : routingList)
            {
               final List<Object> routes = routing.getCoachOrFerryOrRoute();
               itinerary = populateItinerary(routes, itinerary);

            }
            target.setItinerary(itinerary);
         }
         if (null != source.getDisplayResponse().getBkgEnt().getPrices())
         {
            target.setPrices(populateOtherPrice(source.getDisplayResponse().getBkgEnt().getPrices()
               .getPrice()));
         }
      }
   }

   /**
    * Gets the extraFacilityCodes codes from accom.
    *
    * @param accom the source
    * @return the extraFacilityCodes codes from accom
    */
   private List<String> getTransferCodesFromAccom(final Accom accom)
   {
      final List<String> extraFacilityCodes = new ArrayList<String>();
      if (isTransferAvailableInBothLegs(accom.getTransferInfo()))
      {
         extraFacilityCodes.add(accom.getTransferInfo().get(0).getTransferCode());
      }
      return extraFacilityCodes;
   }

   /**
    * Checks if is transfer available in both legs.
    *
    * @param list the transfer info
    * @return true, if is transfer available in both legs
    */
   private boolean isTransferAvailableInBothLegs(final List<TransferInfo> list)
   {
      boolean transferAvailable = false;
      for (final TransferInfo info : list)
      {
         transferAvailable = checkTransferAvailableOrNot(info);
         if (!transferAvailable)
         {
            return false;
         }
      }
      return transferAvailable;
   }

   /**
    * Check transfer available or not.
    *
    * @param info the info
    * @return true, if successful
    */
   private boolean checkTransferAvailableOrNot(final TransferInfo info)
   {
      return StringUtils.isNotEmpty(info.getTransferCode());
   }

   /**
    * Populate default values.
    *
    * @param communicationPreference the communication preference
    */
   public void populateDefaultValues(final CommunicationPreference communicationPreference)
   {

      communicationPreference.setByEmail(true);
      communicationPreference.setByPhone(false);
      communicationPreference.setByPost(false);
      communicationPreference.setThirdPartySharing(false);
   }

   /*
    * To popluate the accommodation details
    */

   /**
    * @param source
    * @param target
    */
   public void populateHighLevelBooking(final TILDisplayResponse source,
      final DisplayBookingResponse target)
   {
      final HighLevelBookingType bookingType = new HighLevelBookingType();
      if (source.getDisplayResponse().getBkgEnt().getAtolProtTp() != null
         && !"NONE".equalsIgnoreCase(source.getDisplayResponse().getBkgEnt().getAtolProtTp()))
      {
         bookingType.setAtolProtType(true);
      }
      else
      {
         bookingType.setAtolProtType(false);
      }
      target.setHighLevelBookingType(bookingType);
   }

   protected void populateAccomodationDetails(
      final atcomres.common.AtcomresBookingBaseResponse.BkgEnt.Package.Accom source,
      final Stay target, final List<Passenger> passengerList)
   {
      /* Remove once get proper data */

      try
      {
         target.setEndDate(formatter.parse(source.getEndDt()));
         target.setStartDate(formatter.parse(source.getStDt().toString()));
         target.setCode(source.getHtlPrd().getAccCd());
         target.setName(source.getHtlPrd().getName());

         List<Passenger> passgengers;
         final BoardBasis boardBasis = new BoardBasis();

         if (CollectionUtils.isNotEmpty(source.getMemo()) && source.getMemo().get(0) != null
            && "ERR".equals(source.getMemo().get(0).getMemoCd()))
         {
            final List<Memo> errataMemo = populateErrataMemoForAccom(source);
            target.setErrataMemo(errataMemo);
         }

         final List<Room> rooms = new ArrayList<Room>();
         for (final atcomres.common.AtcomresBookingBaseResponse.BkgEnt.Package.Accom.RmCd rmCode : source
            .getRmCd())
         {
            passgengers = new ArrayList<Passenger>();
            final Room room = new Room();
            final RoomOccupancy occupancy = new RoomOccupancy();
            room.setCode(rmCode.getCode());
            room.setRoomTitle(rmCode.getDesc());
            occupancy.setMinOccupancy(rmCode.getMinPax().intValue());
            occupancy.setMaxOccupancy(rmCode.getMaxPax().intValue());
            room.setRoomOccupancy(occupancy);

            for (final atcomres.common.AtcomresBookingBaseResponse.BkgEnt.Package.Accom.RmCd.SubServPaxs.SubServPax subServPaxs : rmCode
               .getSubServPaxs().getSubServPax())
            {
               for (final Passenger passenger : passengerList)
               {
                  if (passenger.getId().intValue() == subServPaxs.getPaxId().intValue())
                  {
                     passgengers.add(passenger);
                  }

               }
            }
            boardBasis.setCode(rmCode.getBBCd());
            boardBasis.setDescription(rmCode.getBBName());
            room.setBoardBasis(boardBasis);
            room.setPassgengers(passgengers);
            room.setQuantity(Integer.valueOf(1));
            populateAccomPrice(rmCode, room);
            rooms.add(room);
         }
         target.setRooms(rooms);
         populateAccomAddress(source, target);
      }
      catch (final ParseException ex)
      {
         LOGGER.info("error in date converstion");
      }

   }

   /**
    *
    * @param source
    * @param target
    */
   private void populateAccomAddress(final Accom source, final Stay target)
   {

      final Address address = new Address();
      if (source.getHtlPrd().getHotel().getAdd() != null)
      {
         address.setHouseNumber(source.getHtlPrd().getHotel().getAdd().getName());
         address.setStreetname(source.getHtlPrd().getHotel().getAdd().getStreet());
         address.setTown(source.getHtlPrd().getHotel().getAdd().getCity());
         address.setPostalcode(source.getHtlPrd().getHotel().getAdd().getZipCode());
         address.setPobox(source.getHtlPrd().getHotel().getAdd().getPOBox());
         address.setCounty(source.getHtlPrd().getHotel().getAdd().getCountryISOCode());

      }
      if (CollectionUtils.isNotEmpty(source.getHtlPrd().getHotel().getComm()))
      {
         address.setPhone1(source.getHtlPrd().getHotel().getComm().get(0).getNum());
      }
      target.setAddress(address);
   }

   protected void populatePackageExtras(final TILDisplayResponse source,
      final DisplayBookingResponse target)
   {

      final List<atcomres.common.AtcomresBookingBaseResponse.BkgEnt.Item> items =
         source.getDisplayResponse().getBkgEnt().getItem();
      if (items != null)
      {

         final List<ExtraFacility> exFacilityList = new ArrayList<ExtraFacility>();
         for (final atcomres.common.AtcomresBookingBaseResponse.BkgEnt.Item item : items)
         {
            final ExtraFacility extraFacility = new ExtraFacility();
            extraFacility.setExtraFacilityCode(item.getItemType().getCode());
            extraFacility.setDescription(item.getName());
            extraFacility.setInventoryId(String.valueOf(item.getId()));
            extraFacility.setInventoryCode(item.getCode());
            extraFacility.setQuantity(item.getBkgQty().intValue());
            extraFacility.setCorporateCode(StringUtils.defaultIfEmpty(item.getCorporateCd(),
               StringUtils.EMPTY));
            try
            {
               if (item.getStDt() != null)
               {
                  extraFacility.getExtraFacilitySchedule().setArrivalDate(
                     formatter.parse(item.getStDt()));
               }
               if (item.getEndDt() != null)
               {
                  extraFacility.getExtraFacilitySchedule().setDepartureDate(
                     formatter.parse(item.getEndDt()));
               }
            }
            catch (final ParseException e)
            {
               LOGGER.info("Error in Date conversion");
            }
            final List<Price> pricess = populateExtraPrice(item, extraFacility);
            extraFacility.setPrices(pricess);
            for (final atcomres.common.AtcomresBookingBaseResponse.BkgEnt.Item.SubServPaxs.SubServPax subServPaxs : item
               .getSubServPaxs().getSubServPax())
            {
               populatePaxToExtraBasedOnID(target.getPassengers(), extraFacility, subServPaxs
                  .getPaxId().intValue());
            }

            extraFacility.setSelection(populateSelectionType(item));
            updateSelectionTypeForCharity(extraFacility, item);
            exFacilityList.add(extraFacility);
            target.setIntegratedExtraFacilities(exFacilityList);

         }

      }

   }

   private List<Price> populateExtraPrice(
      final atcomres.common.AtcomresBookingBaseResponse.BkgEnt.Item item,
      final ExtraFacility extraFacility)
   {
      final List<Price> prices = new ArrayList<Price>();
      if (item.getPrices() != null && CollectionUtils.isNotEmpty(item.getPrices().getPrice()))
      {
         for (final atcomres.common.AtcomresBookingBaseResponse.BkgEnt.Item.Prices.Price price : item
            .getPrices().getPrice())
         {
            prices.add(getPrice(new BigDecimal(price.getPrc().getValue()), price.getPrcCd(), price
               .getQty().intValue()));
         }
      }
      else
      {
         prices.add(getPrice(BigDecimal.ZERO, "", extraFacility.getPassengers().size()));
      }
      return prices;
   }

   /**
    * Gets the price.
    *
    * @param bigDecimal the big decimal
    * @param code the code
    * @return the price
    */
   private Price getPrice(final BigDecimal bigDecimal, final String code, final int quantity)
   {
      final Price price = new Price();
      price.setRate(populateMoney(bigDecimal));
      price.setAmount(populateMoney(bigDecimal.multiply(new BigDecimal(quantity))));
      price.setCode(code);
      price.setQuantity(quantity);
      return price;
   }

   /**
    * Populate money.
    *
    * @param price the price
    * @return the money
    */
   private Money populateMoney(final BigDecimal price)
   {
      final Money money = new Money();
      money.setAmount(price);
      money.setCurrency(Currency.getInstance(currencyResolver.getSiteCurrency()));
      return money;
   }

   protected void populatePassengerList(final TILDisplayResponse source,
      final DisplayBookingResponse target)
   {
      if (source.getDisplayResponse().getPax() != null)
      {
         Validate.notEmpty(source.getDisplayResponse().getPax(), "Passenger list is empty");
      }
      final List<Passenger> passengers = new ArrayList<Passenger>();

      for (final Pax pax : source.getDisplayResponse().getPax())
      {
         final Passenger passenger = new Passenger();
         if (pax.getAge() != null)
         {
            passenger.setAge(Integer.valueOf(pax.getAge().intValue()));
         }
         if (pax.getPaxTp() != null)
         {
            final PersonType type = PersonType.valueOf(pax.getPaxTp());
            if (type != null)
            {
               passenger.setType(type);
            }
         }
         if (pax.getPerson().getTitle().length() >= FIVE)
         {
            passenger.setTitle(pax.getPerson().getTitle().substring(ZERO, FOUR).toUpperCase());
            // Fixing to resolve validation issue for failure email. Will
            // come back with proper approach

         }
         else
         {
            passenger.setTitle(pax.getPerson().getTitle().toUpperCase());
         }

         if ("MAST".equalsIgnoreCase(passenger.getTitle()))
         {
            passenger.setTitle(MAST);
         }
         else if (DOCT.equalsIgnoreCase(passenger.getTitle())
            || DOCTOR.equalsIgnoreCase(passenger.getTitle()))
         {
            passenger.setTitle(DR);
         }

         else if (INFANT.equalsIgnoreCase(passenger.getTitle()))
         {
            passenger.setTitle(REV);
         }

         passenger.setId(pax.getIndex().intValue());
         populatePassengerNames(pax, passenger);
         if (pax.isLeadPax() != null)
         {
            passenger.setLead(pax.isLeadPax().booleanValue());
            // Need to check, whether all the Pax required the address
         }
         // Need to check, whether all the Pax required the address

         populateLeadPassengerAddress(passenger, source.getDisplayResponse().getCusDet());

         passengers.add(passenger);
      }

      target.setPassengers(passengers);

   }

   /**
    * This method will populate lead passenger address
    *
    * Will have to re-look after book flow code re-factor
    *
    * @param passenger
    * @param cusDetList
    */
   private void populateLeadPassengerAddress(final Passenger passenger,
      final List<CusDet> cusDetList)
   {

      if (CollectionUtils.isNotEmpty(cusDetList))
      {
         final Address address = new Address();

         for (final CusDet custDet : cusDetList)
         {
            if (custDet.getPerson() != null
               && CollectionUtils.isNotEmpty(custDet.getPerson().getAdd()))
            {
               final atcomres.common.AtcomresBookingBaseResponse.CusDet.Person.Add add =
                  custDet.getPerson().getAdd().get(0);
               address.setHouseNumber(add.getName());
               address.setStreetname(add.getStreet());
               address.setTown(add.getCity());
               address.setPostalcode(add.getZipCode());
               address.setPobox(add.getPOBox());
               address.setCounty(add.getRegion());
               address.setCountry(add.getCountryISOCode());
            }
            if (custDet.getPerson().getComm() != null && !custDet.getPerson().getComm().isEmpty())
            {
               address.setPhone1(custDet.getPerson().getComm().get(0).getNum());
            }
            if (custDet.getPerson().getEmail() != null && !custDet.getPerson().getEmail().isEmpty())
            {
               address.setEmail(custDet.getPerson().getEmail().get(0).getAddress());
            }
            final List<Address> list = new ArrayList<Address>();
            list.add(address);
            passenger.setAddresses(list);
         }
      }
   }

   /**
    *
    * This method will populate passenger first name, last name and tile This will be removed after
    * book flow refactoring
    *
    *
    * @param source
    * @param passenger
    */
   protected void populatePassengerNames(final Pax source, final Passenger passenger)
   {

      if (source.getPerson().getFirstName().length() >= THIRTY)
      {
         passenger.setFirstname(source.getPerson().getFirstName().substring(0, THIRTY));
      }
      else
      {
         passenger.setFirstname(source.getPerson().getFirstName());
      }
      if (source.getPerson().getLastName().length() >= THIRTY)
      {
         passenger.setLastname(source.getPerson().getLastName().substring(0, THIRTY));
         passenger.setName(passenger.getLastname());

      }
      else
      {
         passenger.setLastname(source.getPerson().getLastName());
         passenger.setName(passenger.getLastname());

      }
      passenger.setDateOfBirth(source.getPerson().getDateOfBirth());

      passenger.setGender(getValidGender(source.getPerson().getSex()));

   }

   protected Gender getValidGender(final String genger)
   {
      if ("SEX_MALE".equals(genger))
      {
         return Gender.MALE;
      }
      else
      {
         return Gender.FEMALE;
      }
   }

   /**
    * Populate itinerary.
    *
    * @param routes the routes
    * @return the flight itinerary model
    */
   protected Itinerary populateItinerary(final List<Object> routes, final Itinerary itinerary)
   {

      final List<Leg> outboundLegs = new ArrayList<Leg>();
      final List<Leg> inboundLegs = new ArrayList<Leg>();
      BigDecimal amount = BigDecimal.ZERO;

      for (final Object routeObj : routes)
      {

         final RouteTp tp = RouteTp.class.cast(JAXBElement.class.cast(routeObj).getValue());
         final Leg leg = new FlightLeg();
         if ("outbound".equalsIgnoreCase(tp.getRtDir()))
         {
            populateInboundAndOutBoundData(tp, leg);
            if (leg.getPrice() != null)
            {
               for (final Price prc : leg.getPriceList())
               {
                  final Money legPrice = prc.getAmount();
                  amount = amount.add(legPrice.getAmount());
               }
            }
            outboundLegs.add(leg);
         }
         else
         {
            populateInboundAndOutBoundData(tp, leg);
            if (leg.getPrice() != null)
            {
               for (final Price prc : leg.getPriceList())
               {
                  final Money legPrice = prc.getAmount();
                  amount = amount.add(legPrice.getAmount());
               }
            }
            inboundLegs.add(leg);

         }

      }

      if (itinerary.getInBound() != null)
      {
         itinerary.getInBound().addAll(inboundLegs);
      }
      else
      {
         itinerary.setInBound(inboundLegs);

      }
      if (itinerary.getOutBound() != null)
      {
         itinerary.getOutBound().addAll(outboundLegs);
      }
      else
      {
         itinerary.setOutBound(outboundLegs);

      }
      return itinerary;

   }

   protected List<Memo> populateErrataMemoForAccom(final Accom source)
   {
      final List<Memo> memoList = new ArrayList<Memo>();
      final Memo memo = new Memo();
      if (source.getMemo() != null)
      {
         for (final atcomres.common.AtcomresBookingBaseResponse.BkgEnt.Package.Accom.Memo errataMemo : source
            .getMemo())
         {
            memo.setCode(errataMemo.getMemoCd());
            memo.setDescription(errataMemo.getMemoDes());
            memo.setName(errataMemo.getMemoName());
            memoList.add(memo);
         }
      }
      return memoList;

   }

   /**
    * @param source
    */
   protected List<Memo> populateErrataMemoForBkng(
      final List<atcomres.common.AtcomresBookingBaseResponse.BkgEnt.Memo> source)
   {
      final List<Memo> memoList = new ArrayList<Memo>();
      if (CollectionUtils.isNotEmpty(source))
      {
         for (final atcomres.common.AtcomresBookingBaseResponse.BkgEnt.Memo errataMemo : source)
         {
            final Memo memo = new Memo();
            memo.setCode(errataMemo.getMemoCd());
            memo.setDescription(errataMemo.getMemoDes());
            memo.setName(errataMemo.getMemoName());
            memoList.add(memo);
         }
      }
      return memoList;
   }

   /**
    * flight errata
    *
    * @param routing
    */
   protected List<Memo> populateFlightErrata(final List<Routing> routing)
   {
      final List<Memo> memoList = new ArrayList<Memo>();
      if (CollectionUtils.isNotEmpty(routing))
      {
         for (final Routing flightErrata : routing)
         {
            final List<Object> routes = flightErrata.getCoachOrFerryOrRoute();
            for (final Object routeObj : routes)
            {
               populateMemo(memoList, routeObj);
            }

         }
      }
      return memoList;
   }

   /**
    * @param memoList
    * @param routeObj
    */
   private void populateMemo(final List<Memo> memoList, final Object routeObj)
   {
      final RouteTp tp = RouteTp.class.cast(JAXBElement.class.cast(routeObj).getValue());
      for (final atcomres.common.RouteTp.Memo flightMemos : tp.getMemo())
      {
         final Memo memo = new Memo();
         memo.setCode(flightMemos.getMemoCd());
         memo.setDescription(flightMemos.getMemoDes());
         memo.setName(flightMemos.getMemoName());
         memoList.add(memo);
      }
   }

   protected void populateInboundAndOutBoundData(final RouteTp tp, final Leg leg)
   {
      final Carrier carrier = new Flight();

      final Port arrivalAirport = new Port();
      final Port departureAirport = new Port();

      // final AirportModel arriavalAirportModel = getAirportName(tp.getArrAirCd());
      // final AirportModel departureAirportModel = getAirportName(tp.getDepAirCd());
      // if (arriavalAirportModel != null)
      // {
      arrivalAirport.setCode(tp.getArrAirCd());
      // arrivalAirport.setName(arriavalAirportModel.getName());
      // }
      // if (departureAirportModel != null)
      // {
      departureAirport.setCode(tp.getDepAirCd());
      // departureAirport.setName(departureAirportModel.getName());
      // }
      leg.setArrivalAirport(arrivalAirport);
      leg.setDepartureAirport(departureAirport);
      leg.setJourneyDuration(tp.getJnyDur());
      populateCarrierInformation(tp, carrier);
      leg.setCarrier(carrier);

      leg.setInventoryId(tp.getFltInvId());

      final Schedule schedule = populateFlightSchedule(tp);
      leg.setSchedule(schedule);
      populateFlightPrice(tp, leg);
      populateCabinClass(tp, carrier);
      populateEquipmentDescription(tp, carrier);
      populateExtrernalInventoryValue(leg, tp);

   }

   /**
    * Method to getAirport Name from PIM.
    *
    * @param airportCode the airport code
    * @return String- airportName
    */
   /*
    * protected AirportModel getAirportName(final String airportCode) { if (isNotEmpty(airportCode))
    * { return airportService.getAirportByCode(airportCode); } return null; }
    */

   /**
    * Populate carrier information.
    *
    * @param tp the route
    * @return the flight model
    */
   protected Carrier populateCarrierInformation(final RouteTp tp, final Carrier flight)
   {
      final CarrierInformation carrierInfo = new CarrierInformation();
      final String carrierCode = tp.getCarCd();
      carrierInfo.setMarketingAirlineCode(carrierCode);
      flight.setCode(carrierCode);
      final ElementNSImpl flightNo = (ElementNSImpl) tp.getFltNo();
      flight.setNumber(flightNo.getFirstChild().getNodeValue());
      flight.setCarrierInformation(carrierInfo);

      return flight;
   }

   /**
    * Populate flight schedule.
    *
    * @param tp the route
    * @return the flight schedule model
    */
   protected Schedule populateFlightSchedule(final RouteTp tp)
   {
      final Schedule schedule = new FlightSchedule();
      final SimpleDateFormat formatterLocal = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

      try
      {

         final List<FltDtTm> flightDateTime = tp.getFltDtTm();
         for (final FltDtTm flightDtTm : flightDateTime)
         {
            if ("DEPARTURE".equalsIgnoreCase(flightDtTm.getDirType()))
            {
               final Date date = formatterLocal.parse(flightDtTm.getLocal());
               schedule.setDepartureDate(date);
            }
            else
            {
               if ("ARRIVAL".equalsIgnoreCase(flightDtTm.getDirType()))
               {
                  final Date date = formatterLocal.parse(flightDtTm.getLocal());
                  schedule.setArrivalDate(date);
               }
            }
         }
      }

      catch (final ParseException e)
      {
         LOGGER.info("error in date converstion");
      }
      return schedule;
   }

   /**
    * Populate inventory id.
    *
    * @param leg the leg
    *
    */
   /**
    * The Method to Populate the External Inventory Value.
    *
    *
    * @param leg the leg
    */
   protected void populateExtrernalInventoryValue(final Leg leg, final RouteTp tp)
   {
      if (tp.getExtRefId() != null)
      {
         leg.setExternalInventory(StringUtils.equalsIgnoreCase(tp.getExtRefId().toString(), "Y")
            ? Boolean.TRUE : Boolean.FALSE);
      }
   }

   protected void populateCabinClass(final RouteTp tp, final Carrier flight)
   {

      final CabinClass cabinClass = new CabinClass();
      cabinClass.setCabinCode(tp.getCabCls().getCode());
      cabinClass.setCabinName(tp.getCabCls().getName());
      ((Flight) flight).setCabinClass(cabinClass);
   }

   private void populateEquipmentDescription(final RouteTp tp, final Carrier flight)
   {
      for (final Sec sector : tp.getSec())
      {
         flight.setEquipementDescription(sector.getEqmt());

      }
   }

   protected void populateFlightExtra(final TILDisplayResponse source,
      final DisplayBookingResponse target)
   {
      target.setFlightExtraFacilities(new ArrayList<ExtraFacility>());

      final List<FltExtraCatList> fltExtraCatList =
         source.getDisplayResponse().getBkgEnt().getFltExtraCatList();
      for (final FltExtraCatList extraCaltList : fltExtraCatList)
      {
         for (final FltExtraCat flightExtraCat : extraCaltList.getFltExtraCat())
         {
            populateExtrasBasedOnCategory(flightExtraCat, extraCaltList.getFltInvId(), target);
         }

      }

      if (!isFlightExtraPresent(target.getFlightExtraFacilities(), SEAT)
         && !isFlightExtraPresent(target.getFlightExtraFacilities(), GSEAT)
         && checkForEconomy(target))
      {
         for (final FltExtraCatList extraCaltList : fltExtraCatList)
         {
            target.getFlightExtraFacilities().add(
               populateDefaultSeatExtra(target, extraCaltList.getFltInvId()));
         }
      }

   }

   /**
    * Returns the existence of flight extra in the booking
    *
    * @param flightExtraFacilities
    * @param fltExtraCatCode
    * @return boolean
    */
   private boolean isFlightExtraPresent(final List<ExtraFacility> flightExtraFacilities,
      final String fltExtraCatCode)
   {
      for (final ExtraFacility extra : flightExtraFacilities)
      {
         if (fltExtraCatCode.equalsIgnoreCase(extra.getExtraFacilityCode()))
         {
            return true;
         }
      }
      return false;
   }

   /**
    * @param flightExtraCat
    * @param inventoryId
    * @param target
    */

   private void populateExtrasBasedOnCategory(final FltExtraCat flightExtraCat,
      final String inventoryId, final DisplayBookingResponse target)
   {
      target.getFlightExtraFacilities().addAll(
         populateExtraFacility(flightExtraCat, inventoryId, target.getPassengers()));
   }

   /**
    * @param flightExtraCat
    * @param inventoryId
    * @param passengers
    * @return ExtraFacility
    */
   private List<ExtraFacility> populateExtraFacility(final FltExtraCat flightExtraCat,
      final String inventoryId, final List<Passenger> passengers)
   {
      final List<ExtraFacility> extraFacilities = new ArrayList<ExtraFacility>();

      if (StringUtils.equalsIgnoreCase(InventoryServiceConstants.BAGGAGE, flightExtraCat.getCode()))
      {
         return populateBaggageExtraFacilty(flightExtraCat, inventoryId, passengers,
            extraFacilities);
      }
      else
      {
         return populateOtherExtraFacilty(flightExtraCat, inventoryId, passengers, extraFacilities);
      }
   }

   /**
    * @param flightExtraCat
    * @param inventoryId
    * @param passengers
    * @return extrafacilityList
    */
   private List<ExtraFacility> populateOtherExtraFacilty(final FltExtraCat flightExtraCat,
      final String inventoryId, final List<Passenger> passengers,
      final List<ExtraFacility> extrafacilityList)
   {

      for (final FltExtra flightExtra : flightExtraCat.getFltExtra())
      {
         final ExtraFacility othFltExtra = new ExtraFacility();
         for (final atcomres.common.FltExtra.SubServPaxs.SubServPax othDetails : flightExtra
            .getSubServPaxs().getSubServPax())
         {
            populatePaxToExtraBasedOnID(passengers, othFltExtra, othDetails.getPaxId().intValue());
         }

         othFltExtra.setInventoryCode(flightExtra.getCode());
         othFltExtra.setInventoryId(inventoryId);

         /* Extra Facility Code is used add default Extras */
         othFltExtra.setExtraFacilityCode(flightExtraCat.getCode());
         othFltExtra.setCorporateCode(flightExtra.getCorporateCd());
         othFltExtra.setExtraFacilityGroup(uk.co.tui.book.domain.lite.ExtraFacilityGroup.FLIGHT);
         othFltExtra.setDescription(flightExtra.getName());

         othFltExtra.setPrices(populateFlightExtPrice(flightExtra.getPrices(), othFltExtra));
         extrafacilityList.add(othFltExtra);
      }
      return extrafacilityList;
   }

   /**
    * @param flightExtraCat
    * @param inventoryId
    * @param passengers
    * @return baggageExtraFacilities
    */
   private List<ExtraFacility> populateBaggageExtraFacilty(final FltExtraCat flightExtraCat,
      final String inventoryId, final List<Passenger> passengers,
      final List<ExtraFacility> baggageExtraFacilities)
   {
      int baggageCounter = 0;
      for (final FltExtra flightExtra : flightExtraCat.getFltExtra())
      {
         baggageCounter++;
         final BaggageExtraFacility defaultBagExtra = new BaggageExtraFacility();
         defaultBagExtra.setBaggageWeight(Integer.parseInt(flightExtra.getBaggage().getWeight()
            .get(0).getCd()));

         for (final atcomres.common.FltExtra.SubServPaxs.SubServPax bagDetails : flightExtra
            .getSubServPaxs().getSubServPax())
         {
            populatePaxToExtraBasedOnID(passengers, defaultBagExtra, bagDetails.getPaxId()
               .intValue());

         }
         defaultBagExtra.setInventoryCode(flightExtraCat.getCode() + baggageCounter);
         defaultBagExtra.setInventoryId(inventoryId);
         defaultBagExtra
            .setPrices(populateFlightExtPrice(flightExtra.getPrices(), defaultBagExtra));
         defaultBagExtra.setCorporateCode(flightExtra.getCorporateCd());

         /* Extra Facility Code is used add default Extras */
         defaultBagExtra.setExtraFacilityCode(flightExtraCat.getCode());
         defaultBagExtra
            .setExtraFacilityGroup(uk.co.tui.book.domain.lite.ExtraFacilityGroup.FLIGHT);
         defaultBagExtra.setDescription(defaultBagExtra.getBaggageWeight() + "kg luggage x "
            + defaultBagExtra.getPassengers().size());
         defaultBagExtra
            .setBaggageExtraFacilityRestrictions(new BaggageExtraFacilityRestrictions());
         baggageExtraFacilities.add(defaultBagExtra);
      }
      return baggageExtraFacilities;
   }

   private void populatePaxToExtraBasedOnID(final List<Passenger> passengers,
      final ExtraFacility extra, final int paxId)
   {
      for (final Passenger pax : passengers)
      {
         if (paxId == pax.getId().intValue())
         {
            if (CollectionUtils.isEmpty(pax.getExtraFacilities()))
            {
               pax.setExtraFacilities(new ArrayList<ExtraFacility>());
            }
            pax.getExtraFacilities().add(extra);

            if (CollectionUtils.isEmpty(extra.getPassengers()))
            {
               extra.setPassengers(new ArrayList<Passenger>());
            }
            extra.getPassengers().add(pax);

         }

      }
   }

   private List<Price> populateFlightExtPrice(final atcomres.common.Prices prices,
      final ExtraFacility extra)
   {
      final List<Price> priceList = new ArrayList<Price>();
      if (prices != null && prices.getPrice() != null && !prices.getPrice().isEmpty())
      {
         for (final atcomres.common.Prices.Price price : prices.getPrice())
         {
            final Price pric = new Price();
            pric.setCode(price.getPrcCd());
            pric.setDescription(price.getPrcCdName());
            final Money money = new Money();
            final Money rate = new Money();

            if (price.getPrc() != null)
            {
               money.setAmount(new BigDecimal(price.getPrc().getValue()).multiply(new BigDecimal(
                  price.getQty().intValue())));
               rate.setAmount(new BigDecimal(price.getPrc().getValue()));
            }
            pric.setAmount(money);
            if (price.getQty() != null)
            {
               pric.setQuantity(price.getQty().intValue());
            }

            pric.setRate(rate);

            priceList.add(pric);
            extra.setSelection(FacilitySelectionType.SELECTABLE);
         }
      }
      else
      {
         final Price defaultprice = new Price();

         final Money money = new Money();
         money.setAmount(BigDecimal.ZERO);
         defaultprice.setAmount(money);

         final Money rate = new Money();
         rate.setAmount(BigDecimal.ZERO);
         defaultprice.setRate(rate);

         defaultprice.setQuantity(extra.getPassengers().size());
         priceList.add(defaultprice);
         extra.setSelection(FacilitySelectionType.INCLUDED);

      }
      return priceList;

   }

   protected void populateRoutingDetails(
      final atcomres.common.AtcomresBookingBaseResponse.BkgEnt.Package.RouteList routeList,
      final DisplayBookingResponse target)
   {
      Itinerary itenerary = new FlightItinerary();
      for (final Routing routing : routeList.getRouting())
      {

         final List<Object> routes = routing.getCoachOrFerryOrRoute();
         itenerary = populateItinerary(routes, itenerary);
      }
      target.setItinerary(itenerary);

   }

   private void populatePaymentData(final atcomres.displayresponse.TILDisplayResponse source,
      final DisplayBookingResponse target)
   {

      final List<Deposit> deposits = new ArrayList<Deposit>();

      if (source.getDisplayResponse().getPayData() != null)

      {
         for (final atcomres.common.AtcomresBookingBaseResponse.PayData.Dpt deposit : source
            .getDisplayResponse().getPayData().getDpt())
         {
            populateDeposits(deposits, deposit);
         }

         paymentBkgPriceInclusive(source, target);
      }
      target.setDeposits(deposits);

   }

   /**
    * @param deposits
    * @param deposit
    */
   private void populateDeposits(final List<Deposit> deposits,
      final atcomres.common.AtcomresBookingBaseResponse.PayData.Dpt deposit)
   {
      if ("LOW".equalsIgnoreCase(deposit.getType()))

      {
         final Deposit lowDeposit = populateDeposit(deposit);
         deposits.add(lowDeposit);
         lowDeposit.setDepositType(DepositType.LOW_DEPOSIT);
      }
      else if ("HIGH".equalsIgnoreCase(deposit.getType()))
      {
         final Deposit highDeposit = populateDeposit(deposit);
         deposits.add(highDeposit);
         highDeposit.setDepositType(DepositType.STANDARD_DEPOSIT);
      }
   }

   private Deposit populateDeposit(
      final atcomres.common.AtcomresBookingBaseResponse.PayData.Dpt deposit)
   {

      final Deposit dep = new Deposit();

      try
      {
         final Price depositPrice = new Price();
         final BigDecimal amt = new BigDecimal(deposit.getAmt());
         final Money money = new Money();
         money.setAmount(amt);
         depositPrice.setRate(money);
         depositPrice.setAmount(money);
         money.setCurrency(Currency.getInstance(deposit.getCurISO()));
         dep.setDepositDueDate(formatter.parse(deposit.getDepDt()));
         dep.setDepositAmount(depositPrice);
      }
      catch (final ParseException e)
      {
         LOGGER.error(DATE_PARSE_EXCEPTION);
      }
      return dep;

   }

   private void paymentBkgPriceInclusive(final atcomres.displayresponse.TILDisplayResponse source,
      final DisplayBookingResponse target)
   {

      final List<BookingHistory> bookingHistorylist = new ArrayList<BookingHistory>();

      final BookingHistory bookingHistory = new BookingHistory();
      final Agent agent = new Agent();
      agent.setAgentType(AgentType.WEB);
      agent.setAgentID(source.getDisplayResponse().getAgtNo());
      bookingHistory.setAgent(agent);

      final SalesChannel channel = new SalesChannel();
      channel.setSalesChannelType(SalesChannelType.RNP);
      bookingHistory.setSalesChannel(channel);
      final List<PaymentReference> paymentRefList = new ArrayList<PaymentReference>();
      final BookingDetails bookingDetails = new BookingDetails();
      final Money balDueAmt = new Money();

      final Money totalAmtPaid = new Money();

      if (source.getDisplayResponse().getPayData() != null)

      {

         for (final atcomres.common.AtcomresBookingBaseResponse.PayData.BkgPrcInc bkgPrcInc : source
            .getDisplayResponse().getPayData().getBkgPrcInc())
         {
            if (source.getDisplayResponse().getPayData().getPaymentReceived().get(0) != null)
            {
               totalAmtPaid.setAmount(new BigDecimal(source.getDisplayResponse().getPayData()
                  .getPaymentReceived().get(0)));

            }
            else
            {
               totalAmtPaid.setAmount(new BigDecimal(bkgPrcInc.getAmt()));
            }
            totalAmtPaid.setCurrency(Currency.getInstance(source.getDisplayResponse().getPayData()
               .getBkgPrcInc().get(0).getCurISO()));
            balDueAmt.setAmount(new BigDecimal(bkgPrcInc.getBalDueAmt()));
            balDueAmt.setCurrency(Currency.getInstance(bkgPrcInc.getCurISO()));
            try
            {
               bookingDetails.setDueDate(formatter.parse(bkgPrcInc.getBalDueDt()));

            }
            catch (final ParseException e)
            {
               LOGGER.error(DATE_PARSE_EXCEPTION);
            }

         }
      }
      paymentMadeDetails(source, paymentRefList);
      bookingHistory.setPaymentReferences(paymentRefList);
      bookingHistory.setBookingReference(source.getDisplayResponse().getBkgNum().getBkgId());
      bookingHistory.setBookingType(BookingType.PAYMENT);
      bookingHistorylist.add(bookingHistory);
      bookingDetails.setBookingHistory(bookingHistorylist);
      bookingDetails.setAmountDue(balDueAmt);
      bookingDetails.setAmountPaid(totalAmtPaid);
      target.setBookingDetails(bookingDetails);
   }

   private void populateAccomPrice(
      final atcomres.common.AtcomresBookingBaseResponse.BkgEnt.Package.Accom.RmCd roomCd,
      final Room room)
   {
      final List<Price> priceslist = new ArrayList<Price>();

      if (roomCd.getPrices() != null && roomCd.getPrices().getPrice() != null
         && !roomCd.getPrices().getPrice().isEmpty())
      {
         for (final atcomres.common.AtcomresBookingBaseResponse.BkgEnt.Package.Accom.RmCd.Prices.Price price : roomCd
            .getPrices().getPrice())
         {
            final uk.co.tui.book.domain.lite.Price accomPrice =
               new uk.co.tui.book.domain.lite.Price();
            accomPrice.setCode(price.getPrcCd());
            accomPrice.setDescription(price.getPrcCdName());
            if ("AA".equalsIgnoreCase(price.getPrcCd()))
            {
               accomPrice.setPriceType(PriceType.ADULT);
            }
            if ("AC".equalsIgnoreCase(price.getPrcCd()))
            {
               accomPrice.setPriceType(PriceType.CHILD);
            }
            if ("AI".equalsIgnoreCase(price.getPrcCd()))
            {
               accomPrice.setPriceType(PriceType.INFANT);
            }
            accomPrice.setQuantity(price.getQty().intValue());
            final Money money = new Money();
            money.setAmount(new BigDecimal(price.getPrc().getValue()).multiply(new BigDecimal(price
               .getQty().intValue())));
            accomPrice.setAmount(money);

            final Money moneyPerPerson = new Money();
            moneyPerPerson.setAmount(new BigDecimal(price.getPrc().getValue()));
            accomPrice.setRate(moneyPerPerson);

            accomPrice.setCode(price.getPrcCd());
            accomPrice.setPriceCodeType(price.getPrcCdTp());
            priceslist.add(accomPrice);
         }

         room.setPrices(priceslist);
      }
      else
      {
         final Price defaultprice = new Price();

         final Money money = new Money();
         money.setAmount(BigDecimal.ZERO);
         defaultprice.setAmount(money);
         defaultprice.setQuantity(Integer.valueOf(0));
         defaultprice.setRate(money);
         priceslist.add(defaultprice);
      }

   }

   private void populateFlightPrice(final RouteTp tp, final Leg leg)
   {
      final List<Price> priceslist = new ArrayList<Price>();
      BigDecimal legPrices = BigDecimal.ZERO;

      if (tp.getPrices() != null && tp.getPrices().getPrice() != null
         && !tp.getPrices().getPrice().isEmpty())
      {
         for (final atcomres.common.RouteTp.Prices.Price price : tp.getPrices().getPrice())
         {

            final Price flightPrice = new Price();
            flightPrice.setCode(price.getPrcCd());
            if ("FA".equalsIgnoreCase(price.getPrcCd()))
            {
               flightPrice.setPriceType(PriceType.ADULT);

            }
            if ("FI".equalsIgnoreCase(price.getPrcCd()))
            {
               flightPrice.setPriceType(PriceType.ADULT);

            }
            if ("FC".equalsIgnoreCase(price.getPrcCd()))
            {
               flightPrice.setPriceType(PriceType.ADULT);

            }
            flightPrice.setDescription(price.getPrcCdName());
            flightPrice.setQuantity(price.getQty().intValue());
            final Money money = new Money();
            money.setAmount(new BigDecimal(price.getPrc().getValue()).multiply(new BigDecimal(price
               .getQty().intValue())));
            flightPrice.setAmount(money);
            flightPrice.setRate(money);
            priceslist.add(flightPrice);
            legPrices = legPrices.add(flightPrice.getAmount().getAmount());

         }

         leg.setPriceList(priceslist);
      }

      else
      {
         final Price defaultprice = new Price();

         final Money money = new Money();
         money.setAmount(BigDecimal.ZERO);
         defaultprice.setAmount(money);
         defaultprice.setRate(money);
         defaultprice.setQuantity(Integer.valueOf(0));
         priceslist.add(defaultprice);
      }
      leg.setPriceList(priceslist);

   }

   private void totalPackagePrice(final atcomres.displayresponse.TILDisplayResponse source,
      final DisplayBookingResponse target)
   {
      if (source.getDisplayResponse().getPayData() != null)

      {
         for (final atcomres.common.AtcomresBookingBaseResponse.PayData.BkgPrcEx bkgPrcEx : source
            .getDisplayResponse().getPayData().getBkgPrcEx())
         {

            final Money money = new Money();
            money.setAmount(new BigDecimal(bkgPrcEx.getAmt()));
            money.setCurrency(Currency.getInstance(currencyResolver.getSiteCurrency()));

            final Money rate = new Money();
            rate.setAmount(new BigDecimal(bkgPrcEx.getAmt()));
            rate.setCurrency(Currency.getInstance(currencyResolver.getSiteCurrency()));

            final Price price = new Price();
            price.setRate(money);
            price.setAmount(rate);
            target.setPrice(price);

         }
      }

      else
      {
         final Price defaultprice = new Price();
         final Money money = new Money();
         money.setAmount(BigDecimal.ZERO);
         defaultprice.setAmount(money);

         final Money rate = new Money();
         rate.setAmount(BigDecimal.ZERO);
         defaultprice.setRate(rate);

         defaultprice.setQuantity(Integer.valueOf(0));
         target.setPrice(defaultprice);

      }
   }

   /**
    * .
    *
    * @param source
    * @param paymentRefList the
    */
   private void paymentMadeDetails(final atcomres.displayresponse.TILDisplayResponse source,
      final List<PaymentReference> paymentRefList)
   {
      if (source.getDisplayResponse().getPayData().getPay() != null)
      {
         final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-dd-MM");

         for (final Pay paymentRefernce : source.getDisplayResponse().getPayData().getPay())
         {

            final PaymentReference paymentRef = new PaymentReference();
            if (paymentRefernce.getCCPay() != null)
            {
               paymentRef.setAuthorizationCode(paymentRefernce.getAuthCode());
               final CardDetails cardDetails = new CardDetails();
               cardDetails.setCardNumber(paymentRefernce.getCCPay().getCNum());
               cardDetails.setCardType(CardTypes.VISA);
               paymentRef.setCardDetails(cardDetails);
            }
            paymentRef.setPaymentType(paymentRefernce.getPayDetails());
            BigDecimal amt = new BigDecimal(BigInteger.ZERO);
            if (paymentRefernce.getAmt() != null)
            {
               amt = new BigDecimal(paymentRefernce.getAmt());
            }
            final Money money = new Money();
            money.setAmount(amt);
            money.setCurrency(Currency.getInstance(paymentRefernce.getCurISO()));
            paymentRef.setAmountPaid(money);
            paymentRef.setAuthorizationCode(paymentRefernce.getAuthCode());
            try
            {
               final GregorianCalendar calendar =
                  paymentRefernce.getPayDtTm().toGregorianCalendar();
               final Date gregorianDate = calendar.getTime();
               final String dateString = dateFormatter.format(gregorianDate);
               final Date date = dateFormatter.parse(dateString);
               paymentRef.setPaymentDate(date);
            }
            catch (final IllegalFormatException e)
            {
               LOGGER.error(DATE_PARSE_EXCEPTION, e);
            }
            catch (final ParseException e)
            {
               LOGGER.error(DATE_PARSE_EXCEPTION, e);
            }
            paymentRefList.add(paymentRef);

         }

      }
   }

   /*
    * Car Hire Population
    */
   /*
    * (non-Javadoc)
    * 
    * @see de.hybris.platform.commerceservices.converter.Populator#populate(java .lang.Object,
    * java.lang.Object)
    */
   public void populateCar(final TILDisplayResponse source, final DisplayBookingResponse target)
      throws ConversionException
   {
      final List<ExtraFacility> carExtrafaciltyList = new ArrayList<ExtraFacility>();

      final List<ExtraFacility> exFacilityList = new ArrayList<ExtraFacility>();

      if (CollectionUtils.isNotEmpty(source.getDisplayResponse().getBkgEnt().getCarRental()))
      {
         for (final CarRental car : source.getDisplayResponse().getBkgEnt().getCarRental())
         {

            final Depot pickUpDepot = getPickUpDepot(car);
            final Depot dropOffDepot = getDropOffDepot(car);
            if (CollectionUtils.isNotEmpty(car.getFreeCars()))
            {
               for (final FreeCars freeCar : car.getFreeCars())
               {

                  populateCarRentalUpgrades(freeCar, exFacilityList, pickUpDepot, dropOffDepot,
                     target);
               }
            }
            if (CollectionUtils.isNotEmpty(car.getCarRentalDet()))
            {
               for (final CarRentalDet carRentailDetails : car.getCarRentalDet())
               {
                  final CarHireExtraFacility carExtra = new CarHireExtraFacility();
                  populateCarExtraFacility(carExtra, carRentailDetails, pickUpDepot, dropOffDepot,
                     target);
                  exFacilityList.add(carExtra);
                  carExtrafaciltyList.add(carExtra);
               }
            }
         }
         target.getIntegratedExtraFacilities().addAll(carExtrafaciltyList);

      }

   }

   /**
    * @param freeCar
    * @param dropOffDepot
    * @param pickUpDepot
    * @param target
    */
   private void populateCarRentalUpgrades(final FreeCars freeCar,
      final List<ExtraFacility> exFacilityList, final Depot pickUpDepot, final Depot dropOffDepot,
      final DisplayBookingResponse target)
   {

      if (CollectionUtils.isNotEmpty(freeCar.getCarRentalDet()))
      {
         for (final CarRentalDet carRental : freeCar.getCarRentalDet())
         {
            final CarHireExtraFacility carExtra = new CarHireExtraFacility();
            populateCarExtraFacility(carExtra, carRental, pickUpDepot, dropOffDepot, target);
            exFacilityList.add(carExtra);
         }
      }

   }

   /**
    * @param carExtra
    * @param carRental
    * @param dropOffDepot
    * @param pickUpDepot
    */
   private void populateCarExtraFacility(final CarHireExtraFacility carExtra,
      final CarRentalDet carRental, final Depot pickUpDepot, final Depot dropOffDepot,
      final DisplayBookingResponse target)
   {
      final Car car = carRental.getCar();
      carExtra.setInventoryCode(car.getCode());
      carExtra.setExtraFacilityCode(carRental.getCarType().getCode());
      carExtra.setDescription(car.getName());
      carExtra.setSelected(false);
      carExtra.setQuantity(carExtra.getQuantity());

      populateCarPrice(carExtra, carRental);

      for (final atcomres.common.SubServPaxs.SubServPax subServPaxs : carRental.getSubServPaxs()
         .getSubServPax())
      {
         populatePaxToExtraBasedOnID(target.getPassengers(), carExtra, subServPaxs.getPaxId()
            .intValue());
      }
      carExtra.setType(uk.co.tui.book.domain.lite.ExtraFacilityType.PACKAGE);
      carExtra.setPickupDepot(pickUpDepot);
      carExtra.setDropUpOff(dropOffDepot);
      carExtra.setCorporateCode(carRental.getCorporateCd());
   }

   /**
    * @param carExtra
    * @param carRental
    */
   private void populateCarPrice(final ExtraFacility carExtra, final CarRentalDet carRental)
   {
      Integer qnty = Integer.valueOf(0);
      String prc = null;
      final List<Price> priceList = new ArrayList<Price>();

      if (carRental.getPrices() != null
         && CollectionUtils.isNotEmpty(carRental.getPrices().getPrice()))
      {
         final List<atcomres.common.Prices.Price> carPriceList = carRental.getPrices().getPrice();
         final Price price = new Price();
         final Money money = new Money();
         price.setRate(money);
         price.setAmount(money);
         price.setPriceType(PriceType.ADULT);
         for (final atcomres.common.Prices.Price carHireList : carPriceList)
         {
            qnty = carHireList.getQty().intValue();
            prc = carHireList.getPrc().getValue();
         }
         final BigDecimal prc2 = new BigDecimal(prc);
         price.setQuantity(qnty);
         money.setAmount(prc2);
         priceList.add(price);
         carExtra.setSelection(uk.co.tui.book.domain.lite.FacilitySelectionType.SELECTABLE);
         carExtra.setPrices(priceList);
      }
      else
      {
         final Price defaultprice = new Price();

         final Money money = new Money();
         money.setAmount(BigDecimal.ZERO);
         defaultprice.setAmount(money);
         defaultprice.setRate(money);
         defaultprice.setQuantity(Integer.valueOf(0));
         priceList.add(defaultprice);
         carExtra.setSelection(uk.co.tui.book.domain.lite.FacilitySelectionType.FREE);
         carExtra.setPrices(priceList);

      }
      carExtra.setQuantity(qnty);
   }

   /**
    * @param car
    * @return dropOffDepot
    */
   private Depot getDropOffDepot(final CarRental car)
   {
      final Depot dropOffDepot = new Depot();
      dropOffDepot.setCode(car.getDropoff().getDepot().getCode());
      dropOffDepot.setType(car.getDropoff().getDepot().getDepotType());
      return dropOffDepot;
   }

   /**
    * @param car
    * @return pickupDepot
    */
   private Depot getPickUpDepot(final CarRental car)
   {
      final Depot pickupDepot = new Depot();
      pickupDepot.setCode(car.getPickup().getDepot().getCode());
      pickupDepot.setType(car.getPickup().getDepot().getDepotType());
      return pickupDepot;
   }

   private List<Price> populateOtherPrice(
      final List<atcomres.common.AtcomresBookingBaseResponse.BkgEnt.Prices.Price> list)
   {
      final List<Price> prices = new ArrayList<Price>();
      if (CollectionUtils.isNotEmpty(list))
      {
         for (final atcomres.common.AtcomresBookingBaseResponse.BkgEnt.Prices.Price price : list)
         {
            final Price pric = new Price();
            pric.setCode(price.getPrcCd());
            pric.setDescription(price.getPrcCdName());
            pric.setPriceCodeType(price.getPrcCdTp());
            final Money money = new Money();
            money.setAmount(new BigDecimal(price.getPrc().getValue()).multiply(new BigDecimal(price
               .getQty().intValue())));
            pric.setAmount(money);
            pric.setRate(money);

            if (price.getQty() != null)
            {
               pric.setQuantity(price.getQty().intValue());
            }
            prices.add(pric);
         }
      }
      else
      {
         final Price defaultprice = new Price();

         final Money money = new Money();
         money.setAmount(BigDecimal.ZERO);
         defaultprice.setAmount(money);
         defaultprice.setRate(money);
         prices.add(defaultprice);

      }
      return prices;
   }

   private boolean checkForEconomy(final DisplayBookingResponse target)
   {
      return ((Flight) target.getItinerary().getInBound().get(0).getCarrier()).getCabinClass()
         .getCabinName().contains("Economy");
   }

   private ExtraFacility populateDefaultSeatExtra(final DisplayBookingResponse target,
      final String fltInvId)
   {
      final ExtraFacility defaultSeatExtra = new ExtraFacility();
      defaultSeatExtra.setExtraFacilityGroup(ExtraFacilityGroup.FLIGHT);
      defaultSeatExtra.setInventoryCode(DEFAULT_SEAT);
      defaultSeatExtra.setInventoryId(fltInvId);
      final int quantity =
         PassengerUtils.getPersonTypeCountFromPassengers(target.getPassengers(),
            EnumSet.of(PersonType.ADULT, PersonType.TEEN, PersonType.CHILD, PersonType.SENIOR));
      defaultSeatExtra.setQuantity(quantity);
      defaultSeatExtra.setDescription("Allocated Seats ");
      final List<Price> priceList = new ArrayList<Price>();
      priceList.add(getPrice(PriceType.ADULT, "0.0", DEFAULT_SEAT, quantity));
      defaultSeatExtra.setPrices(priceList);
      defaultSeatExtra.setSelection(FacilitySelectionType.INCLUDED);
      for (final Passenger pax : target.getPassengers())
      {
         if (!(pax.getType() == PersonType.INFANT))
         {
            pax.getExtraFacilities().add(defaultSeatExtra);
            defaultSeatExtra.getPassengers().add(pax);
         }
      }
      return defaultSeatExtra;
   }

   private Price getPrice(final PriceType priceType, final String value, final String fltExtraCode,
      final int quantity)
   {
      final Price price = new Price();
      price.setPriceType(priceType);
      price.setRate(getMoney(value));
      price.setCode(fltExtraCode);
      price.setQuantity(quantity);
      price.setAmount(getMoney(value));
      return price;
   }

   /**
    * @param value
    */
   private Money getMoney(final String value)
   {
      final Money money = new Money();
      money.setAmount(new BigDecimal(value));
      return money;
   }

   /**
    * @param extraFacility
    * @param item
    */
   private void updateSelectionTypeForCharity(final ExtraFacility extraFacility, final Item item)
   {
      if (isSameCategory(item, "WC"))
      {
         extraFacility.setSelection(FacilitySelectionType.SELECTABLE);
      }
   }

   /**
    * @param item
    * @return is Charity
    */
   private boolean isSameCategory(final Item item, final String categoryCode)
   {
      return item.getItemType() != null
         && StringUtils.equalsIgnoreCase(item.getItemType().getCode(), categoryCode);
   }

   private FacilitySelectionType populateSelectionType(final Item item)
   {
      if (isItemSelected(item))
      {
         return FacilitySelectionType.INCLUDED;
      }
      return FacilitySelectionType.SELECTABLE;
   }

   /**
    * Checks if is item selected.
    *
    * @param item the item
    * @return true, if is item selected
    */
   private boolean isItemSelected(final Item item)
   {
      return item.isAutoInc();
   }

}
