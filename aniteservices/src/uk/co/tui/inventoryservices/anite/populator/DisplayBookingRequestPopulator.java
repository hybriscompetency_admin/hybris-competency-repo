/**
 *
 */
package uk.co.tui.inventoryservices.anite.populator;

import de.hybris.platform.commerceservices.converter.Populator;

import org.apache.log4j.Logger;

import atcomres.common.BkgNum;
import atcomres.displayrequest.DisplayRequest;
import co.uk.tui.phoenix.book.aniteservices.constants.AniteservicesConstants;
import uk.co.travel.domain.manage.request.BookingSearchRequest;

/**
 * @author sunil.bd
 *
 */
public class DisplayBookingRequestPopulator extends AtComresCommonPopulator
   implements Populator<BookingSearchRequest, atcomres.displayrequest.DisplayRequest>
{
   private static final Logger LOGGER = Logger.getLogger(DisplayBookingRequestPopulator.class);

   @Override
   public void populate(final BookingSearchRequest source, final DisplayRequest target)
   {
      LOGGER.info("Populating Booking Search Request");

      final BkgNum bknNum = new BkgNum();
      bknNum.setBkgId(source.getBookingReferenceNum());
      target.setBkgNum(bknNum);
      target.setAdm(getAdminInfo());
      target.setCltInfo(getClientInfo(AniteservicesConstants.COMRES_INVENTORY));
   }
}
