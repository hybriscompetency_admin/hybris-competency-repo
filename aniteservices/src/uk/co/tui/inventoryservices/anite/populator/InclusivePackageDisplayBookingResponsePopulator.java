package uk.co.tui.inventoryservices.anite.populator;

import de.hybris.platform.commerceservices.converter.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.List;

import org.apache.log4j.Logger;

import uk.co.travel.domain.manage.response.DisplayBookingResponse;
import uk.co.tui.book.domain.lite.Passenger;
import uk.co.tui.book.domain.lite.Stay;
import atcomres.common.AtcomresBookingBaseResponse.BkgEnt.Package.Accom;
import atcomres.common.AtcomresBookingBaseResponse.BkgEnt.Package.RouteList;

/**
 * @author premkumar.nd
 *
 */
public class InclusivePackageDisplayBookingResponsePopulator extends
   DisplayBookingResponsePopulator implements
   Populator<atcomres.displayresponse.TILDisplayResponse, DisplayBookingResponse>
{

   /** The Constant LOGGER. */
   private static final Logger LOGGER = Logger
      .getLogger(InclusivePackageDisplayBookingResponsePopulator.class);

   @Override
   public void populate(final atcomres.displayresponse.TILDisplayResponse source,
      final DisplayBookingResponse target) throws ConversionException
   {
      LOGGER.info("Inside the InclusiveResponsePopulator");
      populatePassengerList(source, target);
      if (source.getDisplayResponse() != null && source.getDisplayResponse().getBkgEnt() != null
         && source.getDisplayResponse().getBkgEnt().getPackage() != null
         && !source.getDisplayResponse().getBkgEnt().getPackage().isEmpty())
      {
         final RouteList routeList =
            source.getDisplayResponse().getBkgEnt().getPackage().get(0).getRouteList();

         if (routeList.getRouting() != null)
         {
            target.getMemos().addAll(populateFlightErrata(routeList.getRouting()));
         }
         populateRoutingDetails(routeList, target);

         final List<Object> accomList =
            source.getDisplayResponse().getBkgEnt().getPackage().get(0)
               .getAccomOrMultiCentreOrRoundtrip();

         final Stay availableAccommodation = new Stay();

         checkAccommodationList(accomList, availableAccommodation, target.getPassengers());
         target.getMemos().addAll(availableAccommodation.getErrataMemo());
         target.setStay(availableAccommodation);
      }

      super.populate(source, target);
   }

   /**
    * @param accomList
    * @param availableAccommodation
    * @param passenger
    */
   private void checkAccommodationList(final List<Object> accomList,
      final Stay availableAccommodation, final List<Passenger> passenger)
   {
      for (final Object objAccomodation : accomList)
      {
         final Accom ammo = (Accom) objAccomodation;
         populateAccomodationDetails(ammo, availableAccommodation, passenger);
      }
   }
}
