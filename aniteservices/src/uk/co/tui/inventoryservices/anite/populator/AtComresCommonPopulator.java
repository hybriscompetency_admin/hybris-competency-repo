/**
 *
 */
package uk.co.tui.inventoryservices.anite.populator;

import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.servicelayer.cluster.ClusterService;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.map.MultiValueMap;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;

import uk.co.tui.async.logging.TUILogUtils;
import uk.co.tui.book.domain.lite.BaggageExtraFacility;
import uk.co.tui.book.domain.lite.BasePackage;
import uk.co.tui.book.domain.lite.ExtraFacility;
import uk.co.tui.book.domain.lite.ExtraFacilityCategory;
import uk.co.tui.book.domain.lite.ExtraFacilityGroup;
import uk.co.tui.book.domain.lite.ExtraFacilityType;
import uk.co.tui.book.domain.lite.FlightLeg;
import uk.co.tui.book.domain.lite.HighLights;
import uk.co.tui.book.domain.lite.InsuranceExtraFacility;
import uk.co.tui.book.domain.lite.InsuranceType;
import uk.co.tui.book.domain.lite.InventoryType;
import uk.co.tui.book.domain.lite.Itinerary;
import uk.co.tui.book.domain.lite.Leg;
import uk.co.tui.book.domain.lite.Passenger;
import uk.co.tui.book.domain.lite.PersonType;
import uk.co.tui.book.domain.lite.Price;
import uk.co.tui.book.domain.lite.Room;
import uk.co.tui.book.domain.lite.Stay;
import uk.co.tui.book.domain.lite.SupplierProductDetails;
import uk.co.tui.book.services.CurrencyResolver;
import uk.co.tui.book.services.PackageComponentService;
import uk.co.tui.inventoryservices.anite.constants.InventoryServiceConstants;
import uk.co.tui.inventoryservices.anite.vrp.AniteGender;
import atcomres.common.AtcomresBookingBaseRequest.BkgEnt.Item;
import atcomres.common.AtcomresBookingBaseRequest.BkgEnt.Item.ItemType;
import atcomres.common.AtcomresBookingBaseRequest.BkgEnt.Package.Accom.RmCd;
import atcomres.common.AtcomresBookingBaseRequest.BkgEnt.Package.RouteList;
import atcomres.common.Baggage;
import atcomres.common.FltExtra;
import atcomres.common.FltExtra.SubServPaxs;
import atcomres.common.FltExtra.SubServPaxs.SubServPax;
import atcomres.common.FltExtraCat;
import atcomres.common.FltExtraCatList;
import atcomres.common.Piece;
import atcomres.common.PrcType;
import atcomres.common.Prom;
import atcomres.common.RouteCd;
import atcomres.common.RouteTp;
import atcomres.common.Source;
import atcomres.common.Weight;

/**
 * d to populate all the elements in the AtcomresBookingBaseRequest. Any other
 * request(flightextrassearchrequest, infobookingrequest and bookrequest) that has the common
 * elements as this request can make use of this class's methods to populate them in their request.
 *
 * @author anithamani.s
 *
 */

class AtComresCommonPopulator
{

   /** The Constant MULTICENTRE_BB_CD. */
   private static final String MULTICENTRE_BB_CD = "NB";

   /** The Constant MULTICENTRE_RM_CD. */
   private static final String MULTICENTRE_RM_CD = "MC";

   private static final String EXTRAFACILITY_PROMOCODE = "extrafacility.promocode.";

   /** The Constant LOG used for logging. */
   private static final TUILogUtils LOG = new TUILogUtils("AtComresCommonPopulator");

   private static final String INBOUND = "IN";

   private static final String OUTBOUND = "OUT";

   /** (Magic number Sonar fix). */
   private static final int THREE = 3;

   private static final int EIGHT = 8;

   private static final int TWO = 2;

   /** The Constant ONE. */
   private static final int ONE = 1;

   /** The Constant DATE_FORMAT. */
   private static final String DATE_FORMAT = "dd-MM-yyyy";

   /** The Constant DATE_FORMAT_1. */
   private static final String DATE_FORMAT_1 = "yyyy-MM-dd";

   @Resource
   private ClusterService clusterService;

   @Resource
   private CMSSiteService cmsSiteService;

   @Resource
   private CurrencyResolver currencyResolver;

   /** The package component service. */
   @Resource
   private PackageComponentService packageComponentService;

   private static final List<String> TRACSDIFFERENTQAUNTITY_EXTRA_CATEGORIES_CODES = Arrays.asList(
      "Excursion", "Attraction", "INS");

   /**
    * Gets the BigInteger value of the value passed.
    *
    * @param value the value to be converted into BigInteger
    * @return the BigInteger equivalent value of the passed value
    */
   BigInteger getBigIntegerValueOf(final int value)
   {
      return BigInteger.valueOf(value);
   }

   /**
    * Gets the current date in xML gregorian format.
    *
    * @return the xML gregorian date
    */
   XMLGregorianCalendar getXMLGregorianDate()

   {
      final GregorianCalendar gregorianCalendar = new GregorianCalendar();
      try
      {
         final DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
         return datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
      }
      catch (final DatatypeConfigurationException dataTypeConfigExc)
      {
         LOG.error("error in generating XMLGregorian date format for current date... "
            + dataTypeConfigExc);
      }
      return null;
   }

   /**
    * Gets the current date in xML gregorian format.
    *
    * @return the xML gregorian date
    */
   public XMLGregorianCalendar getXMLGregorianDate(final Date date)

   {
      final GregorianCalendar gregorianCalendar = new GregorianCalendar();
      gregorianCalendar.setTime(date);
      try
      {
         final DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
         return datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
      }
      catch (final DatatypeConfigurationException dataTypeConfigExc)
      {
         LOG.error("error in generating XMLGregorian date format for the holiday date... "
            + dataTypeConfigExc);
      }
      return null;
   }

   /**
    * This method is used to get the tracking details. The tracking defines the direction of
    * communication and the participating system names
    *
    * @return the trk details
    */
   public atcomres.common.AtcomresBaseRequest.Adm.Trk getTrackingDetails()
   {
      final atcomres.common.AtcomresBaseRequest.Adm.Trk trk =
         new atcomres.common.ObjectFactory().createAtcomresBaseRequestAdmTrk();
      trk.setFrom("phoenix");
      trk.setTo("atcomres");
      return trk;
   }

   /**
    * Gets the administration info.
    *
    * @return the admin info
    */
   public atcomres.common.AtcomresBaseRequest.Adm getAdminInfo()
   {
      final atcomres.common.AtcomresBaseRequest.Adm adm =
         new atcomres.common.ObjectFactory().createAtcomresBaseRequestAdm();
      adm.setXsdVer("4.46.0");
      adm.setDebug(Boolean.FALSE);
      adm.setReqId(generateRequestID());
      adm.setTm(getXMLGregorianDate());
      adm.setTrk(getTrackingDetails());
      return adm;
   }

   /**
    * @return String auto-generated request ID
    */
   private String generateRequestID()
   {
      final StringBuilder correlationID = new StringBuilder();
      correlationID.append("node");
      correlationID.append(clusterService.getClusterId());
      correlationID.append("-");
      correlationID.append(RandomStringUtils.randomAlphanumeric(EIGHT));
      return correlationID.toString();
   }

   /**
    * Gets the Client Information describing the requesting client.
    *
    * @return the client info
    */
   atcomres.common.AtcomresBaseRequest.CltInfo getClientInfo(final String inventorySource)
   {
      final atcomres.common.AtcomresBaseRequest.CltInfo cltInfo =
         new atcomres.common.ObjectFactory().createAtcomresBaseRequestCltInfo();

      final StringBuilder configAppender = new StringBuilder();
      configAppender.append(inventorySource).append(".");
      configAppender.append("TH");

      cltInfo.setLocale("en_EN");
      cltInfo.setCltSysContext("3");

      cltInfo.setAgtNo("H6265");
      cltInfo.setAgtPwd("Welcome8");
      cltInfo.setUserName("THUSER");

      cltInfo.setTermCode("VTX");
      cltInfo.setChan("1100");
      cltInfo.setUserRole("INTERNAL");
      cltInfo.setExpNo("WEB");

      return cltInfo;
   }

   /**
    * Gets the persons assigned to this room.
    *
    * @param roomModel the room model
    * @return the list containing the indices for each passenger in the room
    */

   private atcomres.common.AtcomresBookingBaseRequest.BkgEnt.Package.Accom.RmCd.SubServPaxs getSubServicePassengers(
      final Room roomModel)
   {
      final atcomres.common.AtcomresBookingBaseRequest.BkgEnt.Package.Accom.RmCd.SubServPaxs srvcPaxs =
         new atcomres.common.ObjectFactory()
            .createAtcomresBookingBaseRequestBkgEntPackageAccomRmCdSubServPaxs();
      if (CollectionUtils.isNotEmpty(roomModel.getPassgengers()))
      {
         for (final Passenger pax : roomModel.getPassgengers())
         {
            final atcomres.common.AtcomresBookingBaseRequest.BkgEnt.Package.Accom.RmCd.SubServPaxs.SubServPax srvcPax =
               new atcomres.common.ObjectFactory()
                  .createAtcomresBookingBaseRequestBkgEntPackageAccomRmCdSubServPaxsSubServPax();
            srvcPax.setPaxId(getBigIntegerValueOf(pax.getId().intValue()));
            srvcPaxs.getSubServPax().add(srvcPax);
         }
      }
      return srvcPaxs;
   }

   /**
    * Populate routing.
    *
    * @param packageModel the source
    * @return RouteList the route list
    */
   private atcomres.common.AtcomresBookingBaseRequest.BkgEnt.RouteList getFORoutingDetails(
      final BasePackage packageModel)
   {
      final atcomres.common.AtcomresBookingBaseRequest.BkgEnt.RouteList routelist =
         new atcomres.common.ObjectFactory().createAtcomresBookingBaseRequestBkgEntRouteList();
      final Itinerary flightItinerary = packageComponentService.getFlightItinerary(packageModel);
      final List<Leg> outBoundLegs = flightItinerary.getOutBound();
      final List<Leg> inBoundLegs = flightItinerary.getInBound();
      final String productCode =
         StringUtils.substringBefore(getProductCodeFromPackage(packageModel), "/");
      final atcomres.common.AtcomresBookingBaseRequest.BkgEnt.RouteList.Routing routing =
         new atcomres.common.ObjectFactory()
            .createAtcomresBookingBaseRequestBkgEntRouteListRouting();
      routing.setRoutingId(BigInteger.ONE);

      List<JAXBElement<atcomres.common.RouteTp>> routesTps =
         populateRoutes(packageModel, outBoundLegs, OUTBOUND, packageModel.getDuration(),
            packageModel.getPassengers(), productCode);
      for (final JAXBElement<atcomres.common.RouteTp> routeTP : routesTps)
      {
         routing.getCoachOrFerryOrRoute().add(routeTP);
      }

      if (CollectionUtils.isNotEmpty(inBoundLegs))
      {
         routesTps =
            populateRoutes(packageModel, inBoundLegs, INBOUND, packageModel.getDuration(),
               packageModel.getPassengers(), productCode);
         for (final JAXBElement<atcomres.common.RouteTp> routeTP : routesTps)
         {
            routing.getCoachOrFerryOrRoute().add(routeTP);
         }
      }

      routelist.getRouting().add(routing);
      return routelist;
   }

   /**
    * Populate routing.
    *
    * @param packageModel the source
    * @return RouteList the route list
    */
   protected RouteList getRoutingDetails(final BasePackage packageModel)
   {
      final RouteList routelist =
         new atcomres.common.ObjectFactory()
            .createAtcomresBookingBaseRequestBkgEntPackageRouteList();
      final Itinerary flightItinerary = packageComponentService.getFlightItinerary(packageModel);
      final List<Leg> outBoundLegs = flightItinerary.getOutBound();
      final List<Leg> inBoundLegs = flightItinerary.getInBound();
      final String productCode = getProductCodeFromPackage(packageModel);
      LOG.debug("PromoCode is " + productCode);
      final atcomres.common.AtcomresBookingBaseRequest.BkgEnt.Package.RouteList.Routing routing =
         new atcomres.common.ObjectFactory()
            .createAtcomresBookingBaseRequestBkgEntPackageRouteListRouting();
      routing.setRoutingId(BigInteger.ONE);

      List<JAXBElement<atcomres.common.RouteTp>> routesTps =
         populateRoutes(packageModel, outBoundLegs, OUTBOUND, packageModel.getDuration(),
            packageModel.getPassengers(), productCode);
      LOG.debug("Adding routeTp one by one - Outbound not null");
      for (final JAXBElement<atcomres.common.RouteTp> routeTP : routesTps)
      {
         routing.getCoachOrFerryOrRoute().add(routeTP);
      }
      // Handled for one-way flight
      if (CollectionUtils.isNotEmpty(inBoundLegs))
      {
         routesTps =
            populateRoutes(packageModel, inBoundLegs, INBOUND, packageModel.getDuration(),
               packageModel.getPassengers(), productCode);
         LOG.debug("Adding routeTp one by one - Inbound not null");
         for (final JAXBElement<atcomres.common.RouteTp> routeTP : routesTps)
         {
            routing.getCoachOrFerryOrRoute().add(routeTP);
         }
         routing.setRoutingType(InventoryServiceConstants.FLIGHT_WITH_RETURN);
      }
      else
      {
         routing.setRoutingType(InventoryServiceConstants.FLIGHT_ONE_WAY);
      }
      routelist.getRouting().add(routing);

      return routelist;
   }

   /**
    * Populate routes.
    *
    * @param outBoundLegs the leg model
    * @param bound the bound
    * @param duration the duration
    * @param passengers the passengers
    * @param productCode the product code
    */
   public List populateRoutes(final BasePackage packageModel, final List<Leg> outBoundLegs,
      final String bound, final Integer duration, final List<Passenger> passengers,
      final String productCode)
   {
      final List routeTps = new ArrayList();
      for (final Leg leg : outBoundLegs)
      {
         final atcomres.common.RouteTp routeTP =
            new atcomres.common.ObjectFactory().createRouteTp();

         final atcomres.common.RouteTp.FltDtTm outFlightDate =
            new atcomres.common.RouteTp.FltDtTm();
         final atcomres.common.RouteTp.FltDtTm inFlightDate = new atcomres.common.RouteTp.FltDtTm();
         final String departureAirportCode = leg.getDepartureAirport().getCode();
         final String arrivalAirportCode = leg.getArrivalAirport().getCode();
         setRouteDirection(bound, routeTP);
         setExtraFlightFlag(leg, routeTP);
         routeTP.setFltInvId(leg.getInventoryId());
         setFlightPrice(packageComponentService.getFlightItinerary(packageModel), routeTP);

         routeTP.setDepAirCd(departureAirportCode);
         routeTP.setArrAirCd(arrivalAirportCode);

         outFlightDate.setDirType("DEPARTURE");
         outFlightDate.setLocal(getXMLGregorianDate(leg.getSchedule().getDepartureDate())
            .toString());
         routeTP.getFltDtTm().add(outFlightDate);

         inFlightDate.setDirType("ARRIVAL");
         inFlightDate.setLocal(getXMLGregorianDate(leg.getSchedule().getArrivalDate()).toString());

         routeTP.getFltDtTm().add(inFlightDate);

         routeTP.setProm(getAtcomresCommonRouteTpProm(productCode));

         if (leg.getCarrier().getCarrierInformation() != null)
         {
            final String marketingAirlineCode =
               leg.getCarrier().getCarrierInformation().getMarketingAirlineCode();
            routeTP.setCarCd(StringUtils.substring(marketingAirlineCode, 0, THREE).trim());
         }

         routeTP.getSerSts().add("FIX");
         routeTP.setSubServPaxs(getAtcomresCommonRouteTpSubServPaxs(passengers));

         final JAXBElement<atcomres.common.RouteTp> routeT =
            new atcomres.common.ObjectFactory()
               .createAtcomresBookingBaseRequestBkgEntPackageRouteListRoutingRoute(routeTP);
         routeTps.add(routeT);
      }

      return routeTps;
   }

   /**
    * @param bound
    * @param routeTP
    */
   protected void setRouteDirection(final String bound, final atcomres.common.RouteTp routeTP)
   {
      if (StringUtils.equalsIgnoreCase(bound, "IN"))
      {
         routeTP.setRtDir("inbound");
      }

      if (StringUtils.equalsIgnoreCase(bound, "OUT"))
      {
         routeTP.setRtDir("outbound");
      }
   }

   /**
    * The method checks whether the flight is from third party inventory.
    *
    * @param leg
    * @param routeTP
    */
   protected void setExtraFlightFlag(final Leg leg, final atcomres.common.RouteTp routeTP)
   {
      if (BooleanUtils.isTrue(Boolean.valueOf(leg.isExternalInventory())))
      {
         routeTP.setRtInvState("EXTERNAL");
         routeTP.setFltNo(leg.getCarrier().getNumber());
      }
   }

   /**
    * The method populates the Flight Price.
    *
    * @param routeTP
    */
   protected void setFlightPrice(final Itinerary flightItinerary,
      final atcomres.common.RouteTp routeTP)
   {
      final List<Price> priceList = getFlightPrice(flightItinerary, routeTP);
      if (CollectionUtils.isNotEmpty(priceList))
      {
         for (final Price packagePrice : priceList)
         {
            final atcomres.common.PrcType price = new atcomres.common.PrcType();
            price.setValue(packagePrice.getRate().getAmount().toString());
            price.setCurISO(currencyResolver.getSiteCurrency());
            setPrice(price, packagePrice.getCode(), routeTP);
         }
      }
   }

   /**
    * @param flightItinerary
    * @return flightPriceList
    */
   private List<Price> getFlightPrice(final Itinerary flightItinerary,
      final atcomres.common.RouteTp routeTP)
   {
      if (StringUtils.equalsIgnoreCase("outbound", routeTP.getRtDir()))
      {
         return getPriceModel(flightItinerary.getOutBound(), routeTP.getFltInvId());
      }
      else
      {
         return getPriceModel(flightItinerary.getInBound(), routeTP.getFltInvId());
      }
   }

   /**
    * Gets the price model.
    *
    * @param list the leg models
    * @param inventoryId the inventory id
    * @return the price model
    */
   private List<Price> getPriceModel(final List<Leg> list, final String inventoryId)
   {
      List<Price> flightPrices = Collections.emptyList();
      for (final Leg leg : list)
      {
         if (StringUtils.equalsIgnoreCase(leg.getInventoryId(), inventoryId))
         {
            flightPrices = leg.getPriceList();
            break;
         }
      }
      return flightPrices;
   }

   /**
    * Sets the price.
    *
    * @param price the price
    * @param code the code
    * @param routeTP the route tp
    */
   private void setPrice(final PrcType price, final String code, final RouteTp routeTP)
   {
      if (StringUtils.equalsIgnoreCase(code, "FA"))
      {
         routeTP.setAdtFltRate(price);
      }
      else if (StringUtils.equalsIgnoreCase(code, "FC"))
      {
         routeTP.setChdFltRate(price);
      }
   }

   /**
    * Gets the sub service passenger. Used for MultiCentre packages.
    *
    * @param passengers the passengers
    * @return the sub service passenger
    */
   atcomres.common.RouteTp.SubServPaxs getAtcomresCommonRouteTpSubServPaxs(
      final List<Passenger> passengers)
   {
      final atcomres.common.RouteTp.SubServPaxs subServPaxs =
         new atcomres.common.ObjectFactory().createRouteTpSubServPaxs();
      if (CollectionUtils.isNotEmpty(passengers))
      {
         for (final Passenger passenger : passengers)
         {
            final atcomres.common.RouteTp.SubServPaxs.SubServPax subServPax =
               new atcomres.common.ObjectFactory().createRouteTpSubServPaxsSubServPax();
            subServPax.setPaxId(getBigIntegerValueOf(passenger.getId().intValue()));
            subServPaxs.getSubServPax().add(subServPax);
         }
         return subServPaxs;
      }
      return null;
   }

   /**
    * Gets the selling code for holiday.
    *
    * @param sellingCode the selling code
    * @return the selling code for holiday
    */
   private atcomres.common.SellProm getAtcomresCommonSellProm(final String sellingCode)
   {
      final atcomres.common.SellProm sellingPromotion =
         new atcomres.common.ObjectFactory().createSellProm();
      sellingPromotion.setCode(sellingCode);
      return sellingPromotion;
   }

   /**
    * Populate route code.
    *
    * @param legModel the leg
    * @return RouteCd
    */
   private atcomres.common.RouteCd getRouteCode(final Leg legModel)
   {
      if (StringUtils.isNotEmpty(((FlightLeg) legModel).getRouteCode()))
      {

         final RouteCd routeCode = new atcomres.common.ObjectFactory().createRouteCd();
         routeCode.setValue(((FlightLeg) legModel).getRouteCode());
         return routeCode;
      }
      return null;
   }

   /**
    * Gets the flight journey duration.
    *
    * @param duration the duration
    * @return the flight journey duration
    */
   public BigInteger getFlightJourneyDuration(final String duration)
   {
      if (StringUtils.isNotEmpty(duration))
      {
         return getBigIntegerValueOf(Integer.parseInt(duration));
      }
      return null;
   }

   /**
    * Gets the prom code.
    *
    * @param productCode the product code
    * @return the prom code
    */
   atcomres.common.RouteTp.Prom getAtcomresCommonRouteTpProm(final String productCode)
   {
      final atcomres.common.RouteTp.Prom promotion =
         new atcomres.common.ObjectFactory().createRouteTpProm();
      final String promotionCode = StringUtils.substringBefore(productCode, "/");
      promotion.setCode(promotionCode);
      return promotion;
   }

   /**
    * Populate all flight extras.
    *
    * @param extraFacilityCategories the extra facility categories
    * @return the list
    */
   public List<FltExtraCatList> populateAllFlightExtras(
      final List<ExtraFacilityCategory> extraFacilityCategories)
   {
      final List<FltExtraCatList> fltExtraCatLists = new ArrayList<FltExtraCatList>();
      final Map<String, List<ExtraFacility>> extrasGroupedByInventoryId =
         getExtraFacilitiesGroupedByInvetoryId(extraFacilityCategories);

      for (final Entry<String, List<ExtraFacility>> extrasEntry : extrasGroupedByInventoryId
         .entrySet())
      {
         final FltExtraCatList fltExtraCatList = new FltExtraCatList();
         fltExtraCatList.setFltInvId(extrasEntry.getKey());

         final Map<String, List<ExtraFacility>> extrasGroupedByExtraType =
            getExtrasGroupedByExtraType(extrasEntry.getValue());

         for (final Entry<String, List<ExtraFacility>> extraEntry : extrasGroupedByExtraType
            .entrySet())
         {
            final FltExtraCat fltExtraCat = new FltExtraCat();
            fltExtraCat.setCode(extraEntry.getKey());
            final List<ExtraFacility> extrasList = extraEntry.getValue();
            final ExtraFacilityCategory extraFacilityCategory =
               extrasList.get(0).getExtraFacilityCategory();
            fltExtraCat.setName(extraFacilityCategory.getDescription());
            fltExtraCat.setMethod(extraFacilityCategory.getCode());
            final List<FltExtra> fltExtras = getFlightExtra(extrasList);
            fltExtraCat.getFltExtra().addAll(fltExtras);
            fltExtraCatList.getFltExtraCat().add(fltExtraCat);

         }
         fltExtraCatLists.add(fltExtraCatList);
      }
      return fltExtraCatLists;
   }

   /**
    * Gets the extras grouped by extra type.
    *
    * @param extraList the extra list
    * @return the extras grouped by extra type
    */
   protected Map<String, List<ExtraFacility>> getExtrasGroupedByExtraType(
      final List<ExtraFacility> extraList)
   {
      final MultiValueMap extrasGroupedByExtraType = new MultiValueMap();
      for (final ExtraFacility extra : extraList)
      {
         if (StringUtils.equalsIgnoreCase(extra.getInventoryCode(),
            InventoryServiceConstants.GROUP_SEAT_EXTRA_CODE))
         {
            extrasGroupedByExtraType.put(InventoryServiceConstants.GROUP_SEAT_CATEGORY_CODE, extra);
            continue;
         }
         extrasGroupedByExtraType.put(extra.getExtraFacilityCategory().getCode(), extra);
      }

      return extrasGroupedByExtraType;
   }

   /**
    * Gets the extra facilities grouped by invetory id.
    *
    * @param extraFacilityCategories the extra facility categories
    * @return the extra facilities grouped by invetory id
    */
   protected Map<String, List<ExtraFacility>> getExtraFacilitiesGroupedByInvetoryId(
      final List<ExtraFacilityCategory> extraFacilityCategories)
   {
      final MultiValueMap extrasGroupedByInventoryId = new MultiValueMap();
      for (final ExtraFacilityCategory flightExtraCat : extraFacilityCategories)
      {
         for (final ExtraFacility extra : flightExtraCat.getExtraFacilities())
         {
            addExtraFacilityGroupedByInventoryId(extrasGroupedByInventoryId, extra);
         }
      }
      return extrasGroupedByInventoryId;
   }

   /**
    * @param extrasGroupedByInventoryId
    * @param extra
    */
   private void addExtraFacilityGroupedByInventoryId(
      final MultiValueMap extrasGroupedByInventoryId, final ExtraFacility extra)
   {
      if (isExtraGroupedByInventoryId(extra))
      {
         extrasGroupedByInventoryId.put(extra.getInventoryId(), extra);
      }
      else if (StringUtils.equalsIgnoreCase(extra.getExtraFacilityCategory().getCode(), "BAG")
         && !isPriceZero(extra))
      {
         extrasGroupedByInventoryId.put(extra.getInventoryId(), extra);
      }
   }

   /**
    * @param extra
    * @return
    */
   private boolean isPriceZero(final ExtraFacility extra)
   {
      return extra.getPrices().get(0).getRate().getAmount().compareTo(BigDecimal.ZERO) == 0;
   }

   /**
    * @param extra
    * @return true if isExtraGroupedByInventoryId
    */
   private boolean isExtraGroupedByInventoryId(final ExtraFacility extra)
   {
      return (StringUtils.equals(extra.getExtraFacilityGroup().toString(),
         ExtraFacilityGroup.FLIGHT.toString()))
         && !isExtraRemovableFromInfoBook(extra.getInventoryCode()) && extra.isBookable();
   }

   /**
    * Gets the flight extra.
    *
    * @param extrasList the extras list
    *
    * @return the flight extra
    */
   protected List<FltExtra> getFlightExtra(final List<ExtraFacility> extrasList)
   {
      final List<FltExtra> flightExtras = new ArrayList<FltExtra>();
      for (final ExtraFacility extra : extrasList)
      {
         final List<String> extraCodes =
            Arrays.asList(StringUtils.split(extra.getInventoryCode(), '_'));

         populateFlightExtras(flightExtras, extra, extraCodes);
      }
      return flightExtras;
   }

   /**
    * This method populates flight extras
    *
    * @param flightExtras
    * @param extra
    * @param extraCodes
    */
   private void populateFlightExtras(final List<FltExtra> flightExtras, final ExtraFacility extra,
      final List<String> extraCodes)
   {
      for (final String extraCode : extraCodes)
      {
         FltExtra fltExtra = getFlightExtraIfAlreadyPresent(flightExtras, extraCode);
         if (fltExtra == null)
         {
            fltExtra = new FltExtra();
            updateBaggageExtraFacility(extra, fltExtra);
            final SubServPaxs subServPax = new SubServPaxs();
            fltExtra.setSubServPaxs(subServPax);
            final String bookMethod = extra.getType() == ExtraFacilityType.PAX ? "PAX" : "SER";
            fltExtra.setBookMth(bookMethod);
            fltExtra.setCode(extraCode);
            fltExtra.setName(extra.getDescription());

            flightExtras.add(fltExtra);
         }
         setPaxConfiguration(extra, fltExtra);
      }
   }

   /**
    * This method updates the baggage extra facility details.
    *
    * @param extra
    * @param fltExtra
    */
   protected void updateBaggageExtraFacility(final ExtraFacility extra, final FltExtra fltExtra)
   {
      if (extra instanceof BaggageExtraFacility)
      {

         final BaggageExtraFacility baggageExtra = (BaggageExtraFacility) extra;

         final Baggage baggage = new Baggage();

         final Piece piece = new Piece();
         // Price of the baggage is the value for the piece
         // component
         // No of pieces for the weight will be cd value for the
         // pieces
         piece.setCd(String.valueOf(baggageExtra.getBaggagePieces()));
         piece.setValue(baggageExtra.getPrice().getAmount().getAmount().toString());
         final Weight weight = new Weight();
         weight.getPiece().add(piece);
         // Baggage allowed per a person will be the baggage
         // weight which will be cd value for weight
         weight.setCd(String.valueOf(baggageExtra.getBaggageWeight()));
         baggage.getWeight().add(weight);

         fltExtra.setBaggage(baggage);
      }
   }

   /**
    * This method sets the PaxConfiguration
    *
    * @param extra
    * @param fltExtra
    */
   protected void setPaxConfiguration(final ExtraFacility extra, final FltExtra fltExtra)
   {
      for (final Passenger pax : extra.getPassengers())
      {
         final atcomres.common.FltExtra.SubServPaxs.SubServPax subServPax = new SubServPax();
         subServPax.setPaxId(BigInteger.valueOf(pax.getId().intValue()));
         fltExtra.getSubServPaxs().getSubServPax().add(subServPax);
      }
   }

   /**
    * Gets the flight extra if already present.
    *
    * @param flightExtras the flight extras
    * @param extraCode the extra code
    * @return the flight extra if already present
    */
   protected FltExtra getFlightExtraIfAlreadyPresent(final List<FltExtra> flightExtras,
      final String extraCode)
   {
      final List<FltExtra> fltExtras =
         (List<FltExtra>) CollectionUtils.select(flightExtras, new Predicate()
         {
            @Override
            public boolean evaluate(final Object flightExtra)
            {

               return StringUtils.equalsIgnoreCase(((FltExtra) flightExtra).getCode(), extraCode);
            }

         });
      return CollectionUtils.isEmpty(fltExtras) ? null : fltExtras.get(0);

   }

   /**
    * Gets the all extra facilities.
    *
    * @param packageModel the package model
    * @return the all extra facilities
    */
   @SuppressWarnings("deprecation")
   List<Item> getAllExtraFacilities(final BasePackage packageModel)
   {
      final List<Item> items = new ArrayList<Item>();
      final String extraFacilityPromoCode = getExtraFacilityPromoCode(packageModel);
      for (final ExtraFacilityCategory extraFacilityCategoryModel : packageModel
         .getExtraFacilityCategories())
      {
         for (final ExtraFacility extraFacilityModel : extraFacilityCategoryModel
            .getExtraFacilities())
         {
            populateItems(packageModel, items, extraFacilityCategoryModel, extraFacilityModel,
               extraFacilityPromoCode);
         }
      }
      return items;
   }

   /**
    * Gets the extra facility promo code.
    *
    * @param packageModel the package model
    * @return the extra facility promo code
    */
   protected String getExtraFacilityPromoCode(final BasePackage packageModel)
   {
      if (packageModel.getInventory().getInventoryType() == InventoryType.TRACS)
      {
         return getProductCodeFromPackage(packageModel);
      }
      return "AUCI" + packageModel.getPrice().getAmount().getCurrency().getCurrencyCode();
   }

   /**
    * This method populates items from extra facilities.
    *
    *
    * @param packageModel
    * @param items
    * @param extraFacilityCategoryModel
    * @param extraFacilityModel
    */
   protected void populateItems(final BasePackage packageModel, final List<Item> items,
      final ExtraFacilityCategory extraFacilityCategoryModel,
      final ExtraFacility extraFacilityModel, final String extraFacilityPromoCode)
   {
      if (isBookableNonFlightExtra(extraFacilityModel))
      {

         final Itinerary flightitinerary = packageComponentService.getFlightItinerary(packageModel);

         final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

         final ItemType itemType = new ItemType();
         itemType.setCode(extraFacilityCategoryModel.getCode());

         final Prom prom = new Prom();

         prom.setCode(extraFacilityPromoCode);

         createItem(packageModel, items, extraFacilityModel, flightitinerary, dateFormat, itemType,
            prom);
      }
   }

   /**
    * @param packageModel
    * @param items
    * @param extraFacilityModel
    * @param flightitinerary
    * @param dateFormat
    * @param itemType
    * @param prom
    */
   protected void createItem(final BasePackage packageModel, final List<Item> items,
      final ExtraFacility extraFacilityModel, final Itinerary flightitinerary,
      final SimpleDateFormat dateFormat, final ItemType itemType, final Prom prom)
   {
      final atcomres.common.AtcomresBookingBaseRequest.BkgEnt.Item.SubServPaxs subServPaxs =
         new atcomres.common.AtcomresBookingBaseRequest.BkgEnt.Item.SubServPaxs();
      populateSubServPaxs(packageModel, extraFacilityModel, subServPaxs);

      final Item item = new Item();
      item.setCode(extraFacilityModel.getInventoryCode());
      item.setName(extraFacilityModel.getDescription());
      item.setSetType("EXTRA");
      final String itemMethod = extraFacilityModel.getType() == ExtraFacilityType.PAX ? "PP" : "PI";
      item.setItemMethod(itemMethod);
      item.setSubServPaxs(subServPaxs);

      Date startDate = extraFacilityModel.getExtraFacilitySchedule().getArrivalDate();
      if (null == startDate)
      {
         startDate = flightitinerary.getOutBound().get(0).getSchedule().getDepartureDate();
      }

      item.setStDt(dateFormat.format(startDate));

      final Date endDate = extraFacilityModel.getExtraFacilitySchedule().getDepartureDate();
      if (null != endDate)
      {
         item.setEndDt(dateFormat.format(endDate));
      }
      item.setItemType(itemType);
      item.setProm(prom);
      item.setBkgQty(getValidQuantity(extraFacilityModel));
      item.getSerSts().add("FIX");

      items.add(item);
   }

   /**
    * @param extraFacilityModel
    * @return int
    */
   private BigInteger getValidQuantity(final ExtraFacility extraFacilityModel)
   {
      final String extraCatCode = extraFacilityModel.getExtraFacilityCategory().getCode();
      if (TRACSDIFFERENTQAUNTITY_EXTRA_CATEGORIES_CODES.contains(extraCatCode))
      {
         return populateQuantityFromBothPriceModels(extraFacilityModel);
      }
      else
      {
         return populateQuantityFromAvalablePriceModels(extraFacilityModel);
      }
   }

   /**
    * @param extraFacilityModel
    * @return
    */
   private BigInteger populateQuantityFromAvalablePriceModels(final ExtraFacility extraFacilityModel)
   {
      final BigInteger qauntity = BigInteger.ZERO;
      for (final Price price : extraFacilityModel.getPrices())
      {
         if (BigInteger.valueOf(price.getQuantity().intValue()).compareTo(BigInteger.ZERO) > 0)
         {
            return BigInteger.valueOf(price.getQuantity().intValue());
         }
      }
      return qauntity;
   }

   /**
    * @return
    */
   private BigInteger populateQuantityFromBothPriceModels(final ExtraFacility extraFacilityModel)
   {
      BigInteger qauntity = BigInteger.ZERO;
      for (final Price price : extraFacilityModel.getPrices())
      {
         if (BigInteger.valueOf(price.getQuantity().intValue()).compareTo(BigInteger.ZERO) > 0)
         {
            qauntity = qauntity.add(BigInteger.valueOf(price.getQuantity().intValue()));
         }
      }
      return qauntity;
   }

   /**
    * This method checks if extra facility is bookable non flight extra
    *
    * @param extraFacilityModel
    * @return boolean
    */
   // failing tracs flow. The logic will be replaced with a common logic for
   // removal based on category code for all extras.
   protected boolean isBookableNonFlightExtra(final ExtraFacility extraFacilityModel)
   {
      return !(StringUtils.equals(extraFacilityModel.getExtraFacilityGroup().toString(),
         ExtraFacilityGroup.FLIGHT.toString()))
         && !isExtraRemovableFromInfoBook(StringUtils.equals(extraFacilityModel
            .getExtraFacilityCategory().getCode(), "INS") ? extraFacilityModel
            .getExtraFacilityCode() : extraFacilityModel.getInventoryCode());
   }

   /**
    * @param packageModel
    * @param extraFacilityModel
    * @param subServPaxs
    */
   protected void populateSubServPaxs(final BasePackage packageModel,
      final ExtraFacility extraFacilityModel,
      final atcomres.common.AtcomresBookingBaseRequest.BkgEnt.Item.SubServPaxs subServPaxs)
   {
      int count = 0;
      for (final Passenger pax : extraFacilityModel.getPassengers())
      {
         // setting the infant extras to adults
         if (pax.getAge().intValue() < TWO)
         {
            final Passenger adult = packageModel.getPassengers().get(count);
            count++;
            final atcomres.common.AtcomresBookingBaseRequest.BkgEnt.Item.SubServPaxs.SubServPax subServPax =
               new atcomres.common.AtcomresBookingBaseRequest.BkgEnt.Item.SubServPaxs.SubServPax();
            subServPax.setPaxId(BigInteger.valueOf(adult.getId().intValue()));
            subServPaxs.getSubServPax().add(subServPax);
         }
         else
         {
            final atcomres.common.AtcomresBookingBaseRequest.BkgEnt.Item.SubServPaxs.SubServPax subServPax =
               new atcomres.common.AtcomresBookingBaseRequest.BkgEnt.Item.SubServPaxs.SubServPax();
            subServPax.setPaxId(BigInteger.valueOf(pax.getId().intValue()));
            subServPaxs.getSubServPax().add(subServPax);
         }
      }
   }

   /**
    * @param extraFacilityCode
    * @return true if extra should be removed from info Book.
    */
   protected boolean isExtraRemovableFromInfoBook(final String extraFacilityCode)
   {
      final List<String> removableExtras =
         Arrays
            .asList(StringUtils
               .split(
                  "DEF,DEF_BAG,HLA,DEF_SEAT,DEF_HMZ,INS/SMWW1,INS/SMEUR,INS/SMWW2,INS/FMEUR,INS/FMWW1,INS/FMWW2,INT/EWS,INT/EWF,BAG1,STMS,N99999,INS/SMW1,INS/FMW1,INT/SEW,INS/FMEU,INS/SMEU,INT/FEW",
                  ','));

      return removableExtras.contains(extraFacilityCode);
   }

   /**
    * This method returns a list of all the rooms in the package with the room details populated.
    *
    * @param roomModels the room details model list
    * @return the room code list
    */
   protected List<RmCd> getRoomCodeList(final List<Room> roomModels)
   {
      int index = 1;
      final List<RmCd> rmCodeList = new ArrayList<RmCd>();
      for (final Room room : roomModels)
      {
         final RmCd roomCode = new RmCd();
         roomCode.setRmNo(getBigIntegerValueOf(index));
         roomCode.setCode(room.getCode());
         roomCode.setBBCd(room.getBoardBasis().getCode());
         roomCode.setSubServPaxs(getSubServicePassengers(room));
         index++;
         rmCodeList.add(roomCode);
      }
      return rmCodeList;
   }

   /**
    * Gets the fO atcomres common atcomres booking base request bkg ent.
    *
    * @param packageModel the package model
    * @return the fO atcomres common atcomres booking base request bkg ent
    */
   public atcomres.common.AtcomresBookingBaseRequest.BkgEnt getFOAtcomresCommonAtcomresBookingBaseRequestBkgEnt(
      final BasePackage packageModel)
   {
      final atcomres.common.AtcomresBookingBaseRequest.BkgEnt bkgEnt =
         new atcomres.common.ObjectFactory().createAtcomresBookingBaseRequestBkgEnt();

      bkgEnt.getRouteList().add(getFORoutingDetails(packageModel));

      if (packageModel.getExtraFacilityCategories() != null)
      {
         bkgEnt.getFltExtraCatList().addAll(
            populateAllFlightExtras(packageModel.getExtraFacilityCategories()));
         bkgEnt.getItem().addAll(getAllExtraFacilities(packageModel));
      }

      return bkgEnt;
   }

   /**
    * This method returns a list of all the rooms in the package with the room details populated.
    *
    * @param roomModels the room details model list
    * @return the room code list
    */
   protected List<RmCd> getInfoBookDayTripRoomCode(final List<Room> roomModels)
   {
      int index = 1;
      final List<RmCd> rmCodeList = new ArrayList<RmCd>();
      for (final Room room : roomModels)
      {
         int roomindex = 0;
         for (final Passenger pax : room.getPassgengers())
         {
            if (pax.getType() != PersonType.INFANT)
            {
               final RmCd roomCode = new RmCd();
               roomCode.setRmNo(getBigIntegerValueOf(index));
               roomCode.setCode(room.getCode());
               roomCode.setBBCd(room.getBoardBasis().getCode());
               roomCode.setSubServPaxs(getInfoBookDayTripPassengers(pax));
               index++;
               rmCodeList.add(roomCode);
            }
            else
            {
               final RmCd roomCode = rmCodeList.get(roomindex);
               final atcomres.common.AtcomresBookingBaseRequest.BkgEnt.Package.Accom.RmCd.SubServPaxs srvcPaxs =
                  roomCode.getSubServPaxs();
               final atcomres.common.AtcomresBookingBaseRequest.BkgEnt.Package.Accom.RmCd.SubServPaxs.SubServPax srvcPax =
                  new atcomres.common.ObjectFactory()
                     .createAtcomresBookingBaseRequestBkgEntPackageAccomRmCdSubServPaxsSubServPax();
               srvcPax.setPaxId(getBigIntegerValueOf(pax.getId().intValue()));
               srvcPaxs.getSubServPax().add(srvcPax);
               roomCode.setSubServPaxs(srvcPaxs);
            }
         }
         roomindex++;
      }
      return rmCodeList;
   }

   /**
    * Gets the day trip sub service passengers.
    *
    * @param pax the pax
    * @return the day trip sub service passengers
    */
   private atcomres.common.AtcomresBookingBaseRequest.BkgEnt.Package.Accom.RmCd.SubServPaxs getInfoBookDayTripPassengers(
      final Passenger pax)
   {
      final atcomres.common.AtcomresBookingBaseRequest.BkgEnt.Package.Accom.RmCd.SubServPaxs srvcPaxs =
         new atcomres.common.ObjectFactory()
            .createAtcomresBookingBaseRequestBkgEntPackageAccomRmCdSubServPaxs();
      final atcomres.common.AtcomresBookingBaseRequest.BkgEnt.Package.Accom.RmCd.SubServPaxs.SubServPax srvcPax =
         new atcomres.common.ObjectFactory()
            .createAtcomresBookingBaseRequestBkgEntPackageAccomRmCdSubServPaxsSubServPax();
      srvcPax.setPaxId(getBigIntegerValueOf(pax.getId().intValue()));
      srvcPaxs.getSubServPax().add(srvcPax);
      return srvcPaxs;
   }

   /**
    * @param packageModel
    * @return tracs selling code.
    */
   protected String getTracsSellingCode(final BasePackage packageModel)
   {
      return getSupplierProductDetails(packageModel).getSellingCode();
   }

   /**
    * Gets the tracs id for package.
    *
    * @param packageModel the package model
    * @return the tracs id for package
    */
   private String getTracsIdforPackage(final BasePackage packageModel)
   {
      return getSupplierProductDetails(packageModel).getSupplierNumber();
   }

   /**
    * Gets the product code from package.
    *
    * @param packageModel the package model
    * @return the product code from package
    */
   String getProductCodeFromPackage(final BasePackage packageModel)
   {
      return getSupplierProductDetails(packageModel).getPromoCode();
   }

   /**
    * @param packageModel
    * @return tracsSupplierProductDetailsModel
    */
   private SupplierProductDetails getSupplierProductDetails(final BasePackage packageModel)
   {
      return packageModel.getInventory().getSupplierProductDetails();
   }

   /**
    * Convert string form of a date to Date form.
    *
    * @param date the date
    * @return the date
    */
   String getSimpleDate(final Date date)
   {
      String formatedDate;
      if (null != date)
      {
         final DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
         formatedDate = df.format(date).toString();
      }
      else
      {
         formatedDate = StringUtils.EMPTY;
      }
      return formatedDate;
   }

   /**
    * Populates person details.
    *
    * @param personModel the person model
    * @return the person
    */
   atcomres.common.Person getAtcomresCommonPerson(final Passenger personModel)
   {
      final atcomres.common.Person person = new atcomres.common.ObjectFactory().createPerson();

      if (isPersonDetailsAvailable(personModel))
      {
         person.setFirstName(personModel.getFirstname());
         person.setLastName(personModel.getLastname());
         person.setTitle(StringUtils.capitalize(personModel.getTitle()));
         if (personModel.getGender() != null)
         {
            person.setSex(getPassengerGender(personModel.getGender().getCode()));
         }
         // accepts YYYY-MM-DD format
         final String dateOfBirth = personModel.getDateOfBirth();
         if (StringUtils.isNotEmpty(dateOfBirth))
         {
            person.setDateOfBirth(formatdate(dateOfBirth));
         }
      }
      else
      {
         person.setFirstName("TEST");
         person.setLastName("PASSENGER");
         person.setTitle("Mr");
         person.setSex("SEX_MALE");
      }
      person.setPersonType("TYPE_NATURAL");
      return person;
   }

   /**
    * @param personModel
    * @return
    */
   private boolean isPersonDetailsAvailable(final Passenger personModel)
   {
      if (personModel != null)
      {
         return StringUtils.isNotEmpty(personModel.getFirstname())
            && StringUtils.isNotEmpty(personModel.getLastname());
      }
      return false;
   }

   /**
    * Gets the passenger gender.
    *
    * @param gender the gender
    * @return Anite PAssenger Sex determinination.
    */
   private String getPassengerGender(final String gender)
   {
      if (StringUtils.equalsIgnoreCase(gender, "MALE"))
      {
         return AniteGender.SEX_MALE.toString();
      }
      else if (StringUtils.equalsIgnoreCase(gender, "FEMALE"))
      {
         return AniteGender.SEX_FEMALE.toString();
      }
      return AniteGender.SEX_UNKNOWN.toString();
   }

   /**
    * Gets the list of passengers in the package.
    *
    * @param packageModel the holiday package
    * @return the passengers list
    */
   List<atcomres.common.AtcomresBookingBaseRequest.Pax> getAtcomresCommonAtcomresBookingBaseRequestPaxList(
      final BasePackage packageModel)
   {
      final List<atcomres.common.AtcomresBookingBaseRequest.Pax> paxList =
         new ArrayList<atcomres.common.AtcomresBookingBaseRequest.Pax>();

      final List<Passenger> passengers = packageModel.getPassengers();
      if (CollectionUtils.isNotEmpty(passengers))
      {
         /*
          * here indexing each passenger is based on the assumption that the
          * PassengerConfigurationModel has the sorted list of passengers based on their age.
          */
         for (final Passenger passenger : passengers)
         {
            final atcomres.common.AtcomresBookingBaseRequest.Pax pax =
               new atcomres.common.ObjectFactory().createAtcomresBookingBaseRequestPax();
            pax.setLeadPax(BooleanUtils.toBooleanObject(passenger.isLead()));
            pax.setAge(getBigIntegerValueOf(resetAgeIfInsuranceIfInsuranceIsAvailed(passenger)));
            pax.setPerson(getAtcomresCommonPerson(passenger));
            pax.setIndex(getBigIntegerValueOf(passenger.getId().intValue()));
            paxList.add(pax);
         }
      }
      return paxList;
   }

   /**
    * Reset age if insurance if insurance is availed.
    *
    * @param passenger the passenger
    * @return the passenger age.
    */
   private int resetAgeIfInsuranceIfInsuranceIsAvailed(final Passenger passenger)
   {
      final int insuranceAge = getPassengerAgeIfInsuranceIsAvailed(passenger);
      if (adultPassengers(passenger) && insuranceAge != 0)
      {
         return insuranceAge;
      }
      return passenger.getAge().intValue();
   }

   /**
    * To update the Age.
    *
    * @param eachPassenger the each passenger
    * @return the passenger age if insurance is availed
    */
   private int getPassengerAgeIfInsuranceIsAvailed(final Passenger eachPassenger)
   {
      for (final ExtraFacility eachExtra : eachPassenger.getExtraFacilities())
      {
         if (individualInsurance(eachExtra))
         {
            return getInsuranceMinAge(eachPassenger, eachExtra);
         }
      }
      return 0;
   }

   /**
    * Individual insurance.
    *
    * @param packageExtra the package extra
    * @return true, if successful
    */
   private boolean individualInsurance(final ExtraFacility packageExtra)
   {
      return packageExtra instanceof InsuranceExtraFacility
         && ((InsuranceExtraFacility) packageExtra).getInsuranceType() == InsuranceType.INDIVIDUAL;
   }

   /**
    * Reset age as per insurance min age.
    *
    * @param eachPassenger the each passenger
    * @param eachExtra the each extra
    */
   private int getInsuranceMinAge(final Passenger eachPassenger, final ExtraFacility eachExtra)
   {
      for (final Price eachPriceModel : eachExtra.getPrices())
      {
         if (ageResetRequiredForPassenger(eachPassenger, eachPriceModel))
         {
            return eachPriceModel.getPriceProfile().getMinAge().intValue();
         }
      }
      return 0;
   }

   /**
    * Age reset required for passenger.
    *
    * @param eachPassenger the each passenger
    * @param eachPriceModel the each price model
    * @return true, if successful
    */
   private boolean ageResetRequiredForPassenger(final Passenger eachPassenger,
      final Price eachPriceModel)
   {
      return personTypeMatches(eachPassenger, eachPriceModel)
         && passengerDoesNotHaveDOB(eachPassenger);
   }

   /**
    * Does not have dob.
    *
    * @param eachPassenger the each passenger
    * @return true, if successful
    */
   private boolean passengerDoesNotHaveDOB(final Passenger eachPassenger)
   {
      return StringUtils.isEmpty(eachPassenger.getDateOfBirth());
   }

   /**
    * Person type matches.
    *
    * @param eachPassenger the each passenger
    * @param eachPriceModel the each price model
    * @return true, if successful
    */
   private boolean personTypeMatches(final Passenger eachPassenger, final Price eachPriceModel)
   {
      return eachPriceModel.getPriceProfile().getPersonType() == eachPassenger.getType();
   }

   /**
    * Adult passengers.
    *
    * @param eachPassenger the each passenger
    * @return true, if successful
    */
   private boolean adultPassengers(final Passenger eachPassenger)
   {
      return EnumSet.of(PersonType.ADULT, PersonType.TEEN, PersonType.SENIOR,
         PersonType.SUPERSENIOR).contains(eachPassenger.getType());
   }

   /**
    * Gets the accommodation code.
    *
    * @param packageModel the package model
    * @return the accommodation code
    */
   public String getAccommodationCodeforPackage(final BasePackage packageModel)
   {
      final Stay stay = packageComponentService.getStay(packageModel);
      if (stay != null)
      {
         return stay.getCode();
      }
      return null;
   }

   protected Source getSoruce(final String brandType)
   {
      final Source source = new atcomres.common.ObjectFactory().createSource();
      if (StringUtils.equalsIgnoreCase(brandType, "TH"))
      {
         source.setCd("IJU");
      }
      else
      {
         source.setCd("IJU");
      }
      return source;
   }

   /**
    * Populate pax occupancy.
    *
    * @param room the room
    * @return List<atcomres.common.AtcomresBaseOfferSearchRequest.Occs.Occ.Pax>
    */
   private List<atcomres.common.AtcomresBaseOfferSearchRequest.Occs.Occ.Pax> getAtcomresCommonAtcomresBaseOfferSearchRequestOccsOccPax(
      final Room room)
   {
      final List<atcomres.common.AtcomresBaseOfferSearchRequest.Occs.Occ.Pax> passengersInRoom =
         new ArrayList<atcomres.common.AtcomresBaseOfferSearchRequest.Occs.Occ.Pax>();

      for (final Passenger passenger : room.getPassgengers())
      {
         final atcomres.common.AtcomresBaseOfferSearchRequest.Occs.Occ.Pax pax =
            new atcomres.common.ObjectFactory().createAtcomresBaseOfferSearchRequestOccsOccPax();

         pax.setIndex(getBigIntegerValueOf(passenger.getId().intValue()));
         if (passenger.getAge() != null)
         {
            pax.setAge(BigInteger.valueOf(passenger.getAge().intValue()));
         }
         passengersInRoom.add(pax);
      }

      return passengersInRoom;
   }

   private List<atcomres.common.AtcomresBaseOfferSearchRequest.Occs.Occ.Pax> getAtcomresCommonAtcomresBaseOfferSearchRequestOccsOccPaxForFlightOnly(
      final List<Passenger> passgengers)
   {
      final List<atcomres.common.AtcomresBaseOfferSearchRequest.Occs.Occ.Pax> passengersInRoom =
         new ArrayList<atcomres.common.AtcomresBaseOfferSearchRequest.Occs.Occ.Pax>();

      for (final Passenger passenger : passgengers)
      {
         final atcomres.common.AtcomresBaseOfferSearchRequest.Occs.Occ.Pax pax =
            new atcomres.common.ObjectFactory().createAtcomresBaseOfferSearchRequestOccsOccPax();

         pax.setIndex(getBigIntegerValueOf(passenger.getId().intValue()));
         if (passenger.getAge() != null)
         {
            pax.setAge(BigInteger.valueOf(passenger.getAge().intValue()));
         }
         passengersInRoom.add(pax);
      }

      return passengersInRoom;
   }

   /**
    * Gets the atcomres common accom.
    *
    * @param packageModel the package model
    * @return the atcomres common accom
    */
   protected atcomres.common.Accom getAtcomresCommonAccom(final BasePackage packageModel)
   {
      final atcomres.common.Accom selectedAcomm = new atcomres.common.ObjectFactory().createAccom();
      if (CollectionUtils.size(packageComponentService.getAllStays(packageModel)) > ONE)
      {
         populateProductDetails(packageModel, selectedAcomm);
         final Itinerary itinerary = packageComponentService.getFlightItinerary(packageModel);
         selectedAcomm.setStDt(getSimpleDate(getOutboundDepartureDate(itinerary)));
         selectedAcomm.setEndDt(getSimpleDate(getInboundDepartureDate(itinerary)));
         final atcomres.common.Accom.RmCd roomCode =
            new atcomres.common.ObjectFactory().createAccomRmCd();
         roomCode.setRmNo(BigInteger.ONE);
         roomCode.setCode(MULTICENTRE_RM_CD);
         roomCode.setBBCd(MULTICENTRE_BB_CD);
         roomCode
            .setSubServPaxs(getAtcomresCommonAccomRmCdSubServPaxs(packageModel.getPassengers()));
         selectedAcomm.getRmCd().add(roomCode);
      }
      else
      {
         final Stay acommodation = packageComponentService.getStay(packageModel);
         final boolean isLaplandDayTrip =
            packageModel.getListOfHighlights().contains(HighLights.LAPLAND_DAYTRIP);
         populateProductDetails(packageModel, selectedAcomm);
         if (acommodation != null && CollectionUtils.isNotEmpty(acommodation.getRooms()))
         {
            selectedAcomm.setStDt(getStartDate(packageModel));
            selectedAcomm.setEndDt(getSimpleDate(acommodation.getEndDate()));
            selectedAcomm.getRmCd().addAll(getAtcomresCommonAccomRmCdList(acommodation.getRooms()));
            populateLaplandRooms(selectedAcomm, acommodation.getRooms(), isLaplandDayTrip);
         }
      }
      return selectedAcomm;
   }

   /**
    * Populate product details.
    *
    * @param packageModel the package model
    * @param selectedAcomm the selected acomm
    */
   private void populateProductDetails(final BasePackage packageModel,
      final atcomres.common.Accom selectedAcomm)
   {
      final atcomres.common.Accom.HtlPrd.Prom prom =
         new atcomres.common.ObjectFactory().createAccomHtlPrdProm();
      final atcomres.common.Accom.HtlPrd htlPrd =
         new atcomres.common.ObjectFactory().createAccomHtlPrd();
      prom.setCode(getProductCodeFromPackage(packageModel));
      htlPrd.setProm(prom);
      htlPrd.setAccCd(getTracsSellingCode(packageModel));
      selectedAcomm.setHtlPrd(htlPrd);
      selectedAcomm.setId(BigInteger.ONE);
   }

   /**
    * Gets the atcomres common accom rm cd sub serv paxs.
    *
    * @param passengers the passengers
    * @return the atcomres common accom rm cd sub serv paxs
    */
   private atcomres.common.Accom.RmCd.SubServPaxs getAtcomresCommonAccomRmCdSubServPaxs(
      final List<Passenger> passengers)
   {
      final atcomres.common.Accom.RmCd.SubServPaxs srvcPaxs =
         new atcomres.common.ObjectFactory().createAccomRmCdSubServPaxs();
      if (CollectionUtils.isNotEmpty(passengers))
      {
         for (final Passenger passenger : passengers)
         {
            final atcomres.common.Accom.RmCd.SubServPaxs.SubServPax srvcPax =
               new atcomres.common.ObjectFactory().createAccomRmCdSubServPaxsSubServPax();
            srvcPax.setPaxId(getBigIntegerValueOf(passenger.getId().intValue()));
            srvcPaxs.getSubServPax().add(srvcPax);
         }
         return srvcPaxs;
      }
      return null;
   }

   /**
    * Gets the start date.
    *
    * @param packageModel the package model
    * @return the start date
    */
   private String getStartDate(final BasePackage packageModel)
   {
      if (packageModel.getInventory().getInventoryType() == InventoryType.ATCOM)
      {
         return getStDt(packageModel);
      }
      return getSimpleDate(getOutboundDepartureDate(packageComponentService
         .getFlightItinerary(packageModel)));
   }

   /**
    * Gets the outbound departure date.
    *
    * @param itinerary the itinerary
    * @return the outbound departure date
    * @returnoutboundDepartureDate
    */
   private Date getOutboundDepartureDate(final Itinerary itinerary)
   {
      return itinerary.getOutBound().get(0).getSchedule().getDepartureDate();
   }

   /**
    * Gets the outbound departure date.
    *
    * @param itinerary the itinerary
    * @return the outbound departure date
    * @returnoutboundDepartureDate
    */
   private Date getInboundDepartureDate(final Itinerary itinerary)
   {
      return itinerary.getInBound().get(0).getSchedule().getDepartureDate();
   }

   /**
    * Gets the st dt.
    *
    * @param packageModel the package model
    * @return Accommodation Start Date.
    */
   private String getStDt(final BasePackage packageModel)
   {
      final Stay accommodation = packageComponentService.getStay(packageModel);
      return getSimpleDate(accommodation.getStartDate());
   }

   /**
    * Populate lapland rooms
    *
    * this method populates one passenger per room as for a day trip that is how the request is
    * expected
    *
    * @param selectedAcomm the selected acomm
    * @param selectedRooms the selected rooms
    * @param isLaplandDayTrip the is lapland day trip
    */
   private void populateLaplandRooms(final atcomres.common.Accom selectedAcomm,
      final List<Room> selectedRooms, final boolean isLaplandDayTrip)
   {
      if (isLaplandDayTrip)
      {
         final List<atcomres.common.Accom.RmCd> rooms = selectedAcomm.getRmCd();
         selectedAcomm.getRmCd().removeAll(rooms);
         selectedAcomm.getRmCd().addAll(getDayTripRoomCode(selectedRooms));
      }
   }

   /**
    * This method returns a list of all the rooms in the package with the room details populated.
    *
    * @param roomModels the room details model list
    * @return the room code list
    */
   private List<atcomres.common.Accom.RmCd> getDayTripRoomCode(final List<Room> roomModels)
   {
      int index = 1;
      final List<atcomres.common.Accom.RmCd> rmCodeList =
         new ArrayList<atcomres.common.Accom.RmCd>();
      for (final Room room : roomModels)
      {
         int roomindex = 0;
         for (final Passenger pax : room.getPassgengers())
         {
            if (pax.getType() != PersonType.INFANT)
            {
               final atcomres.common.Accom.RmCd roomCode = new atcomres.common.Accom.RmCd();
               roomCode.setRmNo(getBigIntegerValueOf(index));
               roomCode.setCode(room.getCode());
               roomCode.setBBCd(room.getBoardBasis().getCode());
               roomCode.setSubServPaxs(getDayTripPassengers(pax));
               index++;
               rmCodeList.add(roomCode);
            }
            else
            {
               final atcomres.common.Accom.RmCd roomCode = rmCodeList.get(roomindex);
               final atcomres.common.Accom.RmCd.SubServPaxs srvcPaxs = roomCode.getSubServPaxs();
               final atcomres.common.Accom.RmCd.SubServPaxs.SubServPax srvcPax =
                  new atcomres.common.ObjectFactory().createAccomRmCdSubServPaxsSubServPax();
               srvcPax.setPaxId(getBigIntegerValueOf(pax.getId().intValue()));
               srvcPaxs.getSubServPax().add(srvcPax);
               roomCode.setSubServPaxs(srvcPaxs);
            }
         }
         roomindex++;
      }
      return rmCodeList;
   }

   /**
    * Gets the day trip sub service passengers.
    *
    * @param pax the pax
    * @return the day trip sub service passengers
    */
   private atcomres.common.Accom.RmCd.SubServPaxs getDayTripPassengers(final Passenger pax)
   {
      final atcomres.common.Accom.RmCd.SubServPaxs srvcPaxs =
         new atcomres.common.ObjectFactory().createAccomRmCdSubServPaxs();
      final atcomres.common.Accom.RmCd.SubServPaxs.SubServPax srvcPax =
         new atcomres.common.ObjectFactory().createAccomRmCdSubServPaxsSubServPax();
      srvcPax.setPaxId(getBigIntegerValueOf(pax.getId().intValue()));
      srvcPaxs.getSubServPax().add(srvcPax);
      return srvcPaxs;
   }

   /**
    * This method returns a list of all the rooms in the package with the room details populated.
    *
    * @param roomModels the room details model list
    * @return the room code list
    */
   private List<atcomres.common.Accom.RmCd> getAtcomresCommonAccomRmCdList(
      final List<Room> roomModels)
   {
      int index = 1;
      final List<atcomres.common.Accom.RmCd> rmCodeList =
         new ArrayList<atcomres.common.Accom.RmCd>();
      for (final Room room : roomModels)
      {
         final atcomres.common.Accom.RmCd roomCode =
            new atcomres.common.ObjectFactory().createAccomRmCd();
         roomCode.setRmNo(getBigIntegerValueOf(index));
         roomCode.setCode(room.getCode());
         roomCode.setBBCd(room.getBoardBasis().getCode());
         // not generated from WSDL need to
         // rework
         roomCode.setSubServPaxs(getAtcomresCommonAccomRmCdSubServicePassengers(room));

         index++;
         rmCodeList.add(roomCode);
      }
      return rmCodeList;
   }

   /**
    * Gets the persons assigned to this room.
    *
    * @param roomModel the room model
    * @return the list containing the indices for each passenger in the room
    */
   private atcomres.common.Accom.RmCd.SubServPaxs getAtcomresCommonAccomRmCdSubServicePassengers(
      final Room roomModel)
   {
      final atcomres.common.Accom.RmCd.SubServPaxs srvcPaxs =
         new atcomres.common.ObjectFactory().createAccomRmCdSubServPaxs();
      if (CollectionUtils.isNotEmpty(roomModel.getPassgengers()))
      {
         for (final Passenger pax : roomModel.getPassgengers())
         {
            final atcomres.common.Accom.RmCd.SubServPaxs.SubServPax srvcPax =
               new atcomres.common.ObjectFactory().createAccomRmCdSubServPaxsSubServPax();
            srvcPax.setPaxId(getBigIntegerValueOf(pax.getId().intValue()));
            srvcPaxs.getSubServPax().add(srvcPax);
         }
      }
      return srvcPaxs;
   }

   /**
    *
    * @param source
    * @return atcomres.common.RouteList
    */
   protected atcomres.common.RouteList getAtcomresCommonRouteList(final BasePackage source)
   {
      final atcomres.common.RouteList routelist =
         new atcomres.common.ObjectFactory().createRouteList();

      routelist.getRouting().add(getAtcomresCommonRouteListRouting(source));

      return routelist;
   }

   /**
    * Gets the atcomres common route list routing.
    *
    * @param source the source
    * @return the atcomres common route list routing
    */
   private atcomres.common.RouteList.Routing getAtcomresCommonRouteListRouting(
      final BasePackage source)
   {
      final atcomres.common.RouteList.Routing routing =
         new atcomres.common.ObjectFactory().createRouteListRouting();

      final Itinerary flightItinerary = packageComponentService.getFlightItinerary(source);
      for (final Leg legModel : flightItinerary.getOutBound())
      {
         final JAXBElement<atcomres.common.RouteTp> route =
            new atcomres.common.ObjectFactory().createRoute(getAtcomresCommonRouteTp(legModel,
               OUTBOUND, source));
         routing.getCoachOrFerryOrRoute().add(route);
      }

      // Check for Flight Only One-way
      if (CollectionUtils.isNotEmpty(flightItinerary.getInBound()))
      {
         for (final Leg legModel : flightItinerary.getInBound())
         {
            final JAXBElement<atcomres.common.RouteTp> route =
               new atcomres.common.ObjectFactory().createRoute(getAtcomresCommonRouteTp(legModel,
                  INBOUND, source));
            routing.getCoachOrFerryOrRoute().add(route);
         }
      }

      return routing;
   }

   /**
    *
    * @param legModel
    * @param bound
    * @param source
    * @return atcomres.common.RouteTp
    */
   private atcomres.common.RouteTp getAtcomresCommonRouteTp(final Leg legModel, final String bound,
      final BasePackage source)
   {
      final atcomres.common.RouteTp routeTP = new atcomres.common.ObjectFactory().createRouteTp();

      final atcomres.common.RouteTp.FltDtTm outFlightDate = new atcomres.common.RouteTp.FltDtTm();
      final atcomres.common.RouteTp.FltDtTm inFlightDate = new atcomres.common.RouteTp.FltDtTm();

      final atcomres.common.RouteTp.BkgCls bkgCls = new atcomres.common.RouteTp.BkgCls();
      final atcomres.common.RouteTp.CabCls cabCls = new atcomres.common.RouteTp.CabCls();
      setRouteDirection(bound, routeTP);
      routeTP.setRouteCd(getRouteCode(legModel));

      populateTracsOnlyInfo(source, legModel, legModel.getCarrier().getNumber(), routeTP);

      routeTP.setDepAirCd(legModel.getDepartureAirport().getCode());
      routeTP.setArrAirCd(legModel.getArrivalAirport().getCode());

      outFlightDate.setDirType("DEPARTURE");
      popualteRoutingDepDate(legModel, source, outFlightDate);
      routeTP.getFltDtTm().add(outFlightDate);

      inFlightDate.setDirType("ARRIVAL");
      inFlightDate
         .setLocal(getXMLGregorianDate(legModel.getSchedule().getArrivalDate()).toString());
      routeTP.getFltDtTm().add(inFlightDate);

      routeTP.setProm(getAtcomresCommonRouteTpProm(getProductCodeFromPackage(source)));
      routeTP.setSellProm(getAtcomresCommonSellProm(getProductCodeFromPackage(source)));

      if (legModel.getCarrier().getCarrierInformation() != null)
      {
         final String marketingAirlineCode =
            legModel.getCarrier().getCarrierInformation().getMarketingAirlineCode();
         routeTP.setCarCd(StringUtils.substring(marketingAirlineCode, 0, THREE).trim());
      }
      // bkgCls and cabCls is needed to get proper set of meals
      bkgCls.setCode("Y");
      routeTP.setBkgCls(bkgCls);
      routeTP.setCabCls(cabCls);
      // source unknown
      routeTP.getSerSts().add("FIX");
      routeTP.setSubServPaxs(getAtcomresCommonRouteTpSubServPaxs(source.getPassengers()));

      return routeTP;
   }

   /**
    * @param legModel
    * @param source
    * @param outFlightDate
    */
   private void popualteRoutingDepDate(final Leg legModel, final BasePackage source,
      final atcomres.common.RouteTp.FltDtTm outFlightDate)
   {
      if (source.getInventory().getInventoryType() == InventoryType.ATCOM)
      {
         outFlightDate
            .setLocal(getXMLGregorianDate(((FlightLeg) legModel).getCycDate()).toString());
      }
      else
      {
         outFlightDate.setLocal(getXMLGregorianDate(legModel.getSchedule().getDepartureDate())
            .toString());
      }
   }

   /**
    * Populate tracs only info.
    *
    * @param pkg the pkg
    * @param legModel the leg
    * @param flightNumber the flight number
    * @param routeTP the route tp
    */
   private void populateTracsOnlyInfo(final BasePackage pkg, final Leg legModel,
      final String flightNumber, final RouteTp routeTP)
   {
      if (StringUtils.isNotEmpty(getPackageNo(pkg)))
      {
         routeTP.setSellProm(getAtcomresCommonSellProm(getTracsSellingCode(pkg)));
         final String flightDepartureDate =
            getDateInStringFormat(legModel.getSchedule().getDepartureDate(), "yyyyMMdd");
         // set the legmodel with inventory id.
         legModel.setInventoryId(flightNumber.concat(flightDepartureDate));
         routeTP.setFltInvId(legModel.getInventoryId());
      }
   }

   /**
    * Populate occupancies.
    *
    * @param packageModel the source
    */
   protected atcomres.common.AtcomresBaseOfferSearchRequest.Occs getAtcomresCommonAtcomresBaseOfferSearchRequestOccs(
      final BasePackage packageModel)
   {
      final atcomres.common.AtcomresBaseOfferSearchRequest.Occs occs =
         new atcomres.common.ObjectFactory().createAtcomresBaseOfferSearchRequestOccs();
      final Stay stay = packageComponentService.getStay(packageModel);
      if (stay != null && CollectionUtils.isNotEmpty(stay.getRooms()))
      {
         for (final Room room : stay.getRooms())
         {
            final atcomres.common.AtcomresBaseOfferSearchRequest.Occs.Occ occ =
               new atcomres.common.ObjectFactory().createAtcomresBaseOfferSearchRequestOccsOcc();
            occ.getPax().addAll(getAtcomresCommonAtcomresBaseOfferSearchRequestOccsOccPax(room));
            occs.getOcc().add(occ);
         }
      }
      // FO we dont have the Stay.and for occ number is not required.and we
      // have to get pax from package model only.
      else
      {

         final atcomres.common.AtcomresBaseOfferSearchRequest.Occs.Occ occ =
            new atcomres.common.ObjectFactory().createAtcomresBaseOfferSearchRequestOccsOcc();
         occ.getPax().addAll(
            getAtcomresCommonAtcomresBaseOfferSearchRequestOccsOccPaxForFlightOnly(packageModel
               .getPassengers()));
         occs.getOcc().add(occ);
      }
      return occs;
   }

   /**
    * Gets the packageNo in case of tracs
    *
    * @param packageModel
    * @return packageNo
    */
   protected String getPackageNo(final BasePackage packageModel)
   {
      String packageNo = null;
      if (packageModel != null && packageModel.getInventory().getSupplierProductDetails() != null)
      {
         packageNo = getTracsIdforPackage(packageModel);
      }

      return packageNo;
   }

   /**
    * Gets the date in string format.
    *
    * @param date the date
    * @return the date in string format
    */
   private String getDateInStringFormat(final Date date, final String format)
   {
      return new SimpleDateFormat(format).format(date);
   }

   /**
    * Formatdate.
    *
    * @param dateString the date string
    * @return the string
    */
   private static String formatdate(final String dateString)
   {
      final DateFormat from = new SimpleDateFormat(DATE_FORMAT);
      Date date = null;
      try
      {
         date = from.parse(dateString);
      }
      catch (final ParseException e)
      {
         LOG.error(e.getMessage(), e);
         return null;
      }
      final SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT_1);
      return format.format(date);

   }

}
