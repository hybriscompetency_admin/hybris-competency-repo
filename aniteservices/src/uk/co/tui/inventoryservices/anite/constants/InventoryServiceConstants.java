/**
 *
 */
package uk.co.tui.inventoryservices.anite.constants;

import java.util.Arrays;
import java.util.List;

/**
 * @author munisekhar.k
 *
 */
public final class InventoryServiceConstants
{

   public static final String SELECTION_OPTION_AUTO = "AUTO";

   public static final String SELECTION_OPTION_MAN = "MAN";

   public static final String ZERO = "0";

   public static final String BOXING = "boxing";

   public static final String BAGGAGE = "BAG";

   public static final String INBOUND = "inbound";

   public static final String OUTBOUND = "outbound";

   public static final String EXTRAFACILITY_TYPE_PACKAGE = "SER";

   public static final String EXTRAFACILITY_TYPE_PAX = "PAX";

   public static final String SEAT = "SEAT";

   public static final String DEFAULT_SEAT = "DEF_SEAT";

   public static final String DEFAULT_BAGGAGE = "DEF_BAG";

   public static final List<String> DEFAULT_INVENTORY_BAGGAGE = Arrays.asList("HLA", "BAG1");

   public static final String FLIGHT_WITH_RETURN = "OWR";

   public static final String FLIGHT_ONE_WAY = "OW";

   public static final String FLIGHT_CHILD_PRICE = "FC";

   public static final String FLIGHT_ADULT_PRICE = "FA";

   public static final String FLIGHT_PRICE_DIFFERENCE = "FLIGHT_PRICE_DIFFERENCE";

   public static final String THIRD_PARTY_FLIGHT_CARD_CHARGE = "THIRD_PARTY_FLIGHT_CARD_FEE";

   public static final String DEFAULT_SEAT_CABIN_CLASS = "Y";

   public static final String GROUP_SEAT_CATEGORY_CODE = "GSEAT";

   public static final String GROUP_SEAT_EXTRA_CODE = "GPST";

   public static final String GROUP_SEAT_METHOD = "OTH";

   public static final String ATCOMRES_FLIGHT_ONLY_PROMOCODE = "BTFO";

   private InventoryServiceConstants()
   {
      //
   }
}
