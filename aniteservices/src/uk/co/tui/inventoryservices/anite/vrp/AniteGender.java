/**
 *
 */
package uk.co.tui.inventoryservices.anite.vrp;

/**
 * @author ganapna
 *
 */
public enum AniteGender
{
    SEX_MALE, SEX_FEMALE, SEX_UNKNOWN;
}
