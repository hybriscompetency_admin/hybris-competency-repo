/**
 *
 */
package uk.co.tui.inventoryservices.anite.processor;

import co.uk.tui.phoenix.book.aniteservices.exeption.AniteException;


/**
 * The Interface RequestProcessor.
 *
 * @param <S>
 *           the generic type
 * @param <T>
 *           the generic type
 * @author ganapna
 */
public interface RequestProcessor<S, T>
{
    /**
     * Process request.
     *
     * @param src
     *           the source object
     * @return the target
     * @throws AniteException
     */
    T processRequest(S src) throws AniteException;
}
