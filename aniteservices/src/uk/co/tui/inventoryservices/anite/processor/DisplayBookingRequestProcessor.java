/**
 *
 */
package uk.co.tui.inventoryservices.anite.processor;

import de.hybris.platform.commerceservices.converter.Populator;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import uk.co.travel.domain.manage.request.BookingSearchRequest;
import uk.co.travel.domain.manage.response.DisplayBookingResponse;
import uk.co.tui.book.services.ServiceLocator;
import uk.co.tui.integration.tibco.exception.TibcoIntegrationException;
import uk.co.tui.integration.tibco.service.TibcoService;
import co.uk.tui.phoenix.book.aniteservices.exeption.AniteException;

/**
 * @author sunil.bd
 * 
 */
public class DisplayBookingRequestProcessor implements
   RequestProcessor<BookingSearchRequest, DisplayBookingResponse>
{

   /** The booking search request populator. */
   @Resource
   private Populator<BookingSearchRequest, atcomres.displayrequest.DisplayRequest> displayBookingRequestPopulator;

   /** The logger to be used. */
   private static final Logger LOG = Logger.getLogger(DisplayBookingRequestProcessor.class);

   private static final String SUCCESS_CODE = "TIL-0000";

   /** The service used to interact with the inventory. */
   @Resource
   private TibcoService<atcomres.displayrequest.DisplayRequest, atcomres.displayresponse.TILDisplayResponse> displayBookingTibcoService;

   @Resource(name = "aniteServiceLocator")
   private ServiceLocator serviceLocator;

   /**
    * Gets the request object.
    * 
    * @param bookingRequest the booking request
    * @return the atcomres booking base request
    */
   public atcomres.displayrequest.DisplayRequest getBookingSearchRequest(
      final BookingSearchRequest src)
   {
      final atcomres.displayrequest.DisplayRequest displayRequest =
         new atcomres.displayrequest.ObjectFactory().createDisplayRequest();

      displayBookingRequestPopulator.populate(src, displayRequest);

      return displayRequest;
   }

   /**
    * Gets the request object.
    * 
    * @param bookingRequest the booking request
    * @return the atcomres booking base request
    */
   public atcomres.displayresponse.TILDisplayResponse getBookingSearchResponse(
      final atcomres.displayrequest.DisplayRequest displayRequest) throws AniteException
   {

      LOG.info("calling tibcoservice to get bookingsearchresponse....");
      final atcomres.displayresponse.TILDisplayResponse displayResponse =
         new atcomres.displayresponse.ObjectFactory().createTILDisplayResponse();
      try
      {
         return displayBookingTibcoService.processRequest(displayRequest, displayResponse);
      }
      catch (final TibcoIntegrationException e)
      {

         throw new AniteException(e.getErrorCode(), e.getCustomMessage(), e);
      }

   }

   /*
    * (non-Javadoc)
    * 
    * @see uk.co.tui.inventoryservices.anite.processor.RequestProcessor#processRequest
    * (java.lang.Object)
    */
   @Override
   public DisplayBookingResponse processRequest(final BookingSearchRequest src)
      throws AniteException
   {

      LOG.info("Initiating Booking Search Request Processor");

      final atcomres.displayrequest.DisplayRequest bookingRequest = getBookingSearchRequest(src);

      final DisplayBookingResponse target = new DisplayBookingResponse();

      final atcomres.displayresponse.TILDisplayResponse displayResponse =
         getBookingSearchResponse(bookingRequest);

      isValidResponse(displayResponse);
      ((Populator) serviceLocator.locateByPackageType("INCLUSIVE")).populate(displayResponse,
         target);

      return target;
   }

   private void isValidResponse(final atcomres.displayresponse.TILDisplayResponse displayResponse)
      throws AniteException
   {

      final List<schema.tilresponse.TUIEnvelopeHeader.ResponseCodes> responseCodeList =
         displayResponse.getTUIEnvelopeHeader().getResponseCodes();
      if (CollectionUtils.isNotEmpty(responseCodeList)
         && responseCodeList.get(0).getResponseCode() != null)
      {
         final String responseCode = responseCodeList.get(0).getResponseCode().getValue();
         checkForResponseCode(responseCodeList, responseCode);
      }
   }

   /**
    * @param responseCodeList
    * @param responseCode
    * @throws AniteException
    */
   private void checkForResponseCode(
      final List<schema.tilresponse.TUIEnvelopeHeader.ResponseCodes> responseCodeList,
      final String responseCode) throws AniteException
   {
      if (responseCode != null && !StringUtils.equalsIgnoreCase(responseCode, SUCCESS_CODE))
      {
         LOG.info("isValidResponse: Error code" + responseCode);
         throw new AniteException(responseCodeList.get(0).getResponseCode().getValue(),
            responseCodeList.get(0).getResponseMessage().getValue());
      }
   }

}
