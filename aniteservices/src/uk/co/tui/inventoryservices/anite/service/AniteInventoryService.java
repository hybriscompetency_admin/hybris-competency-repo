/**
 *
 */
package uk.co.tui.inventoryservices.anite.service;

import uk.co.tui.inventoryservices.anite.processor.RequestProcessor;
import co.uk.tui.phoenix.book.aniteservices.exeption.AniteException;

/**
 * The Class AniteInventoryService.
 *
 * @param <S>
 *            the generic type
 * @param <T>
 *            the generic type
 * @author anithamani.s
 */
public class AniteInventoryService<S, T> {

    /** The request processor. */
    private RequestProcessor requestProcessor;

    /**
     * Sets the request processor.
     *
     * @param requestProcessor
     *            the requestProcessor to set
     */
    public void setRequestProcessor(final RequestProcessor requestProcessor) {
        this.requestProcessor = requestProcessor;
    }

    /**
     * Send request.
     *
     * @param req
     *            the req
     * @return the response
     * @throws AniteException
     */
    public T sendRequest(final S req) throws AniteException {
        return (T) requestProcessor.processRequest(req);
    }
}
