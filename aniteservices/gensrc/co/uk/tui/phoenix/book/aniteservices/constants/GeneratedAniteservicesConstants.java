/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Aug 26, 2016 11:25:11 AM                    ---
 * ----------------------------------------------------------------
 */
package co.uk.tui.phoenix.book.aniteservices.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedAniteservicesConstants
{
	public static final String EXTENSIONNAME = "aniteservices";
	
	protected GeneratedAniteservicesConstants()
	{
		// private constructor
	}
	
	
}
