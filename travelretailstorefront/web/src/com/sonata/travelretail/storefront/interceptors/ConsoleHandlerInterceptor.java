/**
 *
 */
package com.sonata.travelretail.storefront.interceptors;

import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.sonata.travelretail.storefront.util.CartUtil;


/**
 * @author moumita.m
 *
 */
public class ConsoleHandlerInterceptor extends HandlerInterceptorAdapter
{

	@Resource(name = "cartFacade")
	private CartFacade cartFacade;

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "commerceCartService")
	private CommerceCartService commerceCartService;

	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "userCartService")
	private com.sonata.travelretail.storefront.cart.CartService userCartService;

	@Resource(name = "modelService")
	private ModelService modelService;

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.springframework.web.servlet.handler.HandlerInterceptorAdapter#preHandle(javax.servlet.http.HttpServletRequest,
	 * javax.servlet.http.HttpServletResponse, java.lang.Object)
	 */
	@Override
	public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler)
			throws Exception
	{
		// YTODO Auto-generated method stub

		//		if (request.getServletPath().equals("/login"))
		//		{
		//
		//			final List<CartModel> cartModelList = commerceCartService.getCartsForSiteAndUser(baseSiteService.getCurrentBaseSite(),
		//					userService.getCurrentUser());
		//
		//			for (final CartModel cartModel : cartModelList)
		//			{
		//				modelService.remove(cartModel);
		//			}
		//
		//			userCartService.createCartForUser();
		//		}

		if (request.getServletPath().equals("/cart/add"))
		{
			//System.out.println("Interceptor");

			final List<CartData> cartList = cartFacade.getCartsForCurrentUser();
			/*
			 * for (final CartData cartData : cartList) { System.out.println(cartData.getCode());
			 * System.out.println(cartData.getGuid()); System.out.println(cartData.getName()); }
			 */
			CartModel sessionCartModel = cartService.getSessionCart();
			//System.out.println("old cart" + sessionCartModel.getName());

			final List<CartModel> cartModelList = userCartService.getCartsForUser();


			if (!request.getParameter("cartValue").equals(sessionCartModel.getName()))
			{



				if (org.apache.commons.collections.CollectionUtils.isNotEmpty(cartModelList)
						&& StringUtils.isNotEmpty(sessionCartModel.getName()))
				{
					final CartModel cartModel = CartUtil.getMatchingCart(cartModelList, request.getParameter("cartValue"));
					if (cartModel != null)
					{
						cartService.setSessionCart(cartModel);
					}
					else
					{
						throw new Exception("Could not get Associated cart");
					}
				}
				sessionCartModel = cartService.getSessionCart();
				System.out.println("new cart" + sessionCartModel.getName());
			}
			else
			{

				System.out.println("new cart" + sessionCartModel.getName());
			}
		}
		if (request.getServletPath().equals("/cart/update"))
		{
			CartModel sessionCartModel = cartService.getSessionCart();
			System.out.println("old cart" + sessionCartModel.getName());

			final List<CartModel> cartModelList = userCartService.getCartsForUser();

			if (!request.getParameter("cartName").equals(sessionCartModel.getName()))
			{
				if (org.apache.commons.collections.CollectionUtils.isNotEmpty(cartModelList)
						&& StringUtils.isNotEmpty(sessionCartModel.getName()))
				{
					final CartModel cartModel = CartUtil.getMatchingCart(cartModelList, request.getParameter("cartName"));
					if (cartModel != null)
					{
						cartService.setSessionCart(cartModel);
					}
					else
					{
						throw new Exception("Could not get Associated cart");
					}
				}
				sessionCartModel = cartService.getSessionCart();
				System.out.println("new cart" + sessionCartModel.getName());
			}
			else
			{
				System.out.println("new cart" + sessionCartModel.getName());
			}
		}
		return true;
	}
}
