/**
 *
 */
package com.sonata.travelretail.storefront.util;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.core.model.order.CartModel;

import java.util.List;


/**
 * @author soumya.t
 *
 */
public class CartUtil
{


	public static CartModel getMatchingCart(final List<CartModel> cartModelList, final String cartName)
	{
		for (final CartModel cartModel : cartModelList)
		{
			if (cartModel.getName().equals(cartName))
			{
				return cartModel;
			}
		}

		return null;

	}


	public static CartData getMatchingCartData(final List<CartData> cartDataList, final String cartName)
	{
		for (final CartData cartdata : cartDataList)
		{
			if (cartdata.getName().equals(cartName))
			{
				return cartdata;
			}
		}

		return null;

	}

}
