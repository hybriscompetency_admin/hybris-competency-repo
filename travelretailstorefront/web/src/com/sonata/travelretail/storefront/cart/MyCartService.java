/**
 *
 */
package com.sonata.travelretail.storefront.cart;

/**
 * @author tejaswini.g
 *
 */
public interface MyCartService
{

	public int getInOutCartCount();

}
