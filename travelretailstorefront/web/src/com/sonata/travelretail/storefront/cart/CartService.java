/**
 *
 */
package com.sonata.travelretail.storefront.cart;

import de.hybris.platform.core.model.order.CartModel;

import java.util.List;


/**
 * @author soumya.t
 *
 */
public interface CartService
{

	void createCartForUser();

	List<CartModel> getCartsForUser();

	void setSessionCart();



}
