/**
 *
 */
package com.sonata.travelretail.storefront.cart.dao.impl;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.impl.SearchResultImpl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sonata.travelretail.storefront.cart.dao.CartDao;


/**
 * @author soumya.t
 *
 */
public class DefaultCartDao extends AbstractItemDao implements CartDao
{


	public List<CartModel> getCartsForSiteAndUser(final BaseSiteModel site, final UserModel user)
	{
		final Map params = new HashMap();
		params.put("site", site);
		params.put("user", user);
		return doSearch("SELECT {pk} FROM {Cart} WHERE {site} = ?site AND {user} = ?user  ORDER BY {code} DESC", params,
				CartModel.class);
	}


	protected <T> List<T> doSearch(final String query, final Map<String, Object> params, final Class<T> resultClass)
	{
		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(query);
		if (params != null)
		{
			fQuery.addQueryParameters(params);
		}

		fQuery.setResultClassList(Collections.singletonList(resultClass));
		final SearchResultImpl searchResult = (SearchResultImpl) search(fQuery);
		return searchResult.getResult();
	}

}
