/**
 *
 */
package com.sonata.travelretail.storefront.cart.impl;

import de.hybris.platform.commercefacades.order.converters.populator.CartPopulator;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.core.model.order.CartModel;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.sonata.travelretail.storefront.cart.MyCartService;


/**
 * @author tejaswini.g
 *
 */
public class MyDefaultCartService implements MyCartService
{

	@Resource(name = "cartPopulator")
	private CartPopulator cartPopulator;

	@Resource(name = "userCartService")
	private com.sonata.travelretail.storefront.cart.CartService userCartService;



	/*
	 * (non-Javadoc)
	 *
	 * @see com.sonata.travelretail.storefront.cart.MyCartService#getInOutCartCount()
	 */
	@Override
	public int getInOutCartCount()
	{
		try
		{

			final List<CartModel> cartModelList = userCartService.getCartsForUser();
			List entries2 = new ArrayList();
			List entries1 = new ArrayList();
			CartData cd1 = null;
			CartData cd2 = null;
			Integer cartSize1 = 0;
			Integer cartSize2 = 0;
			if (cartModelList != null)
			{
				for (final CartModel cm : cartModelList)
				{
					if (cd1 == null && cm.getName().equalsIgnoreCase("IN-CART"))
					{
						cd1 = new CartData();
						if (cd1.getEntries() == null)
						{
							cartPopulator.populate(cm, cd1);
							if (cd1.getEntries().size() != 0)
							{
								entries1 = cd1.getEntries();
								cartSize1 = entries1.size();

							}
						}
					}

					if (cd2 == null && cm.getName().equalsIgnoreCase("OUT-CART"))
					{
						cd2 = new CartData();
						if (cd2.getEntries() == null)
						{
							cartPopulator.populate(cm, cd2);
							if (cd2.getEntries().size() != 0)
							{
								entries2 = cd2.getEntries();
								cartSize2 = entries2.size();

							}
						}
					}
				}
			}
			return cartSize2 + cartSize1;

		}
		catch (final Exception ex)
		{

			ex.printStackTrace();
			return 0;
		}

	}

}
