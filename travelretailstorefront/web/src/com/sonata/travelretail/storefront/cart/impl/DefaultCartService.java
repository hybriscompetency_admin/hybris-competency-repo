/**
 *
 */
package com.sonata.travelretail.storefront.cart.impl;

import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartFactory;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import uk.co.tui.book.domain.lite.FlightItinerary;

import com.sonata.travelretail.storefront.cart.CartService;
import com.sonata.travelretail.storefront.cart.MyCartService;
import com.sonata.travelretail.storefront.cart.dao.CartDao;
import com.sonata.travelretail.storefront.controllers.ControllerConstants;
import com.sonata.travelretail.storefront.util.CartUtil;


/**
 * @author soumya.t
 *
 */
public class DefaultCartService implements CartService
{

	private de.hybris.platform.order.CartService cartService;
	private SessionService sessionService;
	private CartFactory cartFactory;
	private ModelService modelService;
	@Resource(name = "commerceCartService")
	private CommerceCartService commerceCartService;

	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "defaultCartDao")
	private CartDao cartDao;

	@Resource
	private MyCartService myDefaultCartService;


	public void createCartForUser()
	{


		final FlightItinerary flightItinerary = getSessionService().getAttribute("flightItinerary");
		final List<CartModel> newCartModelList = new ArrayList<CartModel>();



		//get PNR from session service and check PNR condition and create no of carts
		final CartModel outBoundCartModel = cartService.getSessionCart();
		outBoundCartModel.setName(ControllerConstants.Views.Fragments.UserCart.OutBoundCart);
		modelService.save(outBoundCartModel);
		newCartModelList.add(outBoundCartModel);
		if (CollectionUtils.isNotEmpty(flightItinerary.getInBound()))
		{
			final CartModel inBoundCartModel = cartFactory.createCart();
			inBoundCartModel.setName(ControllerConstants.Views.Fragments.UserCart.InBoundCart);
			modelService.save(inBoundCartModel);
			newCartModelList.add(inBoundCartModel);
		}



		getSessionService().setAttribute("Users_Cart_List", newCartModelList);

		//		final List<CartModel> cartModelList = commerceCartService.getCartsForSiteAndUser(baseSiteService.getCurrentBaseSite(),
		//				userService.getCurrentUser());
		//		if (org.apache.commons.collections.CollectionUtils.isNotEmpty(cartModelList))
		//		{
		//
		//			for (final CartModel cartModel : cartModelList)
		//			{
		//				if (!cartModel.getCode().equals(outBoundCartModel.getCode())
		//						&& !cartModel.getCode().equals(inBoundCartModel.getCode()))
		//				{
		//					modelService.remove(cartModel);
		//				}
		//			}
		//
		//
		//		}





	}


	public List<CartModel> getCartsForUser()
	{
		//		final List<CartModel> cartModelList = cartDao.getCartsForSiteAndUser(baseSiteService.getCurrentBaseSite(),
		//				userService.getCurrentUser());
		//
		//
		//		final List<CartModel> modifiableList = new ArrayList<CartModel>(cartModelList);
		//
		//		Collections.sort(modifiableList, new Comparator()
		//		{
		//
		//			public int compare(final Object arg0, final Object arg1)
		//			{
		//				if (!(arg0 instanceof CartModel))
		//				{
		//					return -1;
		//				}
		//				if (!(arg1 instanceof CartModel))
		//				{
		//					return -1;
		//				}
		//
		//				final CartModel cart0 = (CartModel) arg0;
		//				final CartModel cart1 = (CartModel) arg1;
		//
		//
		//				// COMPARE NOW WHAT YOU WANT
		//				// Thanks to Steve Kuo for your comment!
		//				return Integer.parseInt(cart0.getCode()) - Integer.parseInt(cart1.getCode());
		//			}
		//		});
		//
		//		final List<CartModel> newCartModelList = new ArrayList<CartModel>();
		//
		//		for (int i = 0; i < 2 && cartModelList.size() > i; i++)
		//		{
		//			newCartModelList.add(cartModelList.get(i));
		//		}
		final List<CartModel> newCartModelList = getSessionService().getAttribute("Users_Cart_List");
		return newCartModelList;
	}


	public void setSessionCart()
	{


		final List<CartModel> sessionCartsList = new ArrayList<CartModel>();
		final List<CartModel> cartModelList = commerceCartService.getCartsForSiteAndUser(baseSiteService.getCurrentBaseSite(),
				userService.getCurrentUser());

		final List<String> cartNames = new ArrayList<String>();

		cartNames.add(ControllerConstants.Views.Fragments.UserCart.OutBoundCart);
		cartNames.add(ControllerConstants.Views.Fragments.UserCart.InBoundCart);



		for (int i = 0; i < cartNames.size(); i++)
		{
			final String cartName = cartNames.get(i);
			CartModel cartModel = CartUtil.getMatchingCart(cartModelList, cartName);

			if (cartModel == null)
			{
				cartModel = cartFactory.createCart();
				cartModel.setName(cartName);


			}

			sessionCartsList.add(cartModel);
		}



		getSessionService().setAttribute("Users_Cart_List", sessionCartsList);







	}

	/**
	 * @return the cartService
	 */
	public de.hybris.platform.order.CartService getCartService()
	{
		return cartService;
	}


	/**
	 * @param cartService
	 *           the cartService to set
	 */
	public void setCartService(final de.hybris.platform.order.CartService cartService)
	{
		this.cartService = cartService;
	}


	/**
	 * @return the sessionService
	 */
	public SessionService getSessionService()
	{
		return sessionService;
	}


	/**
	 * @param sessionService
	 *           the sessionService to set
	 */
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}


	/**
	 * @return the cartFactory
	 */
	public CartFactory getCartFactory()
	{
		return cartFactory;
	}


	/**
	 * @param cartFactory
	 *           the cartFactory to set
	 */
	public void setCartFactory(final CartFactory cartFactory)
	{
		this.cartFactory = cartFactory;
	}


	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}


	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}


}
