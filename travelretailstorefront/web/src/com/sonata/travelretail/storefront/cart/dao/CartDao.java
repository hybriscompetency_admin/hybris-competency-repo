/**
 *
 */
package com.sonata.travelretail.storefront.cart.dao;

import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.internal.dao.Dao;

import java.util.List;


/**
 * @author soumya.t
 *
 */
public interface CartDao extends Dao
{
	public List<CartModel> getCartsForSiteAndUser(BaseSiteModel paramBaseSiteModel, UserModel paramUserModel);

}
