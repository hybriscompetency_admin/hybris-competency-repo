/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.sonata.travelretail.storefront.forms.validation;



import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.sonata.travelretail.storefront.forms.AddressForm;


/**
 * Validator for address forms. Enforces the order of validation
 */
@Component("storeFrontAddressValidator")
public class AddressValidator implements Validator
{
	private static final int MAX_FIELD_LENGTH = 255;
	private static final int MAX_POSTCODE_LENGTH = 10;


	@Override
	public boolean supports(final Class<?> aClass)
	{
		return AddressForm.class.equals(aClass);
	}

	@Override
	public void validate(final Object object, final Errors errors)
	{
		final AddressForm addressForm = (AddressForm) object;
		validateStandardFields(addressForm, errors);
		validateCountrySpecificFields(addressForm, errors);
	}

	public void validateInbound(final Object object, final Errors errors)
	{
		final AddressForm addressForm = (AddressForm) object;
		validateInboundStandardFields(addressForm, errors);
		validateInboundCountrySpecificFields(addressForm, errors);
	}

	protected void validateInboundStandardFields(final AddressForm addressForm, final Errors errors)
	{

		validateStringField(addressForm.getInboundCountryIso(), AddressField.ICOUNTRY, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getInboundFirstName(), AddressField.IFIRSTNAME, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getInboundLastName(), AddressField.ILASTNAME, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getInboundLine1(), AddressField.ILINE1, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getInboundTownCity(), AddressField.ITOWN, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getInboundPostcode(), AddressField.IPOSTCODE, MAX_POSTCODE_LENGTH, errors);
	}

	protected void validateInboundCountrySpecificFields(final AddressForm addressForm, final Errors errors)
	{

		final String isoCode1 = addressForm.getInboundCountryIso();

		if (isoCode1 != null)
		{
			switch (CountryCode.lookup(isoCode1))
			{
				case CHINA:
					validateStringField(addressForm.getInboundTitleCode(), AddressField.ITITLE, MAX_FIELD_LENGTH, errors);
					validateFieldNotNull(addressForm.getRegionIso(), AddressField.REGION, errors);
					break;
				case CANADA:
					validateStringField(addressForm.getInboundTitleCode(), AddressField.ITITLE, MAX_FIELD_LENGTH, errors);
					validateFieldNotNull(addressForm.getRegionIso(), AddressField.REGION, errors);
					break;
				case USA:
					validateStringField(addressForm.getInboundTitleCode(), AddressField.ITITLE, MAX_FIELD_LENGTH, errors);
					validateFieldNotNull(addressForm.getRegionIso(), AddressField.REGION, errors);
					break;
				case JAPAN:
					validateFieldNotNull(addressForm.getRegionIso(), AddressField.REGION, errors);
					validateStringField(addressForm.getInboundLine2(), AddressField.ILINE2, MAX_FIELD_LENGTH, errors);
					break;
				default:
					validateStringField(addressForm.getInboundTitleCode(), AddressField.ITITLE, MAX_FIELD_LENGTH, errors);
					break;
			}
		}
	}

	protected void validateStandardFields(final AddressForm addressForm, final Errors errors)
	{
		validateStringField(addressForm.getCountryIso(), AddressField.COUNTRY, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getFirstName(), AddressField.FIRSTNAME, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getLastName(), AddressField.LASTNAME, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getLine1(), AddressField.LINE1, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getTownCity(), AddressField.TOWN, MAX_FIELD_LENGTH, errors);
		validateStringField(addressForm.getPostcode(), AddressField.POSTCODE, MAX_POSTCODE_LENGTH, errors);
	}

	protected void validateCountrySpecificFields(final AddressForm addressForm, final Errors errors)
	{
		final String isoCode = addressForm.getCountryIso();
		if (isoCode != null)
		{
			switch (CountryCode.lookup(isoCode))
			{
				case CHINA:
					validateStringField(addressForm.getTitleCode(), AddressField.TITLE, MAX_FIELD_LENGTH, errors);
					validateFieldNotNull(addressForm.getRegionIso(), AddressField.REGION, errors);
					break;
				case CANADA:
					validateStringField(addressForm.getTitleCode(), AddressField.TITLE, MAX_FIELD_LENGTH, errors);
					validateFieldNotNull(addressForm.getRegionIso(), AddressField.REGION, errors);
					break;
				case USA:
					validateStringField(addressForm.getTitleCode(), AddressField.TITLE, MAX_FIELD_LENGTH, errors);
					validateFieldNotNull(addressForm.getRegionIso(), AddressField.REGION, errors);
					break;
				case JAPAN:
					validateFieldNotNull(addressForm.getRegionIso(), AddressField.REGION, errors);
					validateStringField(addressForm.getLine2(), AddressField.LINE2, MAX_FIELD_LENGTH, errors);
					break;
				default:
					validateStringField(addressForm.getTitleCode(), AddressField.TITLE, MAX_FIELD_LENGTH, errors);
					break;
			}
		}
	}

	protected static void validateStringField(final String addressField, final AddressField fieldType, final int maxFieldLength,
			final Errors errors)
	{
		if (addressField == null || StringUtils.isEmpty(addressField) || (StringUtils.length(addressField) > maxFieldLength))
		{
			errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
		}
	}

	protected static void validateFieldNotNull(final String addressField, final AddressField fieldType, final Errors errors)
	{
		if (addressField == null)
		{
			errors.rejectValue(fieldType.getFieldKey(), fieldType.getErrorKey());
		}
	}

	protected enum CountryCode
	{
		USA("US"), CANADA("CA"), JAPAN("JP"), CHINA("CN"), BRITAIN("GB"), GERMANY("DE"), DEFAULT("");

		private final String isoCode;

		private static Map<String, CountryCode> lookupMap = new HashMap<String, CountryCode>();
		static
		{
			for (final CountryCode code : CountryCode.values())
			{
				lookupMap.put(code.getIsoCode(), code);
			}
		}

		private CountryCode(final String isoCodeStr)
		{
			this.isoCode = isoCodeStr;
		}

		public static CountryCode lookup(final String isoCodeStr)
		{
			CountryCode code = lookupMap.get(isoCodeStr);
			if (code == null)
			{
				code = DEFAULT;
			}
			return code;
		}

		public String getIsoCode()
		{
			return isoCode;
		}
	}

	protected enum AddressField
	{
		TITLE("titleCode", "address.title.invalid"), FIRSTNAME("firstName", "address.firstName.invalid"), LASTNAME("lastName",
				"address.lastName.invalid"), LINE1("line1", "address.line1.invalid"), LINE2("line2", "address.line2.invalid"), TOWN(
				"townCity", "address.townCity.invalid"), POSTCODE("postcode", "address.postcode.invalid"), REGION("regionIso",
				"address.regionIso.invalid"), COUNTRY("countryIso", "address.country.invalid"), ITITLE("inboundTitleCode",
				"address.title.invalid"), IFIRSTNAME("inboundFirstName", "address.firstName.invalid"), ILASTNAME("inboundLastName",
				"address.lastName.invalid"), ILINE1("inboundLine1", "address.line1.invalid"), ILINE2("inboundLine2",
				"address.line2.invalid"), ITOWN("inboundTownCity", "address.townCity.invalid"), IPOSTCODE("inboundPostcode",
				"address.postcode.invalid"), ICOUNTRY("inboundCountryIso", "address.country.invalid");

		private final String fieldKey;
		private final String errorKey;

		private AddressField(final String fieldKey, final String errorKey)
		{
			this.fieldKey = fieldKey;
			this.errorKey = errorKey;
		}

		public String getFieldKey()
		{
			return fieldKey;
		}

		public String getErrorKey()
		{
			return errorKey;
		}
	}
}
