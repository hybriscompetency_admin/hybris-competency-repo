/**
 *
 */
package com.sonata.travelretail.storefront.controllers.pages;


import de.hybris.platform.acceleratorcms.model.components.PnrDetailsComponentModel;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sonata.travelretail.storefront.controllers.ControllerConstants;
import com.sonata.travelretail.storefront.controllers.cms.AbstractAcceleratorCMSComponentController;


/**
 * @author nataraja.y
 *
 */
@Controller("PnrDetailsComponentController")
@Scope("tenant")
@RequestMapping(value = ControllerConstants.Actions.Cms.PnrDetailsComponent)
public class PnrDetailsComponentController extends AbstractAcceleratorCMSComponentController<PnrDetailsComponentModel>
{
	protected static final Logger LOG = Logger.getLogger(PnrDetailsComponentController.class);

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final PnrDetailsComponentModel component)
	{
		LOG.info("Start of PnrDetailsComponent");
		model.addAttribute("title", component.getTitle());
		LOG.info("End of PnrDetailsComponent");
	}







}
