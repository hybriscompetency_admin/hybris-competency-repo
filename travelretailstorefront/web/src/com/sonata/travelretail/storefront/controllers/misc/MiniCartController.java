/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.sonata.travelretail.storefront.controllers.misc;

import de.hybris.platform.acceleratorcms.model.components.MiniCartComponentModel;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.servicelayer.services.CMSComponentService;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.converters.populator.CartPopulator;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sonata.travelretail.storefront.controllers.ControllerConstants;


/**
 * Controller for MiniCart functionality which is not specific to a page.
 */
@Controller
@Scope("tenant")
public class MiniCartController extends AbstractController
{
	protected static final Logger LOG = Logger.getLogger(MiniCartController.class);
	/**
	 * We use this suffix pattern because of an issue with Spring 3.1 where a Uri value is incorrectly extracted if it
	 * contains on or more '.' characters. Please see https://jira.springsource.org/browse/SPR-6164 for a discussion on
	 * the issue and future resolution.
	 */
	private static final String TOTAL_DISPLAY_PATH_VARIABLE_PATTERN = "{totalDisplay:.*}";
	private static final String COMPONENT_UID_PATH_VARIABLE_PATTERN = "{componentUid:.*}";


	@Resource(name = "cartFacade")
	private CartFacade cartFacade;

	@Resource(name = "cmsComponentService")
	private CMSComponentService cmsComponentService;

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;

	@Resource(name = "userCartService")
	private com.sonata.travelretail.storefront.cart.CartService userCartService;

	@Resource(name = "cartPopulator")
	private CartPopulator cartPopulator;


	@Resource(name = "cartService")
	private CartService cartService;


	@RequestMapping(value = "/cart/miniCart/" + TOTAL_DISPLAY_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public String getMiniCart(@PathVariable final String totalDisplay, final Model model)
	{
		final CartData cartData = cartFacade.getMiniCart();

		getTotalAndSubTotal(model);


		if (cartData.getDeliveryCost() != null)
		{
			final PriceData withoutDelivery = cartData.getDeliveryCost();
			withoutDelivery.setValue(cartData.getTotalPrice().getValue().subtract(cartData.getDeliveryCost().getValue()));
			model.addAttribute("totalNoDelivery", withoutDelivery);
		}
		else
		{
			model.addAttribute("totalNoDelivery", cartData.getTotalPrice());
		}
		model.addAttribute("totalItems", cartData.getTotalUnitCount());
		model.addAttribute("totalDisplay", totalDisplay);
		return ControllerConstants.Views.Fragments.Cart.MiniCartPanel;
	}

	private void getTotalAndSubTotal(final Model model)
	{
		final List entries = new ArrayList();

		BigDecimal x = new BigDecimal(0);
		BigDecimal y = new BigDecimal(0);
		BigDecimal a = new BigDecimal(0);
		BigDecimal b = new BigDecimal(0);

		List entries2 = new ArrayList();
		List entries1 = new ArrayList();

		PriceData cart2, cart1;

		int cd1Count = 0;
		int cd2Count = 0;

		final List<CartModel> cartModelList = userCartService.getCartsForUser();
		final CurrencyModel currencyModel = commonI18NService.getCurrentCurrency();
		CartData cd1 = null;
		CartData cd2 = null;

		if (cartModelList != null)
		{
			for (final CartModel cm : cartModelList)
			{
				if (cd1 == null && cm.getName().equalsIgnoreCase("IN-CART"))
				{
					cd1 = new CartData();
					if (cd1.getEntries() == null)
					{
						cartPopulator.populate(cm, cd1);
						if (cd1.getEntries().size() != 0)
						{
							entries1 = cd1.getEntries();
							entries.add(entries1);
							x = cd1.getTotalPrice().getValue();
							a = cd1.getSubTotal().getValue();
							model.addAttribute("cartName1", cd1.getName());
							cart1 = priceDataFactory.create(PriceDataType.BUY, x, currencyModel);
							model.addAttribute("cart1", cart1);
							model.addAttribute("entries1", entries1);
							model.addAttribute("numberItemsInCart1", Integer.valueOf(entries1.size()));
						}
					}
					cd1Count = cd1.getTotalUnitCount().intValue();
				}

				if (cd2 == null && cm.getName().equalsIgnoreCase("OUT-CART"))
				{
					cd2 = new CartData();
					if (cd2.getEntries() == null)
					{
						cartPopulator.populate(cm, cd2);
						if (cd2.getEntries().size() != 0)
						{
							entries2 = cd2.getEntries();
							entries.add(entries2);
							y = cd2.getTotalPrice().getValue();
							b = cd2.getSubTotal().getValue();
							model.addAttribute("cartName2", cd2.getName());
							cart2 = priceDataFactory.create(PriceDataType.BUY, y, currencyModel);
							model.addAttribute("cart2", cart2);
							model.addAttribute("entries2", entries2);
							model.addAttribute("numberItemsInCart2", Integer.valueOf(entries2.size()));
						}
					}
					cd2Count = cd2.getTotalUnitCount().intValue();
				}
			}
		}

		final BigDecimal cartTotal = x.add(y);
		final BigDecimal subTotal = a.add(b);
		final PriceData total = priceDataFactory.create(PriceDataType.BUY, cartTotal, currencyModel);
		final PriceData cartSubTotal = priceDataFactory.create(PriceDataType.BUY, subTotal, currencyModel);
		model.addAttribute("totalItems", new Integer(cd1Count + cd2Count));
		model.addAttribute("totalPrice", total);
		model.addAttribute("subTotal", cartSubTotal);
	}

	@RequestMapping(value = "/cart/rollover/" + COMPONENT_UID_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public String rolloverMiniCartPopup(@PathVariable final String componentUid, final Model model)
			throws CMSItemNotFoundException
	{
		final CartData cartData = cartFacade.getSessionCart();
		final List<CartData> cartList = cartFacade.getCartsForCurrentUser();
		final List entries = new ArrayList();

		BigDecimal x = new BigDecimal(0);
		BigDecimal y = new BigDecimal(0);
		BigDecimal a = new BigDecimal(0);
		BigDecimal b = new BigDecimal(0);

		List entries2 = new ArrayList();
		List entries1 = new ArrayList();

		int cd1Count = 0;
		int cd2Count = 0;

		PriceData cart2, cart1;
		model.addAttribute("cartData", cartData);
		model.addAttribute("cartList", cartList);

		final CurrencyModel currencyModel = commonI18NService.getCurrentCurrency();
		final MiniCartComponentModel component = (MiniCartComponentModel) cmsComponentService.getSimpleCMSComponent(componentUid);

		//final CartModel sessionCartModel = cartService.getSessionCart();
		CartData cd1 = null;
		CartData cd2 = null;

		final List<CartModel> cartModelList = userCartService.getCartsForUser();

		if (cartModelList != null)
		{
			for (final CartModel cm : cartModelList)
			{
				if (cd1 == null && cm.getName().equalsIgnoreCase("IN-CART"))
				{
					cd1 = new CartData();
					if (cd1.getEntries() == null)
					{
						cartPopulator.populate(cm, cd1);
						if (cd1.getEntries().size() != 0)
						{
							entries1 = cd1.getEntries();
							entries.add(entries1);
							x = cd1.getTotalPrice().getValue();
							a = cd1.getSubTotal().getValue();
							model.addAttribute("cartName1", cd1.getName());
							cart1 = priceDataFactory.create(PriceDataType.BUY, x, currencyModel);
							model.addAttribute("cart1", cart1);
							model.addAttribute("entries1", entries1);
							//model.addAttribute("numberItemsInCart1", Integer.valueOf(entries1.size()));
						}
					}
					cd1Count = cd1.getTotalUnitCount().intValue();
				}

				if (cd2 == null && cm.getName().equalsIgnoreCase("OUT-CART"))
				{
					cd2 = new CartData();
					if (cd2.getEntries() == null)
					{
						cartPopulator.populate(cm, cd2);
						if (cd2.getEntries().size() != 0)
						{
							entries2 = cd2.getEntries();
							entries.add(entries2);
							y = cd2.getTotalPrice().getValue();
							b = cd2.getSubTotal().getValue();
							model.addAttribute("cartName2", cd2.getName());
							cart2 = priceDataFactory.create(PriceDataType.BUY, y, currencyModel);
							model.addAttribute("cart2", cart2);
							model.addAttribute("entries2", entries2);
							//model.addAttribute("numberItemsInCart2", Integer.valueOf(entries2.size()));
						}
					}
					cd2Count = cd2.getTotalUnitCount().intValue();
				}
			}
		}

		final BigDecimal cartTotal = x.add(y);
		final BigDecimal subTotal = a.add(b);
		final PriceData total = priceDataFactory.create(PriceDataType.BUY, cartTotal, currencyModel);
		final PriceData cartSubTotal = priceDataFactory.create(PriceDataType.BUY, subTotal, currencyModel);
		model.addAttribute("totalItems", new Integer(cd1Count + cd2Count));
		model.addAttribute("total", total);
		model.addAttribute("subTotal", cartSubTotal);


		if (entries.size() != 0)
		{
			Collections.reverse(entries);
		}



		if (entries1.size() < component.getShownProductCount() && entries2.size() < component.getShownProductCount())
		{

			model.addAttribute("numberShowing1", Integer.valueOf(entries1.size()));
			model.addAttribute("numberShowing2", Integer.valueOf(entries2.size()));
		}

		else
		{

			model.addAttribute("numberShowing1", Integer.valueOf(component.getShownProductCount()));
			model.addAttribute("numberShowing2", Integer.valueOf(component.getShownProductCount()));

		}

		model.addAttribute("numberItemsInCart1", Integer.valueOf(entries1.size()));
		model.addAttribute("numberItemsInCart2", Integer.valueOf(entries2.size()));

		cd1 = null;
		cd2 = null;
		model.addAttribute("lightboxBannerComponent", component.getLightboxBannerComponent());

		return ControllerConstants.Views.Fragments.Cart.CartPopup;
	}
}
