/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.sonata.travelretail.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.security.AutoLoginStrategy;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegisterData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.Session;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Calendar;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import uk.co.travel.domain.manage.request.BookingSearchRequest;
import uk.co.tui.book.domain.lite.Address;
import uk.co.tui.book.domain.lite.BasePackage;
import uk.co.tui.book.domain.lite.FlightItinerary;
import uk.co.tui.book.domain.lite.Leg;
import uk.co.tui.book.domain.lite.Passenger;
import uk.co.tui.book.services.PackageComponentService;
import uk.co.tui.manage.services.exception.AmendNCancelServiceException;
import uk.co.tui.manage.services.exception.TUIBusinessException;
import uk.co.tui.manage.services.inventory.DisplayBookingService;

import com.sonata.travelretail.core.price.RouteEurope1PriceFactory;
import com.sonata.travelretail.storefront.cart.CartService;
import com.sonata.travelretail.storefront.cart.MyCartService;
import com.sonata.travelretail.storefront.controllers.ControllerConstants;


/**
 * Controller for home page
 */
@Controller
@Scope("tenant")
@RequestMapping("/login")
public class HomePageController extends AbstractPageController
{


	private final String VIEW_LOGIN = "pnrdetailscomponent";


	@Resource
	private DisplayBookingService displayBookingService;

	@Resource
	private UserService userService;

	@Resource(name = "userCartService")
	private CartService cartService;

	@Resource
	private CartFacade cartFacade;

	@Resource(name = "customerFacade")
	protected CustomerFacade customerFacade;

	@Resource
	private UserFacade userFacade;

	@Resource(name = "autoLoginStrategy")
	private AutoLoginStrategy autoLoginStrategy;

	@Resource
	private PackageComponentService packageComponentService;

	@Resource
	private ModelService modelService;

	@Resource
	private Populator<AddressData, AddressModel> addressReversePopulator;

	@Resource
	private CustomerAccountService customerAccountService;

	@Resource
	private MyCartService myDefaultCartService;

	@RequestMapping(method = RequestMethod.GET)
	public String home(final Model model, final RedirectAttributes redirectModel, final HttpServletRequest request,
			final HttpServletResponse response) throws CMSItemNotFoundException
	{

		final String bookingRefNo = request.getParameter("BookingReferenceNum");
		final String surName = request.getParameter("paxSurName");
		final String deptDate = request.getParameter("departureDateFrom");
		if (getSessionService().getAttribute("outboundCartExists") != null)
		{
			getSessionService().removeAttribute("outboundCartExists");
		}
		if (getSessionService().getAttribute("inboundCartExists") != null)
		{
			getSessionService().removeAttribute("inboundCartExists");
		}
		String[] dateArray = null;
		if (null != deptDate)
		{
			dateArray = deptDate.split("/");
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(null));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(null));
		updatePageTitle(model, getContentPageForLabelOrId(null));

		final Session session3 = getSessionService().getCurrentSession();
		Boolean continueshopping = session3.getAttribute("continueshopping");

		if (null != continueshopping)
		{
			if (continueshopping)
			{
				continueshopping = false;
				cartService.createCartForUser();
			}
		}

		if (null != bookingRefNo)
		{

			if (!getBookResponse(bookingRefNo, surName, dateArray, request, response))

			{
				GlobalMessages.addErrorMessage(model, "account.error.login.message");
				final HttpSession session = request.getSession();
				session.setAttribute("loginErrorMessage", "Invalid criteria,please enter valid criteria");
				storeCmsPageInModel(model, getContentPageForLabelOrId("login"));
				setUpMetaDataForContentPage(model, getContentPageForLabelOrId("login"));
				updatePageTitle(model, getContentPageForLabelOrId("login"));
				return ControllerConstants.Views.Pages.Account.TravelProductLandingLayoutPage;
			}
			else
			{

				if (!cartFacade.hasEntries())
				{
					//getSessionService().setAttribute(WebConstants.CART_RESTORATION_SHOW_MESSAGE, Boolean.TRUE);
					//	try
					//	{
					//	getSessionService().setAttribute(WebConstants.CART_RESTORATION, cartFacade.restoreSavedCart(null));
					cartService.createCartForUser();
					//}
					//	catch (final CommerceCartRestorationException e)
					//	{
					//		getSessionService().setAttribute(WebConstants.CART_RESTORATION_ERROR_STATUS,
					//				WebConstants.CART_RESTORATION_ERROR_STATUS);
					//	}
				}
				else
				{
					cartService.setSessionCart();
					//model.addAttribute("inOutCartCount", inOutCartCount);
					//getSessionService().getCurrentSession().setAttribute("inOutCartCount", inOutCartCount);
				}
				final int inOutCartCount = myDefaultCartService.getInOutCartCount();
				System.out.println("inOutCartCount------------------------------------------->" + inOutCartCount);
				request.getSession().setAttribute("inOutCartCount", inOutCartCount);
			}
		}
		final Session session = getSessionService().getCurrentSession();

		final Boolean checkout = session.getAttribute("checkout");
		if (null != checkout)
		{
			if (checkout)

			{
				session.setAttribute("checkout", false);
				return "redirect:/cart";
			}
		}
		return getViewForPage(model);
	}

	/**
	 * @param deptDate
	 * @param surname
	 * @param bookingRefNo
	 *
	 */
	private boolean getBookResponse(final String bookingRefNo, final String surname, final String[] deptDateArry,
			final HttpServletRequest request, final HttpServletResponse response)
	{
		final BookingSearchRequest bookingSearchRequest = new BookingSearchRequest();
		bookingSearchRequest.setBookingReferenceNum(bookingRefNo);
		final Calendar cal1 = Calendar.getInstance();
		cal1.set(Integer.parseInt(deptDateArry[2]), Integer.parseInt(deptDateArry[1]) - 1, Integer.parseInt(deptDateArry[0]));
		bookingSearchRequest.setDepartureDateFrom(cal1.getTime());
		bookingSearchRequest.setPaxSurName(surname);
		try
		{
			final BasePackage packageHoliday = displayBookingService.displayBooking(bookingSearchRequest);
			System.out.println("After response *&*******" + packageHoliday.getBookingRefNum());
			if (CollectionUtils.isEmpty(packageHoliday.getPassengers()))
			{
				return false;
			}
			else
			{
				final FlightItinerary flightItinerary = (FlightItinerary) packageComponentService.getFlightItinerary(packageHoliday);
				final HttpSession sessionReq = request.getSession();
				getSessionService().setAttribute("bookResponse", packageHoliday);
				getSessionService().setAttribute("flightItinerary", flightItinerary);
				sessionReq.setAttribute("flightItinerary", flightItinerary);
				sessionReq.setAttribute("bookResponse", packageHoliday);

				List<Leg> inboundLeg = null;
				List<Leg> outboundLeg = null;
				//final List<PriceRowModel> tempPriceRowModels = new ArrayList<PriceRowModel>();
				if (null != flightItinerary)
				{
					inboundLeg = flightItinerary.getInBound();
					outboundLeg = flightItinerary.getOutBound();
				}
				final RouteEurope1PriceFactory priceFactory = (RouteEurope1PriceFactory) Registry.getGlobalApplicationContext()
						.getBean("europe1.manager");
				JaloSession.getCurrentSession().setPriceFactory(priceFactory);
				getSessionService().setAttribute("sourceInbound", inboundLeg.get(0).getDepartureAirport().getCode());
				getSessionService().setAttribute("destinationInbound", inboundLeg.get(0).getArrivalAirport().getCode());
				getSessionService().setAttribute("sourceOutbound", outboundLeg.get(0).getDepartureAirport().getCode());
				getSessionService().setAttribute("destinationOutbound", outboundLeg.get(0).getArrivalAirport().getCode());
				validateUser(packageHoliday, request, response);
				return true;
			}

		}
		catch (TUIBusinessException | AmendNCancelServiceException e)
		{

			e.printStackTrace();
			return false;
		}

	}

	/**
	 * @param packageHoliday
	 */
	private void validateUser(final BasePackage packageHoliday, final HttpServletRequest request,
			final HttpServletResponse response)
	{
		final List<Passenger> paxList = packageHoliday.getPassengers();
		Passenger leadPax = new Passenger();
		for (final Passenger pax : paxList)
		{
			if (pax.isLead())
			{
				leadPax = pax;
			}
		}
		String userId = null;
		final List<Address> addList = leadPax.getAddresses();
		for (final Address address : addList)
		{
			if (StringUtils.isNotEmpty(address.getEmail()))
			{
				userId = address.getEmail();
			}
		}
		if (!checkUserExists(userId))
		{

			final RegisterData data = new RegisterData();
			data.setFirstName(leadPax.getFirstname());
			data.setLastName(leadPax.getLastname());
			data.setLogin(userId);
			data.setPassword("1234");
			data.setTitleCode(leadPax.getTitle().toLowerCase());
			try
			{
				getCustomerFacade().register(data);
				final AddressData newAddress = loadAddressDetails(request);
				newAddress.setEmail(userId);
				newAddress.setFirstName(leadPax.getFirstname());
				newAddress.setLastName(leadPax.getLastname());
				final AddressModel newAddressModel = modelService.create(AddressModel.class);
				final CustomerModel customerModel = (CustomerModel) userService.getUserForUID(userId);
				addressReversePopulator.populate(newAddress, newAddressModel);
				newAddressModel.setEmail(userId);
				// Store the address against the user
				customerAccountService.saveAddressEntry(customerModel, newAddressModel);
				checkUserExists(userId);
			}
			catch (final UnknownIdentifierException e)
			{
				e.printStackTrace();
			}
			catch (final IllegalArgumentException e)
			{
				e.printStackTrace();
			}
			catch (final DuplicateUidException e)
			{
				e.printStackTrace();
			}
		}

		autoLoginStrategy.login(userId, "1234", request, response);


	}

	private AddressData loadAddressDetails(final HttpServletRequest request)
	{
		final AddressData addressData = new AddressData();
		final HttpSession session = request.getSession();
		final BasePackage packageHoliday = (BasePackage) session.getAttribute("bookResponse");
		if (null != packageHoliday)
		{
			final List<Passenger> paxList = packageHoliday.getPassengers();
			if (CollectionUtils.isNotEmpty(paxList))
			{
				final Passenger leadPax = paxList.get(0);
				final List<Address> addressList = leadPax.getAddresses();
				final CountryData countryData = new CountryData();
				countryData.setIsocode(addressList.get(0).getCountry());
				addressData.setCountry(countryData);
				addressData.setTown(addressList.get(0).getTown());
				addressData.setLine1(addressList.get(0).getHouseNumber());
				addressData.setLine2(addressList.get(0).getStreetname());
				addressData.setShippingAddress(true);
				addressData.setDefaultAddress(true);
				addressData.setVisibleInAddressBook(true);
				addressData.setIsResortAddress(true);
				addressData.setPostalCode(addressList.get(0).getPostalcode());
				return addressData;
			}
		}
		return null;

	}


	/**
	 * @param request
	 * @param leadPax
	 * @return
	 */

	private boolean checkUserExists(final String userId)
	{
		try
		{
			final CustomerModel customerModel = (CustomerModel) userService.getUserForUID(userId);
			if (null != customerModel)
			{
				final Session session = getSessionService().getCurrentSession();
				System.out.println("user inside customerModel is " + customerModel.getDisplayName());
				//model.addAttribute("user", customerFacade.getCurrentCustomer());
				session.setAttribute("user", customerModel);

				session.setAttribute("loggedin", true);
				return true;
			}
			else
			{
				return false;
			}
		}
		catch (final UnknownIdentifierException ex)
		{
			return false;
		}
	}

	protected void updatePageTitle(final Model model, final AbstractPageModel cmsPage)
	{
		storeContentPageTitleInModel(model, getPageTitleResolver().resolveHomePageTitle(cmsPage.getTitle()));
	}

	/**
	 * Action to show the login page.
	 *
	 * @param referer
	 *           the referer
	 * @param model
	 *           the model
	 * @param request
	 *           the request
	 * @param response
	 *           the response
	 * @return String
	 */
	@RequestMapping(value =
	{ "/userLogin" }, method = RequestMethod.GET)
	public String login(@RequestHeader(value = "referer", required = false) final String referer, final Model model,
			final HttpServletRequest request, final HttpServletResponse response)
	{
		return VIEW_LOGIN;
	}



}
