/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.sonata.travelretail.storefront.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorfacades.order.AcceleratorCheckoutFacade;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.checkout.steps.AbstractCheckoutStepController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.DeliveryModeData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sonata.travelretail.storefront.controllers.ControllerConstants;


@Controller
@RequestMapping(value = "/checkout/multi/delivery-method")
public class DeliveryMethodCheckoutStepController extends AbstractCheckoutStepController
{

	@Resource(name = "cartService")
	@Autowired
	private CartService cartService;

	/**
	 * @return the cartService
	 */
	public CartService getCartService()
	{
		return cartService;
	}

	/**
	 * @param cartService
	 *           the cartService to set
	 */
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}

	/**
	 * @return the checkoutFacade
	 */
	@Override
	public AcceleratorCheckoutFacade getCheckoutFacade()
	{
		return checkoutFacade;
	}

	/**
	 * @param checkoutFacade
	 *           the checkoutFacade to set
	 */
	public void setCheckoutFacade(final AcceleratorCheckoutFacade checkoutFacade)
	{
		this.checkoutFacade = checkoutFacade;
	}

	@Resource(name = "acceleratorCheckoutFacade")
	private AcceleratorCheckoutFacade checkoutFacade;


	private final static String DELIVERY_METHOD = "delivery-method";

	private static String CART_DATA = "cartData";

	private static String DELIVERY_ADDRESSES = "deliveryAddresses";

	private static String DELIVERY_METHODS = "deliveryMethods";

	@RequestMapping(value = "/choose", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	//@PreValidateCheckoutStep(checkoutStep = DELIVERY_METHOD)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{

		final List<CartData> cartList = getUserCheckedOutCart();
		List<DeliveryModeData> deliveryList = new ArrayList<DeliveryModeData>();
		for (int i = 0; i < cartList.size(); i++)
		{

			if (cartList.get(i).getName().equals(ControllerConstants.Views.Fragments.UserCart.InBoundCart))
			{

				CART_DATA = "inboundCartData";
				/*
				 * NO_ADDRESS = "inboundNoAddress"; SAVE_ADDRESS_BOOK = "inboundShowSaveToAddressBook";
				 */
				DELIVERY_ADDRESSES = "inboundDeliveryAddresses";
				DELIVERY_METHODS = "inboundDeliveryMethods";
				//ADDRESS_FORM = "inboundAddressForm";
				model.addAttribute("inboundCartExists", Boolean.TRUE);
				try
				{
					setUserCheckoutCartToSession(ControllerConstants.Views.Fragments.UserCart.InBoundCart);
				}
				catch (final Exception e)
				{
					// YTODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else if (cartList.get(i).getName().equals(ControllerConstants.Views.Fragments.UserCart.OutBoundCart))
			{

				CART_DATA = "cartData";
				DELIVERY_ADDRESSES = "deliveryAddresses";
				DELIVERY_METHODS = "deliveryMethods";
				model.addAttribute("outboundCartExists", Boolean.TRUE);
				try
				{
					setUserCheckoutCartToSession(ControllerConstants.Views.Fragments.UserCart.OutBoundCart);
				}
				catch (final Exception e)
				{
					// YTODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			getCheckoutFacade().setDeliveryModeIfAvailable();

			final CartData cartData = getCheckoutFacade().getCheckoutCart();
			model.addAttribute(CART_DATA, cartData);
			if (CollectionUtils.isNotEmpty(getCheckoutFacade().getSupportedDeliveryModes()))
			{
				deliveryList = (List<DeliveryModeData>) getCheckoutFacade().getSupportedDeliveryModes();
			}
			model.addAttribute(DELIVERY_METHODS, getCheckoutFacade().getSupportedDeliveryModes());
			model.addAttribute(DELIVERY_ADDRESSES, getDeliveryAddresses(cartData.getDeliveryAddress()));
			/*
			 * model.addAttribute(NO_ADDRESS, Boolean.valueOf(getCheckoutFlowFacade().hasNoDeliveryAddress()));
			 * model.addAttribute(ADDRESS_FORM, new AddressForm()); model.addAttribute(SAVE_ADDRESS_BOOK, Boolean.TRUE);
			 */
		}

		if (CollectionUtils.isNotEmpty(deliveryList))
		{
			model.addAttribute("inboundDeliveryMethods", deliveryList);
			model.addAttribute("deliveryMethods", deliveryList);
		}

		//getCheckoutFacade().setDeliveryModeIfAvailable();

		//final CartData cartData = getCheckoutFacade().getCheckoutCart();
		//model.addAttribute("cartData", cartData);
		this.prepareDataForPage(model);
		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.deliveryMethod.breadcrumb"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		setCheckoutStepLinksForModel(model, getCheckoutStep());

		return ControllerConstants.Views.Pages.MultiStepCheckout.ChooseDeliveryMethodPage;
	}

	public List<CartData> getUserCheckedOutCart()
	{

		final List<CartData> cartDataList = new ArrayList<CartData>();
		String checkoutType = "";
		if (isCombinedCheckout())
		{
			for (int i = 0; i < 2; i++)
			{
				switch (i)
				{
					case 0:
						checkoutType = ControllerConstants.Views.Fragments.UserCart.OutBoundCart;
						break;
					case 1:
						checkoutType = ControllerConstants.Views.Fragments.UserCart.InBoundCart;
						break;
				}

				try
				{
					setUserCheckoutCartToSession(checkoutType);
				}
				catch (final Exception e)
				{
					// YTODO Auto-generated catch block
					e.printStackTrace();
				}
				cartDataList.add(getCartFacade().getSessionCart());
			}
		}
		else
		{

			checkoutType = getSessionService().getAttribute("checkoutType");
			try
			{
				setUserCheckoutCartToSession(checkoutType);
			}
			catch (final Exception e)
			{
				// YTODO Auto-generated catch block
				e.printStackTrace();
			}
			cartDataList.add(getCartFacade().getSessionCart());
		}
		return cartDataList;
	}

	protected boolean isCombinedCheckout()
	{
		final String checkoutType = getSessionService().getAttribute("checkoutType");
		final boolean isCombinedCheckout = checkoutType.equals(ControllerConstants.Views.Fragments.UserCart.CombinedCart);
		final List<CartData> cartDataList = getCartFacade().getCartsForCurrentUser();
		if (cartDataList.size() == 1)
		{
			cartDataList.add(getCartFacade().getSessionCart());
		}
		if (isCombinedCheckout && CollectionUtils.isNotEmpty(cartDataList) && cartDataList.size() >= 2)
		{
			return true;
		}
		return false;
	}

	protected void setUserCheckoutCartToSession(final String checkoutType) throws Exception
	{
		//final String checkoutType = getSessionService().getAttribute("checkoutType");

		if (StringUtils.isNotEmpty(checkoutType))
		{
			//final List<CartData> cartList = getCartFacade().getCartsForCurrentUser();
			CartModel sessionCartModel = getCartService().getSessionCart();

			if (!checkoutType.equals(sessionCartModel.getName()))
			{
				//				final List<CartModel> cartModelList = getCommerceCartService().getCartsForSiteAndUser(
				//						baseSiteService.getCurrentBaseSite(), getUserService().getCurrentUser());

				final List<CartModel> cartModelList = getSessionService().getAttribute("Users_Cart_List");

				if (org.apache.commons.collections.CollectionUtils.isNotEmpty(cartModelList)
						&& StringUtils.isNotEmpty(sessionCartModel.getName()))
				{
					CartModel cartModel = getMatchingCart(cartModelList, checkoutType);
					if (cartModelList.size() == 1)
					{
						cartModel = cartModelList.get(0);
					}
					if (cartModel != null)
					{
						cartService.setSessionCart(cartModel);
					}
					else
					{
						throw new Exception("Could not get Associated cart");
					}
				}
				sessionCartModel = cartService.getSessionCart();
				System.out.println("new cart" + sessionCartModel.getCode());
			}
		}
	}

	public CartModel getMatchingCart(final List<CartModel> cartModelList, final String cartName)
	{
		for (final CartModel cartModel : cartModelList)
		{
			if (cartModel.getName().equals(cartName))
			{
				return cartModel;
			}
		}

		return null;

	}

	/**
	 * This method gets called when the "Use Selected Delivery Method" button is clicked. It sets the selected delivery
	 * mode on the checkout facade and reloads the page highlighting the selected delivery Mode.
	 *
	 * @param selectedDeliveryMethod
	 *           - the id of the delivery mode.
	 * @return - a URL to the page to load.
	 */
	@RequestMapping(value = "/select", method = RequestMethod.GET)
	@RequireHardLogIn
	public String doSelectDeliveryMode(
			@RequestParam(value = "delivery_method", required = false) final String selectedOutBoundDeliveryMethod,
			@RequestParam(value = "inbound_delivery_method", required = false) final String selectedInBoundDeliveryMethod)
	{
		//From UI needs to send two UI delivery methods for each inbound and outbound cart
		final List<CartData> cartList = getUserCheckedOutCart();

		for (int i = 0; i < cartList.size(); i++)
		{
			if (cartList.get(i).getName().equals(ControllerConstants.Views.Fragments.UserCart.InBoundCart))
			{
				try
				{
					setUserCheckoutCartToSession(ControllerConstants.Views.Fragments.UserCart.InBoundCart);
					if (StringUtils.isNotEmpty(selectedInBoundDeliveryMethod))
					{
						getCheckoutFacade().setDeliveryMode(selectedInBoundDeliveryMethod);
					}
				}
				catch (final Exception e)
				{
					// YTODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else if (cartList.get(i).getName().equals(ControllerConstants.Views.Fragments.UserCart.OutBoundCart))
			{
				try
				{
					setUserCheckoutCartToSession(ControllerConstants.Views.Fragments.UserCart.OutBoundCart);
					if (StringUtils.isNotEmpty(selectedOutBoundDeliveryMethod))
					{
						getCheckoutFacade().setDeliveryMode(selectedOutBoundDeliveryMethod);
					}
				}
				catch (final Exception e)
				{
					// YTODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return getCheckoutStep().nextStep();
	}

	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}

	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(DELIVERY_METHOD);
	}
}
