/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.sonata.travelretail.storefront.controllers.pages.checkout.steps;

import de.hybris.platform.acceleratorfacades.order.AcceleratorCheckoutFacade;
import de.hybris.platform.acceleratorservices.enums.CheckoutPciOptionEnum;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.checkout.steps.AbstractCheckoutStepController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.PlaceOrderForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import uk.co.tui.book.domain.lite.BasePackage;
import uk.co.tui.book.domain.lite.FlightItinerary;

import com.sonata.travelretail.core.model.ProductBookingModel;
import com.sonata.travelretail.storefront.controllers.ControllerConstants;


@Controller
@RequestMapping(value = "/checkout/multi/summary")
public class SummaryCheckoutStepController extends AbstractCheckoutStepController
{

	@Resource(name = "cartService")
	@Autowired
	private CartService cartService;

	/**
	 * @return the cartService
	 */
	public CartService getCartService()
	{
		return cartService;
	}

	/**
	 * @param cartService
	 *           the cartService to set
	 */
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}

	/**
	 * @return the checkoutFacade
	 */
	@Override
	public AcceleratorCheckoutFacade getCheckoutFacade()
	{
		return checkoutFacade;
	}

	/**
	 * @param checkoutFacade
	 *           the checkoutFacade to set
	 */
	public void setCheckoutFacade(final AcceleratorCheckoutFacade checkoutFacade)
	{
		this.checkoutFacade = checkoutFacade;
	}

	@Resource(name = "acceleratorTravelCheckoutFacade")
	private AcceleratorCheckoutFacade checkoutFacade;
	private final static String SUMMARY = "summary";

	private final static String DELIVERY_ADDRESS = "delivery-address";
	private static String CART_DATA = "cartData";
	private static String NO_ADDRESS = "noAddress";
	private static String ADDRESS_FORM = "addressForm";
	private static String SAVE_ADDRESS_BOOK = "showSaveToAddressBook";
	private static String DELIVERY_ADDRESSES = "deliveryAddresses";
	private static String REGIONS = "regions";
	private static String COUNTRY = "country";
	private static String ALL_ITEMS = "allItems";

	private static String DELIVERY_MODE = "deliveryMode";
	private static String PAYMENT_INFO = "paymentInfo";

	@Resource
	private ModelService modelService;

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	//@PreValidateCheckoutStep(checkoutStep = SUMMARY)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException,
			CommerceCartModificationException
	{
		final List<CartData> cartList = getUserCheckedOutCart();

		for (int i = 0; i < cartList.size(); i++)
		{

			if (cartList.get(i).getName().equals(ControllerConstants.Views.Fragments.UserCart.InBoundCart))
			{

				CART_DATA = "inboundCartData";
				NO_ADDRESS = "inboundNoAddress";
				SAVE_ADDRESS_BOOK = "inboundShowSaveToAddressBook";
				DELIVERY_ADDRESSES = "inboundDeliveryAddresses";
				REGIONS = "inboundRegions";
				COUNTRY = "inboundCountry";
				ALL_ITEMS = "inboundItems";
				DELIVERY_MODE = "inboundDeliveryMode";
				PAYMENT_INFO = "inboundPaymentInfo";
				model.addAttribute("inboundCartExists", Boolean.TRUE);
				try
				{
					setUserCheckoutCartToSession(ControllerConstants.Views.Fragments.UserCart.InBoundCart);
				}
				catch (final Exception e)
				{
					// YTODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else if (cartList.get(i).getName().equals(ControllerConstants.Views.Fragments.UserCart.OutBoundCart))
			{

				CART_DATA = "cartData";
				NO_ADDRESS = "noAddress";
				SAVE_ADDRESS_BOOK = "showSaveToAddressBook";
				DELIVERY_ADDRESSES = "deliveryAddresses";
				REGIONS = "regions";
				COUNTRY = "country";
				ALL_ITEMS = "allItems";
				DELIVERY_MODE = "deliveryMode";
				PAYMENT_INFO = "paymentInfo";
				model.addAttribute("outboundCartExists", Boolean.TRUE);
				try
				{
					setUserCheckoutCartToSession(ControllerConstants.Views.Fragments.UserCart.OutBoundCart);
				}
				catch (final Exception e)
				{
					// YTODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			final CartData cartData = getCheckoutFacade().getCheckoutCart();
			if (cartData.getEntries() != null && !cartData.getEntries().isEmpty())
			{
				for (final OrderEntryData entry : cartData.getEntries())
				{
					final String productCode = entry.getProduct().getCode();
					final ProductData product = getProductFacade().getProductForCodeAndOptions(productCode,
							Arrays.asList(ProductOption.BASIC, ProductOption.PRICE));
					entry.setProduct(product);
				}
			}

			/*
			 * model.addAttribute("cartData", cartData); model.addAttribute("allItems", cartData.getEntries());
			 * model.addAttribute("deliveryAddress", cartData.getDeliveryAddress()); model.addAttribute("deliveryMode",
			 * cartData.getDeliveryMode()); model.addAttribute("paymentInfo", cartData.getPaymentInfo());
			 */

			model.addAttribute(CART_DATA, cartData);
			model.addAttribute(ALL_ITEMS, cartData.getEntries());
			model.addAttribute(DELIVERY_ADDRESSES, cartData.getDeliveryAddress());
			model.addAttribute(DELIVERY_MODE, cartData.getDeliveryMode());
			model.addAttribute(PAYMENT_INFO, cartData.getPaymentInfo());
			model.addAttribute(new PlaceOrderForm());
		}
		// Only request the security code if the SubscriptionPciOption is set to Default.
		final boolean requestSecurityCode = (CheckoutPciOptionEnum.DEFAULT.equals(getCheckoutFlowFacade()
				.getSubscriptionPciOption()));
		model.addAttribute("requestSecurityCode", Boolean.valueOf(requestSecurityCode));



		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.summary.breadcrumb"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		setCheckoutStepLinksForModel(model, getCheckoutStep());
		return ControllerConstants.Views.Pages.MultiStepCheckout.CheckoutSummaryPage;
	}

	public List<CartData> getUserCheckedOutCart()
	{

		final List<CartData> cartDataList = new ArrayList<CartData>();
		String checkoutType = "";
		if (isCombinedCheckout())
		{
			for (int i = 0; i < 2; i++)
			{
				switch (i)
				{
					case 0:
						checkoutType = ControllerConstants.Views.Fragments.UserCart.OutBoundCart;
						break;
					case 1:
						checkoutType = ControllerConstants.Views.Fragments.UserCart.InBoundCart;
						break;
				}

				try
				{
					setUserCheckoutCartToSession(checkoutType);
				}
				catch (final Exception e)
				{
					// YTODO Auto-generated catch block
					e.printStackTrace();
				}
				cartDataList.add(getCartFacade().getSessionCart());
			}
		}
		else
		{

			checkoutType = getSessionService().getAttribute("checkoutType");
			try
			{
				setUserCheckoutCartToSession(checkoutType);
			}
			catch (final Exception e)
			{
				// YTODO Auto-generated catch block
				e.printStackTrace();
			}
			cartDataList.add(getCartFacade().getSessionCart());
		}
		return cartDataList;
	}

	protected boolean isCombinedCheckout()
	{
		final String checkoutType = getSessionService().getAttribute("checkoutType");
		final boolean isCombinedCheckout = checkoutType.equals(ControllerConstants.Views.Fragments.UserCart.CombinedCart);
		final List<CartData> cartDataList = getCartFacade().getCartsForCurrentUser();
		if (cartDataList.size() == 1)
		{
			cartDataList.add(getCartFacade().getSessionCart());
		}
		if (isCombinedCheckout && CollectionUtils.isNotEmpty(cartDataList) && cartDataList.size() >= 2)
		{
			return true;
		}
		return false;
	}

	protected void setUserCheckoutCartToSession(final String checkoutType) throws Exception
	{
		//final String checkoutType = getSessionService().getAttribute("checkoutType");

		if (StringUtils.isNotEmpty(checkoutType))
		{
			//final List<CartData> cartList = getCartFacade().getCartsForCurrentUser();
			CartModel sessionCartModel = getCartService().getSessionCart();

			if (!checkoutType.equals(sessionCartModel.getName()))
			{
				//				final List<CartModel> cartModelList = getCommerceCartService().getCartsForSiteAndUser(
				//						baseSiteService.getCurrentBaseSite(), getUserService().getCurrentUser());

				final List<CartModel> cartModelList = getSessionService().getAttribute("Users_Cart_List");

				if (org.apache.commons.collections.CollectionUtils.isNotEmpty(cartModelList)
						&& StringUtils.isNotEmpty(sessionCartModel.getName()))
				{
					CartModel cartModel = getMatchingCart(cartModelList, checkoutType);
					if (cartModelList.size() == 1)
					{
						cartModel = cartModelList.get(0);
					}
					if (cartModel != null)
					{
						cartService.setSessionCart(cartModel);
					}
					else
					{
						throw new Exception("Could not get Associated cart");
					}
				}
				sessionCartModel = cartService.getSessionCart();
				System.out.println("new cart" + sessionCartModel.getCode());
			}
		}
	}

	public CartModel getMatchingCart(final List<CartModel> cartModelList, final String cartName)
	{
		for (final CartModel cartModel : cartModelList)
		{
			if (cartModel.getName().equals(cartName))
			{
				return cartModel;
			}
		}

		return null;

	}


	@RequestMapping(value = "/placeOrder")
	@RequireHardLogIn
	public String placeOrder(@ModelAttribute("placeOrderForm") final PlaceOrderForm placeOrderForm, final Model model,
			final HttpServletRequest request, final RedirectAttributes redirectModel) throws CMSItemNotFoundException,
			InvalidCartException, CommerceCartModificationException
	{
		final List<CartData> cartList = getUserCheckedOutCart();
		final List<OrderData> orderList = new ArrayList<OrderData>();
		OrderData orderData = null;
		for (int i = 0; i < cartList.size(); i++)
		{
			if (cartList.get(i).getName().equals(ControllerConstants.Views.Fragments.UserCart.InBoundCart))
			{
				model.addAttribute("inboundCartExists", Boolean.TRUE);
				//				if (cartList.get(i).getPaymentInfo() == null)
				//				{
				//					final CartData outboundCartData = getMatchingCartData(cartList,
				//							ControllerConstants.Views.Fragments.UserCart.OutBoundCart);
				//					cartList.get(i).setPaymentInfo(outboundCartData.getPaymentInfo());
				//
				//				}
				try
				{
					setUserCheckoutCartToSession(ControllerConstants.Views.Fragments.UserCart.InBoundCart);
				}
				catch (final Exception e)
				{
					// YTODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else
			{
				if (cartList.get(i).getName().equals(ControllerConstants.Views.Fragments.UserCart.OutBoundCart))
				{
					model.addAttribute("outboundCartExists", Boolean.TRUE);

					//					savePaymentInfo();
					//					if (cartList.get(i).getPaymentInfo() == null)
					//					{
					//						final CartData inboundCartData = getMatchingCartData(cartList,
					//								ControllerConstants.Views.Fragments.UserCart.InBoundCart);
					//						cartList.get(i).setPaymentInfo(inboundCartData.getPaymentInfo());
					//
					//					}
					try
					{
						setUserCheckoutCartToSession(ControllerConstants.Views.Fragments.UserCart.OutBoundCart);
					}
					catch (final Exception e)
					{
						// YTODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}



			if (validateOrderForm(placeOrderForm, model, cartList.get(i)))
			{
				return enterStep(model, redirectModel);
			}

			//Validate the cart
			if (validateCart(redirectModel))
			{
				// Invalid cart. Bounce back to the cart page.
				return REDIRECT_PREFIX + "/cart";
			}

			// authorize, if failure occurs don't allow to place the order
			boolean isPaymentUthorized = false;
			try
			{
				isPaymentUthorized = getCheckoutFacade().authorizePayment(placeOrderForm.getSecurityCode());
			}
			catch (final Exception ae)
			{
				// handle a case where a wrong paymentProvider configurations on the store see getCommerceCheckoutService().getPaymentProvider()
				LOG.error(ae.getMessage(), ae);
			}
			if (!isPaymentUthorized)
			{
				GlobalMessages.addErrorMessage(model, "checkout.error.authorization.failed");
				return enterStep(model, redirectModel);
			}


			try
			{
				orderData = getCheckoutFacade().placeOrder();
				if (cartList.get(i).getName().equals(ControllerConstants.Views.Fragments.UserCart.InBoundCart))
				{
					getSessionService().setAttribute("INBOUND_ORDER", orderData.getCode());
				}
				else
				{
					if (cartList.get(i).getName().equals(ControllerConstants.Views.Fragments.UserCart.OutBoundCart))
					{
						getSessionService().setAttribute("OUTBOUND_ORDER", orderData.getCode());
					}
				}
				orderList.add(orderData);
			}
			catch (final Exception e)
			{
				LOG.error("Failed to place Order", e);
				GlobalMessages.addErrorMessage(model, "checkout.placeOrder.failed");
				return enterStep(model, redirectModel);
			}
		}

		if (orderList.size() != 0 && orderList.get(0).getCode() != null)
		{
			savePNRBookDetails(request, orderList);
		}
		getSessionService().setAttribute("ORDERS_LIST", orderList);

		final List<CartData> cartDataList = getUserCheckedOutCart();
		//MinCart Component changes
		for (final CartData cartData : cartDataList)
		{

			if (cartData.getName().equals(ControllerConstants.Views.Fragments.UserCart.OutBoundCart))
			{
				getSessionService().setAttribute("outboundCartExists", Boolean.TRUE);
			}
			else if (cartData.getName().equals(ControllerConstants.Views.Fragments.UserCart.InBoundCart))
			{
				getSessionService().setAttribute("inboundCartExists", Boolean.TRUE);
			}


		}

		final List<CartModel> cartModelList = getSessionService().getAttribute("Users_Cart_List");

		for (final CartModel cartModel : cartModelList)
		{
			getCartService().setSessionCart(cartModel);
			getCartService().removeSessionCart();
		}



		//	getCartService().removeSessionCart();

		return redirectToOrderConfirmationPage(orderData);
	}

	/**
	 * @param request
	 * @param orderList
	 */
	private void savePNRBookDetails(final HttpServletRequest request, final List<OrderData> orderList)
	{
		final HttpSession sessionReq = request.getSession();
		final FlightItinerary flightItinerary = (FlightItinerary) sessionReq.getAttribute("flightItinerary");
		final BasePackage packageHoliday = (BasePackage) sessionReq.getAttribute("bookResponse");
		final String pnr = packageHoliday.getBookingRefNum();

		final String inordercode = getSessionService().getAttribute("INBOUND_ORDER");
		final String outordercode = getSessionService().getAttribute("OUTBOUND_ORDER");



		int k = 0;
		String destination = new String();
		String source = new String();
		for (final OrderData orderData : orderList)
		{
			if (orderData.getCode().equals(inordercode))
			{
				destination = flightItinerary.getInBound().get(0).getArrivalAirport().getCode();
				source = flightItinerary.getInBound().get(0).getDepartureAirport().getCode();
			}
			else if (orderData.getCode().equals(outordercode))
			{
				destination = flightItinerary.getOutBound().get(0).getArrivalAirport().getCode();
				source = flightItinerary.getOutBound().get(0).getDepartureAirport().getCode();
			}
			for (final OrderEntryData orderData2 : orderData.getEntries())
			{

				final ProductBookingModel bookingModel = new ProductBookingModel();
				bookingModel.setSource(source);
				bookingModel.setDestination(destination);
				bookingModel.setPnr(pnr);
				bookingModel.setQunatity(orderData2.getQuantity().intValue());
				bookingModel.setProductCode(orderData2.getProduct().getCode());
				bookingModel.setOrderId(orderData.getCode() + k);
				modelService.save(bookingModel);
				k++;
			}
		}
	}


	/**
	 * Validates the order form before to filter out invalid order states
	 *
	 * @param placeOrderForm
	 *           The spring form of the order being submitted
	 * @param model
	 *           A spring Model
	 * @return True if the order form is invalid and false if everything is valid.
	 */
	protected boolean validateOrderForm(final PlaceOrderForm placeOrderForm, final Model model, final CartData cartData)
	{
		final String securityCode = placeOrderForm.getSecurityCode();
		boolean invalid = false;

		if (getCheckoutFlowFacade().hasNoDeliveryAddress())
		{
			GlobalMessages.addErrorMessage(model, "checkout.deliveryAddress.notSelected");
			invalid = true;
		}

		if (getCheckoutFlowFacade().hasNoDeliveryMode())
		{
			GlobalMessages.addErrorMessage(model, "checkout.deliveryMethod.notSelected");
			invalid = true;
		}

		if (getCheckoutFlowFacade().hasNoPaymentInfo())
		{
			GlobalMessages.addErrorMessage(model, "checkout.paymentMethod.notSelected");
			invalid = true;
		}
		else
		{
			// Only require the Security Code to be entered on the summary page if the SubscriptionPciOption is set to Default.
			if (CheckoutPciOptionEnum.DEFAULT.equals(getCheckoutFlowFacade().getSubscriptionPciOption())
					&& StringUtils.isBlank(securityCode))
			{
				GlobalMessages.addErrorMessage(model, "checkout.paymentMethod.noSecurityCode");
				invalid = true;
			}
		}

		if (!placeOrderForm.isTermsCheck())
		{
			GlobalMessages.addErrorMessage(model, "checkout.error.terms.not.accepted");
			invalid = true;
			return invalid;
		}
		//	final CartData cartData = getCheckoutFacade().getCheckoutCart();

		if (!getCheckoutFacade().containsTaxValues())
		{
			LOG.error(String
					.format(
							"Cart %s does not have any tax values, which means the tax cacluation was not properly done, placement of order can't continue",
							cartData.getCode()));
			GlobalMessages.addErrorMessage(model, "checkout.error.tax.missing");
			invalid = true;
		}

		if (!cartData.isCalculated())
		{
			LOG.error(String.format("Cart %s has a calculated flag of FALSE, placement of order can't continue", cartData.getCode()));
			GlobalMessages.addErrorMessage(model, "checkout.error.cart.notcalculated");
			invalid = true;
		}

		return invalid;
	}

	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}

	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(SUMMARY);
	}


}
