/**
 *
 */
package com.sonata.travelretail.storefront.controllers.pages;

import de.hybris.platform.acceleratorcms.model.components.PnrBookingDetailsComponentModel;

import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import uk.co.tui.book.domain.lite.BasePackage;
import uk.co.tui.book.domain.lite.FlightItinerary;

import com.sonata.travelretail.storefront.controllers.ControllerConstants;
import com.sonata.travelretail.storefront.controllers.cms.AbstractAcceleratorCMSComponentController;


/**
 * @author nataraja.y
 *
 */
@Controller("PnrBookingDetailsComponentController")
@Scope("tenant")
@RequestMapping(value = ControllerConstants.Actions.Cms.PnrBookingDetailsComponent)
public class PnrBookingDetailsComponentController extends
		AbstractAcceleratorCMSComponentController<PnrBookingDetailsComponentModel>
{
	protected static final Logger LOG = Logger.getLogger(PnrBookingDetailsComponentController.class);



	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final PnrBookingDetailsComponentModel component)
	{
		LOG.info("Start of PnrBookingDetailsComponent ***********************************************");

		final HttpSession sessionObj = request.getSession();

		if (null != sessionObj.getAttribute("bookResponse") && null != sessionObj.getAttribute("flightItinerary"))
		{

			final BasePackage basePackage = (BasePackage) sessionObj.getAttribute("bookResponse");

			final FlightItinerary flightItinerary = (FlightItinerary) sessionObj.getAttribute("flightItinerary");
			String outBound = null;
			String inBound = null;
			String departDate = null;
			String arrvDate = null;

			final SimpleDateFormat ft = new SimpleDateFormat("EEE MMM dd HH:mm:ss yyyy");
			if (null != flightItinerary)
			{
				outBound = flightItinerary.getOutBound().get(0).getDepartureAirport().getCode();
				inBound = flightItinerary.getOutBound().get(0).getArrivalAirport().getCode();
				departDate = ft.format(flightItinerary.getOutBound().get(0).getSchedule().getDepartureDate());
				arrvDate = ft.format(flightItinerary.getInBound().get(0).getSchedule().getArrivalDate());
			}

			model.addAttribute("bookingRefNum", basePackage.getBookingRefNum());
			model.addAttribute("outBound", outBound);
			model.addAttribute("inBound", inBound);
			model.addAttribute("depDate", departDate);
			model.addAttribute("arrvDate", arrvDate);


		}
		LOG.info("End of PnrDetailsComponent");
	}



}
