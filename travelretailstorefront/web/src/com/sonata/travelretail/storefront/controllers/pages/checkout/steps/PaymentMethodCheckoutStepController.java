/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.sonata.travelretail.storefront.controllers.pages.checkout.steps;


import de.hybris.platform.acceleratorfacades.order.AcceleratorCheckoutFacade;
import de.hybris.platform.acceleratorservices.enums.CheckoutPciOptionEnum;
import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.checkout.steps.AbstractCheckoutStepController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.PaymentDetailsForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.SopPaymentDetailsForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.CardTypeData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sonata.travelretail.storefront.controllers.ControllerConstants;


@Controller
@RequestMapping(value = "/checkout/multi/payment-method")
public class PaymentMethodCheckoutStepController extends AbstractCheckoutStepController
{

	@Resource(name = "cartService")
	@Autowired
	private CartService cartService;

	/**
	 * @return the cartService
	 */
	public CartService getCartService()
	{
		return cartService;
	}

	/**
	 * @param cartService
	 *           the cartService to set
	 */
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}

	/**
	 * @return the checkoutFacade
	 */
	@Override
	public AcceleratorCheckoutFacade getCheckoutFacade()
	{
		return checkoutFacade;
	}

	/**
	 * @param checkoutFacade
	 *           the checkoutFacade to set
	 */
	public void setCheckoutFacade(final AcceleratorCheckoutFacade checkoutFacade)
	{
		this.checkoutFacade = checkoutFacade;
	}

	@Resource(name = "acceleratorCheckoutFacade")
	private AcceleratorCheckoutFacade checkoutFacade;
	protected static final Map<String, String> cybersourceSopCardTypes = new HashMap<String, String>();
	private final static String PAYMENT_METHOD = "payment-method";
	private static String PAYMENT_ID = "paymentId";

	@ModelAttribute("billingCountries")
	public Collection<CountryData> getBillingCountries()
	{
		return getCheckoutFacade().getBillingCountries();
	}

	@ModelAttribute("cardTypes")
	public Collection<CardTypeData> getCardTypes()
	{
		return getCheckoutFacade().getSupportedCardTypes();
	}

	@ModelAttribute("months")
	public List<SelectOption> getMonths()
	{
		final List<SelectOption> months = new ArrayList<SelectOption>();

		months.add(new SelectOption("1", "01"));
		months.add(new SelectOption("2", "02"));
		months.add(new SelectOption("3", "03"));
		months.add(new SelectOption("4", "04"));
		months.add(new SelectOption("5", "05"));
		months.add(new SelectOption("6", "06"));
		months.add(new SelectOption("7", "07"));
		months.add(new SelectOption("8", "08"));
		months.add(new SelectOption("9", "09"));
		months.add(new SelectOption("10", "10"));
		months.add(new SelectOption("11", "11"));
		months.add(new SelectOption("12", "12"));

		return months;
	}

	@ModelAttribute("startYears")
	public List<SelectOption> getStartYears()
	{
		final List<SelectOption> startYears = new ArrayList<SelectOption>();
		final Calendar calender = new GregorianCalendar();

		for (int i = calender.get(Calendar.YEAR); i > (calender.get(Calendar.YEAR) - 6); i--)
		{
			startYears.add(new SelectOption(String.valueOf(i), String.valueOf(i)));
		}

		return startYears;
	}

	@ModelAttribute("expiryYears")
	public List<SelectOption> getExpiryYears()
	{
		final List<SelectOption> expiryYears = new ArrayList<SelectOption>();
		final Calendar calender = new GregorianCalendar();

		for (int i = calender.get(Calendar.YEAR); i < (calender.get(Calendar.YEAR) + 11); i++)
		{
			expiryYears.add(new SelectOption(String.valueOf(i), String.valueOf(i)));
		}

		return expiryYears;
	}

	public List<CartData> getUserCheckedOutCart()
	{

		final List<CartData> cartDataList = new ArrayList<CartData>();
		String checkoutType = "";
		if (isCombinedCheckout())
		{
			for (int i = 0; i < 2; i++)
			{
				switch (i)
				{
					case 0:
						checkoutType = ControllerConstants.Views.Fragments.UserCart.OutBoundCart;
						break;
					case 1:
						checkoutType = ControllerConstants.Views.Fragments.UserCart.InBoundCart;
						break;
				}

				try
				{
					setUserCheckoutCartToSession(checkoutType);
				}
				catch (final Exception e)
				{
					// YTODO Auto-generated catch block
					e.printStackTrace();
				}
				cartDataList.add(getCartFacade().getSessionCart());
			}
		}
		else
		{

			checkoutType = getSessionService().getAttribute("checkoutType");
			try
			{
				setUserCheckoutCartToSession(checkoutType);
			}
			catch (final Exception e)
			{
				// YTODO Auto-generated catch block
				e.printStackTrace();
			}
			cartDataList.add(getCartFacade().getSessionCart());
		}
		return cartDataList;
	}

	protected boolean isCombinedCheckout()
	{
		final String checkoutType = getSessionService().getAttribute("checkoutType");
		final boolean isCombinedCheckout = checkoutType.equals(ControllerConstants.Views.Fragments.UserCart.CombinedCart);
		final List<CartData> cartDataList = getCartFacade().getCartsForCurrentUser();
		if (cartDataList.size() == 1)
		{
			cartDataList.add(getCartFacade().getSessionCart());
		}
		if (isCombinedCheckout && CollectionUtils.isNotEmpty(cartDataList) && cartDataList.size() >= 2)
		{
			return true;
		}
		return false;
	}

	protected void setUserCheckoutCartToSession(final String checkoutType) throws Exception
	{
		//final String checkoutType = getSessionService().getAttribute("checkoutType");

		if (StringUtils.isNotEmpty(checkoutType))
		{
			//final List<CartData> cartList = getCartFacade().getCartsForCurrentUser();
			CartModel sessionCartModel = getCartService().getSessionCart();

			if (!checkoutType.equals(sessionCartModel.getName()))
			{
				//				final List<CartModel> cartModelList = getCommerceCartService().getCartsForSiteAndUser(
				//						baseSiteService.getCurrentBaseSite(), getUserService().getCurrentUser());

				final List<CartModel> cartModelList = getSessionService().getAttribute("Users_Cart_List");

				if (org.apache.commons.collections.CollectionUtils.isNotEmpty(cartModelList)
						&& StringUtils.isNotEmpty(sessionCartModel.getName()))
				{
					CartModel cartModel = getMatchingCart(cartModelList, checkoutType);
					if (cartModelList.size() == 1)
					{
						cartModel = cartModelList.get(0);
					}
					if (cartModel != null)
					{
						cartService.setSessionCart(cartModel);
					}
					else
					{
						throw new Exception("Could not get Associated cart");
					}
				}
				sessionCartModel = cartService.getSessionCart();
				System.out.println("new cart" + sessionCartModel.getCode());
			}
		}
	}

	public CartModel getMatchingCart(final List<CartModel> cartModelList, final String cartName)
	{
		for (final CartModel cartModel : cartModelList)
		{
			if (cartModel.getName().equals(cartName))
			{
				return cartModel;
			}
		}

		return null;

	}

	@Override
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	@RequireHardLogIn
	//@PreValidateCheckoutStep(checkoutStep = PAYMENT_METHOD)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		setupAddPaymentPage(model);

		// Use the checkout PCI strategy for getting the URL for creating new subscriptions.
		final CheckoutPciOptionEnum subscriptionPciOption = getCheckoutFlowFacade().getSubscriptionPciOption();
		setCheckoutStepLinksForModel(model, getCheckoutStep());
		//		if (CheckoutPciOptionEnum.HOP.equals(subscriptionPciOption))
		//		{
		//			// Redirect the customer to the HOP page or show error message if it fails (e.g. no HOP configurations).
		//			try
		//			{
		//				final PaymentData hostedOrderPageData = getPaymentFacade().beginHopCreateSubscription("/checkout/multi/hop/response",
		//						"/integration/merchant_callback");
		//				model.addAttribute("hostedOrderPageData", hostedOrderPageData);
		//
		//				final boolean hopDebugMode = getSiteConfigService().getBoolean(PaymentConstants.PaymentProperties.HOP_DEBUG_MODE,
		//						false);
		//				model.addAttribute("hopDebugMode", Boolean.valueOf(hopDebugMode));
		//
		//				return ControllerConstants.Views.Pages.MultiStepCheckout.HostedOrderPostPage;
		//			}
		//			catch (final Exception e)
		//			{
		//				LOG.error("Failed to build beginCreateSubscription request", e);
		//				GlobalMessages.addErrorMessage(model, "checkout.multi.paymentMethod.addPaymentDetails.generalError");
		//			}
		//		}

		//		final List<CartData> cartList = getUserCheckedOutCart();
		//
		//		for (int i = 0; i < cartList.size(); i++)
		//		{
		//
		//			if (cartList.get(i).getName().equals(ControllerConstants.Views.Fragments.UserCart.InBoundCart))
		//			{
		//				try
		//				{
		//					setUserCheckoutCartToSession(ControllerConstants.Views.Fragments.UserCart.InBoundCart);
		//				}
		//				catch (final Exception e)
		//				{
		//					// YTODO Auto-generated catch block
		//					e.printStackTrace();
		//				}
		//				if (CheckoutPciOptionEnum.SOP.equals(subscriptionPciOption))
		//				{
		//					// Build up the SOP form data and render page containing form
		//					final SopPaymentDetailsForm sopPaymentDetailsForm = new SopPaymentDetailsForm();
		//					try
		//					{
		//						setupSilentOrderPostPage(sopPaymentDetailsForm, model, cartList.get(i).getName());
		//
		//					}
		//					catch (final Exception e)
		//					{
		//						LOG.error("Failed to build beginCreateSubscription request", e);
		//						GlobalMessages.addErrorMessage(model, "checkout.multi.paymentMethod.addPaymentDetails.generalError");
		//						model.addAttribute("sopPaymentDetailsForm", sopPaymentDetailsForm);
		//					}
		//				}
		//			}
		//			else
		//			{
		//
		//				if (cartList.get(i).getName().equals(ControllerConstants.Views.Fragments.UserCart.OutBoundCart))
		//				{
		//					try
		//					{
		//						setUserCheckoutCartToSession(ControllerConstants.Views.Fragments.UserCart.OutBoundCart);
		//					}
		//					catch (final Exception e)
		//					{
		//						// YTODO Auto-generated catch block
		//						e.printStackTrace();
		//					}
		//					if (CheckoutPciOptionEnum.SOP.equals(subscriptionPciOption))
		//					{
		//						// Build up the SOP form data and render page containing form
		//						final SopPaymentDetailsForm sopPaymentDetailsForm = new SopPaymentDetailsForm();
		//						try
		//						{
		//							setupSilentOrderPostPage(sopPaymentDetailsForm, model, cartList.get(i).getName());
		//
		//						}
		//						catch (final Exception e)
		//						{
		//							LOG.error("Failed to build beginCreateSubscription request", e);
		//							GlobalMessages.addErrorMessage(model, "checkout.multi.paymentMethod.addPaymentDetails.generalError");
		//							model.addAttribute("sopPaymentDetailsForm", sopPaymentDetailsForm);
		//						}
		//					}
		//
		//
		//				}
		//			}
		//		}
		//		return ControllerConstants.Views.Pages.MultiStepCheckout.SilentOrderPostPage;
		// If not using HOP or SOP we need to build up the payment details form
		final PaymentDetailsForm paymentDetailsForm = new PaymentDetailsForm();
		final AddressForm addressForm = new AddressForm();
		paymentDetailsForm.setBillingAddress(addressForm);
		model.addAttribute(paymentDetailsForm);




		final List<CartData> cartList = getUserCheckedOutCart();

		for (int i = 0; i < cartList.size(); i++)
		{

			if (cartList.get(i).getName().equals(ControllerConstants.Views.Fragments.UserCart.InBoundCart))
			{

				CART_DATA = "inboundCartData";
				/*
				 * NO_ADDRESS = "inboundNoAddress"; SAVE_ADDRESS_BOOK = "inboundShowSaveToAddressBook";
				 */
				DELIVERY_ADDRESSES = "inboundDeliveryAddresses";
				//	DELIVERY_METHODS = "inboundDeliveryMethods";
				//ADDRESS_FORM = "inboundAddressForm";
				try
				{
					setUserCheckoutCartToSession(ControllerConstants.Views.Fragments.UserCart.InBoundCart);
				}
				catch (final Exception e)
				{
					// YTODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else if (cartList.get(i).getName().equals(ControllerConstants.Views.Fragments.UserCart.OutBoundCart))
			{

				CART_DATA = "cartData";
				DELIVERY_ADDRESSES = "deliveryAddresses";
				//	DELIVERY_METHODS = "deliveryMethods";
				try
				{
					setUserCheckoutCartToSession(ControllerConstants.Views.Fragments.UserCart.OutBoundCart);
				}
				catch (final Exception e)
				{
					// YTODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			//getCheckoutFacade().setDeliveryAddressIfAvailable();


			final CartData cartData = getCheckoutFacade().getCheckoutCart();
			model.addAttribute(CART_DATA, cartData);
			/*
			 * model.addAttribute(NO_ADDRESS, Boolean.valueOf(getCheckoutFlowFacade().hasNoDeliveryAddress()));
			 * model.addAttribute(ADDRESS_FORM, new AddressForm()); model.addAttribute(SAVE_ADDRESS_BOOK, Boolean.TRUE);
			 */
		}



		return ControllerConstants.Views.Pages.MultiStepCheckout.AddPaymentMethodPage;
	}

	@RequestMapping(value =
	{ "/add" }, method = RequestMethod.POST)
	@RequireHardLogIn
	public String add(final Model model, @Valid final PaymentDetailsForm paymentDetailsForm, final BindingResult bindingResult)
			throws CMSItemNotFoundException
	{
		getPaymentDetailsValidator().validate(paymentDetailsForm, bindingResult);
		setupAddPaymentPage(model);


		final List<CartData> cartList = getUserCheckedOutCart();
		boolean n = false;

		for (int i = 0; i < cartList.size(); i++)
		{

			if (cartList.get(i).getName().equals(ControllerConstants.Views.Fragments.UserCart.InBoundCart))
			{

				PAYMENT_ID = "inboundPaymentId";
				CART_DATA = "inboundCartData";
				try
				{
					setUserCheckoutCartToSession(ControllerConstants.Views.Fragments.UserCart.InBoundCart);
				}
				catch (final Exception e)
				{
					// YTODO Auto-generated catch block
					e.printStackTrace();
				}
				n = true;
			}
			else if (cartList.get(i).getName().equals(ControllerConstants.Views.Fragments.UserCart.OutBoundCart))
			{
				PAYMENT_ID = "paymentId";
				CART_DATA = "cartData";
				try
				{
					setUserCheckoutCartToSession(ControllerConstants.Views.Fragments.UserCart.OutBoundCart);
				}
				catch (final Exception e)
				{
					// YTODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			final CartData cartData = getCheckoutFacade().getCheckoutCart();
			model.addAttribute(CART_DATA, cartData);

			if (bindingResult.hasErrors() && n == true)
			{

				GlobalMessages.addErrorMessage(model, "checkout.error.paymentethod.formentry.invalid");
				return ControllerConstants.Views.Pages.MultiStepCheckout.AddPaymentMethodPage;
			}
			if (!bindingResult.hasErrors())
			{
				final CCPaymentInfoData paymentInfoData = new CCPaymentInfoData();
				paymentInfoData.setId(paymentDetailsForm.getPaymentId());
				paymentInfoData.setCardType(paymentDetailsForm.getCardTypeCode());
				paymentInfoData.setAccountHolderName(paymentDetailsForm.getNameOnCard());
				paymentInfoData.setCardNumber(paymentDetailsForm.getCardNumber());
				paymentInfoData.setStartMonth(paymentDetailsForm.getStartMonth());
				paymentInfoData.setStartYear(paymentDetailsForm.getStartYear());
				paymentInfoData.setExpiryMonth(paymentDetailsForm.getExpiryMonth());
				paymentInfoData.setExpiryYear(paymentDetailsForm.getExpiryYear());
				if (Boolean.TRUE.equals(paymentDetailsForm.getSaveInAccount()) || getCheckoutCustomerStrategy().isAnonymousCheckout())
				{
					paymentInfoData.setSaved(true);
				}
				paymentInfoData.setIssueNumber(paymentDetailsForm.getIssueNumber());

				final AddressData addressData;
				if (Boolean.FALSE.equals(paymentDetailsForm.getNewBillingAddress()))
				{
					addressData = getCheckoutFacade().getCheckoutCart().getDeliveryAddress();
					if (addressData == null)
					{
						GlobalMessages.addErrorMessage(model,
								"checkout.multi.paymentMethod.createSubscription.billingAddress.noneSelectedMsg");
						return ControllerConstants.Views.Pages.MultiStepCheckout.AddPaymentMethodPage;
					}

					addressData.setBillingAddress(true); // mark this as billing address
				}
				else
				{
					final AddressForm addressForm = paymentDetailsForm.getBillingAddress();
					addressData = new AddressData();
					if (addressForm != null)
					{
						addressData.setId(addressForm.getAddressId());
						addressData.setTitleCode(addressForm.getTitleCode());
						addressData.setFirstName(addressForm.getFirstName());
						addressData.setLastName(addressForm.getLastName());
						addressData.setLine1(addressForm.getLine1());
						addressData.setLine2(addressForm.getLine2());
						addressData.setTown(addressForm.getTownCity());
						addressData.setPostalCode(addressForm.getPostcode());
						addressData.setCountry(getI18NFacade().getCountryForIsocode(addressForm.getCountryIso()));
						if (addressForm.getRegionIso() != null)
						{
							addressData.setRegion(getI18NFacade().getRegion(addressForm.getCountryIso(), addressForm.getRegionIso()));
						}

						addressData.setShippingAddress(Boolean.TRUE.equals(addressForm.getShippingAddress()));
						addressData.setBillingAddress(Boolean.TRUE.equals(addressForm.getBillingAddress()));
					}
				}

				getAddressVerificationFacade().verifyAddressData(addressData);
				paymentInfoData.setBillingAddress(addressData);

				final CCPaymentInfoData newPaymentSubscription = getCheckoutFacade().createPaymentSubscription(paymentInfoData);
				if (newPaymentSubscription != null && StringUtils.isNotBlank(newPaymentSubscription.getSubscriptionId()))
				{
					if (Boolean.TRUE.equals(paymentDetailsForm.getSaveInAccount())
							&& getUserFacade().getCCPaymentInfos(true).size() <= 1)
					{
						getUserFacade().setDefaultPaymentInfo(newPaymentSubscription);
					}
					getCheckoutFacade().setPaymentDetails(newPaymentSubscription.getId());
				}
				else
				{
					GlobalMessages.addErrorMessage(model, "checkout.multi.paymentMethod.createSubscription.failedMsg");
					return ControllerConstants.Views.Pages.MultiStepCheckout.AddPaymentMethodPage;
				}

				model.addAttribute(PAYMENT_ID, newPaymentSubscription.getId());
			}
		}

		setCheckoutStepLinksForModel(model, getCheckoutStep());

		return getCheckoutStep().nextStep();
	}


	@RequestMapping(value = "/remove", method = RequestMethod.POST)
	@RequireHardLogIn
	public String remove(@RequestParam(value = "paymentInfoId") final String paymentMethodId,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		getUserFacade().unlinkCCPaymentInfo(paymentMethodId);
		GlobalMessages.addFlashMessage(redirectAttributes, GlobalMessages.CONF_MESSAGES_HOLDER,
				"text.account.profile.paymentCart.removed");
		return getCheckoutStep().currentStep();
	}

	/**
	 * This method gets called when the "Use These Payment Details" button is clicked. It sets the selected payment
	 * method on the checkout facade and reloads the page highlighting the selected payment method.
	 *
	 * @param selectedPaymentMethodId
	 *           - the id of the payment method to use.
	 * @return - a URL to the page to load.
	 */
	@RequestMapping(value = "/choose", method = RequestMethod.GET)
	@RequireHardLogIn
	public String doSelectPaymentMethod(@RequestParam("selectedPaymentMethodId") final String selectedPaymentMethodId)
	{
		if (StringUtils.isNotBlank(selectedPaymentMethodId))
		{
			getCheckoutFacade().setPaymentDetails(selectedPaymentMethodId);
		}
		return getCheckoutStep().nextStep();
	}

	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}

	protected CardTypeData createCardTypeData(final String code, final String name)
	{
		final CardTypeData cardTypeData = new CardTypeData();
		cardTypeData.setCode(code);
		cardTypeData.setName(name);
		return cardTypeData;
	}

	protected void setupAddPaymentPage(final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute("metaRobots", "noindex,nofollow");
		model.addAttribute("hasNoPaymentInfo", Boolean.valueOf(getCheckoutFlowFacade().hasNoPaymentInfo()));
		prepareDataForPage(model);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.paymentMethod.breadcrumb"));
		final ContentPageModel contentPage = getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, contentPage);
		setUpMetaDataForContentPage(model, contentPage);
		setCheckoutStepLinksForModel(model, getCheckoutStep());
	}

	private static String CART_DATA = "cartData";

	private static String DELIVERY_ADDRESSES = "deliveryAddress";


	private static String SILENT_ORDER_FORM = "silentOrderPostForm";
	private static String SOP_PAYMENT_DETAILS_FORM = "sopPaymentDetailsForm";

	private static String PAYMENT_INFOS = "paymentInfos";

	private static String SOP_CARD_TYPES = "sopCardTypes";

	private static String REGIONS = "regions";

	private static String COUNTRY = "country";




	protected void setupSilentOrderPostPage(final SopPaymentDetailsForm sopPaymentDetailsForm, final Model model,
			final String cartName)
	{


		/*
		 * final CartData cartData = getCheckoutFacade().getCheckoutCart(); model.addAttribute("silentOrderPostForm", new
		 * PaymentDetailsForm()); model.addAttribute("cartData", cartData); model.addAttribute("deliveryAddress",
		 * cartData.getDeliveryAddress()); model.addAttribute("sopPaymentDetailsForm", sopPaymentDetailsForm);
		 * model.addAttribute("paymentInfos", getUserFacade().getCCPaymentInfos(true)); model.addAttribute("sopCardTypes",
		 * getSopCardTypes());
		 */


		/*
		 * if (StringUtils.isNotBlank(sopPaymentDetailsForm.getBillTo_country())) { model.addAttribute("regions",
		 * getI18NFacade().getRegionsForCountryIso(sopPaymentDetailsForm.getBillTo_country()));
		 * model.addAttribute("country", sopPaymentDetailsForm.getBillTo_country()); }
		 */
		//	final List<CartData> cartList = getUserCheckedOutCart();
		//	for (int i = 0; i < cartList.size(); i++)
		//	{

		if (cartName.equals(ControllerConstants.Views.Fragments.UserCart.InBoundCart))
		{
			//	SILENT_ORDER_FORM = "inboundSilentOrderPostForm";
			DELIVERY_ADDRESSES = "inboundDeliveryAddress";
			CART_DATA = "inboundCartData";
			//	PAYMENT_INFOS = "inboundPaymentInfos";
			//	SOP_PAYMENT_DETAILS_FORM = "inboundSopPaymentDetailsForm";
			//	SOP_CARD_TYPES = "sopCardTypes";
			//	REGIONS = "inboundRegions";
			//	COUNTRY = "inboundCountry";

			try
			{
				setUserCheckoutCartToSession(ControllerConstants.Views.Fragments.UserCart.InBoundCart);
			}
			catch (final Exception e)
			{
				// YTODO Auto-generated catch block
				e.printStackTrace();
			}
			try
			{
				final PaymentData silentOrderPageData = getPaymentFacade().beginSopCreateSubscription("/checkout/multi/sop/response",
						"/integration/merchant_callback");
				model.addAttribute("silentOrderPageData", silentOrderPageData);
				sopPaymentDetailsForm.setParameters(silentOrderPageData.getParameters());
				model.addAttribute("paymentFormUrl", silentOrderPageData.getPostUrl());
			}
			catch (final IllegalArgumentException e)
			{
				model.addAttribute("paymentFormUrl", "");
				model.addAttribute("silentOrderPageData", null);
				LOG.warn("Failed to set up silent order post page " + e.getMessage());
				GlobalMessages.addErrorMessage(model, "checkout.multi.sop.globalError");
			}


		}
		else if (cartName.equals(ControllerConstants.Views.Fragments.UserCart.OutBoundCart))
		{
			SILENT_ORDER_FORM = "silentOrderPostForm";
			DELIVERY_ADDRESSES = "deliveryAddress";
			CART_DATA = "cartData";
			PAYMENT_INFOS = "paymentInfos";
			SOP_PAYMENT_DETAILS_FORM = "sopPaymentDetailsForm";
			SOP_CARD_TYPES = "sopCardTypes";
			REGIONS = "regions";
			COUNTRY = "country";
			try
			{
				setUserCheckoutCartToSession(ControllerConstants.Views.Fragments.UserCart.OutBoundCart);
			}
			catch (final Exception e)
			{
				// YTODO Auto-generated catch block
				e.printStackTrace();
			}
			try
			{
				final PaymentData silentOrderPageData = getPaymentFacade().beginSopCreateSubscription("/checkout/multi/sop/response",
						"/integration/merchant_callback");
				model.addAttribute("silentOrderPageData", silentOrderPageData);
				sopPaymentDetailsForm.setParameters(silentOrderPageData.getParameters());
				model.addAttribute("paymentFormUrl", silentOrderPageData.getPostUrl());
			}
			catch (final IllegalArgumentException e)
			{
				model.addAttribute("paymentFormUrl", "");
				model.addAttribute("silentOrderPageData", null);
				LOG.warn("Failed to set up silent order post page " + e.getMessage());
				GlobalMessages.addErrorMessage(model, "checkout.multi.sop.globalError");
			}

		}

		final CartData cartData = getCheckoutFacade().getCheckoutCart();
		model.addAttribute(SILENT_ORDER_FORM, new PaymentDetailsForm());
		model.addAttribute(CART_DATA, cartData);
		model.addAttribute(DELIVERY_ADDRESSES, cartData.getDeliveryAddress());
		model.addAttribute(SOP_PAYMENT_DETAILS_FORM, sopPaymentDetailsForm);
		model.addAttribute(PAYMENT_INFOS, getUserFacade().getCCPaymentInfos(true));
		model.addAttribute(SOP_CARD_TYPES, getSopCardTypes());
		if (StringUtils.isNotBlank(sopPaymentDetailsForm.getBillTo_country()))
		{
			model.addAttribute(REGIONS, getI18NFacade().getRegionsForCountryIso(sopPaymentDetailsForm.getBillTo_country()));
			model.addAttribute(COUNTRY, sopPaymentDetailsForm.getBillTo_country());
		}

		//}
	}

	protected Collection<CardTypeData> getSopCardTypes()
	{
		final Collection<CardTypeData> sopCardTypes = new ArrayList<CardTypeData>();

		final List<CardTypeData> supportedCardTypes = getCheckoutFacade().getSupportedCardTypes();
		for (final CardTypeData supportedCardType : supportedCardTypes)
		{
			// Add credit cards for all supported cards that have mappings for cybersource SOP
			if (cybersourceSopCardTypes.containsKey(supportedCardType.getCode()))
			{
				sopCardTypes.add(createCardTypeData(cybersourceSopCardTypes.get(supportedCardType.getCode()),
						supportedCardType.getName()));
			}
		}
		return sopCardTypes;
	}

	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(PAYMENT_METHOD);
	}

	static
	{
		// Map hybris card type to Cybersource SOP credit card
		cybersourceSopCardTypes.put("visa", "001");
		cybersourceSopCardTypes.put("master", "002");
		cybersourceSopCardTypes.put("amex", "003");
		cybersourceSopCardTypes.put("diners", "005");
		cybersourceSopCardTypes.put("maestro", "024");
	}

}
