/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.sonata.travelretail.storefront.controllers.pages.checkout.steps;


import de.hybris.platform.acceleratorfacades.order.AcceleratorCheckoutFacade;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.PreValidateCheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.CheckoutStep;
import de.hybris.platform.acceleratorstorefrontcommons.checkout.steps.validation.ValidationResults;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.checkout.steps.AbstractCheckoutStepController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.address.data.AddressVerificationResult;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.commerceservices.address.AddressVerificationDecision;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sonata.travelretail.storefront.controllers.ControllerConstants;
import com.sonata.travelretail.storefront.forms.AddressForm;
import com.sonata.travelretail.storefront.forms.validation.AddressValidator;


@Controller
@RequestMapping(value = "/checkout/multi/delivery-address")
public class DeliveryAddressCheckoutStepController extends AbstractCheckoutStepController
{
	@Resource(name = "cartService")
	@Autowired
	private CartService cartService;

	@Resource(name = "acceleratorCheckoutFacade")
	private AcceleratorCheckoutFacade checkoutFacade;

	@Resource(name = "storeFrontAddressValidator")
	private AddressValidator addressValidator;

	/**
	 * @return the checkoutFacade
	 */
	@Override
	public AcceleratorCheckoutFacade getCheckoutFacade()
	{
		return checkoutFacade;
	}

	/**
	 * @param checkoutFacade
	 *           the checkoutFacade to set
	 */
	public void setCheckoutFacade(final AcceleratorCheckoutFacade checkoutFacade)
	{
		this.checkoutFacade = checkoutFacade;
	}

	/**
	 * @return the cartService
	 */
	public CartService getCartService()
	{
		return cartService;
	}

	/**
	 * @param cartService
	 *           the cartService to set
	 */
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}


	private final static String DELIVERY_ADDRESS = "delivery-address";
	private static String CART_DATA = "cartData";
	private static String NO_ADDRESS = "noAddress";
	private static String ADDRESS_FORM = "addressForm";
	private static String SAVE_ADDRESS_BOOK = "showSaveToAddressBook";
	private static String DELIVERY_ADDRESSES = "deliveryAddresses";
	private static String REGIONS = "regions";
	private static String COUNTRY = "country";
	private static String EDIT = "edit";



	//"regions", getI18NFacade().getRegionsForCountryIso(addressForm.getCountryIso()));
	//model.addAttribute(""
	//, cartData);
	//model.addAttribute("deliveryAddresses", getDeliveryAddresses(cartData.getDeliveryAddress()));
	//model.addAttribute("noAddress", Boolean.valueOf(getCheckoutFlowFacade().hasNoDeliveryAddress()));
	//model.addAttribute("addressForm", new AddressForm());
	//model.addAttribute("showSaveToAddressBook", Boolean.TRUE);

	@Override
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	@RequireHardLogIn
	@PreValidateCheckoutStep(checkoutStep = DELIVERY_ADDRESS)
	public String enterStep(final Model model, final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{

		final List<CartData> cartList = getUserCheckedOutCart();

		for (int i = 0; i < cartList.size(); i++)
		{

			if (cartList.get(i).getName().equals(ControllerConstants.Views.Fragments.UserCart.InBoundCart))
			{

				CART_DATA = "inboundCartData";
				//NO_ADDRESS = "inboundNoAddress";
				NO_ADDRESS = "noAddress";
				//	SAVE_ADDRESS_BOOK = "inboundShowSaveToAddressBook";
				SAVE_ADDRESS_BOOK = "showSaveToAddressBook";
				//DELIVERY_ADDRESSES = "inboundDeliveryAddresses";
				//DELIVERY_ADDRESSES = "deliveryAddresses";
				//ADDRESS_FORM = "inboundAddressForm";
				ADDRESS_FORM = "addressForm";
				model.addAttribute("inboundCartExists", Boolean.FALSE);
				if (!cartList.get(i).getEntries().isEmpty())
				{
					model.addAttribute("inboundCartExists", Boolean.TRUE);

					//	model.addAttribute("inboundDeliveryAddresses", getDeliveryAddresses(cartData.getDeliveryAddress()));
					try
					{
						setUserCheckoutCartToSession(ControllerConstants.Views.Fragments.UserCart.InBoundCart);
					}
					catch (final Exception e)
					{
						// YTODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			else if (cartList.get(i).getName().equals(ControllerConstants.Views.Fragments.UserCart.OutBoundCart))
			{

				CART_DATA = "cartData";
				NO_ADDRESS = "noAddress";
				SAVE_ADDRESS_BOOK = "showSaveToAddressBook";
				DELIVERY_ADDRESSES = "deliveryAddresses";
				ADDRESS_FORM = "addressForm";
				model.addAttribute("outboundCartExists", Boolean.FALSE);
				if (!cartList.get(i).getEntries().isEmpty())
				{
					model.addAttribute("outboundCartExists", Boolean.TRUE);

					try
					{
						setUserCheckoutCartToSession(ControllerConstants.Views.Fragments.UserCart.OutBoundCart);
					}
					catch (final Exception e)
					{
						// YTODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}

			getCheckoutFacade().setDeliveryAddressIfAvailable();


			final CartData cartData = getCheckoutFacade().getCheckoutCart();
			model.addAttribute(CART_DATA, cartData);
			model.addAttribute(DELIVERY_ADDRESSES, getDeliveryAddresses(cartData.getDeliveryAddress()));
			model.addAttribute(NO_ADDRESS, Boolean.valueOf(getCheckoutFlowFacade().hasNoDeliveryAddress()));
			model.addAttribute(ADDRESS_FORM, new AddressForm());
			model.addAttribute(SAVE_ADDRESS_BOOK, Boolean.TRUE);

		}
		prepareDataForPage(model);
		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.deliveryAddress.breadcrumb"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		setCheckoutStepLinksForModel(model, getCheckoutStep());

		return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;


	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@RequireHardLogIn
	public String add(final AddressForm addressForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		final List<CartData> cartList = getUserCheckedOutCart();
		boolean n = false;
		for (int i = 0; i < cartList.size(); i++)
		{
			final AddressData newAddress = new AddressData();
			if (cartList.get(i).getName().equals(ControllerConstants.Views.Fragments.UserCart.InBoundCart))
			{
				model.addAttribute("inboundCartExists", Boolean.FALSE);
				if (!cartList.get(i).getEntries().isEmpty())
				{
					model.addAttribute("inboundCartExists", Boolean.TRUE);
					CART_DATA = "inboundCartData";
					NO_ADDRESS = "inboundNoAddress";
					//	SAVE_ADDRESS_BOOK = "inboundShowSaveToAddressBook";
					SAVE_ADDRESS_BOOK = "showSaveToAddressBook";
					//	DELIVERY_ADDRESSES = "inboundDeliveryAddresses";
					DELIVERY_ADDRESSES = "deliveryAddresses";
					//REGIONS = "inboundRegions";
					//	COUNTRY = "inboundCountry";
					REGIONS = "regions";
					COUNTRY = "country";

					newAddress.setTitleCode(addressForm.getInboundTitleCode());
					newAddress.setFirstName(addressForm.getInboundFirstName());
					newAddress.setLastName(addressForm.getInboundLastName());
					newAddress.setLine1(addressForm.getInboundLine1());
					newAddress.setLine2(addressForm.getInboundLine2());
					newAddress.setTown(addressForm.getInboundTownCity());
					newAddress.setPostalCode(addressForm.getInboundPostcode());
					newAddress.setBillingAddress(false);
					newAddress.setShippingAddress(true);
					try
					{
						setUserCheckoutCartToSession(ControllerConstants.Views.Fragments.UserCart.InBoundCart);
					}
					catch (final Exception e)
					{
						// YTODO Auto-generated catch block
						e.printStackTrace();
					}
					if (addressForm.getInboundSaveInAddressBook() != null)
					{
						newAddress.setVisibleInAddressBook(addressForm.getInboundSaveInAddressBook().booleanValue());
						if (addressForm.getInboundSaveInAddressBook().booleanValue() && getUserFacade().isAddressBookEmpty())
						{
							newAddress.setDefaultAddress(true);
						}
					}
					if (addressForm.getInboundCountryIso() != null)
					{
						final CountryData countryData = getI18NFacade().getCountryForIsocode(addressForm.getInboundCountryIso());
						newAddress.setCountry(countryData);
					}
					addressValidator.validateInbound(addressForm, bindingResult);
					if (StringUtils.isNotBlank(addressForm.getInboundCountryIso()))
					{
						model.addAttribute(REGIONS, getI18NFacade().getRegionsForCountryIso(addressForm.getInboundCountryIso()));
						model.addAttribute(COUNTRY, addressForm.getInboundCountryIso());
					}
				}
				n = true;

			}
			else if (cartList.get(i).getName().equals(ControllerConstants.Views.Fragments.UserCart.OutBoundCart))
			{
				model.addAttribute("outboundCartExists", Boolean.FALSE);
				if (!cartList.get(i).getEntries().isEmpty())
				{
					model.addAttribute("outboundCartExists", Boolean.TRUE);
					CART_DATA = "cartData";
					NO_ADDRESS = "noAddress";
					SAVE_ADDRESS_BOOK = "showSaveToAddressBook";
					DELIVERY_ADDRESSES = "deliveryAddresses";
					REGIONS = "regions";
					COUNTRY = "country";

					newAddress.setTitleCode(addressForm.getTitleCode());
					newAddress.setFirstName(addressForm.getFirstName());
					newAddress.setLastName(addressForm.getLastName());
					newAddress.setLine1(addressForm.getLine1());
					newAddress.setLine2(addressForm.getLine2());
					newAddress.setTown(addressForm.getTownCity());
					newAddress.setPostalCode(addressForm.getPostcode());
					newAddress.setBillingAddress(false);
					newAddress.setShippingAddress(true);
					try
					{
						setUserCheckoutCartToSession(ControllerConstants.Views.Fragments.UserCart.OutBoundCart);
					}
					catch (final Exception e)
					{
						// YTODO Auto-generated catch block
						e.printStackTrace();
					}
					if (addressForm.getSaveInAddressBook() != null)
					{
						newAddress.setVisibleInAddressBook(addressForm.getSaveInAddressBook().booleanValue());
						if (addressForm.getSaveInAddressBook().booleanValue() && getUserFacade().isAddressBookEmpty())
						{
							newAddress.setDefaultAddress(true);
						}
					}
					if (addressForm.getCountryIso() != null)
					{
						final CountryData countryData = getI18NFacade().getCountryForIsocode(addressForm.getCountryIso());
						newAddress.setCountry(countryData);
					}
					if (StringUtils.isNotBlank(addressForm.getCountryIso()))
					{
						model.addAttribute(REGIONS, getI18NFacade().getRegionsForCountryIso(addressForm.getCountryIso()));
						model.addAttribute(COUNTRY, addressForm.getCountryIso());
					}
					addressValidator.validate(addressForm, bindingResult);
				}
			}

			if (!cartList.get(i).getEntries().isEmpty())
			{
				final CartData cartData = getCheckoutFacade().getCheckoutCart();
				model.addAttribute(CART_DATA, cartData);
				model.addAttribute(DELIVERY_ADDRESSES, getDeliveryAddresses(cartData.getDeliveryAddress()));
				this.prepareDataForPage(model);


				model.addAttribute("NO_ADDRESS", Boolean.valueOf(getCheckoutFlowFacade().hasNoDeliveryAddress()));
				model.addAttribute("SAVE_ADDRESS_BOOK", Boolean.TRUE);

			}
			if (bindingResult.hasErrors() && (n == true))
			{


				for (int j = 0; j < cartList.size(); j++)
				{

					if (cartList.get(j).getName().equals(ControllerConstants.Views.Fragments.UserCart.InBoundCart))
					{
						model.addAttribute("inboundCartExists", Boolean.FALSE);
						if (!cartList.get(j).getEntries().isEmpty())
						{
							model.addAttribute("inboundCartExists", Boolean.TRUE);
							//	model.addAttribute("inboundDeliveryAddresses", getDeliveryAddresses(cartData.getDeliveryAddress()));
							try
							{
								setUserCheckoutCartToSession(ControllerConstants.Views.Fragments.UserCart.InBoundCart);
							}
							catch (final Exception e)
							{
								// YTODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
					else if (cartList.get(j).getName().equals(ControllerConstants.Views.Fragments.UserCart.OutBoundCart))
					{
						model.addAttribute("outboundCartExists", Boolean.FALSE);
						if (!cartList.get(j).getEntries().isEmpty())
						{
							model.addAttribute("outboundCartExists", Boolean.TRUE);
							try
							{
								setUserCheckoutCartToSession(ControllerConstants.Views.Fragments.UserCart.OutBoundCart);
							}
							catch (final Exception e)
							{
								// YTODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}

				}

				prepareDataForPage(model);
				GlobalMessages.addErrorMessage(model, "address.error.formentry.invalid");
				storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
				setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
				setCheckoutStepLinksForModel(model, getCheckoutStep());
				return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
			}

			if (!cartList.get(i).getEntries().isEmpty())
			{

				if (addressForm.getRegionIso() != null && !StringUtils.isEmpty(addressForm.getRegionIso()))
				{
					final RegionData regionData = getI18NFacade().getRegion(addressForm.getCountryIso(), addressForm.getRegionIso());
					newAddress.setRegion(regionData);
				}


				else if (getCheckoutCustomerStrategy().isAnonymousCheckout())
				{
					newAddress.setDefaultAddress(true);
					newAddress.setVisibleInAddressBook(true);
				}

				//final Verify the address data.
				/*
				 * final AddressVerificationResult<AddressVerificationDecision> verificationResult =
				 * getAddressVerificationFacade() .verifyAddressData(newAddress); final boolean addressRequiresReview =
				 * getAddressVerificationResultHandler().handleResult(verificationResult, newAddress, model, redirectModel,
				 * bindingResult, getAddressVerificationFacade().isCustomerAllowedToIgnoreAddressSuggestions(),
				 * "checkout.multi.address.updated");
				 * 
				 * if (addressRequiresReview) { storeCmsPageInModel(model,
				 * getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL)); setUpMetaDataForContentPage(model,
				 * getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL)); return
				 * ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage; }
				 */

				getUserFacade().addAddress(newAddress);

				final AddressData previousSelectedAddress = getCheckoutFacade().getCheckoutCart().getDeliveryAddress();
				// Set the new address as the selected checkout delivery address
				getCheckoutFacade().setDeliveryAddress(newAddress);
				if (previousSelectedAddress != null && !previousSelectedAddress.isVisibleInAddressBook())
				{ // temporary address should be removed
					getUserFacade().removeAddress(previousSelectedAddress);
				}



				// Set the new address as the selected checkout delivery address
				getCheckoutFacade().setDeliveryAddress(newAddress);
			}
		}

		return getCheckoutStep().nextStep();
	}

	public List<CartData> getUserCheckedOutCart()
	{

		final List<CartData> cartDataList = new ArrayList<CartData>();
		String checkoutType = "";
		if (isCombinedCheckout())
		{
			for (int i = 0; i < 2; i++)
			{
				switch (i)
				{
					case 0:
						checkoutType = ControllerConstants.Views.Fragments.UserCart.OutBoundCart;
						break;
					case 1:
						checkoutType = ControllerConstants.Views.Fragments.UserCart.InBoundCart;
						break;
				}

				try
				{
					setUserCheckoutCartToSession(checkoutType);
				}
				catch (final Exception e)
				{
					// YTODO Auto-generated catch block
					e.printStackTrace();
				}
				cartDataList.add(getCartFacade().getSessionCart());
			}
		}
		else
		{

			checkoutType = getSessionService().getAttribute("checkoutType");
			try
			{
				setUserCheckoutCartToSession(checkoutType);
			}
			catch (final Exception e)
			{
				// YTODO Auto-generated catch block
				e.printStackTrace();
			}
			cartDataList.add(getCartFacade().getSessionCart());
		}
		return cartDataList;
	}

	protected boolean isCombinedCheckout()
	{
		final String checkoutType = getSessionService().getAttribute("checkoutType");
		final boolean isCombinedCheckout = checkoutType.equals(ControllerConstants.Views.Fragments.UserCart.CombinedCart);
		final List<CartData> cartDataList = getCartFacade().getCartsForCurrentUser();
		if (cartDataList.size() == 1)
		{
			cartDataList.add(getCartFacade().getSessionCart());
		}
		if (isCombinedCheckout && CollectionUtils.isNotEmpty(cartDataList) && cartDataList.size() >= 2)
		{
			return true;
		}
		return false;
	}

	protected void setUserCheckoutCartToSession(final String checkoutType) throws Exception
	{
		//final String checkoutType = getSessionService().getAttribute("checkoutType");

		if (StringUtils.isNotEmpty(checkoutType))
		{
			//final List<CartData> cartList = getCartFacade().getCartsForCurrentUser();
			CartModel sessionCartModel = getCartService().getSessionCart();

			if (!checkoutType.equals(sessionCartModel.getName()))
			{
				//				final List<CartModel> cartModelList = getCommerceCartService().getCartsForSiteAndUser(
				//						baseSiteService.getCurrentBaseSite(), getUserService().getCurrentUser());

				final List<CartModel> cartModelList = getSessionService().getAttribute("Users_Cart_List");

				if (org.apache.commons.collections.CollectionUtils.isNotEmpty(cartModelList)
						&& StringUtils.isNotEmpty(sessionCartModel.getName()))
				{
					CartModel cartModel = getMatchingCart(cartModelList, checkoutType);
					if (cartModelList.size() == 1)
					{
						cartModel = cartModelList.get(0);
					}
					if (cartModel != null)
					{
						cartService.setSessionCart(cartModel);
					}
					else
					{
						throw new Exception("Could not get Associated cart");
					}
				}
				sessionCartModel = cartService.getSessionCart();
				System.out.println("new cart" + sessionCartModel.getCode());
			}
		}
	}

	public CartModel getMatchingCart(final List<CartModel> cartModelList, final String cartName)
	{
		for (final CartModel cartModel : cartModelList)
		{
			if (cartModel.getName().equals(cartName))
			{
				return cartModel;
			}
		}

		return null;

	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	@RequireHardLogIn
	public String editAddressForm(@RequestParam("editAddressCode") final String editAddressCode, final Model model,
			final RedirectAttributes redirectAttributes) throws CMSItemNotFoundException
	{
		final ValidationResults validationResults = getCheckoutStep().validate(redirectAttributes);
		if (getCheckoutStep().checkIfValidationErrors(validationResults))
		{
			return getCheckoutStep().onValidation(validationResults);
		}


		final List<CartData> cartList = getUserCheckedOutCart();

		for (int i = 0; i < cartList.size(); i++)
		{

			if (cartList.get(i).getName().equals(ControllerConstants.Views.Fragments.UserCart.InBoundCart))
			{

				CART_DATA = "inboundCartData";
				NO_ADDRESS = "inboundNoAddress";
				SAVE_ADDRESS_BOOK = "inboundShowSaveToAddressBook";
				DELIVERY_ADDRESSES = "inboundDeliveryAddresses";
				REGIONS = "inboundRegions";
				COUNTRY = "inboundCountry";
				EDIT = "inboundEdit";
				ADDRESS_FORM = "inboundAddressForm";
			}
			else if (cartList.get(i).getName().equals(ControllerConstants.Views.Fragments.UserCart.OutBoundCart))
			{

				CART_DATA = "cartData";
				NO_ADDRESS = "noAddress";
				SAVE_ADDRESS_BOOK = "showSaveToAddressBook";
				DELIVERY_ADDRESSES = "deliveryAddresses";
				REGIONS = "regions";
				COUNTRY = "country";
				EDIT = "edit";
				ADDRESS_FORM = "addressForm";
			}



			AddressData addressData = null;
			if (StringUtils.isNotEmpty(editAddressCode))
			{
				addressData = getCheckoutFacade().getDeliveryAddressForCode(editAddressCode);
			}

			final AddressForm addressForm = new AddressForm();
			final boolean hasAddressData = addressData != null;
			if (hasAddressData)
			{
				addressForm.setAddressId(addressData.getId());
				addressForm.setTitleCode(addressData.getTitleCode());
				addressForm.setFirstName(addressData.getFirstName());
				addressForm.setLastName(addressData.getLastName());
				addressForm.setLine1(addressData.getLine1());
				addressForm.setLine2(addressData.getLine2());
				addressForm.setTownCity(addressData.getTown());
				addressForm.setPostcode(addressData.getPostalCode());
				addressForm.setCountryIso(addressData.getCountry().getIsocode());
				addressForm.setSaveInAddressBook(Boolean.valueOf(addressData.isVisibleInAddressBook()));
				addressForm.setShippingAddress(Boolean.valueOf(addressData.isShippingAddress()));
				addressForm.setBillingAddress(Boolean.valueOf(addressData.isBillingAddress()));
				if (addressData.getRegion() != null && !StringUtils.isEmpty(addressData.getRegion().getIsocode()))
				{
					addressForm.setRegionIso(addressData.getRegion().getIsocode());
				}
			}

			final CartData cartData = getCheckoutFacade().getCheckoutCart();
			model.addAttribute(CART_DATA, cartData);
			model.addAttribute(DELIVERY_ADDRESSES, getDeliveryAddresses(cartData.getDeliveryAddress()));
			if (StringUtils.isNotBlank(addressForm.getCountryIso()))
			{
				model.addAttribute(REGIONS, getI18NFacade().getRegionsForCountryIso(addressForm.getCountryIso()));
				model.addAttribute(COUNTRY, addressForm.getCountryIso());
			}
			model.addAttribute(NO_ADDRESS, Boolean.valueOf(getCheckoutFlowFacade().hasNoDeliveryAddress()));
			model.addAttribute(EDIT, Boolean.valueOf(hasAddressData));
			model.addAttribute(ADDRESS_FORM, addressForm);
			if (addressData != null)
			{
				model.addAttribute(SAVE_ADDRESS_BOOK, Boolean.valueOf(!addressData.isVisibleInAddressBook()));
			}

		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY,
				getResourceBreadcrumbBuilder().getBreadcrumbs("checkout.multi.deliveryAddress.breadcrumb"));
		model.addAttribute("metaRobots", "noindex,nofollow");
		setCheckoutStepLinksForModel(model, getCheckoutStep());

		return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	@RequireHardLogIn
	public String edit(final AddressForm addressForm, final BindingResult bindingResult, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		addressValidator.validate(addressForm, bindingResult);
		if (StringUtils.isNotBlank(addressForm.getCountryIso()))
		{
			model.addAttribute("regions", getI18NFacade().getRegionsForCountryIso(addressForm.getCountryIso()));
			model.addAttribute("country", addressForm.getCountryIso());
		}
		model.addAttribute("noAddress", Boolean.valueOf(getCheckoutFlowFacade().hasNoDeliveryAddress()));

		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "address.error.formentry.invalid");
			storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
			return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
		}

		final AddressData newAddress = new AddressData();
		newAddress.setId(addressForm.getAddressId());
		newAddress.setTitleCode(addressForm.getTitleCode());
		newAddress.setFirstName(addressForm.getFirstName());
		newAddress.setLastName(addressForm.getLastName());
		newAddress.setLine1(addressForm.getLine1());
		newAddress.setLine2(addressForm.getLine2());
		newAddress.setTown(addressForm.getTownCity());
		newAddress.setPostalCode(addressForm.getPostcode());
		newAddress.setBillingAddress(false);
		newAddress.setShippingAddress(true);
		if (addressForm.getCountryIso() != null)
		{
			final CountryData countryData = getI18NFacade().getCountryForIsocode(addressForm.getCountryIso());
			newAddress.setCountry(countryData);
		}
		if (addressForm.getRegionIso() != null && !StringUtils.isEmpty(addressForm.getRegionIso()))
		{
			final RegionData regionData = getI18NFacade().getRegion(addressForm.getCountryIso(), addressForm.getRegionIso());
			newAddress.setRegion(regionData);
		}

		if (addressForm.getSaveInAddressBook() == null)
		{
			newAddress.setVisibleInAddressBook(true);
		}
		else
		{
			newAddress.setVisibleInAddressBook(Boolean.TRUE.equals(addressForm.getSaveInAddressBook()));
		}

		newAddress.setDefaultAddress(getUserFacade().isAddressBookEmpty() || getUserFacade().getAddressBook().size() == 1
				|| Boolean.TRUE.equals(addressForm.getDefaultAddress()));

		// Verify the address data.
		final AddressVerificationResult<AddressVerificationDecision> verificationResult = getAddressVerificationFacade()
				.verifyAddressData(newAddress);
		final boolean addressRequiresReview = getAddressVerificationResultHandler().handleResult(verificationResult, newAddress,
				model, redirectModel, bindingResult, getAddressVerificationFacade().isCustomerAllowedToIgnoreAddressSuggestions(),
				"checkout.multi.address.updated");

		if (addressRequiresReview)
		{
			if (StringUtils.isNotBlank(addressForm.getCountryIso()))
			{
				model.addAttribute("regions", getI18NFacade().getRegionsForCountryIso(addressForm.getCountryIso()));
				model.addAttribute("country", addressForm.getCountryIso());
			}
			storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));

			if (StringUtils.isNotEmpty(addressForm.getAddressId()))
			{
				final AddressData addressData = getCheckoutFacade().getDeliveryAddressForCode(addressForm.getAddressId());
				if (addressData != null)
				{
					model.addAttribute("showSaveToAddressBook", Boolean.valueOf(!addressData.isVisibleInAddressBook()));
					model.addAttribute("edit", Boolean.TRUE);
				}
			}

			return ControllerConstants.Views.Pages.MultiStepCheckout.AddEditDeliveryAddressPage;
		}

		getUserFacade().editAddress(newAddress);
		getCheckoutFacade().setDeliveryModeIfAvailable();
		getCheckoutFacade().setDeliveryAddress(newAddress);

		return getCheckoutStep().nextStep();
	}

	@RequestMapping(value = "/remove", method =
	{ RequestMethod.GET, RequestMethod.POST })
	@RequireHardLogIn
	public String removeAddress(@RequestParam("addressCode") final String addressCode, final RedirectAttributes redirectModel,
			final Model model) throws CMSItemNotFoundException
	{
		final AddressData addressData = new AddressData();
		addressData.setId(addressCode);
		getUserFacade().removeAddress(addressData);
		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "account.confirmation.address.removed");
		storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_CHECKOUT_SUMMARY_CMS_PAGE_LABEL));
		model.addAttribute("addressForm", new AddressForm());

		final List<CartData> cartList = getUserCheckedOutCart();

		for (int i = 0; i < cartList.size(); i++)
		{

			if (cartList.get(i).getName().equals(ControllerConstants.Views.Fragments.UserCart.InBoundCart))
			{

				DELIVERY_ADDRESSES = "inboundDeliveryAddresses";
			}
			else if (cartList.get(i).getName().equals(ControllerConstants.Views.Fragments.UserCart.OutBoundCart))
			{

				DELIVERY_ADDRESSES = "deliveryAddresses";
			}

			model.addAttribute(DELIVERY_ADDRESSES, getDeliveryAddresses(cartList.get(i).getDeliveryAddress()));
		}


		return getCheckoutStep().currentStep();
	}

	@RequestMapping(value = "/select", method = RequestMethod.POST)
	@RequireHardLogIn
	public String doSelectSuggestedAddress(final AddressForm addressForm, final RedirectAttributes redirectModel)
	{
		final Set<String> resolveCountryRegions = org.springframework.util.StringUtils.commaDelimitedListToSet(Config
				.getParameter("resolve.country.regions"));

		final AddressData selectedAddress = new AddressData();
		selectedAddress.setId(addressForm.getAddressId());
		selectedAddress.setTitleCode(addressForm.getTitleCode());
		selectedAddress.setFirstName(addressForm.getFirstName());
		selectedAddress.setLastName(addressForm.getLastName());
		selectedAddress.setLine1(addressForm.getLine1());
		selectedAddress.setLine2(addressForm.getLine2());
		selectedAddress.setTown(addressForm.getTownCity());
		selectedAddress.setPostalCode(addressForm.getPostcode());
		selectedAddress.setBillingAddress(false);
		selectedAddress.setShippingAddress(true);
		final CountryData countryData = getI18NFacade().getCountryForIsocode(addressForm.getCountryIso());
		selectedAddress.setCountry(countryData);

		if (resolveCountryRegions.contains(countryData.getIsocode()) && addressForm.getRegionIso() != null
				&& !StringUtils.isEmpty(addressForm.getRegionIso()))
		{
			final RegionData regionData = getI18NFacade().getRegion(addressForm.getCountryIso(), addressForm.getRegionIso());
			selectedAddress.setRegion(regionData);
		}

		if (addressForm.getSaveInAddressBook() != null)
		{
			selectedAddress.setVisibleInAddressBook(addressForm.getSaveInAddressBook().booleanValue());
		}

		if (Boolean.TRUE.equals(addressForm.getEditAddress()))
		{
			getUserFacade().editAddress(selectedAddress);
		}
		else
		{
			getUserFacade().addAddress(selectedAddress);
		}

		final AddressData previousSelectedAddress = getCheckoutFacade().getCheckoutCart().getDeliveryAddress();
		// Set the new address as the selected checkout delivery address
		getCheckoutFacade().setDeliveryAddress(selectedAddress);
		if (previousSelectedAddress != null && !previousSelectedAddress.isVisibleInAddressBook())
		{ // temporary address should be removed
			getUserFacade().removeAddress(previousSelectedAddress);
		}

		GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "checkout.multi.address.added");

		return getCheckoutStep().nextStep();
	}


	/**
	 * This method gets called when the "Use this Address" button is clicked. It sets the selected delivery address on
	 * the checkout facade - if it has changed, and reloads the page highlighting the selected delivery address.
	 *
	 * @param selectedAddressCode
	 *           - the id of the delivery address.
	 *
	 * @return - a URL to the page to load.
	 */

	@ResponseBody
	@RequestMapping(value = "/select", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@RequireHardLogIn
	public Map<String, AddressForm> doSelectDeliveryAddress(@RequestParam("selectedAddressCode") final String selectedAddressCode,
			@RequestParam("cartType") final String cartType, final RedirectAttributes redirectAttributes, final Model model)
			throws CMSItemNotFoundException
	{
		final ValidationResults validationResults = getCheckoutStep().validate(redirectAttributes);
		final Map<String, AddressForm> map = new HashMap<String, AddressForm>();
		if (getCheckoutStep().checkIfValidationErrors(validationResults))
		{
			return map;
		}
		if (StringUtils.isNotBlank(selectedAddressCode))
		{
			final AddressData selectedAddressData = getCheckoutFacade().getDeliveryAddressForCode(selectedAddressCode);
			final boolean hasSelectedAddressData = selectedAddressData != null;
			if (hasSelectedAddressData)
			{
				final AddressData cartCheckoutDeliveryAddress = getCheckoutFacade().getCheckoutCart().getDeliveryAddress();
				if (isAddressIdChanged(cartCheckoutDeliveryAddress, selectedAddressData))
				{
					getCheckoutFacade().setDeliveryAddress(selectedAddressData);
					if (cartCheckoutDeliveryAddress != null && !cartCheckoutDeliveryAddress.isVisibleInAddressBook())
					{ // temporary address should be removed
						getUserFacade().removeAddress(cartCheckoutDeliveryAddress);
					}
				}
				final AddressForm addressForm = new AddressForm();
				addressForm.setCountryIso(selectedAddressData.getCountry().getIsocode());
				addressForm.setTitleCode(selectedAddressData.getTitleCode());
				addressForm.setFirstName(selectedAddressData.getFirstName());
				addressForm.setLastName(selectedAddressData.getLastName());
				addressForm.setLine1(selectedAddressData.getLine1());
				addressForm.setLine2(selectedAddressData.getLine2());
				addressForm.setTownCity(selectedAddressData.getTown());
				addressForm.setPostcode(selectedAddressData.getPostalCode());
				if (selectedAddressData.getRegion() != null)
				{
					addressForm.setRegionIso(selectedAddressData.getRegion().getIsocode());
				}
				map.put("addressForm", addressForm);
				model.addAttribute("addressForm", addressForm);
			}
		}
		return map;
	}

	@RequestMapping(value = "/back", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String back(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().previousStep();
	}

	@RequestMapping(value = "/next", method = RequestMethod.GET)
	@RequireHardLogIn
	@Override
	public String next(final RedirectAttributes redirectAttributes)
	{
		return getCheckoutStep().nextStep();
	}


	protected CheckoutStep getCheckoutStep()
	{
		return getCheckoutStep(DELIVERY_ADDRESS);
	}






}
