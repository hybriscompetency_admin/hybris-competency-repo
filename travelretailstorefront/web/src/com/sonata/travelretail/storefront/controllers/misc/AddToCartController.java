/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.sonata.travelretail.storefront.controllers.misc;


import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddToCartForm;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.converters.populator.CartPopulator;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.price.CommercePriceService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.europe1.model.PriceRowModel;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import uk.co.tui.book.domain.lite.BasePackage;
import uk.co.tui.book.domain.lite.FlightItinerary;
import uk.co.tui.book.domain.lite.Leg;

import com.sonata.travelretail.core.model.ProductBookingModel;
import com.sonata.travelretail.core.price.RouteEurope1PriceFactory;
import com.sonata.travelretail.storefront.cart.MyCartService;
import com.sonata.travelretail.storefront.controllers.ControllerConstants;



/**
 * Controller for Add to Cart functionality which is not specific to a certain page.
 */
@Controller
@Scope("tenant")
public class AddToCartController extends AbstractController
{
	private static final String TYPE_MISMATCH_ERROR_CODE = "typeMismatch";
	private static final String ERROR_MSG_TYPE = "errorMsg";
	private static final String QUANTITY_INVALID_BINDING_MESSAGE_KEY = "basket.error.quantity.invalid.binding";

	protected static final Logger LOG = Logger.getLogger(AddToCartController.class);

	@Resource
	private de.hybris.platform.order.CartService cartService;

	@Resource(name = "cartFacade")
	private CartFacade cartFacade;

	@Resource(name = "accProductFacade")
	private ProductFacade productFacade;

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	@Resource
	private ProductService productService;


	@Resource(name = "sessionService")
	private SessionService sessionService;

	/**
	 * @return the sessionService
	 */
	public SessionService getSessionService()
	{
		return sessionService;
	}

	/**
	 * @param sessionService
	 *           the sessionService to set
	 */
	public void setSessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	@Resource
	private CommercePriceService commercePriceService;

	@Resource(name = "userCartService")
	private com.sonata.travelretail.storefront.cart.CartService userCartService;

	@Resource
	private PriceDataFactory priceDataFactory;

	@Resource
	private CommerceCartService commerceCartService;

	@Resource
	private UserService userService;

	private static boolean PRICE_FLAG = false;

	@Resource(name = "cartPopulator")
	private CartPopulator cartPopulator;

	@Resource
	private ModelService modelService;

	@Resource
	private Converter<CommerceCartModification, CartModificationData> cartModificationConverter;

	@Resource
	private MyCartService myDefaultCartService;

	private ProductModel product = null;

	@SuppressWarnings("boxing")
	@RequestMapping(value = "/cart/add", method = RequestMethod.POST, produces = "application/json")
	public String addToCart(@RequestParam("productCodePost") final String code, final Model model,
			@Valid final AddToCartForm form, final BindingResult bindingErrors, final HttpServletRequest request)
	{



		if (bindingErrors.hasErrors())
		{
			return getViewWithBindingErrorMessages(model, bindingErrors);
		}

		final long qty = form.getQty();
		CartModificationData cartModification = null;
		if (qty <= 0)
		{
			model.addAttribute(ERROR_MSG_TYPE, "basket.error.quantity.invalid");
			model.addAttribute("quantity", Long.valueOf(0L));
		}
		else
		{

			if (checkEntryInProductBookDetails(code, qty))
			{
				try
				{
					final RouteEurope1PriceFactory priceFactory = (RouteEurope1PriceFactory) Registry.getGlobalApplicationContext()
							.getBean("europe1.manager");
					JaloSession.getCurrentSession().setPriceFactory(priceFactory);
					//long currentQuantity = cartModification.getQuantity();
					cartModification = addToCart(code, qty);
					model.addAttribute("quantity", Long.valueOf(cartModification.getQuantityAdded()));
					model.addAttribute("cartCode", cartModification.getCartCode());

					if (cartModification.getQuantityAdded() == 0L)
					{
						model.addAttribute(ERROR_MSG_TYPE,
								"basket.information.quantity.noItemsAdded." + cartModification.getStatusCode());
					}
					else if (cartModification.getQuantityAdded() < qty)
					{
						model.addAttribute(ERROR_MSG_TYPE,
								"basket.information.quantity.reducedNumberOfItemsAdded." + cartModification.getStatusCode());
					}
				}
				catch (final CommerceCartModificationException ex)
				{
					model.addAttribute(ERROR_MSG_TYPE, "basket.error.occurred");
					model.addAttribute("quantity", Long.valueOf(0L));
				}
			}
			else
			{
				model.addAttribute(ERROR_MSG_TYPE, "basket.page.error.maxquantity");
				model.addAttribute("quantity", Long.valueOf(0L));
			}
		}

		final ProductData productData = productFacade.getProductForCodeAndOptions(code, Arrays.asList(ProductOption.BASIC));
		if (PRICE_FLAG)
		{
			productData.setPrice(new de.hybris.platform.commercefacades.product.data.PriceData());
			productData.getPrice().setValue(
					BigDecimal.valueOf(((PriceRowModel) (new ArrayList(product.getEurope1Prices())).get(0)).getPrice()));
			productData.getPrice().setFormattedValue(
					BigDecimal.valueOf(((PriceRowModel) (new ArrayList(product.getEurope1Prices())).get(0)).getPrice()).toString());
			cartModification.getEntry().setBasePrice(productData.getPrice());
		}
		model.addAttribute("product", productData);
		if (cartModification != null)
		{
			model.addAttribute("entry", cartModification.getEntry());
		}

		final int inOutCartCount = myDefaultCartService.getInOutCartCount();
		request.getSession().setAttribute("inOutCartCount", inOutCartCount);

		return ControllerConstants.Views.Fragments.Cart.AddToCartPopup;
	}

	/**
	 * Adds the to cart.
	 *
	 * @param code
	 *           the code
	 * @param quantity
	 *           the quantity
	 * @return the cart modification data
	 * @throws CommerceCartModificationException
	 *            the commerce cart modification exception
	 */
	public CartModificationData addToCart(final String code, final long quantity) throws CommerceCartModificationException
	{
		PRICE_FLAG = false;
		product = productService.getProductForCode(code);
		final Collection<PriceRowModel> priceRowModels = product.getEurope1Prices();
		final BasePackage packageHoliday = (BasePackage) sessionService.getAttribute("bookResponse");
		final FlightItinerary flightItinerary = (FlightItinerary) sessionService.getAttribute("flightItinerary");
		List<Leg> inboundLeg = null;
		List<Leg> outboundLeg = null;
		final List<PriceRowModel> tempPriceRowModels = new ArrayList<PriceRowModel>();
		if (null != flightItinerary)
		{
			inboundLeg = flightItinerary.getInBound();
			outboundLeg = flightItinerary.getOutBound();
		}
		sessionService.setAttribute("sourceInbound", inboundLeg.get(0).getDepartureAirport().getCode());
		sessionService.setAttribute("destinationInbound", inboundLeg.get(0).getArrivalAirport().getCode());

		sessionService.setAttribute("sourceOutbound", outboundLeg.get(0).getDepartureAirport().getCode());
		sessionService.setAttribute("destinationOutbound", outboundLeg.get(0).getArrivalAirport().getCode());
		for (final PriceRowModel priceRowModel : priceRowModels)
		{
			if (null != priceRowModel.getSource() && null != priceRowModel.getDestination())
			{
				if ((priceRowModel.getSource().equals(inboundLeg.get(0).getDepartureAirport().getCode()) && priceRowModel
						.getDestination().equals(inboundLeg.get(0).getArrivalAirport().getCode()))
						|| (priceRowModel.getSource().equals(outboundLeg.get(0).getDepartureAirport().getCode()) && priceRowModel
								.getDestination().equals(outboundLeg.get(0).getArrivalAirport().getCode())))
				{
					tempPriceRowModels.add(priceRowModel);
				}

			}
		}

		if (CollectionUtils.isNotEmpty(tempPriceRowModels))
		{
			product.setEurope1Prices(tempPriceRowModels);
			//modelService.save(product);
			PRICE_FLAG = true;
		}
		final CartModel cartModel = cartService.getSessionCart();
		final CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setEnableHooks(true);
		parameter.setCart(cartModel);
		parameter.setProduct(product);
		parameter.setQuantity(quantity);
		parameter.setUnit(product.getUnit());
		parameter.setCreateNewEntry(false);


		final CommerceCartModification modification = commerceCartService.addToCart(parameter);

		return cartModificationConverter.convert(modification);
	}

	protected boolean checkEntryInProductBookDetails(final String code, final Long qunatity)
	{
		final FlightItinerary flightItinerary = (FlightItinerary) sessionService.getAttribute("flightItinerary");
		final BasePackage packageHoliday = (BasePackage) sessionService.getAttribute("bookResponse");
		final String pnr = packageHoliday.getBookingRefNum();
		String destination = new String();
		String source = new String();
		final CartData cartdata = cartFacade.getSessionCart();
		System.out.println("cartdata inbound or outbound ----------------------------------" + cartdata.getName());
		if (cartdata.getName().equals(ControllerConstants.Views.Fragments.UserCart.InBoundCart))
		{

			destination = flightItinerary.getInBound().get(0).getArrivalAirport().getCode();
			source = flightItinerary.getInBound().get(0).getDepartureAirport().getCode();

		}
		else if (cartdata.getName().equals(ControllerConstants.Views.Fragments.UserCart.OutBoundCart))
		{

			destination = flightItinerary.getOutBound().get(0).getArrivalAirport().getCode();
			source = flightItinerary.getOutBound().get(0).getDepartureAirport().getCode();
		}


		final StringBuilder queryString = new StringBuilder();
		queryString.append("SELECT {" + ProductBookingModel.PK + "} ");
		queryString.append("FROM {" + ProductBookingModel._TYPECODE + "} ");
		queryString.append("WHERE {" + ProductBookingModel.SOURCE + "} = ?" + ProductBookingModel.SOURCE + " ");
		queryString.append("AND {" + ProductBookingModel.DESTINATION + "} = ?" + ProductBookingModel.DESTINATION + " ");
		queryString.append("AND {" + ProductBookingModel.PNR + "} = ?" + ProductBookingModel.PNR + " ");
		queryString.append("AND {" + ProductBookingModel.PRODUCTCODE + "} = ?" + ProductBookingModel.PRODUCTCODE + " ");



		final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString.toString());
		query.addQueryParameter(ProductBookingModel.SOURCE, source);
		query.addQueryParameter(ProductBookingModel.DESTINATION, destination);
		query.addQueryParameter(ProductBookingModel.PNR, pnr);
		query.addQueryParameter(ProductBookingModel.PRODUCTCODE, code);

		final SearchResult<ProductBookingModel> bookList = flexibleSearchService.search(query);
		final List<ProductBookingModel> bookModelList = bookList.getResult();




		//	getSessionService().setAttribute("inOutCartCount", inOutCartCount);



		int totalQuantity = 0;

		for (final ProductBookingModel productBookingModel : bookModelList)
		{
			totalQuantity += productBookingModel.getQunatity();
		}

		totalQuantity += qunatity.intValue();

		System.out.println("totalquantity after summation is =======================" + totalQuantity);
		final ProductModel product = productService.getProductForCode(code);

		final int maxQuantity = product.getMaxBookingQunatity();

		if (totalQuantity <= maxQuantity)
		{
			return true;
		}
		else
		{
			return false;
		}


	}

	/**
	 * @return
	 */
	/*
	 * private int getInOutCartCount() { try {
	 *
	 * final List<CartModel> cartModelList = userCartService.getCartsForUser(); List entries2 = new ArrayList(); List
	 * entries1 = new ArrayList(); CartData cd1 = null; CartData cd2 = null; Integer cartSize1 = 0; Integer cartSize2 =
	 * 0; if (cartModelList != null) { for (final CartModel cm : cartModelList) { if (cd1 == null &&
	 * cm.getName().equalsIgnoreCase("IN-CART")) { cd1 = new CartData(); if (cd1.getEntries() == null) {
	 * cartPopulator.populate(cm, cd1); if (cd1.getEntries().size() != 0) { entries1 = cd1.getEntries(); cartSize1 =
	 * entries1.size();
	 *
	 * } } }
	 *
	 * if (cd2 == null && cm.getName().equalsIgnoreCase("OUT-CART")) { cd2 = new CartData(); if (cd2.getEntries() ==
	 * null) { cartPopulator.populate(cm, cd2); if (cd2.getEntries().size() != 0) { entries2 = cd2.getEntries();
	 * cartSize2 = entries2.size();
	 *
	 * } } } } } return cartSize2 + cartSize1;
	 *
	 * } catch (final Exception ex) {
	 *
	 * ex.printStackTrace(); return 0; }
	 *
	 * }
	 */
	protected String getViewWithBindingErrorMessages(final Model model, final BindingResult bindingErrors)
	{
		for (final ObjectError error : bindingErrors.getAllErrors())
		{
			if (isTypeMismatchError(error))
			{
				model.addAttribute(ERROR_MSG_TYPE, QUANTITY_INVALID_BINDING_MESSAGE_KEY);
			}
			else
			{
				model.addAttribute(ERROR_MSG_TYPE, error.getDefaultMessage());
			}
		}
		return ControllerConstants.Views.Fragments.Cart.AddToCartPopup;
	}

	protected boolean isTypeMismatchError(final ObjectError error)
	{
		return error.getCode().equals(TYPE_MISMATCH_ERROR_CODE);
	}
}
