
ACC.checkoutaddress = {

	spinner: $("<img src='" + ACC.config.commonResourcePath + "/images/spinner.gif' />"),
	addressID: '',

	showAddressBook: function ()
	{
		$(document).on("click", "#viewAddressBook", function ()
		{
			var data = $("#savedAddressListHolder").html();
			$.colorbox({

				height: false,
				html: data,
				onComplete: function ()
				{

					$(this).colorbox.resize();
				}
			});

		})
	},

	showRemoveAddressConfirmation: function ()
	{
		$(document).on("click", ".removeAddressButton", function ()
		{
			var addressId = $(this).data("addressId");
			$.colorbox({
				inline: true,
				height: false,
				href: "#popup_confirm_address_removal_" + addressId,
				onComplete: function ()
				{

					$(this).colorbox.resize();
				}
			});

		})
}
}
function getOutboundAddress(addressId){
	
	document.getElementById('selectedAddressCode').value=addressId;
	cartType = "OutBound";
	var options = {
			'selectedAddressCode' : addressId,
			'cartType' : "OutBound"
	};
	
	displayCountrySpecificAddressForm(
			options,
			showAddressFormButtonPanel());
}

function getInboundAddress(addressId){
	document.getElementById('inBoundSelectedAddressCode').value=addressId;
	cartType = "InBound";
	var options = {
			'selectedAddressCode' : addressId,
			'cartType' : "InBound"
	};
	
	displayCountrySpecificInboundAddressForm(
			options,
			showInboundAddressFormButtonPanel());
}

 function populateOutBoundAddress() {
	
	var options = {
		'selectedAddressCode' : document.getElementById('selectedAddressCode').value,
		'cartType' : "OutBound"
	};
	$.ajax({
			url : ACC.config.encodedContextPath
					+ '/checkout/multi/delivery-address/select',
			data : options,
			async : true,
			datatype:'json',
			success : function(addressform) {
				document.getElementById('address.title').value = addressform.addressForm.titleCode;
				document.getElementById('address.firstName').value = addressform.addressForm.firstName;
				document.getElementById('address.surname').value = addressform.addressForm.lastName;
				document.getElementById('address.line1').value = addressform.addressForm.line1;
				document.getElementById('address.line2').value = addressform.addressForm.line2;
				document.getElementById('address.townCity').value = addressform.addressForm.townCity;
				document.getElementById('address.postcode').value = addressform.addressForm.postcode;
			},
			error : function(e) {
				console.log(e.message);
			}

		})
}

 function populateInBoundAddress() {
	
	var options = {
		'selectedAddressCode' : document.getElementById('inBoundSelectedAddressCode').value,
		'cartType' : "InBound"
	};
	$.ajax({
			url : ACC.config.encodedContextPath
					+ '/checkout/multi/delivery-address/select',
			data : options,
			async : true,
			success : function(addressform) {
				document.getElementById('address.inboundTitleCode').value = addressform.addressForm.titleCode;
				document.getElementById('address.inboundFirstName').value = addressform.addressForm.firstName;
				document.getElementById('address.inboundSurname').value = addressform.addressForm.lastName;
				document.getElementById('address.inboundLine1').value = addressform.addressForm.line1;
				document.getElementById('address.inboundLine2').value = addressform.addressForm.line2;
				document.getElementById('address.inboundTownCity').value = addressform.addressForm.townCity;
				document.getElementById('address.inboundPostcode').value = addressform.addressForm.postcode;
			},
			error : function(e) {
				console.log(e.message);
			}

		})
}

  function displayCountrySpecificAddressForm(options, callback) {
	$.ajax({
		url : ACC.config.encodedContextPath
				+ '/checkout/multi/delivery-address/select',
		data : options,
		async : true,
		success : function(data) {
			var options1 = {
				'addressCode' : '',
				'countryIsoCode' : data.addressForm.countryIso
			};
			$.ajax(
					{
						url : ACC.config.encodedContextPath
								+ '/my-account/addressform',
						async : true,
						data : options1,
						dataType : "html",
						beforeSend : function() {
							$("#i18nAddressForm").html(
									ACC.checkoutaddress.spinner);
						}
					}).done(
					function(data1) {
						$("#i18nAddressForm").html($(data1).html());
						$('#countrySelector :input').val(
								data.addressForm.countryIso);
						if (typeof callback == 'function') {
							callback.call();
						}

					});
		},
		complete : function(data) {
			$.colorbox.close();
		},
		error : function(e) {
			console.log(e.message);
		}

	})
}

 function displayCountrySpecificInboundAddressForm(options) {
	$.ajax({
		url : ACC.config.encodedContextPath
				+ '/checkout/multi/delivery-address/select',
		data : options,
		async : true,
		success : function(data) {
			var options1 = {
				'addressCode' : '',
				'countryIsoCode' : data.addressForm.countryIso
			};
			$.ajax(
					{
						url : ACC.config.encodedContextPath
								+ '/my-account/addressform',
						async : true,
						data : options1,
						dataType : "html",
						beforeSend : function() {
							$("#i18nInboundAddressForm").html(
									ACC.checkoutaddress.spinner);
						}
					})
					.done(
							function(data1) {
								data1 = data1.replace(/addressId/g,
										"inboundAddressId");
								data1 = data1.replace(/firstName/g,
										"inboundFirstName");
								data1 = data1.replace(/titleCode/g,
										"inboundTitleCode");
								data1 = data1.replace(/title/g,
										"inboundTitleCode");
								data1 = data1.replace(/lastName/g,
										"inboundLastName");
								data1 = data1.replace(/surname/g,
										"inboundSurname");
								data1 = data1.replace(/line1/g,
										"inboundLine1");
								data1 = data1.replace(/line2/g,
										"inboundLine2");
								data1 = data1.replace(/townCity/g,
										"inboundTownCity");
								data1 = data1.replace(/postcode/g,
										"inboundPostcode");
								data1 = data1.replace(/countryIso/g,
										"inboundCountryIso");
								data1 = data1.replace(/saveInAddressBook/g,
										"inboundSaveInAddressBook");
								data1 = data1.replace(/defaultAddress/g,
										"inboundDefaultAddress");

								$("#i18nInboundAddressForm").html(
										$(data1).html());
								$('#inboundCountrySelector :input').val(
										data.addressForm.countryIso);
								if (typeof callback == 'function') {
									callback.call();
								}

							});
		},
		complete : function(data) {
			$.colorbox.close();
		},
		error : function(e) {
			console.log(e.message);
		}

	})
}

function showAddressFormButtonPanel() {
	if ($('#countrySelector :input').val() !== '') {
		$('#addressform_button_panel').show();
	}
}

  function showInboundAddressFormButtonPanel() {
	if ($('#inboundCountrySelector :input').val() !== '') {
		$('#addressform_button_panel').show();
	}
}

// Address Verification
$(document).ready(function ()
{
	with (ACC.checkoutaddress)
	{

		showAddressBook();
		showRemoveAddressConfirmation();
	}
});


