<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>

<spring:theme code="text.addToCart" var="addToCartText"/>
<spring:theme code="text.popupCartTitle" var="popupCartTitleText"/>
<c:url value="/cart" var="cartUrl"/>
<c:url value="/cart/checkout" var="checkoutUrl"/>


<!-- Start -->
<c:if test="${numberShowing1 > 0 }">
<h3><b> <spring:theme code="popup.cart.cart1"/> &nbsp;  </b></h3>
<div class="legend">
	<spring:theme code="popup.cart.showing" arguments="${numberShowing1},${numberItemsInCart1}"/>
	<c:if test="${numberItemsInCart1 > numberShowing1}">
		<a href="${cartUrl}">Show All</a>
	</c:if>
</div>
</c:if>


<c:if test="${empty numberItemsInCart1 or numberItemsInCart1 eq 0}">
	<div class="cart_modal_popup empty-popup-cart">
		<spring:theme code="popup.cart.empty"/>
	</div>
</c:if>

<c:choose>
	<c:when test="${!empty entries1 && !empty entries2}">
		<input type="hidden" value="${entries1.size()+entries2.size()}" />
		<span id="add">${entries1.size()+entries2.size()}</span>
	</c:when>
	<c:otherwise>
			<c:if test="${!empty entries1}"><input type="hidden" value="${entries1.size()}" id="cartSize"/><span id="one">${entries1.size()}</span></c:if>
			<c:if test="${!empty entries2}"><input type="hidden" value="${entries2.size()}" /><span id="two">${entries2.size()}</span></c:if>
	</c:otherwise>
</c:choose>

<c:if test="${numberShowing1 > 0 }">
	<ul class="itemList">
	<c:forEach items="${entries1}" var="entry1" end="${numberShowing1 - 1}">
		<c:url value="${entry1.product.url}" var="entryProductUrl1"/>
		
		<li class="popupCartItem">
			<div class="itemThumb">
				<a href="${entryProductUrl1}">
					<product:productPrimaryImage product="${entry1.product}" format="cartIcon"/>
				</a>
			</div>
			
			<div class="itemDesc">
				<a class="itemName" href="${entryProductUrl1}">${entry1.product.name}</a>
				<div class="itemQuantity"><span class="label"><spring:theme code="popup.cart.quantity"/></span>${entry1.quantity}</div>
				
				
				
				<c:forEach items="${entry1.product.baseOptions}" var="baseOptions1">
					<c:forEach items="${baseOptions1.selected.variantOptionQualifiers}" var="baseOptionQualifier1">
						<c:if test="${baseOptionQualifier1.qualifier eq 'style' and not empty baseOptionQualifier1.image.url}">
							<div class="itemColor">
								<span class="label"><spring:theme code="product.variants.colour"/></span>
								<img src="${baseOptionQualifier1.image.url}" alt="${baseOptionQualifier1.value}" title="${baseOptionQualifier1.value}"/>
							</div>
						</c:if>
						<c:if test="${baseOptionQualifier1.qualifier eq 'size'}">
							<div class="itemSize">
								<span class="label"><spring:theme code="product.variants.size"/></span>
								${baseOptionQualifier1.value}
							</div>
						</c:if>
					</c:forEach>
				</c:forEach>
				
				
				
				<c:if test="${not empty entry.deliveryPointOfService.name}">
					<div class="itemPickup"><span class="itemPickupLabel"><spring:theme code="popup.cart.pickup"/></span>${entry1.deliveryPointOfService.name}</div>
				</c:if>
				<div class="itemPrice"><format:price priceData="${entry1.basePrice}"/></div>
			</div>
			
		</li>
	</c:forEach>	
	
	</ul>
			<div  class="total">
<spring:theme code="popup.cart.subtotal"/>&nbsp;<span class="right"><format:price priceData="${cart1}"/></span>
</div>
</c:if>


<br/>
<c:if test="${numberShowing2 > 0 }">
<h3><b> <spring:theme code="popup.cart.cart2"/>   &nbsp;</b></h3>

<div class="legend">
	<spring:theme code="popup.cart.showing" arguments="${numberShowing2},${numberItemsInCart2}"/>
	<c:if test="${numberItemsInCart2 > numberShowing2}">
		<a href="${cartUrl}">Show All</a>
	</c:if>
</div>
</c:if>


<c:if test="${empty numberItemsInCart2 or numberItemsInCart2 eq 0}">
	<div class="cart_modal_popup empty-popup-cart">
		<spring:theme code="popup.cart.empty"/>
	</div>
</c:if>



<c:if test="${numberShowing2 > 0 }">
	<ul class="itemList">
	<c:forEach items="${entries2}" var="entry2" end="${numberShowing2 - 1}">
		<c:url value="${entry2.product.url}" var="entryProductUrl2"/>
		
		<li class="popupCartItem">
			<div class="itemThumb">
				<a href="${entryProductUrl2}">
					<product:productPrimaryImage product="${entry2.product}" format="cartIcon"/>
				</a>
			</div>
			
			<div class="itemDesc">
				<a class="itemName" href="${entryProductUrl2}">${entry2.product.name}</a>
				<div class="itemQuantity"><span class="label"><spring:theme code="popup.cart.quantity"/></span>${entry2.quantity}</div>
						
				
				<c:forEach items="${entry2.product.baseOptions}" var="baseOptions2">
					<c:forEach items="${baseOptions2.selected.variantOptionQualifiers}" var="baseOptionQualifier2">
						<c:if test="${baseOptionQualifier2.qualifier eq 'style' and not empty baseOptionQualifier2.image.url}">
							<div class="itemColor">
								<span class="label"><spring:theme code="product.variants.colour"/></span>
								<img src="${baseOptionQualifier2.image.url}" alt="${baseOptionQualifier2.value}" title="${baseOptionQualifier2.value}"/>
							</div>
						</c:if>
						<c:if test="${baseOptionQualifier2.qualifier eq 'size'}">
							<div class="itemSize">
								<span class="label"><spring:theme code="product.variants.size"/></span>
								${baseOptionQualifier1.value}
							</div>
						</c:if>
					</c:forEach>
				</c:forEach>
				
				
				
				<c:if test="${not empty entry.deliveryPointOfService.name}">
					<div class="itemPickup"><span class="itemPickupLabel"><spring:theme code="popup.cart.pickup"/></span>${entry2.deliveryPointOfService.name}</div>
				</c:if>
				<div class="itemPrice"><format:price priceData="${entry2.basePrice}"/></div>
			</div>
			
		</li>
	</c:forEach>	
	
	</ul>
	
		<div  class="total">
<spring:theme code="popup.cart.subtotal"/>&nbsp;<span class="right"><format:price priceData="${cart2}"/></span>
</div>
</c:if>


<div  class="total">
<spring:theme code="popup.cart.total"/>&nbsp;<span class="right"><format:price priceData="${total}"/></span>
</div>

<div  class="banner">
<c:if test="${not empty lightboxBannerComponent && lightboxBannerComponent.visible}">
		<cms:component component="${lightboxBannerComponent}" evaluateRestriction="true"  />
</c:if>
</div>

<div class="links">
<a href="${cartUrl}" class="button positive"><spring:theme code="checkout.checkout" /></a>
</div>

<!-- End -->

















<!-- Old Commented -->


<%-- <c:if test="${numberShowing > 0 }">
	<div class="legend">
		<spring:theme code="popup.cart.showing" arguments="${numberShowing},${numberItemsInCart}"/>
		<c:if test="${numberItemsInCart > numberShowing}">
			<a href="${cartUrl}">Show All</a>
		</c:if>
	</div>
</c:if>


<c:if test="${empty numberItemsInCart or numberItemsInCart eq 0}">
	<div class="cart_modal_popup empty-popup-cart">
		<spring:theme code="popup.cart.empty"/>
	</div>
</c:if>
<c:if test="${numberShowing > 0 }">
	<ul class="itemList">
	<c:forEach items="${entries}" var="entry" end="${numberShowing - 1}">
		<c:url value="${entry.product.url}" var="entryProductUrl"/>
		<li class="popupCartItem">
			<div class="itemThumb">
				<a href="${entryProductUrl}">
					<product:productPrimaryImage product="${entry.product}" format="cartIcon"/>
				</a>
			</div>
			<div class="itemDesc">
				<a class="itemName" href="${entryProductUrl}">${entry.product.name}</a>
				<div class="itemQuantity"><span class="label"><spring:theme code="popup.cart.quantity"/></span>${entry.quantity}</div>
				
				<c:forEach items="${entry.product.baseOptions}" var="baseOptions">
					<c:forEach items="${baseOptions.selected.variantOptionQualifiers}" var="baseOptionQualifier">
						<c:if test="${baseOptionQualifier.qualifier eq 'style' and not empty baseOptionQualifier.image.url}">
							<div class="itemColor">
								<span class="label"><spring:theme code="product.variants.colour"/></span>
								<img src="${baseOptionQualifier.image.url}" alt="${baseOptionQualifier.value}" title="${baseOptionQualifier.value}"/>
							</div>
						</c:if>
						<c:if test="${baseOptionQualifier.qualifier eq 'size'}">
							<div class="itemSize">
								<span class="label"><spring:theme code="product.variants.size"/></span>
								${baseOptionQualifier.value}
							</div>
						</c:if>
					</c:forEach>
				</c:forEach>
				
				<c:if test="${not empty entry.deliveryPointOfService.name}">
					<div class="itemPickup"><span class="itemPickupLabel"><spring:theme code="popup.cart.pickup"/></span>${entry.deliveryPointOfService.name}</div>
				</c:if>
				<div class="itemPrice"><format:price priceData="${entry.basePrice}"/></div>
			</div>
		</li>
	</c:forEach>
	</ul>
</c:if>

<div  class="total">
	<spring:theme code="popup.cart.total"/>&nbsp;<span class="right"><format:price priceData="${cartData.totalPrice}"/></span>
</div>

<div  class="banner">
	<c:if test="${not empty lightboxBannerComponent && lightboxBannerComponent.visible}">
			<cms:component component="${lightboxBannerComponent}" evaluateRestriction="true"  />
	</c:if>
</div>

<div class="links">
	<a href="${cartUrl}" class="button positive"><spring:theme code="checkout.checkout" /></a>
</div>

 --%>