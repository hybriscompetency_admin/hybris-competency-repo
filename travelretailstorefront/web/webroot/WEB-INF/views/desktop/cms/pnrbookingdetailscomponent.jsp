<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/desktop/common/header"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<c:if test="${not empty bookingRefNum}">
<div class="bookingDetails">
<h4>Your Booking Details:</h4>
<p><span class="head">PNR: </span><span><c:out value="${bookingRefNum}"/></span><span class="divider">&nbsp;|</span>

<c:if test="${outBound eq 'NCL'}">
<c:set var="out" value="Tenerife South Airport"></c:set>
</c:if>

<c:choose>
    <c:when test="${outBound eq 'NCL'}">
       <c:set var="out" value="Newcastle Airport"></c:set>
    </c:when>
   <c:when test="${outBound eq 'TFS'}">
       <c:set var="out" value="Tenerife South Airport"></c:set>
    </c:when>
    <c:when test="${outBound eq 'NWI'}">
       <c:set var="out" value="Norwich International Airport "></c:set>
    </c:when>
    <c:when test="${outBound eq 'REU'}">
       <c:set var="out" value="Reus Airport"></c:set>
    </c:when>
</c:choose>
<span class="head">From: </span><span><c:out value="${out}"/></span><span class="divider">&nbsp;|</span>
<c:choose>
    <c:when test="${inBound eq 'NCL'}">
       <c:set var="in" value="Newcastle Airport"></c:set>
    </c:when>
   <c:when test="${inBound eq 'TFS'}">
       <c:set var="in" value="Tenerife South Airport"></c:set>
    </c:when>
    <c:when test="${inBound eq 'NWI'}">
       <c:set var="in" value="Norwich International Airport "></c:set>
    </c:when>
    <c:when test="${inBound eq 'REU'}">
       <c:set var="in" value="Reus Airport"></c:set>
    </c:when>
</c:choose>
<span class="head">To: </span><span><c:out value="${in}"/></span><span class="divider">&nbsp;|</span>
<span class="head">Dep: </span><span><c:out value="${depDate}"/></span><span class="divider">&nbsp;|</span>
<span class="head">Arr: </span><span><c:out value="${arrvDate}"/></span></p>
</div>
</c:if>