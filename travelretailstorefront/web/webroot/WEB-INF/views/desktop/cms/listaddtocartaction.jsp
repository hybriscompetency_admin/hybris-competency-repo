<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<c:set var="buttonType">submit</c:set>
<c:choose>
	<c:when test="${product.stock.stockLevelStatus.code eq 'outOfStock' }">
		<c:set var="buttonType">button</c:set>
		<spring:theme code="text.addToCart.outOfStock" var="addToCartText"/>
	</c:when>
	<c:when test="${product.stock.stockLevelStatus.code eq 'lowStock' }">
		<div class='lowStock'>
			<spring:theme code="product.variants.only.left" arguments="${product.stock.stockLevel}"/>
		</div>
	</c:when>
</c:choose>
</a>
<div class="cart clearfix">
	<c:url value="/cart/add" var="addToCartUrl"/>
	<ycommerce:testId code="searchPage_addToCart_button_${product.code}">
	<script type="text/javascript">
function buttonValue2(code)
{
	
	document.getElementById(code).value = document.getElementById("button2").value;	
	//window.location.reload(); 
	
}
function buttonValue1(code)
{
	document.getElementById(code).value = document.getElementById("button1").value;		
	// window.location.reload();
	
}
</script>
		<form:form id="addToCartForm${product.code}" action="${addToCartUrl}" method="post" class="add_to_cart_form">
			<input type="hidden" name="productCodePost" value="${product.code}"/>
			<input type="hidden" name="productNamePost" value="${product.name}"/>
			<input type="hidden" name="productPostPrice" value="${product.price.value}"/>
			<input type="hidden" name="cartValue" value="" id="${product.code}" />
			<div class="inboundbtnBlk left">
	
		<c:if test="${product.inboundOutBound eq 'INBOUND' }">
			<button type="${buttonType}" class="addToCartButton inbound <c:if test="${product.stock.stockLevelStatus.code eq 'outOfStock' }">out-of-stock</c:if>"
				<c:if test="${product.stock.stockLevelStatus.code eq 'outOfStock' }"> disabled="disabled" aria-disabled="true"</c:if> value="IN-CART" id="button2" onclick="buttonValue2('${product.code}');"
				>Add To Inbound</button>
			</c:if>
			
			</div>
			<div class="outboundbtnBlk left">
			<button type="${buttonType}" value="OUT-CART" id="button1" onclick="buttonValue1('${product.code}');" class="addToCartButton  <c:if test="
			${product.stock.stockLevelStatus.code eq 'outOfStock' }">out-of-stock</c:if>"
			<c:if test="${product.stock.stockLevelStatus.code eq 'outOfStock' }"> disabled="disabled" aria-disabled="true"</c:if> 
			>Add To OutBound</button>
			</div>
		</form:form>
	</ycommerce:testId>

</div>
