<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<script type="text/javascript">
function buttonValue2(a)
{
	alert("buttonvalue 2 clicked-------------------");
	document.getElementById(a).value=document.getElementById("addToCartButton").value;
	if(document.URL.indexOf("#")==-1)
    {
        // Set the URL to whatever it was plus "#".
        url = document.URL+"#";
        location = "#";

        //Reload the page
        location.reload(true);

    }
}
function buttonValue1(a)
{
	alert("buttonvalue 1 clicked-------------------");
	document.getElementById(a).value=document.getElementById("button1").value;
	if(document.URL.indexOf("#")==-1)
    {
        // Set the URL to whatever it was plus "#".
        url = document.URL+"#";
        location = "#";

        //Reload the page
        location.reload(true);

    }
		
}
</script>
<c:url value="${url}" var="addToCartUrl"/>
<form:form method="post" id="addToCartForm" class="add_to_cart_form" action="${addToCartUrl}">
	<c:if test="${product.purchasable}">
		<input type="hidden" maxlength="3" size="1" id="qty" name="qty" class="qty" value="1">
	</c:if>
	<input type="hidden" name="productCodePost" value="${product.code}"/>
	<input type="hidden" name="cartValue" value="" id="${product.code}" />

	<c:if test="${empty showAddToCart ? true : showAddToCart}">
		<c:set var="buttonType">button</c:set>

		<c:if test="${product.purchasable and product.stock.stockLevelStatus.code ne 'outOfStock' }">
			<c:set var="buttonType">submit</c:set>
		</c:if>

		<c:choose>
			<c:when test="${fn:contains(buttonType, 'button')}">
				<button type="${buttonType}" class="addToCartButton outOfStock" disabled="disabled">
					<spring:theme code="product.variants.out.of.stock"/>
				</button>
			</c:when>

			
		</c:choose>
	<c:out value="${product.stock.stockLevelStatus.code}"/>

	
		
		<button type="submit" value="OUT-CART" id="button1" onclick="buttonValue1('${product.code}')" class="addToCartButton outbound-btn <c:if test="
			${product.stock.stockLevelStatus.code eq 'outOfStock' }">out-of-stock</c:if>"
			<c:if test="${product.stock.stockLevelStatus.code eq 'outOfStock' }"> disabled="disabled" aria-disabled="true"</c:if> 
			><spring:theme code="basket.add.to.basket1"/></button>
			<c:if test="${product.inboundOutBound eq 'INBOUND' }">
				<button id="addToCartButton" type="${buttonType}" class="addToCartButton inbound-btn"  value="IN-CART" id="button2" onclick="buttonValue2('${product.code}')">
					<spring:theme code="basket.add.to.basket"/>
				</button>
			</c:if>	
	</c:if>
</form:form>
