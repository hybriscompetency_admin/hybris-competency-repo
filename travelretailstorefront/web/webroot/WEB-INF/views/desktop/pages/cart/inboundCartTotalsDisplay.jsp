<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>

<cart:cartTotals cartData="${inboundCartData}" showTaxEstimate="${taxEstimationEnabled}"/>