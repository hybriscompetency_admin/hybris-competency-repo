<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/desktop/cart" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<spring:theme text="Your Shopping Cart" var="title" code="cart.page.title"/>
<template:page pageTitle="${pageTitle}">
	<spring:theme code="basket.add.to.cart" var="basketAddToCart"/>
	<spring:theme code="cart.page.checkout" var="checkoutText"/>
	<font color="red"><common:globalMessages/></font>
	<cart:cartValidation/>
	<cart:cartPickupValidation/>
	<c:url value="/cart/checkout" var="checkoutUrl" scope="session"/>
	<c:url value="${continueUrl}" var="continueShoppingUrl" scope="session"/>
	<cms:pageSlot position="TopContent" var="feature" element="div" class="span-24">
		<cms:component component="${feature}"/>
	</cms:pageSlot>
	<c:if test="${not empty cartData.entries}">
	<c:set var="outboundExists" value="true"/>
		<div class="clearfix">
			<div class="span-16">
				<cms:pageSlot position="CenterLeftContentSlot" var="feature">
					<cms:component component="${feature}"/>
				</cms:pageSlot>
			</div>
			<div class="span-8 last">
				<cms:pageSlot position="CenterRightContentSlot" var="feature">
					<cms:component component="${feature}"/>
				</cms:pageSlot>
			</div>
		</div>
		<%-- <cms:pageSlot position="BottomContentSlot" var="feature" element="div" class="span-24">
			<cms:component component="${feature}"/>
		</cms:pageSlot> --%>
	</c:if>
	<c:if test="${empty cartData.entries}">
		<div class="headline">
		
			<spring:theme code="basket.page.title.yourOutBoundItems"/>
		
		<span class="cartId">
			<spring:theme code="basket.page.cartId"/>&nbsp;<span class="cartIdNr">${iartData.code}</span>
		</span>
		<hr>
	</div>
		<div class="span-24">
			<div class="span-24 wide-content-slot cms_disp-img_slot">
				<cms:pageSlot position="EmptyCartMiddleContent" var="feature" element="div">
					<cms:component component="${feature}"/>
				</cms:pageSlot>
			</div>
		</div>
	</c:if>
	
	<cms:pageSlot position="InboundTopContent" var="feature" element="div" class="span-24">
		<cms:component component="${feature}"/>
	</cms:pageSlot>
	<c:if test="${not empty inboundCartData.entries}">
		<div class="clearfix">
			<div class="span-16">
				<cms:pageSlot position="InboundCenterLeftContentSlot" var="feature">
					<cms:component component="${feature}"/>
				</cms:pageSlot>
			</div>
			<div class="span-8 last">
				<cms:pageSlot position="InboundCenterRightContentSlot" var="feature">
					<cms:component component="${feature}"/>
				</cms:pageSlot>
			</div>
		</div>
		<%-- <cms:pageSlot position="InboundBottomContentSlot" var="feature" element="div" class="span-24">
			<cms:component component="${feature}"/>
		</cms:pageSlot> --%>
	</c:if>
	<c:if test="${empty inboundCartData.entries}">
	<div class="clearfix">
	</div>
	<div class="headline">
		
			<spring:theme code="basket.page.title.yourInBoundItems"/>
		
		<span class="cartId">
			<spring:theme code="basket.page.cartId"/>&nbsp;<span class="cartIdNr">${iartData.code}</span>
		</span>
		<hr>
	</div>
	<div class="clearfix">
			<div class="span-16">
					<cms:pageSlot position="EmptyCartMiddleContent" var="feature" element="div">
					<cms:component component="${feature}"/>
				</cms:pageSlot>
			</div>
		</div>
		
		
	</c:if>
	
<div class="clearfix">	
<p>
	<a class="button continueShop" href="${continueShoppingUrl}">
	<spring:theme text="Continue Shopping" code="cart.page.continue"/>
</a>
</p>
	</div>
	
	<input type="hidden" name="cartValue" value="" id="cartValue" />
	
<c:choose>	
	<c:when test="${(outboundExists) and (not empty inboundCartData.entries)}">
       <button id="combinedCheckoutButtonTop" class="doCheckoutBut positive right continueCheckout" type="button"  data-checkout-url="${checkoutUrl}?cartValue=COMBINE-CART">
        Combined checkout
    </button>
    </c:when>
    <c:otherwise>
       <c:if test="${(outboundExists)}">
	
       <button id="combinedCheckoutButtonTop" class="doCheckoutBut positive right continueCheckout" type="button"  data-checkout-url="${checkoutUrl}?cartValue=OUT-CART">
        Combined checkout
    </button>
    </c:if>
    <c:if test="${not empty inboundCartData.entries}">
	
       <button id="combinedCheckoutButtonTop" class="doCheckoutBut positive right continueCheckout" type="button"  data-checkout-url="${checkoutUrl}?cartValue=IN-CART">
        Combined checkout
    </button>
	</c:if>
    </c:otherwise>
</c:choose>
	</template:page>

