<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<body>
<div class="userRegister homepgLogin">
<div class="headline">Manage My Booking</div>
<!-- <div class="required right">form.required</div> -->
<div class="description">Please fill in all the fields below.</div>

<form id="BookingSearchRequest" action="/travelretailstorefront/travelretail/en/login" method="get"><div class="form_field-elements js-recaptcha-captchaaddon">
<div class="control-group">
<label class="control-label " >
Booking reference:<span class="mandatory">
*
</span>
<span class="skip"></span>
</label>
 
<div class="controls">
<c:if test="${not empty bookingInfo}">
<input id="BookingSearchRequest.bookingReferenceNum" name="BookingReferenceNum" class="text" type="text" value="${bookingInfo}" required placeholder="2060/123456">
</c:if>
<c:if test="${empty bookingInfo}">
<input id="BookingSearchRequest.bookingReferenceNum" name="BookingReferenceNum" class="text" type="text" value="" required placeholder="2060/123456">
</c:if>
</div>


</div>
<div class="control-group">
<label class="control-label ">
Lead passenger surname:<span class="mandatory">
*
</span>
<span class="skip"></span>
</label>
 
<div class="controls">

<c:if test="${not empty surName}">
<input id="BookingSearchRequest.paxSurName" name="paxSurName" class="text" type="text" value="${surName}" required placeholder="Smith">
</c:if>

<c:if test="${empty surName}">
<input id="BookingSearchRequest.paxSurName" name="paxSurName" class="text" type="text" value="" required placeholder="Smith">
</c:if>
</div>

</div>
<div class="control-group">
<label class="control-label ">
Departure date:<span class="mandatory">
*
</span>
<span class="skip"></span>
</label>
 
<div class="controls">
<c:if test="${not empty deptDate}">
<input id="BookingSearchRequest.departureDateFrom" name="departureDateFrom" class="text" type="text" value="${deptDate}" required placeholder="dd/mm/yy">
</c:if>

<c:if test="${empty deptDate}">
<input id="BookingSearchRequest.departureDateFrom" name="departureDateFrom" class="text" type="text" value="" required placeholder="dd/mm/yy">
</c:if>

</div>

</div>
</div>
<div class="form-actions clearfix">
<button type="submit" class="button cta-grey">Login</button>
</div>
<input type="hidden" name="CSRFToken" value="e471bbdf-576e-4035-9b24-b3df733b2fad">
</form>
</div>


<!-- <div style="width:40%; float:right; padding: 20px; ">
<p>Important Information
Package Holidays and Flight's departing after 1st November 2015 are not accessible on Manage My Booking. We are working on a brand new Manage My Booking Site which we hope to have live as soon as possible. For any changes, amendments or to make payments please contact our call centre.
You can use Manage My Booking to make many changes to your booking, including:
â€¢ Payments
â€¢ Adding extras
MyThomson
If you need to print your e-ticket, or you need another copy of your email confirmation, log-in to MyThomson.
Cruise bookings and travel shop bookings
You canâ€™t use Manage My Booking if youâ€™ve booked a cruise, ski or Lakes & Mountains holiday, or you booked your holiday in one of our travel shops. For information about your holiday, log-in to MyThomson.</p>
</div>  -->


<div style="clear:both;"></div>


</body>