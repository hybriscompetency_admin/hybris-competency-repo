/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Aug 22, 2016 11:42:06 AM                    ---
 * ----------------------------------------------------------------
 */
package com.sonata.travelretailroutepromotion.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedTravelretailroutepromotionConstants
{
	public static final String EXTENSIONNAME = "travelretailroutepromotion";
	
	protected GeneratedTravelretailroutepromotionConstants()
	{
		// private constructor
	}
	
	
}
