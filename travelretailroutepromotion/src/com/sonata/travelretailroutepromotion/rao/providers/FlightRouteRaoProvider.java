/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.sonata.travelretailroutepromotion.rao.providers;

import de.hybris.platform.ruleengineservices.rao.CartRAO;
import de.hybris.platform.ruleengineservices.rao.FlightRouteRAO;
import de.hybris.platform.ruleengineservices.rao.OrderEntryRAO;
import de.hybris.platform.ruleengineservices.rao.providers.impl.DefaultCartRAOProvider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;


/**
 *
 */
public class FlightRouteRaoProvider extends DefaultCartRAOProvider
{

	/*
	 * @Override public Set<Object> expandRAO(final ProductRAO rao, final Collection<String> options) { final Set<Object>
	 * facts = super.expandRAO(rao, options); for (final String option : options) { if (option != null) { final
	 * List<Object> flightroutes = new ArrayList<Object>(); if (rao.getFlightroutes() != null) { for (final
	 * FlightRouteRAO flightRao : rao.getFlightroutes()) { flightroutes.add(flightRao.getSource());
	 * flightroutes.add(flightRao.getDestination()); facts.addAll(flightroutes); } } } } return facts; }
	 * 
	 * @Override public ProductRAO createRAO(final ProductModel source) { return
	 * this.getProductRaoConverter().convert(source); }
	 * 
	 * @Override public Set<?> expandFactModel(final Object modelFact, final Collection<String> options) { if (modelFact
	 * instanceof ProductModel) { return expandRAO(createRAO((ProductModel) modelFact), getFilteredOptions(options)); }
	 * return Collections.EMPTY_SET; }
	 * 
	 * @Override public Set<?> expandFactModel(final Object modelFact) { return expandFactModel(modelFact,
	 * getDefaultOptions()); }
	 */

	public static final String EXPAND_FLIGHT_ROUTE = "EXPAND_FLIGHT_ROUTE";

	@Override
	public Set<?> expandFactModel(final Object modelFact)
	{
		return expandFactModel(modelFact, getOptions());
	}

	@Override
	public Collection<String> getValidOptions()
	{
		return getOptions();
	}

	@Override
	protected Set<Object> expandRAO(final CartRAO cart, final Collection<String> options)
	{
		final Set<Object> facts = super.expandRAO(cart, options);
		for (final String option : options)
		{
			switch (option)
			{
				case EXPAND_FLIGHT_ROUTE:
					if (cart.getEntries() != null)
					{
						for (final OrderEntryRAO oer : cart.getEntries())
						{
							if (oer.getProduct().getFlightroutes() != null)
							{
								for (final FlightRouteRAO frr : oer.getProduct().getFlightroutes())
								{
									facts.add(frr);
								}
							}
						}
					}
					break;
			}
		}
		return facts;
	}

	public Collection<String> getOptions()
	{
		final List<String> defaultOptions = new ArrayList<String>(getDefaultOptions());
		defaultOptions.add(EXPAND_FLIGHT_ROUTE);
		return defaultOptions;
	}
}
