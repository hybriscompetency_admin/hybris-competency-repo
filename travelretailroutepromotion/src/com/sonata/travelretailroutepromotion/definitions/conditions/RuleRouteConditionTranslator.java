/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.sonata.travelretailroutepromotion.definitions.conditions;

import de.hybris.platform.ruleengineservices.compiler.RuleCompilerContext;
import de.hybris.platform.ruleengineservices.compiler.RuleCompilerException;
import de.hybris.platform.ruleengineservices.compiler.RuleConditionTranslator;
import de.hybris.platform.ruleengineservices.compiler.RuleIrAttributeCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrAttributeOperator;
import de.hybris.platform.ruleengineservices.compiler.RuleIrCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrFalseCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrGroupCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrGroupOperator;
import de.hybris.platform.ruleengineservices.rao.FlightRouteRAO;
import de.hybris.platform.ruleengineservices.rule.data.RuleConditionData;
import de.hybris.platform.ruleengineservices.rule.data.RuleConditionDefinitionData;
import de.hybris.platform.ruleengineservices.rule.data.RuleParameterData;

import java.util.ArrayList;
import java.util.List;


/**
 *
 */
public class RuleRouteConditionTranslator implements RuleConditionTranslator
{
	public static final String SOURCES_PARAM = "source";
	public static final String DESTINATIONS_PARAM = "destination";

	@Override
	public RuleIrCondition translate(final RuleCompilerContext context, final RuleConditionData condition,
			final RuleConditionDefinitionData conditionDefinition) throws RuleCompilerException
	{
		final RuleParameterData sourcesParameter = condition.getParameters().get(SOURCES_PARAM);
		final RuleParameterData destinationsParameter = condition.getParameters().get(DESTINATIONS_PARAM);

		if (sourcesParameter == null || destinationsParameter == null)
		{
			return new RuleIrFalseCondition();
		}

		/*
		 * final String sources = sourcesParameter.getValue().toString(); final String destinations =
		 * destinationsParameter.getValue().toString();
		 *
		 *
		 * //final String FlightRouteRaoVariable = context.generateVariable(FlightRouteRAO.class); final
		 * List<RuleIrCondition> irConditions = new ArrayList<RuleIrCondition>();
		 *
		 *
		 *
		 *
		 *
		 * final RuleIrLocalVariablesContainer variablesContainer = context.createLocalContainer(); final String
		 * containsFlightRouteRaoVariable = context.generateLocalVariable(variablesContainer, FlightRouteRAO.class); final
		 * RuleIrAttributeCondition irContainsSourceCondition = new RuleIrAttributeCondition();
		 * irContainsSourceCondition.setVariable(containsFlightRouteRaoVariable);
		 * irContainsSourceCondition.setAttribute(SOURCES_PARAM);
		 * irContainsSourceCondition.setOperator(RuleIrAttributeOperator.EQUAL);
		 * irContainsSourceCondition.setValue(sources);
		 *
		 * final RuleIrExistsCondition irExistsSourceCondition = new RuleIrExistsCondition();
		 * irExistsSourceCondition.setVariablesContainer(variablesContainer);
		 * irExistsSourceCondition.setChildren(Arrays.asList(new RuleIrCondition[] { irContainsSourceCondition }));
		 * irConditions.add(irExistsSourceCondition);
		 *
		 *
		 *
		 *
		 *
		 * // final RuleIrLocalVariablesContainer variablesContainer = context.createLocalContainer(); //final String
		 * containsFlightRouteRaoVariable = context.generateLocalVariable(variablesContainer, FlightRouteRAO.class); final
		 * RuleIrAttributeCondition irContainsDestinationCondition = new RuleIrAttributeCondition();
		 * irContainsDestinationCondition.setVariable(containsFlightRouteRaoVariable);
		 * irContainsDestinationCondition.setAttribute(DESTINATIONS_PARAM);
		 * irContainsDestinationCondition.setOperator(RuleIrAttributeOperator.EQUAL);
		 * irContainsDestinationCondition.setValue(destinations);
		 *
		 * final RuleIrExistsCondition irExistsDestinationCondition = new RuleIrExistsCondition();
		 * irExistsDestinationCondition.setVariablesContainer(variablesContainer);
		 * irExistsDestinationCondition.setChildren(Arrays.asList(new RuleIrCondition[] { irContainsDestinationCondition
		 * })); irConditions.add(irExistsDestinationCondition);
		 *
		 *
		 *
		 * final RuleIrGroupCondition irQualifyingRouteCondition = new RuleIrGroupCondition();
		 * irQualifyingRouteCondition.setOperator(RuleIrGroupOperator.AND);
		 * irQualifyingRouteCondition.setChildren(irConditions); return irQualifyingRouteCondition;
		 */

		final String sources = sourcesParameter.getValue().toString();
		final String destinations = destinationsParameter.getValue().toString();

		final String FlightrouteRaoVariable = context.generateVariable(FlightRouteRAO.class);

		final RuleIrAttributeCondition irContainsSourceCondition = new RuleIrAttributeCondition();
		irContainsSourceCondition.setVariable(FlightrouteRaoVariable);
		irContainsSourceCondition.setAttribute(SOURCES_PARAM);
		irContainsSourceCondition.setOperator(RuleIrAttributeOperator.EQUAL);
		irContainsSourceCondition.setValue(sources);

		final RuleIrAttributeCondition irContainsDestinationCondition = new RuleIrAttributeCondition();
		irContainsDestinationCondition.setVariable(FlightrouteRaoVariable);
		irContainsDestinationCondition.setAttribute(DESTINATIONS_PARAM);
		irContainsDestinationCondition.setOperator(RuleIrAttributeOperator.EQUAL);
		irContainsDestinationCondition.setValue(destinations);

		final List<RuleIrCondition> irConditions = new ArrayList<RuleIrCondition>();
		irConditions.add(irContainsSourceCondition);
		irConditions.add(irContainsDestinationCondition);


		final RuleIrGroupCondition irGroupCondition = new RuleIrGroupCondition();
		irGroupCondition.setOperator(RuleIrGroupOperator.AND);
		irGroupCondition.setChildren(irConditions);

		return irGroupCondition;

	}

}
