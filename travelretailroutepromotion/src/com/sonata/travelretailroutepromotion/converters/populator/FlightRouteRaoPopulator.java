/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.sonata.travelretailroutepromotion.converters.populator;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.ruleengineservices.rao.FlightRouteRAO;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.sonata.travelretail.core.model.FlightRouteModel;


/**
 *
 */
public class FlightRouteRaoPopulator implements Populator<FlightRouteModel, FlightRouteRAO>
{


	@Override
	public void populate(final FlightRouteModel source, final FlightRouteRAO target) throws ConversionException
	{
		target.setSource(source.getSource());
		target.setDestination(source.getDestination());

	}

}
