/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.sonata.travelretailroutepromotion.converters.populator;

import de.hybris.platform.converters.Converters;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ruleengineservices.rao.FlightRouteRAO;
import de.hybris.platform.ruleengineservices.rao.ProductRAO;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import com.sonata.travelretail.core.model.FlightRouteModel;


/**
 *
 */
public class PromotionProductRaoPopulator implements Populator<ProductModel, ProductRAO>
{
	Converter<FlightRouteModel, FlightRouteRAO> flightConverter;

	/**
	 * @return the flightConverter
	 */
	public Converter<FlightRouteModel, FlightRouteRAO> getFlightConverter()
	{
		return flightConverter;
	}

	/**
	 * @param flightConverter
	 *           the flightConverter to set
	 */
	public void setFlightConverter(final Converter<FlightRouteModel, FlightRouteRAO> flightConverter)
	{
		this.flightConverter = flightConverter;
	}

	@Override
	public void populate(final ProductModel source, final ProductRAO target) throws ConversionException
	{
		target.setFlightroutes(Converters.convertAll(source.getFlightRoutes(), getFlightConverter()));

	}

}
