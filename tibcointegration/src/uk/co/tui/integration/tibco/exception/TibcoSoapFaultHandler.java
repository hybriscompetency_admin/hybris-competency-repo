/**
 *
 */
package uk.co.tui.integration.tibco.exception;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Source;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.ws.soap.SoapFault;
import org.springframework.ws.soap.SoapFaultDetail;
import org.springframework.ws.soap.SoapFaultDetailElement;

import uk.co.tui.async.logging.TUILogUtils;

import com.tuiuk.xsd.resource.tib.exceptioninfo.v1.TExceptionInfo;

/**
 * @author naresh.gls
 *
 */
public class TibcoSoapFaultHandler
{

   /** The logger to be used. */
   private static final TUILogUtils LOG = new TUILogUtils("TibcoSoapFaultHandler");

   /**
    * Handle soap fault.
    *
    * @param soapFault the soap fault
    * @throws TibcoIntegrationException the tibco integration exception
    */
   public void handleSoapFault(final SoapFault soapFault) throws TibcoIntegrationException
   {

      final SoapFaultDetail detailElement = soapFault.getFaultDetail();
      final Iterator<SoapFaultDetailElement> detailEntries = detailElement.getDetailEntries();
      try
      {
         while (detailEntries.hasNext())
         {

            final Source source = detailEntries.next().getSource();
            final JAXBContext jc = JAXBContext.newInstance(TExceptionInfo.class);
            final Unmarshaller u = jc.createUnmarshaller();
            final JAXBElement<TExceptionInfo> exInfo = u.unmarshal(source, TExceptionInfo.class);
            final TExceptionInfo tExceptionInfo = exInfo.getValue();
            throwTechnicalOrBusinessException(tExceptionInfo);
         }

      }
      catch (final JAXBException e)
      {
         LOG.error("JAXB Exception", e);
      }
      /**
       * get the exact anite error code from the error description
       *
       * @param errorDescription
       * @return string
       */
      /**
       * Gets the anite error description.
       *
       * @param errorDescription the error description
       * @return the anite error description
       */
   }

   /**
    * @param tExceptionInfo
    */
   private void throwTechnicalOrBusinessException(final TExceptionInfo tExceptionInfo)
   {
      if ("TECHNICAL".equalsIgnoreCase(tExceptionInfo.getExceptionType()))
      {
         LOG.error("Communication Exception code : " + tExceptionInfo.getExceptionCode()
            + " Description : " + tExceptionInfo.getDescription());
         throw new CommunicationException(tExceptionInfo.getExceptionCode(),
            tExceptionInfo.getDescription());
      }
      else if ("BUSINESS".equalsIgnoreCase(tExceptionInfo.getExceptionType()))
      {
         throwTibconIntegrationException(tExceptionInfo);
      }
      else if ("VALIDATION".equalsIgnoreCase(tExceptionInfo.getExceptionType()))
      {
         throw new TibcoIntegrationException(tExceptionInfo.getExceptionCode(),
            tExceptionInfo.getExceptionCode() + ":" + tExceptionInfo.getDescription());
      }
   }

   /**
    * @param tExceptionInfo
    */
   private void throwTibconIntegrationException(final TExceptionInfo tExceptionInfo)
   {
      final String soldOutErrorCodes =
         "1,2,3,4,7,8,9,10,258,1240,14154,14155,14156,14157,15005,15426,98,97,15004,1899,1901,1270,1289,14601";
      final List<String> soldOutErrorCodesList =
         new ArrayList<String>(Arrays.asList(soldOutErrorCodes.split(",")));
      final List<String> exceptionCodes =
         new ArrayList<String>(Arrays.asList(tExceptionInfo.getExceptionCode().split(",")));
      final String errorcode = getSoldoutErrorCode(exceptionCodes, soldOutErrorCodesList);
      if (!StringUtils.equalsIgnoreCase(errorcode, StringUtils.EMPTY))
      {
         throw new TibcoIntegrationException(errorcode, tExceptionInfo.getExceptionCode() + ":"
            + tExceptionInfo.getDescription());
      }
      else
      {
         throw new TibcoIntegrationException(tExceptionInfo.getExceptionCode(),
            tExceptionInfo.getExceptionCode() + ":" + tExceptionInfo.getDescription());
      }
   }

   /**
    * @param exceptionCodes
    * @param soldOutErrorCodesList
    * @return String
    */
   private String getSoldoutErrorCode(final List<String> exceptionCodes,
      final List<String> soldOutErrorCodesList)
   {
      return getMatchedErrorCode(soldOutErrorCodesList, exceptionCodes);
   }

   /**
    * Gets the exact anite error code from the error code property
    *
    * @param soldOutErrorCodesList
    * @param exceptionCodes
    * @return String
    */
   private String getMatchedErrorCode(final List<String> soldOutErrorCodesList,
      final List<String> exceptionCodes)
   {
      String erroCode = StringUtils.EMPTY;
      if (CollectionUtils.isNotEmpty(soldOutErrorCodesList)
         && CollectionUtils.isNotEmpty(exceptionCodes))
      {
         erroCode = getCode(soldOutErrorCodesList, exceptionCodes);
      }
      return erroCode;
   }

   /**
    * @param soldOutErrorCodesList
    * @param exceptionCodes
    * @return String
    */
   private String getCode(final List<String> soldOutErrorCodesList,
      final List<String> exceptionCodes)
   {
      String matchedCode = StringUtils.EMPTY;
      for (final String code : exceptionCodes)
      {
         if (soldOutErrorCodesList.contains(digitsFromString(code)))
         {
            matchedCode = digitsFromString(code);

         }
      }
      return matchedCode;
   }

   /**
    * Takes the String and returns the digits which are there in the given String
    *
    * @param str
    * @return string
    */
   private String digitsFromString(final String str)
   {
      return str.replaceAll("[^0-9]", "");
   }
}
