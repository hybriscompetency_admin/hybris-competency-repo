/**
 *
 */
package uk.co.tui.integration.tibco.exception;

/**
 * @author raghavendra.dm
 *
 *         Class to handle tibco related communication exceptions
 */
public class CommunicationException extends RuntimeException
{

   /** The error code. */
   private final String errorCode;

   /** The custom message. */
   private final String customMessage;

   /** The nested cause. */
   private final Throwable nestedCause;

   /**
    * Constructor used Instantiate a new communication exception with error code in case of any
    * tibco related communication failures.
    *
    * @param errorCode the error code
    */
   public CommunicationException(final String errorCode, final String errorDescription)
   {
      super(errorCode + " : " + errorDescription);
      this.errorCode = errorCode;
      this.customMessage = errorDescription;
      this.nestedCause = null;
   }

   /**
    * @return the errorCode
    */
   public String getErrorCode()
   {
      return errorCode;
   }

   /**
    * @return the customMessage
    */
   public String getCustomMessage()
   {
      return customMessage;
   }

   /**
    * @return the nestedCause
    */
   public Throwable getNestedCause()
   {
      return nestedCause;
   }

}
