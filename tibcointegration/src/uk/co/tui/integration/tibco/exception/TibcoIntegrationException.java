/**
 *
 */
package uk.co.tui.integration.tibco.exception;

import org.apache.commons.lang.StringUtils;
import org.springframework.ws.client.WebServiceClientException;

/**
 * @author ganapna
 *
 */
public class TibcoIntegrationException extends WebServiceClientException
{

   /** The error code. */
   private final String errorCode;

   /** The custom message. */
   private final String customMessage;

   /**
    * Instantiates a new tibco integration exception.
    *
    * @param errorCode the error code
    * @param errorDisc the Error Description
    */
   public TibcoIntegrationException(final String errorCode, final String errorDisc)
   {
      super(errorCode);
      this.errorCode = errorCode;
      this.customMessage = errorDisc;
   }

   /**
    * Instantiates a new tibco integration exception.
    *
    * @param errorCode the error code
    * @param cause the cause
    */
   public TibcoIntegrationException(final String errorCode, final Throwable cause)
   {
      super(errorCode, cause);
      this.errorCode = errorCode;
      this.customMessage = StringUtils.EMPTY;
   }

   /**
    * Gets the error code.
    *
    * @return the errorCode
    */
   public String getErrorCode()
   {
      return errorCode;
   }

   /**
    * Gets the custom message.
    *
    * @return the customMessage
    */
   public String getCustomMessage()
   {
      return customMessage;
   }

}
