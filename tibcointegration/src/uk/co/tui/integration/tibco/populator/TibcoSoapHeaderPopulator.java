/**
 *
 */
package uk.co.tui.integration.tibco.populator;

import de.hybris.platform.servicelayer.cluster.ClusterService;

import java.util.GregorianCalendar;

import javax.annotation.Resource;
import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;

import uk.co.tui.service.UserService;

import com.tuiuk.xsd.resource.tib.messagecontext.v1.ObjectFactory;
import com.tuiuk.xsd.resource.tib.messagecontext.v1.TMessageContext;
import com.tuiuk.xsd.resource.tib.messagecontext.v1.TMessageType;

/**
 * @author ganapna
 *
 */
public class TibcoSoapHeaderPopulator
{

   /** The cluster service. */
   @Resource
   private ClusterService clusterService;

   @Resource
   private UserService tibcoIntegrationUserService;

   /** (Magic number Sonar fix). */
   private static final int EIGHT = 8;

   /** The logger to be used. */
   private static final Logger LOG = Logger.getLogger(TibcoSoapHeaderPopulator.class);

   /**
    * Gets the message context soap header.
    *
    * @return the message context soap header
    */
   private TMessageContext getmessageContextSoapHeader()
   {

      final TMessageContext tMessageContext = new ObjectFactory().createTMessageContext();
      tMessageContext.setApplicationID("HYBRIS");
      tMessageContext.setBusinessKey("1");
      tMessageContext.setCorrelationID(generateCorrelationID());
      tMessageContext.setMessageID("1");
      tMessageContext.setServiceInstanceID("1");
      tMessageContext.setUserID("1234");

      tMessageContext.setSourceProcess("1");
      tMessageContext.setMessageType(TMessageType.REQUEST);
      try
      {
         tMessageContext.setTimestamp(DatatypeFactory.newInstance().newXMLGregorianCalendar(
            new GregorianCalendar()));
      }
      catch (final DatatypeConfigurationException dce)
      {
         LOG.error(">> Error populating Timestamp for Latest Tibco request- Response will fail : ",
            dce);
      }
      return tMessageContext;
   }

   /**
    * Generates the correlation id.
    *
    * @return correlationID
    */
   private String generateCorrelationID()
   {

      final StringBuilder correlationID = new StringBuilder();
      correlationID.append("null");
      correlationID.append(clusterService.getClusterId());
      correlationID.append("-");
      correlationID.append(RandomStringUtils.randomAlphanumeric(EIGHT));
      return correlationID.toString();
   }

   /**
    * Gets the message context element.
    *
    * @return the message context element
    */
   public JAXBElement<TMessageContext> getmessageContextElement()
   {
      return new ObjectFactory().createMessageContext(getmessageContextSoapHeader());
   }

}
