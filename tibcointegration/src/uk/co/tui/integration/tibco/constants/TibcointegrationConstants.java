/*
 * [y] hybris Platform
 * 
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of SAP 
 * Hybris ("Confidential Information"). You shall not disclose such 
 * Confidential Information and shall use it only in accordance with the 
 * terms of the license agreement you entered into with SAP Hybris.
 */
package uk.co.tui.integration.tibco.constants;

/**
 * Global class for all Tibcointegration constants. You can add global constants for your extension into this class.
 */
public final class TibcointegrationConstants extends GeneratedTibcointegrationConstants
{
	public static final String EXTENSIONNAME = "tibcointegration";

	public static final String TIBCO_CORRELATION_LENGTH = "tibco.correlationid.length";

   public static final String TIBCO_APPLICATIONID = "tibco.applicationid";

   public static final String TIBCO_BUSINESSID = "tibco.businessid";

   public static final String TIBCO_MESSAGEID = "tibco.messageid";

   public static final String TIBCO_SERVICEINSTANCEID = "tibco.serviceinstanceid";

   public static final String TIBCO_USERID = "tibco.userid";

   public static final String TIBCO_CORRELATIONID_PREFIX = "tibco.correlationid.prefix";

   public static final String TIBCO_SOURCE_PROCESS = "tibco.sourceProcess";

   public static final String EXCEPTION_DISCRIPTION = "<p2:Desc>";

   public static final String EXCEPTION_CODE = "</p2:Desc>";

   public static final String COLON = ":";

	private TibcointegrationConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
