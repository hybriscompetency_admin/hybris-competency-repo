/**
 *
 */
package uk.co.tui.integration.tibco.service.interceptor;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.ws.client.WebServiceClientException;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.soap.saaj.SaajSoapMessage;

import uk.co.tui.integration.tibco.exception.TibcoSoapFaultHandler;
import uk.co.tui.integration.tibco.service.logger.TibcoWebServiceLogger;

/**
 * @author naresh.gls
 *
 */
public class TibcoWebServiceClientInterceptor implements ClientInterceptor
{
   /** The logger to be used. */
   private static final Logger LOG = Logger.getLogger(TibcoWebServiceClientInterceptor.class);

   @Resource
   private TibcoSoapFaultHandler tibcoSoapFaultHandler;

   @Resource
   private TibcoWebServiceLogger tibcoWebServiceLogger;

   /*
    * (non-Javadoc)
    * 
    * @see org.springframework.ws.client.support.interceptor.ClientInterceptor#handleRequest(org.
    * springframework.ws.context .MessageContext)
    */
   @Override
   public boolean handleRequest(final MessageContext messageContext)
      throws WebServiceClientException
   {
      return true;
   }

   /*
    * (non-Javadoc)
    * 
    * @see org.springframework.ws.client.support.interceptor.ClientInterceptor#handleResponse(org.
    * springframework.ws.context .MessageContext)
    */
   @Override
   public boolean handleResponse(final MessageContext messageContext)
      throws WebServiceClientException
   {
      if (true)
      {
         tibcoWebServiceLogger.logMessage(messageContext);
      }

      return true;
   }

   /*
    * (non-Javadoc)
    * 
    * @see org.springframework.ws.client.support.interceptor.ClientInterceptor#handleFault(org.
    * springframework.ws.context .MessageContext)
    */
   @Override
   public boolean handleFault(final MessageContext messageContext) throws WebServiceClientException
   {
      if (true)
      {
         tibcoWebServiceLogger.logMessage(messageContext);
      }

      final SaajSoapMessage soapMessage = (SaajSoapMessage) messageContext.getResponse();

      if (soapMessage.hasFault())
      {
         tibcoSoapFaultHandler.handleSoapFault(soapMessage.getSoapBody().getFault());
      }

      return true;
   }

}
