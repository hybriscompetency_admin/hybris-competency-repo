/**
 *
 */
package uk.co.tui.integration.tibco.service;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.oxm.XmlMappingException;
import org.springframework.util.StopWatch;
import org.springframework.ws.client.WebServiceClientException;
import org.springframework.ws.client.WebServiceIOException;
import org.springframework.ws.client.core.WebServiceTemplate;

import uk.co.tui.integration.tibco.exception.CommunicationException;
import uk.co.tui.integration.tibco.exception.TibcoIntegrationException;


/**
 * The Class TibcoService.
 *
 * @author ganapna
 *
 */
public class TibcoService<S, T>
{
   /** The logger to be used. */
   private static final Logger LOG = Logger.getLogger(TibcoService.class);

   /** The flight extras search web service template (value injected). */
   private WebServiceTemplate tibcoAniteWebServiceTemplate;

   private static final String RECEIVED_REQUEST_TYPE = "************* Received request type is : ";

   /** The tibco web service message callback factory. */
   @Resource
   private TibcoWebServiceMessageCallbackFactory tibcoWebServiceMessageCallbackFactory;

   /** The soap action (value injected). */
   private String soapAction;

   /**
    * Gets the tibco anite web service template.
    *
    * @return the tibcoAniteWebServiceTemplate
    */
   public WebServiceTemplate getTibcoAniteWebServiceTemplate()
   {
      return tibcoAniteWebServiceTemplate;
   }

   /**
    * Sets the tibcoanite web service template.
    *
    * @param tibcoAniteWebServiceTemplate
    *           the tibcoAniteWebServiceTemplate to set
    */
   public void setTibcoAniteWebServiceTemplate(final WebServiceTemplate tibcoAniteWebServiceTemplate)
   {
      this.tibcoAniteWebServiceTemplate = tibcoAniteWebServiceTemplate;
   }

   /**
    * Gets the soap action.
    *
    * @return the soapAction
    */
   public String getSoapAction()
   {
      return soapAction;
   }

   /**
    * Sets the soap action.
    *
    * @param soapAction
    *           the soapAction to set
    */
   public void setSoapAction(final String soapAction)
   {
      this.soapAction = soapAction;
   }

   /**
    * Process request.
    *
    * @param request
    *           the request
    * @param response
    *           the response
    * @throws TibcoIntegrationException
    *            the tibco Integration exception
    */
   public T processRequest(final S request, T response) throws TibcoIntegrationException
   {
      final StopWatch tibcoResponseTime = new StopWatch();
      tibcoResponseTime.start();
      LOG.info(RECEIVED_REQUEST_TYPE + request.toString());
      response = sendAndReceive(request, response);
      tibcoResponseTime.stop();
      LOG.info("************* Received response : " + response);
      if (LOG.isDebugEnabled())
      {
         LOG.debug("End Point for request : " + tibcoAniteWebServiceTemplate.getDefaultUri() + " & soapAction : " + soapAction);

         LOG.info("Time took for TIBCO to respond for message - " + soapAction + " : " + tibcoResponseTime.getTotalTimeMillis()
               + " ms");
      }

      return response;
   }

   /**
    * Send and receive.
    *
    * @param request
    *           the request
    * @param response
    *           the response
    * @return the T
    */
   private T sendAndReceive(final S request, T response)
   {
      try
      {
         response = (T) tibcoAniteWebServiceTemplate.marshalSendAndReceive(request,
               tibcoWebServiceMessageCallbackFactory.getTibcoWebServiceMessageCallback(soapAction));
      }
      catch (final WebServiceIOException e)
      {
         LOG.error(RECEIVED_REQUEST_TYPE + request.toString());
         LOG.error("######## Read Timeout from Tibco::" + e);
         throw new TibcoIntegrationException("150001", "Read Timeout from Tibco");
      }
      catch (final CommunicationException e)
      {
         LOG.error(RECEIVED_REQUEST_TYPE + request.toString());
         LOG.error("######### Communication Exception::" + e);
         throw new TibcoIntegrationException(e.getErrorCode(), e.getCustomMessage());
      }
      catch (final XmlMappingException e)
      {
         LOG.error(RECEIVED_REQUEST_TYPE + request.toString());
         LOG.error("######### Error while marshalling or unmarshalling , Unable to log request-response" + e);
         throw new TibcoIntegrationException(e.getMessage(), e.getCause());
      }
      catch (final WebServiceClientException e)
      {
         LOG.error(RECEIVED_REQUEST_TYPE + request.toString());
         LOG.error("######### Error while sending or receiving the message - Unable to log request-response" + e);
         throw new TibcoIntegrationException(e.getMessage(), e.getCause());
      }
      return response;
   }

}
