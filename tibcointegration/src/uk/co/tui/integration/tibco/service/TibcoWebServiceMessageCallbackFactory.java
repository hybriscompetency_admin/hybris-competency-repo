/**
 *
 */
package uk.co.tui.integration.tibco.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.xml.bind.Marshaller;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;

import org.springframework.oxm.XmlMappingException;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.soap.SoapMessage;
import org.springframework.xml.transform.StringResult;
import org.springframework.xml.transform.StringSource;

import uk.co.tui.async.logging.TUILogUtils;
import uk.co.tui.integration.tibco.populator.TibcoSoapHeaderPopulator;


/**
 * A factory for creating TibcoWebServiceMessageCallback objects.
 *
 * @author ganapna
 */
class TibcoWebServiceMessageCallbackFactory
{
   @Resource
   private TibcoSoapHeaderPopulator tibcoSoapHeaderPopulator;


   /** The Constant LOGGER. */
   private static final TUILogUtils LOGGER = new TUILogUtils("TibcoWebServiceMessageCallbackFactory");


   private Jaxb2Marshaller marshaller;


   /**
    * @param marshaller
    *           the anite/tibco classes to set
    */
   public void setMarshaller(final Jaxb2Marshaller marshaller)
   {
      this.marshaller = marshaller;
   }

   /**
    * Gets the tibco web service message callback.
    *
    * @param soapAction
    *           the soap action
    * @return the tibco web service message callback
    */
   WebServiceMessageCallback getTibcoWebServiceMessageCallback(final String soapAction)
   {
      return new WebServiceMessageCallback()
      {
         @Override
         public void doWithMessage(final WebServiceMessage message) throws IOException, TransformerException
         {
            try
            {
               final SoapMessage soapMessage = (SoapMessage) message;
               soapMessage.setSoapAction(soapAction);
               final Map<String, Boolean> marshallarProperties = new HashMap<String, Boolean>();
               marshallarProperties.put(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
               marshaller.setMarshallerProperties(marshallarProperties);
               final StringResult messageContextElementResult = new StringResult();
               marshaller.marshal(tibcoSoapHeaderPopulator.getmessageContextElement(), messageContextElementResult);
               (TransformerFactory.newInstance().newTransformer()).transform(new StringSource(messageContextElementResult
                     .getWriter().toString()), soapMessage.getSoapHeader().getResult());
            }
            catch (final TransformerException e)
            {
               LOGGER.error("TransformerException", e);
            }
            catch (final XmlMappingException e)
            {
               LOGGER.error("JAXBException", e);
            }
         }

      };
   }
}
