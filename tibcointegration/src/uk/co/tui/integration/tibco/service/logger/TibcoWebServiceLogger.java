/**
 *
 */
package uk.co.tui.integration.tibco.service.logger;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.lang.StringUtils;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.soap.saaj.SaajSoapMessage;

import uk.co.tui.async.logging.TUILogUtils;

/**
 * @author naresh.gls
 *
 */
public class TibcoWebServiceLogger
{

   /** The logger to be used. */
   private static final TUILogUtils LOG = new TUILogUtils("TibcoWebServiceLogger");

   /**
    * @param messageContext
    */
   public void logMessage(final MessageContext messageContext)
   {
      try
      {
         LOG.debug("Request : ");

         final SaajSoapMessage soapMessage = (SaajSoapMessage) messageContext.getRequest();

         final ByteArrayOutputStream requestStream = new ByteArrayOutputStream();
         messageContext.getRequest().writeTo(requestStream);
         final byte[] requestCharData = requestStream.toByteArray();
         final String request = prettyFormat(new String(requestCharData, "ISO-8859-1"));

         String soapAction = soapMessage.getSoapAction();
         soapAction = StringUtils.substringAfter(soapAction, "\"");
         soapAction = StringUtils.chomp(soapAction, "\"");

         // final String dirPath = Config.getParameter("HYBRIS_LOG_DIR") + "/tibco/" + soapAction +
         // "/";

         final SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd_HHmmss_SSS");
         final String formattedDate = formatter.format(new Date());

         // checkAndWriteToFile(request, dirPath, "request_" + formattedDate + ".xml");

         LOG.debug(request);

         if (messageContext.hasResponse())
         {
            LOG.debug("Response : ");

            final ByteArrayOutputStream responseStream = new ByteArrayOutputStream();
            messageContext.getResponse().writeTo(responseStream);
            final byte[] responseCharData = responseStream.toByteArray();
            final String response = prettyFormat(new String(responseCharData, "ISO-8859-1"));

            // checkAndWriteToFile(response, dirPath, "response_" + formattedDate + ".xml");

            LOG.debug(response);
         }

      }
      catch (final UnsupportedEncodingException e)
      {
         LOG.error("UnsupportedEncodingException : ", e);
      }
      catch (final IOException e1)
      {
         LOG.error("IOException : ", e1);
      }
   }

   /**
    *
    * @param unformatedXML Unformatted String
    * @return pretty Format XML String
    */
   private String prettyFormat(final String unformatedXML)
   {
      try
      {
         final Source xmlInput = new StreamSource(new StringReader(unformatedXML));
         final StringWriter stringWriter = new StringWriter();
         final StreamResult xmlOutput = new StreamResult(stringWriter);
         final TransformerFactory transformerFactory = TransformerFactory.newInstance();
         final Transformer transformer = transformerFactory.newTransformer();
         transformer.setOutputProperty(OutputKeys.INDENT, "yes");
         transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
         transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
         transformer.transform(xmlInput, xmlOutput);
         return xmlOutput.getWriter().toString();
      }
      catch (final IllegalArgumentException e)
      {
         LOG.error("IllegalArgumentException : ", e);
         return unformatedXML;
      }
      catch (final TransformerConfigurationException e)
      {
         LOG.error("TransformerConfigurationException : ", e);
         return unformatedXML;
      }
      catch (final TransformerException e)
      {
         LOG.error("TransformerException : ", e);
         return unformatedXML;
      }
   }

   /**
    *
    * @param content the content of the file.
    * @param filename filename along with complete directory path.
    */
   private void checkAndWriteToFile(final String content, final String directory,
      final String filename)
   {
      try
      {
         final File dir = new File(directory);
         // if directory doesn't exists, then create it
         if (!dir.exists())
         {
            if (dir.mkdirs())
            {
               writeToFile(content, directory, filename);

            }
         }
         else
         {
            writeToFile(content, directory, filename);

         }

      }
      catch (final IOException e)
      {
         LOG.error("IOException : ", e);
      }
   }

   /**
    * @param content
    * @param directory
    * @param filename
    * @throws IOException
    */
   private void writeToFile(final String content, final String directory, final String filename)
      throws IOException
   {
      final File file = new File(directory + filename);
      // if file doesn't exists, then create it
      if (file.createNewFile())
      {
         final BufferedWriter bufferedWriter =
            new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
         bufferedWriter.write(content);
         bufferedWriter.close();
      }
   }
}
