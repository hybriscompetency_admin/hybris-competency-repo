/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Aug 26, 2016 11:25:11 AM                    ---
 * ----------------------------------------------------------------
 */
package uk.co.tui.integration.tibco.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedTibcointegrationConstants
{
	public static final String EXTENSIONNAME = "tibcointegration";
	
	protected GeneratedTibcointegrationConstants()
	{
		// private constructor
	}
	
	
}
