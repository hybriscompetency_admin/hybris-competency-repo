/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Aug 22, 2016 11:42:06 AM                    ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 *  
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 *  
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 *  
 */
package de.hybris.platform.omsb2b.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedOmsb2bConstants
{
	public static final String EXTENSIONNAME = "omsb2b";
	public static class Attributes
	{
		public static class ConsignmentProcess
		{
			public static final String DONE = "done".intern();
			public static final String WAITINGFORCONSIGNMENT = "waitingForConsignment".intern();
			public static final String WAREHOUSECONSIGNMENTSTATE = "warehouseConsignmentState".intern();
		}
	}
	
	protected GeneratedOmsb2bConstants()
	{
		// private constructor
	}
	
	
}
